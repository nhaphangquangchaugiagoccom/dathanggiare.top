﻿using System;
using System.Data.Entity;
using OrderChina.Models;

namespace OrderChina.Migrations
{
    public class DBInitialize : DropCreateDatabaseIfModelChanges<DBContext>
    {
        protected override void Seed(DBContext context)
        {

            context.UserProfiles.Add(new UserProfile
            {
                Email = "Accounting@gmail.com",
                Name = "Accounting",
                UserType = ((int)UserType.Accounting).ToString(),
                Phone = "0915468665",
                Password = "123456123456",
                Address = "bababibo",
                Account = "123456789",
                Gender = "Nam",
                Birthday = DateTime.Now
            });


            context.UserProfiles.Add(new UserProfile
            {
                Email = "sale@gmail.com",
                Name = "Sale",
                UserType = ((int)UserType.Sale).ToString(),
                Phone = "01672363002",
                Password = "123456789",
                Address = "dathanggiare",
                Account = "123456789",
                Gender = "Nam",
                Birthday = DateTime.Now
            });
            

            context.Rates.Add(new Rate
            {
                fee1 = 20000,
                fee2 = 24000,
                fee3 = 28000,
                Price = 4000,
                rate_forex = 3306,
                userUpdate = "sale@gmail.com",
                lastUpdated = DateTime.Now
            });

            context.Fees.Add(new Fee
            {
                From = 0,
                To = 1,
                Value = 7,
                LastUpdate = DateTime.Now
            });

            context.Fees.Add(new Fee
            {
                From = 1,
                To = 2,
                Value = 6,
                LastUpdate = DateTime.Now
            });

            context.Fees.Add(new Fee
            {
                From = 2,
                To = 10,
                Value = 5,
                LastUpdate = DateTime.Now
            });


            context.Fees.Add(new Fee
            {
                From = 10,
                To = 30,
                Value = 4,
                LastUpdate = DateTime.Now
            });

            context.Fees.Add(new Fee
                                 {
                                     From = 30,
                                     To = 50,
                                     Value = 3,
                                     LastUpdate = DateTime.Now
                                 });
            context.Fees.Add(new Fee
                                 {
                                     From = 50,
                                     To = 0,
                                     Value = 2,
                                     LastUpdate = DateTime.Now
                                 });
            //context.Currencys.Add(new Currency
            //{
            //    Code = "AUD",
            //    Description = "Australia Dollar"
            //});
            //context.Currencys.Add(new Currency
            //{
            //    Code = "CAD",
            //    Description = "Canada Dollar"
            //}); 
            context.Currencys.Add(new Currency
                                      {
                                          Code = "CNY",
                                          Description = "China Yuan Renminbi"
                                      });
            //context.Currencys.Add(new Currency
            //{
            //    Code = "EUR",
            //    Description = "Euro Member Countries"
            //});
            //context.Currencys.Add(new Currency
            //{
            //    Code = "HKD",
            //    Description = "Hong Kong Dollar"
            //});
            //context.Currencys.Add(new Currency
            //{
            //    Code = "JPY",
            //    Description = "Japan Yen"
            //});
            //context.Currencys.Add(new Currency
            //{
            //    Code = "RUB",
            //    Description = "Russia Ruble"
            //});
            //context.Currencys.Add(new Currency
            //{
            //    Code = "THB",
            //    Description = "Thailand Baht"
            //});
            //context.Currencys.Add(new Currency
            //{
            //    Code = "USD",
            //    Description = "United States Dollar"
            //});
            context.Currencys.Add(new Currency
                                      {
                                          Code = "VND",
                                          Description = "Viet Nam Dong"
                                      });

            //Australia Dollar	AUD		
            //Canada Dollar	CAD		
            //China Yuan Renminbi	CNY	
            //Euro Member Countries	EUR	
            //Hong Kong Dollar	HKD	
            //Japan Yen	JPY		
            //Russia Ruble	RUB		
            //Thailand Baht	THB	
            //United States Dollar	USD	
            //Viet Nam Dong	VND	 
            context.CNYs.Add(new CNY
                                 {
                                     NameType = NameType.Alipay.ToString(),
                                     From = 0,
                                     To = 5000,
                                     Value = 30000,
                                     LastUpdate = DateTime.Now
                                 });
            context.CNYs.Add(new CNY
                                 {
                                     NameType = NameType.Alipay.ToString(),
                                     From = 5000,
                                     To = 0,
                                     Value = 0,
                                     LastUpdate = DateTime.Now
                                 });
            context.CNYs.Add(new CNY
                                 {
                                     NameType = NameType.Bank.ToString(),
                                     From = 0,
                                     To = 2000,
                                     Value = 40000,
                                     LastUpdate = DateTime.Now
                                 });
            context.CNYs.Add(new CNY
                                 {
                                     NameType = NameType.Bank.ToString(),
                                     From = 2000,
                                     To = 5000,
                                     Value = 30000,
                                     LastUpdate = DateTime.Now
                                 });
            context.CNYs.Add(new CNY
                                 {
                                     NameType = NameType.Bank.ToString(),
                                     From = 5000,
                                     To = 10000,
                                     Value = 40000,
                                     LastUpdate = DateTime.Now
                                 });
            context.CNYs.Add(new CNY
                                 {
                                     NameType = NameType.Bank.ToString(),
                                     From = 10000,
                                     To = 0,
                                     Value = 0,
                                     LastUpdate = DateTime.Now
                                 });
            context.Freights.Add(new Freight
            {
                Name = NameType.Common.ToString(),
                From = 0,
                To = 0,
                Value = 22000,
                LastUpdate = DateTime.Now
            });
            context.Freights.Add(new Freight
            {
                Name = NameType.Batch.ToString(),
                From = 0,
                To = 100,
                Value = 9000,
                LastUpdate = DateTime.Now
            });
            context.Freights.Add(new Freight
            {
                Name = NameType.Batch.ToString(),
                From = 100,
                To = 200,
                Value = 10000,
                LastUpdate = DateTime.Now
            });
            context.Freights.Add(new Freight
            {
                Name = NameType.Batch.ToString(),
                From = 200,
                To = 500,
                Value = 11000,
                LastUpdate = DateTime.Now
            });
            context.Freights.Add(new Freight
            {
                Name = NameType.Batch.ToString(),
                From = 500,
                To = 1000,
                Value = 12000,
                LastUpdate = DateTime.Now
            });
            context.Clots.Add(new Clot
            {
                Name = NameType.Weigh.ToString(),
                From = 0,
                To = 0,
                Value = 2400000,
                LastUpdate = DateTime.Now
            });
            #region Add Default Wallet For System

            context.Wallets.Add(new Wallet
            {
                Client = FeeOrderChinaSystem.FeeOrderChinaVND.ToString(),
                Account = FeeOrderChinaSystem.FeeOrderChinaVND.ToString(),
                Currency = "VND",
                LastUpdate = DateTime.Now,
                Money = 0,
                Name = FeeOrderChinaSystem.FeeOrderChinaVND.ToString(),
                User_Update = "0",
                WalletType = ""
            });

            #endregion

           

            context.Wallets.Add(new Wallet
            {
                Account = "0915468663",
                Client = "0915468663",
                Currency = "CNY",
                Id = 977261063,
                LastUpdate = DateTime.Now,
                Money = 0,
                Name = "Orderer",
                User_Update = "0915468661",
                WalletType = "Online"
            });

            context.OrderInternalInDays.Add(new OrderInternalInDay
            {
                ID = 1,
                OrderInternalInDayID = 0,
                CreateDate = DateTime.Now
            });
        }
    }
}