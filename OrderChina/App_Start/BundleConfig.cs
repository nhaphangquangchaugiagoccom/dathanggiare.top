﻿using System.Web;
using System.Web.Optimization;

namespace OrderChina
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/signalR").Include(
                        "~/Scripts/jquery.signalR-2.2.1.js"));
            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                        "~/Scripts/moment.js",
                        "~/Scripts/bootstrap.js",
                        "~/Scripts/bootstrap-*"));
            bundles.Add(new ScriptBundle("~/bundles/filesaver").Include(
                        "~/Scripts/FileSaver.js"));

            bundles.Add(new ScriptBundle("~/bundles/javamain").Include(
                        "~/Scripts/main.js",
                        "~/Scripts/wow.min.js",
                        "~/Scripts/owl.carousel.js",
                        "~/Scripts/tuto_tab_system.js"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/javamain").Include(
                        "~/Scripts/main.js",
                        "~/Scripts/wow.min.js",
                        "~/Scripts/owl.carousel.js",
                        "~/Scripts/tuto_tab_system.js"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/chat_client").Include(
                        "~/Scripts/chat_client.js"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/chat_manage_client").Include(
                        "~/Scripts/chat_manage_client.js"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/bootbox").Include(
                        "~/Scripts/bootbox.js"));

            bundles.Add(new StyleBundle("~/Content/bootstrap")
                .Include("~/Content/bootstrap.css"));

            bundles.Add(new StyleBundle("~/Content/fontawesome")
                .Include("~/Content/font-awesome.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/Content/chat")
                .Include("~/Content/ChatStyle.css", "~/Content/chat-manage-style.css"));

            bundles.Add(new StyleBundle("~/Content/css")
                .Include("~/Content/style.css",
                  "~/Content/owl.carousel.css",
                  "~/Content/LayoutUser.css"));
            bundles.Add(new StyleBundle("~/Content/css2")
                .Include("~/Content/GiaoDich.css",
                "~/Content/LayoutUser.css"));
            bundles.Add(new StyleBundle("~/Content/Minh_user")
               .Include("~/Content/Minh_user.css"));
            bundles.Add(new StyleBundle("~/Content/PagedList")
               .Include("~/Content/PagedList.css"));
            bundles.Add(new StyleBundle("~/Content/register")
              .Include("~/Content/register.css"));
            //bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
            //            "~/Content/themes/base/jquery.ui.core.css",
            //            "~/Content/themes/base/jquery.ui.resizable.css",
            //            "~/Content/themes/base/jquery.ui.selectable.css",
            //            "~/Content/themes/base/jquery.ui.accordion.css",
            //            "~/Content/themes/base/jquery.ui.autocomplete.css",
            //            "~/Content/themes/base/jquery.ui.button.css",
            //            "~/Content/themes/base/jquery.ui.dialog.css",
            //            "~/Content/themes/base/jquery.ui.slider.css",
            //            "~/Content/themes/base/jquery.ui.tabs.css",
            //            "~/Content/themes/base/jquery.ui.datepicker.css",
            //            "~/Content/themes/base/jquery.ui.progressbar.css",
            //            "~/Content/themes/base/jquery.ui.theme.css"));
        }
    }
}