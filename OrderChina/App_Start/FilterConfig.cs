﻿using OrderChina.Filters;
using System.Web;
using System.Web.Mvc;

namespace OrderChina
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new RequreSecureConnectionFilter());
            //filters.Add(new AuthorizeAttribute());

        }
    }
}