﻿using OrderChina.Models;
using OrderChina.Temp_Reports.SurviveEarlyMonth;
using QuantEdge.Worker.ReportManager.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Telerik.Reporting;

namespace OrderChina.Controllers
{
    public class ReportsController : Controller
    {
        //
        // GET: /Reports/

        public ActionResult Index()
        {
            var model = new ReportsModel();
            ViewBag.ListReportType = GetListReportType();
            ViewBag.ListMonths = GetListMonths();
            return View(model);
        }

        [HttpPost]
        public ActionResult GetReports(ReportsModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.ReportType))
                {
                    return Json(new { success = false, msg = "Yêu cầu chọn báo cáo" });
                }
                if (model.ReportType == ReportType.SurviveEarlyMonth.ToString())
                {
                    if (model.Month <= 0)
                    {
                        return Json(new { success = false, msg = "Chưa chọn tháng" });
                    }
                    var report = new SurviveEarlyMonth(model.Month);
                    var renderingResult = ExportFileUtils.GetByteForReportOutputType_RenderReport(report, ExportType.Xlsx);

                    return Json(new { success = true, BytesData = Convert.ToBase64String(renderingResult.DocumentBytes), ReportName = renderingResult.DocumentName, Type = renderingResult.MimeType, Extension = renderingResult.Extension });
                }

            }
            catch (Exception ex)
            {
                return Json(new { success = false, msg = ex.ToString() });
            }

            return Json(new { success = false, msg = "Xuất báo cáo lỗi" });

        }


        private SelectList GetListReportType()
        {
            var list = new List<object>();
            foreach (FieldInfo fieldInfo in typeof(ReportType).GetFields())
            {
                if (fieldInfo.FieldType.Name != "ReportType")
                    continue;
                var attribute = Attribute.GetCustomAttribute(fieldInfo,
                    typeof(DisplayAttribute)) as DisplayAttribute;

                list.Add(attribute != null
                    ? new { name = fieldInfo.Name, display = attribute.Name }
                    : new { name = fieldInfo.Name, display = fieldInfo.Name });
            }

            return new SelectList(list, "name", "display");
        }

        private SelectList GetListMonths()
        {
            var dDur = new List<object>();
            dDur.Add(new { value = 1, display = "Tháng một" });
            dDur.Add(new { value = 2, display = "Tháng hai" });
            dDur.Add(new { value = 3, display = "Tháng ba" });
            dDur.Add(new { value = 4, display = "Tháng bốn" });
            dDur.Add(new { value = 5, display = "Tháng năm" });
            dDur.Add(new { value = 6, display = "Tháng sáu" });
            dDur.Add(new { value = 7, display = "Tháng bảy" });
            dDur.Add(new { value = 8, display = "Tháng tám" });
            dDur.Add(new { value = 9, display = "Tháng chín" });
            dDur.Add(new { value = 10, display = "Tháng mười" });
            dDur.Add(new { value = 11, display = "Tháng 11" });
            dDur.Add(new { value = 12, display = "Tháng 12" });

            return new SelectList(dDur, "value", "display", DateTime.Now.Month);
        }

    }
}
