﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Transactions;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.Web.WebPages.OAuth;
using Newtonsoft.Json;
using OrderChina.Common;
using OrderChina.Filters;
using OrderChina.Models;
using PagedList;
using WebMatrix.WebData;
using IsolationLevel = System.Transactions.IsolationLevel;

namespace OrderChina.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class AccountController : Controller
    {
        private readonly DBContext db = new DBContext();
        private readonly int pageSize = Convert.ToInt32(WebConfigurationManager.AppSettings["page_size"]);
        private const string sheet_name = "File đặt hàng của khách";

        #region User

        //
        // GET: /Account/Login

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (IsEmail(model.Email))
            {
                if (IsValid(model.Email, model.Password))
                {
                    UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Email == model.Email);
                    if (db.Wallets.FirstOrDefault(a => a.Client == user.Phone) != null)
                    {
                        var vi = db.Wallets.FirstOrDefault(a => a.Client == user.Phone);
                        if (db.Rates.Any())
                        {
                            Rate rate = db.Rates.FirstOrDefault();
                            if (rate != null)
                            {
                                Session["Price"] = rate.Price.ToString("##,###");
                                Session["fee1"] = rate.FormatPrice(rate.fee1);
                                Session["fee2"] = rate.FormatPrice(rate.fee2);
                                Session["fee3"] = rate.FormatPrice(rate.fee3);
                            }
                        }
                        if (user != null)
                        {
                            Session["Name"] = user.Name;
                            Session["ID"] = user.UserId;
                            Session["Email"] = user.Email;
                            Session["Phone"] = user.Phone;
                            Session["UserType"] = user.UserType;

                        }
                    }
                    if (user != null)
                    {
                        FormsAuthentication.SetAuthCookie(user.Phone, model.RememberMe);
                        //var authTicket = new FormsAuthenticationTicket(model.Email, model.RememberMe, 1);
                        //var EncryptedTicket = FormsAuthentication.Encrypt(authTicket);
                        //var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, EncryptedTicket);

                        //Response.Cookies.Add(authCookie);
                        if (db.Rates.Any())
                        {
                            Rate rate = db.Rates.FirstOrDefault();
                            if (rate != null)
                            {
                                Session["Price"] = rate.Price.ToString("##,###");
                                Session["fee1"] = rate.FormatPrice(rate.fee1);
                                Session["fee2"] = rate.FormatPrice(rate.fee2);
                                Session["fee3"] = rate.FormatPrice(rate.fee3);
                            }
                        }
                        Session["Name"] = user.Name;
                        Session["ID"] = user.UserId;
                        Session["UserType"] = user.UserType;
                    }
                    return RedirectToLocal(returnUrl);
                }
            }
            else
            {
                //phone
                if (IsValidPhone(model.Email, model.Password))
                {
                    FormsAuthentication.SetAuthCookie(model.Email, model.RememberMe);
                    //var authTicket = new FormsAuthenticationTicket(model.Email, model.RememberMe, 1);
                    //var EncryptedTicket = FormsAuthentication.Encrypt(authTicket);
                    //var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, EncryptedTicket);

                    //Response.Cookies.Add(authCookie);
                    if (db.Rates.Any())
                    {
                        Rate rate = db.Rates.FirstOrDefault();
                        if (rate != null)
                        {
                            Session["Price"] = rate.Price.ToString("##,###");
                            Session["fee1"] = rate.FormatPrice(rate.fee1);
                            Session["fee2"] = rate.FormatPrice(rate.fee2);
                            Session["fee3"] = rate.FormatPrice(rate.fee3);
                        }
                    }
                    UserProfile userProfile = db.UserProfiles.FirstOrDefault(a => a.Email == model.Email);
                    if (db.Wallets.FirstOrDefault(a => a.Client == userProfile.Phone) != null)
                    {
                        var vi = db.Wallets.FirstOrDefault(a => a.Client == userProfile.Phone);
                        if (userProfile != null)
                        {
                            Session["Name"] = userProfile.Name;
                            Session["ID"] = userProfile.UserId;
                            Session["Email"] = userProfile.Email;
                            Session["Phone"] = userProfile.Phone;
                            Session["UserType"] = userProfile.UserType;

                        }
                    }
                    if (userProfile != null)
                    {

                        Session["Name"] = userProfile.Name;
                        Session["ID"] = userProfile.UserId;
                        Session["UserType"] = userProfile.UserType;
                    }
                    return RedirectToLocal(returnUrl);
                }
            }


            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "Tài khoản và mật khẩu không chính xác.");
            return View(model);
        }

        private bool IsValid(string email, string password)
        {
            bool IsValid = false;

            UserProfile user = db.UserProfiles.FirstOrDefault(u => u.Email == email);
            if (user != null)
            {
                if (user.Password == password)
                {
                    IsValid = true;
                }
            }

            return IsValid;
        }

        private bool IsValidPhone(string phone, string password)
        {
            bool IsValid = false;

            UserProfile user = db.UserProfiles.FirstOrDefault(u => u.Phone == phone);
            if (user != null)
            {
                if (user.Password == password)
                {
                    IsValid = true;
                }
            }

            return IsValid;
        }

        public ActionResult LogOff()
        {
            WebSecurity.Logout();
            FormsAuthentication.SignOut();

            Session.Remove("Name");
            Session.Remove("ID");
            Session.Remove("UserType");
            Session.Remove("Phone");
            Session.Remove("Email");
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult DeleteUser(string id)
        {
            UserProfile model = db.UserProfiles.FirstOrDefault(a => a.Phone == id);
            if (model != null)
            {
                db.UserProfiles.Remove(model);
                db.SaveChanges();

                return Json(new { success = true });
            }
            return Json(new { success = false });
        }

        //
        // GET: /Account/Register

        [AllowAnonymous]
        public ActionResult Register(string phone, string id, string cmd)
        {
            var order = db.Orders.FirstOrDefault(a => a.OrderId.ToString() == id);
            var model = new RegisterModel
            {
                Phone = order != null ? order.Phone : "",
                Email = order != null ? order.UserName : "",
                Name = order != null ? order.ClientName : "",
                Address = order != null ? order.ClientAddress : "",
                Birthday = DateTime.Now,
                orderid = id,
                customercode = order != null ? order.CustomerCode : ""
            };
            ViewBag.cmd = cmd;
            if (cmd == "client")
            {
                var list = new List<object> { new { name = (int)UserType.Client, display = "Khách hàng" } };
                ViewBag.listUserType = new SelectList(list, "name", "display", (int)UserType.Client);
            }
            else
            {
                ViewBag.listUserType = GetListUserType();
            }
            return View(model);
        }

        //
        // POST: /Account/Register

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterModel model, string id, string cmd)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                try
                {
                    //insert userProfile
                    var userProfile = new UserProfile
                    {
                        Email = model.Email,
                        Address = model.Address,
                        Phone = model.Phone,
                        Gender = model.Gender,
                        Birthday = model.Birthday,
                        Name = model.Name,
                        Password = model.Password,
                        Account = model.Account
                    };
                    if (model.UserType == null || !model.UserType.Any())
                    {
                        userProfile.UserType = ((int)UserType.Client).ToString(CultureInfo.InvariantCulture);
                        userProfile.DownPaymentRate = model.DownPaymentRate ?? 80;
                    }
                    else
                    {
                        string userType = model.UserType.Aggregate(string.Empty, (current, s) => current + "," + s);
                        userType = userType.Substring(1, userType.Length - 1);
                        userProfile.UserType = userType;
                    }
                    db.UserProfiles.Add(userProfile);
                    db.SaveChanges();
                    if (!string.IsNullOrEmpty(model.customercode))
                    {
                        //Mã khách hàng
                        userProfile.customercode = model.customercode;
                    }
                    else
                    {
                        //Mã khách hàng
                        string ma = userProfile.UserId.ToString("0000");
                        userProfile.customercode = "AP" + ma;
                    }
                    //create wallet, default currency = VND
                    db.Wallets.Add(new Wallet
                    {
                        LastUpdate = DateTime.Now,
                        User_Update = "",
                        Client = userProfile.Phone,
                        Currency = "VND",
                        Money = 0,
                        Name = userProfile.Name,
                        Account = userProfile.Phone
                    });
                    db.SaveChanges();
                    if (!Request.IsAuthenticated)
                    {
                        FormsAuthentication.SetAuthCookie(model.Phone, false);
                        if (db.Rates.Any())
                        {
                            Rate rate = db.Rates.FirstOrDefault();
                            if (rate != null)
                            {
                                Session["Price"] = rate.Price.ToString("##,###");
                                Session["fee1"] = rate.FormatPrice(rate.fee1);
                                Session["fee2"] = rate.FormatPrice(rate.fee2);
                                Session["fee3"] = rate.FormatPrice(rate.fee3);
                            }
                        }

                        Session["Name"] = userProfile.Name;
                        Session["ID"] = userProfile.UserId;
                        Session["UserType"] = userProfile.UserType;
                        Session["Phone"] = userProfile.Phone;
                        Session["Email"] = userProfile.Email;

                        return RedirectToAction("Index", "Home");
                    }
                    if (Session["UserType"] != null &&
                        Utils.CheckRole((string)Session["UserType"], (int)UserType.Sale, false))
                    {
                        //sale manage
                        var salemanage = new SaleManageClient
                        {
                            User_Client = userProfile.Phone,
                            User_Sale = User.Identity.Name,
                            LastUpdate = DateTime.Now,
                            User_Update = User.Identity.Name
                        };
                        db.SaleManageClients.Add(salemanage);
                        db.SaveChanges();

                        //get order
                        IQueryable<Order> listOrder =
                            db.Orders.Where(a => a.Phone == userProfile.Phone && string.IsNullOrEmpty(a.SaleManager));
                        foreach (Order order in listOrder)
                        {
                            order.SaleManager = User.Identity.Name;
                        }
                        db.SaveChanges();

                        return RedirectToAction("ListClient", "Account", new { id });
                    }
                    if (cmd == "user")
                    {
                        return RedirectToAction("ListUser", "Account");
                    }
                    return RedirectToAction("ListClient", "Account");
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/Disassociate
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Disassociate(string provider, string providerUserId)
        {
            string ownerAccount = OAuthWebSecurity.GetUserName(provider, providerUserId);
            ManageMessageId? message = null;

            // Only disassociate the account if the currently logged in user is the owner
            if (ownerAccount == User.Identity.Name)
            {
                // Use a transaction to prevent the user from deleting their last login credential
                using (
                    var scope = new TransactionScope(TransactionScopeOption.Required,
                        new TransactionOptions { IsolationLevel = IsolationLevel.Serializable }))
                {
                    bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
                    if (hasLocalAccount || OAuthWebSecurity.GetAccountsFromUserName(User.Identity.Name).Count > 1)
                    {
                        OAuthWebSecurity.DeleteAccount(provider, providerUserId);
                        scope.Complete();
                        message = ManageMessageId.RemoveLoginSuccess;
                    }
                }
            }

            return RedirectToAction("Manage", new { Message = message });
        }

        //
        // GET: /Account/Manage

        //
        // GET: /Account/Manage
        [Authorize]
        public ActionResult ManageNoShop(string username, string listStatus, string fromDate, string toDate, string OrderId,
        int? page)
        {
            var listOrder = new List<Order>();
            var lismnager = new List<ViewManager>();
            listOrder = db.Orders.Where(a => a.OrderType != OrderType.Ship.ToString() && a.IsShop == false).ToList();
            var customer = db.UserProfiles.FirstOrDefault(a => a.customercode == username);

            bool isFilter = false;
            if (!string.IsNullOrEmpty(username))
            {
                isFilter = true;
                listOrder = listOrder.Where(a => a.Phone == customer.Phone).ToList();
            }

            if (!string.IsNullOrEmpty(fromDate))
            {
                DateTime fromdate = DateTime.ParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                if (isFilter)
                {
                    listOrder = (from order in listOrder
                                 where fromdate <= order.CreateDate
                                 select order).ToList();
                }
                else
                {
                    isFilter = true;
                    listOrder = (from order in listOrder
                                 where fromdate <= order.CreateDate
                                 select order).ToList();
                }
            }

            if (!string.IsNullOrEmpty(toDate))
            {
                DateTime todate = DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(1);
                if (isFilter)
                {
                    listOrder = (from order in listOrder
                                 where order.CreateDate <= todate
                                 select order).ToList();
                }
                else
                {
                    isFilter = true;
                    listOrder = (from order in listOrder
                                 where order.CreateDate <= todate
                                 select order).ToList();
                }
            }

            if (!string.IsNullOrEmpty(listStatus))
            {
                if (isFilter)
                {
                    listOrder = listOrder.FindAll(a => a.Status == listStatus);
                }
                else
                {
                    isFilter = true;
                    listOrder = db.Orders.Where(a => a.Status == listStatus).ToList();
                }
            }
            if (!string.IsNullOrEmpty(OrderId))
            {
                if (isFilter)
                {
                    listOrder = listOrder.FindAll(a => a.OrderId.ToString() == OrderId);
                }
                else
                {
                    isFilter = true;
                    listOrder = listOrder.Where(a => a.OrderId.ToString() == OrderId).ToList();
                }
            }

            ViewBag.ListStatus = GetListStatus(listStatus);

            var listOrderDisplay = new List<Order>();

            if (isFilter)
            {
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Sale, false))
                {
                    List<string> listUserManage =
                        db.SaleManageClients.Where(a => a.User_Sale == User.Identity.Name)
                            .Select(a => a.User_Client)
                            .ToList();
                    listOrderDisplay.AddRange(
                        listOrder.Where(a => listUserManage.Contains(a.Phone) || string.IsNullOrEmpty(a.SaleManager))
                            .ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Client, false))
                {
                    listOrderDisplay.AddRange(listOrder.Where(a => a.Phone == User.Identity.Name).ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"]))
                {
                    listOrderDisplay = listOrder.ToList();
                }
            }
            else
            {
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Sale, false))
                {
                    List<string> listUserManage =
                        db.SaleManageClients.Where(a => a.User_Sale == User.Identity.Name)
                            .Select(a => a.User_Client)
                            .ToList();
                    listOrderDisplay.AddRange(
                        listOrder.Where(a => listUserManage.Contains(a.Phone) || string.IsNullOrEmpty(a.SaleManager))
                            .ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Client, false))
                {
                    listOrderDisplay.AddRange(listOrder.Where(a => a.Phone == User.Identity.Name).ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"]))
                {
                    listOrderDisplay = listOrder.ToList();
                }
            }

            listOrderDisplay = listOrderDisplay.Distinct().OrderByDescending(a => a.UpdateDate).ToList();
            if (listOrderDisplay.Count >= 0)
            {
                var lisorder = (from a in listOrderDisplay select a.OrderId).ToList();
                foreach (int orderid in lisorder)
                {
                    Order order = db.Orders.First(a => a.OrderId == orderid);
                    UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Phone == order.Phone);
                    if (user != null)
                    {
                        var NewView = new ViewManager
                        {
                            Orders = order,
                            UserName = user.Name,
                            IdUser = user.customercode
                        };
                        lismnager.Add(NewView);
                    }
                    else
                    {
                        var NewView = new ViewManager
                        {
                            Orders = order,
                            UserName = "Khách hàng chưa lập TK",
                            IdUser = "Khách hàng chưa lập TK"
                        };
                        lismnager.Add(NewView);
                    }
                }
            }
            ViewBag.CurrentToDate = toDate;
            ViewBag.CurrentFromDate = fromDate;
            ViewBag.CurrentStatus = listStatus;
            ViewBag.CurrentOrderId = OrderId;
            ViewBag.CurrentUserName = username;


            int pageNumber = (page ?? 1);

            return View(lismnager.ToPagedList(pageNumber, pageSize));
        }

        [Authorize]
        public ActionResult Manage(string username, string listStatus, string fromDate, string toDate, string OrderId, string TransId, string ShippingCode,
            int? page)
        {

            var listOrder = new List<OrderInternal>();
            var lismnager = new List<ViewManager>();
            listOrder = db.OrderInternals.Where(a => a.OrderType != OrderType.Ship.ToString() && a.OrderType != OrderType.Complaint.ToString()).ToList();
            bool isFilter = false;
            if (!string.IsNullOrEmpty(username))
            {
                isFilter = true;
                listOrder = listOrder.Where(a => a.Phone.ToLower().Contains(username.ToLower())).ToList();
            }

            if (!string.IsNullOrEmpty(fromDate))
            {
                DateTime fromdate = DateTime.ParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                if (isFilter)
                {
                    listOrder = (from order in listOrder
                                 where fromdate <= order.CreateDate
                                 select order).ToList();
                }
                else
                {
                    isFilter = true;
                    listOrder = (from order in listOrder
                                 where fromdate <= order.CreateDate
                                 select order).ToList();
                }
            }

            if (!string.IsNullOrEmpty(toDate))
            {

                DateTime todate = DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(1);
                if (isFilter)
                {
                    listOrder = (from order in listOrder
                                 where order.CreateDate <= todate
                                 select order).ToList();
                }
                else
                {
                    isFilter = true;
                    listOrder = (from order in listOrder
                                 where order.CreateDate <= todate
                                 select order).ToList();
                }
            }

            if (!string.IsNullOrEmpty(listStatus))
            {
                if (isFilter)
                {
                    listOrder = listOrder.FindAll(a => a.Status == listStatus);
                }
                else
                {
                    isFilter = true;
                    listOrder = listOrder.Where(a => a.Status == listStatus).ToList();
                }
            }
            if (!string.IsNullOrEmpty(OrderId))
            {
                if (isFilter)
                {
                    listOrder = listOrder.FindAll(a => a.OrderInternalId.ToString() == OrderId);
                }
                else
                {
                    isFilter = true;
                    listOrder = listOrder.Where(a => a.OrderInternalId.ToString() == OrderId).ToList();
                }
            }
            if (!string.IsNullOrEmpty(TransId))
            {
                if (isFilter)
                {
                    listOrder = listOrder.FindAll(a => a.TransId.ToString() == TransId);
                }
                else
                {
                    isFilter = true;
                    listOrder = listOrder.Where(a => a.TransId.ToString() == TransId).ToList();
                }
            }
            if (!string.IsNullOrEmpty(ShippingCode))
            {
                if (isFilter)
                {
                    listOrder = listOrder.FindAll(a => a.ShippingCode.ToString() == ShippingCode);
                }
                else
                {
                    isFilter = true;
                    listOrder = listOrder.Where(a => a.ShippingCode.ToString() == ShippingCode).ToList();
                }
            }
            ViewBag.ListStatus = GetListStatus(listStatus);

            var listOrderDisplay = new List<OrderInternal>();

            if (isFilter)
            {
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Sale, false))
                {
                    List<string> listUserManage =
                        db.SaleManageClients.Where(a => a.User_Sale == User.Identity.Name)
                            .Select(a => a.User_Client)
                            .ToList();
                    listOrderDisplay.AddRange(
                        listOrder.Where(a => listUserManage.Contains(a.Phone) || string.IsNullOrEmpty(a.SaleManager))
                            .ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Client, false))
                {
                    listOrderDisplay.AddRange(listOrder.Where(a => a.Phone == User.Identity.Name).ToList());
                }

                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Orderer, false))
                {
                    listOrderDisplay.AddRange(listOrder.Where(a => a.Status == OrderStatus.Paid.ToString()).ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Accounting, false))
                {
                    listOrderDisplay.AddRange(
                        db.OrderInternals.Where(
                            a =>
                                a.Status != OrderStatus.New.ToString())
                            .ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Recieve, false))
                {
                    listOrderDisplay.AddRange(
                        listOrder.Where(
                            a =>
                                a.Status == OrderStatus.Order.ToString() ||
                                a.Status == OrderStatus.FullCollect.ToString()).ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"]))
                {
                    listOrderDisplay = listOrder.ToList();
                }
            }
            else
            {
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Sale, false))
                {
                    List<string> listUserManage =
                        db.SaleManageClients.Where(a => a.User_Sale == User.Identity.Name)
                            .Select(a => a.User_Client)
                            .ToList();
                    listOrderDisplay.AddRange(
                        listOrder.Where(a => listUserManage.Contains(a.Phone) || string.IsNullOrEmpty(a.SaleManager))
                            .ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Client, false))
                {
                    listOrderDisplay.AddRange(listOrder.Where(a => a.Phone == User.Identity.Name).ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Accounting, false))
                {
                    listOrderDisplay.AddRange(
                        listOrder.Where(
                            a =>
                                a.Status != OrderStatus.New.ToString())
                            .ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Orderer, false))
                {
                    listOrderDisplay.AddRange(listOrder.Where(a => a.Status == OrderStatus.Paid.ToString()).ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Recieve, false))
                {
                    listOrderDisplay.AddRange(
                        listOrder.Where(
                            a =>
                                a.Status == OrderStatus.Order.ToString() ||
                                a.Status == OrderStatus.FullCollect.ToString()).ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"]))
                {
                    listOrderDisplay = listOrder.ToList();
                }
            }

            listOrderDisplay = listOrderDisplay.Distinct().OrderByDescending(a => a.UpdateDate).ToList();
            if (listOrderDisplay.Count >= 0)
            {
                var lisorder = (from a in listOrderDisplay select a.OrderInternalId).ToList();
                foreach (int orderid in lisorder)
                {
                    OrderInternal order = db.OrderInternals.First(a => a.OrderInternalId == orderid);
                    UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Phone == order.Phone);
                    if (user != null)
                    {
                        var NewView = new ViewManager
                        {
                            OrderInternals = order,
                            UserName = user.Name,
                            IdUser = user.customercode
                        };
                        lismnager.Add(NewView);
                    }
                    else
                    {
                        var NewView = new ViewManager
                        {
                            OrderInternals = order,
                            UserName = "Khách hàng chưa lập TK",
                            IdUser = "Khách hàng chưa lập TK"
                        };
                        lismnager.Add(NewView);
                    }
                }
            }
            ViewBag.CurrentToDate = toDate;
            ViewBag.CurrentFromDate = fromDate;
            ViewBag.CurrentStatus = listStatus;
            ViewBag.CurrentOrderId = OrderId;
            ViewBag.CurrentUserName = username;
            ViewBag.CurrentTranID = TransId;
            ViewBag.CurrentShipCode = ShippingCode;


            int pageNumber = (page ?? 1);

            return View(lismnager.ToPagedList(pageNumber, pageSize));
        }


        public ActionResult ManageReceiver(string username, string fromDate, string toDate, string OrderId, int? page)
        {
            ViewBag.danger = "";
            var listOrder = new List<OrderInternal>();
            var lismnager = new List<ViewManager>();
            listOrder =
                db.OrderInternals.Where(
                    a => a.Status == OrderStatus.Receive.ToString() || a.Status == OrderStatus.FullCollect.ToString())
                    .ToList();

            if (!string.IsNullOrEmpty(username))
            {
                listOrder = listOrder.Where(a => a.Phone == username).ToList();
            }

            if (!string.IsNullOrEmpty(fromDate))
            {
                DateTime fromdate = DateTime.ParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);


                listOrder = (from order in listOrder
                             where fromdate <= order.CreateDate
                             select order).ToList();
            }

            if (!string.IsNullOrEmpty(toDate))
            {
                DateTime todate = DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(1);

                listOrder = (from order in listOrder
                             where order.CreateDate <= todate
                             select order).ToList();
            }

            if (!string.IsNullOrEmpty(OrderId))
            {
                listOrder = listOrder.FindAll(a => a.OrderInternalId.ToString() == OrderId);
            }

            if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Sale, false))
            {
                List<string> listUserManage =
                    db.SaleManageClients.Where(a => a.User_Sale == User.Identity.Name)
                        .Select(a => a.User_Client)
                        .ToList();
                listOrder =
                    listOrder.FindAll(a => listUserManage.Contains(a.Phone) || string.IsNullOrEmpty(a.SaleManager));
            }
            listOrder = listOrder.OrderByDescending(a => a.UpdateDate).ToList();
            if (listOrder.Count > 0)
            {
                var lisorder = (from a in listOrder select a.OrderInternalId).ToList();
                foreach (int orderid in lisorder)
                {
                    OrderInternal order = db.OrderInternals.First(a => a.OrderInternalId == orderid);
                    UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Phone == order.Phone);
                    if (user != null)
                    {
                        var NewView = new ViewManager
                        {
                            OrderInternals = order,
                            UserName = user.Name,
                            IdUser = user.customercode
                        };
                        lismnager.Add(NewView);
                    }
                    else
                    {
                        var NewView = new ViewManager
                        {
                            OrderInternals = order,
                            UserName = "Khách hàng chưa lập TK",
                            IdUser = "Khách hàng chưa lập TK"
                        };
                        lismnager.Add(NewView);
                    }
                    var ordertrain = db.OrderInternals.Where(a => a.ShippingCode == order.ShippingCode && a.Status == OrderStatus.Finish.ToString() && a.OrderInternalId != order.OrderInternalId);
                    if (ordertrain.Count() > 0)
                    {
                        ViewBag.danger = "<a href='/Account/ViewShippingid/" + order.ShippingCode + "' class='btn btn-xs btn-default btn-shippingid'><img src='/Images/Alert_icon.gif'style='width:20px' /></a>";
                    }
                }
            }
            ViewBag.CurrentToDate = toDate;
            ViewBag.CurrentFromDate = fromDate;
            ViewBag.CurrentOrderId = OrderId;
            ViewBag.CurrentUserName = username;


            int pageNumber = (page ?? 1);

            return View(lismnager.ToPagedList(pageNumber, pageSize));
        }
        public ActionResult ViewShippingid(string id)
        {
            var order = db.OrderInternals.Where(a => a.ShippingCode == id && a.Status == OrderStatus.Finish.ToString());
            return View(order);
        }
        public ActionResult ManageOrdererReject(string username, string fromDate, string toDate, string OrderId,
            int? page)
        {
            var listOrder = new List<OrderInternal>();
            var lismnager = new List<ViewManager>();
            listOrder = db.OrderInternals.Where(a => a.Status == OrderStatus.OrdererReject.ToString()).ToList();
            if (!string.IsNullOrEmpty(username))
            {
                listOrder = listOrder.Where(a => a.Phone == username).ToList();
            }

            if (!string.IsNullOrEmpty(fromDate))
            {
                DateTime fromdate = DateTime.ParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);


                listOrder = (from order in listOrder
                             where fromdate <= order.CreateDate
                             select order).ToList();
            }

            if (!string.IsNullOrEmpty(toDate))
            {
                DateTime todate = DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(1);

                listOrder = (from order in listOrder
                             where order.CreateDate <= todate
                             select order).ToList();
            }

            if (!string.IsNullOrEmpty(OrderId))
            {
                listOrder = listOrder.FindAll(a => a.OrderInternalId.ToString() == OrderId);
            }

            if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Sale, false))
            {
                List<string> listUserManage =
                    db.SaleManageClients.Where(a => a.User_Sale == User.Identity.Name)
                        .Select(a => a.User_Client)
                        .ToList();
                listOrder =
                    listOrder.FindAll(a => listUserManage.Contains(a.Phone) || string.IsNullOrEmpty(a.SaleManager));
            }

            listOrder = listOrder.OrderByDescending(a => a.CreateDate).ToList();
            if (listOrder.Count < 0)
            {
                var lisorder = (from a in listOrder select a.OrderInternalId).ToList();
                foreach (int orderid in lisorder)
                {
                    OrderInternal order = db.OrderInternals.First(a => a.OrderInternalId == orderid);
                    UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Phone == order.Phone);
                    if (user != null)
                    {
                        var NewView = new ViewManager
                        {
                            OrderInternals = order,
                            UserName = user.Name,
                            IdUser = user.customercode
                        };
                        lismnager.Add(NewView);
                    }
                    else
                    {
                        var NewView = new ViewManager
                        {
                            OrderInternals = order,
                            UserName = "Khách hàng chưa lập TK",
                            IdUser = "Khách hàng chưa lập TK"
                        };
                        lismnager.Add(NewView);
                    }
                }
            }
            ViewBag.CurrentToDate = toDate;
            ViewBag.CurrentFromDate = fromDate;
            ViewBag.CurrentOrderId = OrderId;
            ViewBag.CurrentUserName = username;


            int pageNumber = (page ?? 1);

            return View(lismnager.ToPagedList(pageNumber, pageSize));
        }



        private SelectList GetListStatus(string value)
        {
            var list = new List<object>();
            foreach (FieldInfo fieldInfo in typeof(OrderStatus).GetFields())
            {
                if (fieldInfo.FieldType.Name != "OrderStatus")
                    continue;
                var attribute = Attribute.GetCustomAttribute(fieldInfo,
                    typeof(DisplayAttribute)) as DisplayAttribute;

                list.Add(attribute != null
                    ? new { name = fieldInfo.Name, display = attribute.Name }
                    : new { name = fieldInfo.Name, display = fieldInfo.Name });
            }

            return new SelectList(list, "name", "display", value);
        }

        private SelectList GetListUserType(string[] value = null)
        {
            var list = new List<object>();
            foreach (FieldInfo fieldInfo in typeof(UserType).GetFields())
            {
                if (fieldInfo.FieldType.Name != "UserType" || fieldInfo.Name == UserType.Admin.ToString())
                    continue;
                var attribute = Attribute.GetCustomAttribute(fieldInfo,
                    typeof(DisplayAttribute)) as DisplayAttribute;

                list.Add(attribute != null
                    ? new { name = (int)fieldInfo.GetValue(fieldInfo), display = attribute.Name }
                    : new { name = (int)fieldInfo.GetValue(fieldInfo), display = fieldInfo.Name });
            }

            return new SelectList(list, "name", "display", value);
        }

        private SelectList GetListUserType(string value = "")
        {
            var list = new List<object>();
            foreach (FieldInfo fieldInfo in typeof(UserType).GetFields())
            {
                if (fieldInfo.FieldType.Name != "UserType" || fieldInfo.Name == UserType.Admin.ToString())
                    continue;
                var attribute = Attribute.GetCustomAttribute(fieldInfo,
                    typeof(DisplayAttribute)) as DisplayAttribute;

                list.Add(attribute != null
                    ? new { name = (int)fieldInfo.GetValue(fieldInfo), display = attribute.Name }
                    : new { name = (int)fieldInfo.GetValue(fieldInfo), display = fieldInfo.Name });
            }

            return new SelectList(list, "name", "display", value);
        }

        private SelectList GetListUserType()
        {
            var list = new List<object>();
            foreach (FieldInfo fieldInfo in typeof(UserType).GetFields())
            {
                if (fieldInfo.FieldType.Name != "UserType" || fieldInfo.Name == UserType.Admin.ToString())
                    continue;
                var attribute = Attribute.GetCustomAttribute(fieldInfo,
                    typeof(DisplayAttribute)) as DisplayAttribute;

                if (attribute != null)
                    list.Add(new { name = (int)fieldInfo.GetValue(fieldInfo), display = attribute.Name });
                else
                    list.Add(new { name = (int)fieldInfo.GetValue(fieldInfo), display = fieldInfo.Name });
            }

            return new SelectList(list, "name", "display");
        }

        [AllowAnonymous]
        public ActionResult IsCheckExitsPhone(string Phone)
        {
            return Json(!db.UserProfiles.Any(lo => lo.Phone == Phone), JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult IsCheckExitsCustomercode(string customercode)
        {
            return Json(!db.UserProfiles.Any(lo => lo.customercode == customercode), JsonRequestBehavior.AllowGet);
        }

        public ActionResult IsCheckExitsAccount(string Account)
        {
            return Json(!db.Wallets.Any(lo => lo.Account == Account), JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult IsCheckEmail(string Email)
        {
            if (string.IsNullOrEmpty(Email))
                return Json(true, JsonRequestBehavior.AllowGet);

            string str = Email.ToLower();
            //kiem tra cac ki tu
            const String pattern =
                @"^([0-9a-zA-Z]" + //Start with a digit or alphabetical
                @"([\+\-_\.][0-9a-zA-Z]+)*" + // No continuous or ending +-_. chars in email
                @")+" +
                @"@(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9]{2,17})$";
            if (!Regex.IsMatch(str, pattern))
            {
                return Json("Email không đúng định dạng", JsonRequestBehavior.AllowGet);
            }
            //check trung
            if (db.UserProfiles.Any(lo => lo.Email.ToLower() == Email.ToLower()))
            {
                return Json("Email đã tồn tại trong hệ thống", JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        [AllowAnonymous]
        public bool IsEmail(string Email)
        {
            string str = Email.ToLower();
            //kiem tra cac ki tu
            const String pattern =
                @"^([0-9a-zA-Z]" + //Start with a digit or alphabetical
                @"([\+\-_\.][0-9a-zA-Z]+)*" + // No continuous or ending +-_. chars in email
                @")+" +
                @"@(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9]{2,17})$";

            return Regex.IsMatch(str, pattern);
        }

        #region Helpers

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return
                        "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return
                        "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return
                        "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return
                        "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }

        #endregion

        #endregion

        #region Order

        //
        // GET: /Account/CreateOrder

        [AllowAnonymous]
        public ActionResult CreateOrderManage()
        {
            return View();
        }

        //
        // GET: /Account/CreateOrderFromExcel
        [AllowAnonymous]
        public ActionResult CreateOrderFromExcel()
        {
            return View();
        }


        [HttpPost]
        [AllowAnonymous]
        public JsonResult CreateOrderExcel()
        {
            var ds = new DataSet();
            HttpPostedFileBase httpPostedFileBase = Request.Files["file"];
            if (httpPostedFileBase != null && httpPostedFileBase.ContentLength > 0)
            {
                try
                {
                    string fileExtension =
                        Path.GetExtension(httpPostedFileBase.FileName);

                    if (fileExtension == ".xls" || fileExtension == ".xlsx")
                    {
                        string fileLocation = Server.MapPath("~/Upload/Images/") + httpPostedFileBase.FileName;
                        if (System.IO.File.Exists(fileLocation))
                        {
                            System.IO.File.Delete(fileLocation);
                        }
                        httpPostedFileBase.SaveAs(fileLocation);

                        string excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation +
                                                       ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                        //connection String for xls file format.
                        if (fileExtension == ".xls")
                        {
                            excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileLocation +
                                                    ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                        }
                        //connection String for xlsx file format.
                        else if (fileExtension == ".xlsx")
                        {
                            excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation +
                                                    ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                        }

                        //Create Connection to Excel work book and add oledb namespace
                        using (var excelConnection = new OleDbConnection(excelConnectionString))
                        {
                            excelConnection.Open();
                            var dt = new DataTable();

                            dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                            if (dt == null)
                            {
                                return Json(new { success = false, message = "File không đúng mẫu" });
                            }

                            var excelSheet = string.Empty;
                            //excel data saves in temp file here.
                            foreach (DataRow row in dt.Rows)
                            {
                                if (row["TABLE_NAME"].ToString().IndexOf(sheet_name) > 0)
                                {
                                    excelSheet = row["TABLE_NAME"].ToString();
                                }
                            }
                            if (string.IsNullOrEmpty(excelSheet))
                            {
                                if (Request.IsAuthenticated)
                                {
                                    if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Client, false))
                                    {
                                        //check kh. insert file error.
                                        string fileErrorLocation = Server.MapPath("~/Upload/ExcelError/") + httpPostedFileBase.FileName;
                                        if (System.IO.File.Exists(fileErrorLocation))
                                        {
                                            System.IO.File.Delete(fileErrorLocation);
                                        }
                                        httpPostedFileBase.SaveAs(fileErrorLocation);

                                        //Insert errorExcel Notice
                                        var excelnotice = new ExcelNotice();
                                        excelnotice.Phone = User.Identity.Name;
                                        excelnotice.Location = fileErrorLocation;
                                        excelnotice.Name = httpPostedFileBase.FileName;
                                        excelnotice.Note = "Không tìm thấy sheet: File đặt hàng của khách";
                                        excelnotice.LastUpdate = DateTime.Now;
                                        excelnotice.Status = ExcelNoticeStatus.UnDone.ToString();

                                        excelnotice.IsHaveSaleManage = db.SaleManageClients.Any(a => a.User_Client == User.Identity.Name);
                                        db.ExcelNotices.Add(excelnotice);
                                        db.SaveChanges();
                                    }
                                }
                                //else
                                //{
                                //    //check kh. insert file error.
                                //    string fileErrorLocation = Server.MapPath("~/Upload/ExcelError/") + httpPostedFileBase.FileName;
                                //    if (System.IO.File.Exists(fileErrorLocation))
                                //    {
                                //        System.IO.File.Delete(fileErrorLocation);
                                //    }
                                //    httpPostedFileBase.SaveAs(fileErrorLocation);

                                //    //Insert errorExcel Notice
                                //    var excelnotice = new ExcelNotice();
                                //    excelnotice.Phone = User.Identity.Name;
                                //    excelnotice.Location = fileErrorLocation;
                                //    excelnotice.Name = httpPostedFileBase.FileName;
                                //    excelnotice.Note = "Không tìm thấy sheet: File đặt hàng của khách";
                                //    db.ExcelNotices.Add(excelnotice);
                                //}
                                return Json(new { success = false, message = "Không tìm thấy sheet \"File đặt hàng của khách\"" });
                            }

                            var excelConnection1 = new OleDbConnection(excelConnectionString);

                            string query = string.Format("Select * from [{0}]", excelSheet);
                            using (var dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                            {
                                dataAdapter.Fill(ds);
                                dataAdapter.Dispose();
                            }

                            excelConnection1.Close();
                            excelConnection.Close();
                        }
                    }

                    var order = new Order();
                    var orderDetails = new List<OrderDetail>();

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        //lấy thông tin khách hàng
                        if (i == 2)
                        {
                            //Mã khách hàng:
                            order.CustomerCode = ds.Tables[0].Rows[i].ItemArray[2].ToString();
                        }
                        else if (i == 3)
                        {
                            //Tên khách hàng
                            order.ClientName = ds.Tables[0].Rows[i].ItemArray[2].ToString();
                        }
                        else if (i == 4)
                        {
                            //SDT
                            order.Phone = ds.Tables[0].Rows[i].ItemArray[2].ToString();
                        }
                        else if (i == 5)
                        {
                            //Email
                            order.UserName = ds.Tables[0].Rows[i].ItemArray[2].ToString();
                        }
                        else if (i == 6)
                        {
                            //Address
                            order.ClientAddress = ds.Tables[0].Rows[i].ItemArray[2].ToString();
                        }

                        else if (i >= 25)
                        {
                            var link = ds.Tables[0].Rows[i].ItemArray[2].ToString();
                            if (!string.IsNullOrEmpty(link))
                            {
                                var orderdetail = new OrderDetail
                                {
                                    Link = link,
                                    Color = ds.Tables[0].Rows[i].ItemArray[3].ToString(),
                                    Size = ds.Tables[0].Rows[i].ItemArray[4].ToString(),
                                    Quantity = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[6].ToString()),
                                    Price = Convert.ToDouble(ds.Tables[0].Rows[i].ItemArray[7].ToString())
                                };
                                orderDetails.Add(orderdetail);
                            }

                        }
                    }



                    var success = true;
                    var message = string.Empty;
                    //tiến hành update
                    if (Request.IsAuthenticated)
                    {
                        if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Client, false))
                        {
                            if (string.IsNullOrEmpty(order.Phone) || string.IsNullOrEmpty(order.UserName))
                            {

                                success = false;
                                message = "Không có thông tin khách hàng";
                            }
                        }
                    }

                    if (orderDetails.Count(a => string.IsNullOrEmpty(a.Link)) > 0)
                    {
                        success = false;
                        message = "Link trống";
                    }
                    if (orderDetails.Count(a => a.Quantity == null || a.Quantity == 0) > 0)
                    {
                        success = false;
                        message = "Link không có khối lượng";
                    }
                    if (orderDetails.Count(a => a.Price == null || a.Price == 0) > 0)
                    {
                        success = false;
                        message = "Link không có giá";
                    }

                    if (!success)
                    {
                        if (Request.IsAuthenticated)
                        {
                            if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Client, false))
                            {

                                //check kh. insert file error.
                                string fileErrorLocation = Server.MapPath("~/Upload/ExcelError/") + httpPostedFileBase.FileName;
                                if (System.IO.File.Exists(fileErrorLocation))
                                {
                                    System.IO.File.Delete(fileErrorLocation);
                                }
                                httpPostedFileBase.SaveAs(fileErrorLocation);

                                //Insert errorExcel Notice
                                var excelnotice = new ExcelNotice();
                                excelnotice.Phone = User.Identity.Name;
                                excelnotice.Location = fileErrorLocation;
                                excelnotice.Name = httpPostedFileBase.FileName;
                                excelnotice.Note = message;
                                excelnotice.Status = ExcelNoticeStatus.UnDone.ToString();
                                excelnotice.LastUpdate = DateTime.Now;
                                excelnotice.IsHaveSaleManage = db.SaleManageClients.Any(a => a.User_Client == User.Identity.Name);

                                db.ExcelNotices.Add(excelnotice);

                                db.SaveChanges();

                            }
                        }
                        return Json(new { success = success, message = message });
                    }

                    //if là client. refill data
                    if (Request.IsAuthenticated)
                    {
                        if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Client, false))
                        {
                            var useProfile = db.UserProfiles.FirstOrDefault(a => a.Phone == User.Identity.Name);
                            if (useProfile != null)
                            {
                                order.Phone = useProfile.Phone;
                                order.UserName = useProfile.Email;
                                order.CustomerCode = useProfile.customercode;
                            }
                        }
                    }
                    //kiểm tra phone va email ton tai
                    using (DbContextTransaction dbContextTransaction = db.Database.BeginTransaction())
                    {
                        try
                        {
                            Rate rate = db.Rates.FirstOrDefault();
                            order.Rate = rate != null ? rate.Price : 0;
                            order.CreateDate = DateTime.Now;
                            order.UpdateDate = DateTime.Now;
                            order.OrderType = OrderType.Order.ToString();
                            order.TotalPrice = orderDetails.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0));
                            order.Status = OrderStatus.New.ToString();
                            order.Fee = 5;
                            order.TotalPriceConvert = order.TotalPrice * order.Rate;

                            SaleManageClient saleManage =
                                db.SaleManageClients.FirstOrDefault(a => a.User_Client == order.Phone);
                            if (saleManage != null) order.SaleManager = saleManage.User_Sale;
                            db.Entry(order).State = EntityState.Added;
                            db.Orders.Add(order);
                            db.SaveChanges();

                            int orderId = order.OrderId;

                            foreach (OrderDetail orderDetail in orderDetails)
                            {
                                orderDetail.OrderId = orderId;
                                orderDetail.Phone = order.Phone;
                                orderDetail.OrderDetailStatus = OrderDetailStatus.Active.ToString();
                                db.Entry(orderDetail).State = EntityState.Added;
                                db.OrderDetails.Add(orderDetail);
                                db.SaveChanges();
                            }

                            dbContextTransaction.Commit();

                            if (Request.IsAuthenticated)
                            {

                                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Client, false))
                                {
                                    return Json(new { success = true, url = Url.Action("AllOrder", "Account") });
                                }
                                else
                                {
                                    return Json(new { success = true, url = Url.Action("Manage", "Account") });
                                }
                            }
                            return Json(new { success = true });
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();

                            //check kh. insert file error.
                            string fileErrorLocation = Server.MapPath("~/Upload/ExcelError/") + httpPostedFileBase.FileName;
                            if (System.IO.File.Exists(fileErrorLocation))
                            {
                                System.IO.File.Delete(fileErrorLocation);
                            }
                            httpPostedFileBase.SaveAs(fileErrorLocation);

                            //Insert errorExcel Notice
                            var excelnotice = new ExcelNotice();
                            excelnotice.Phone = User.Identity.Name;
                            excelnotice.Location = fileErrorLocation;
                            excelnotice.Name = httpPostedFileBase.FileName;
                            excelnotice.Status = ExcelNoticeStatus.UnDone.ToString();
                            excelnotice.LastUpdate = DateTime.Now;
                            excelnotice.Note = "Không tìm thấy sheet: File đặt hàng của khách";
                            db.ExcelNotices.Add(excelnotice);
                            db.SaveChanges();


                            return Json(new { success = false, message = ex.ToString() });
                        }
                    }
                }
                catch (Exception ex)
                {
                    //check kh. insert file error.
                    string fileErrorLocation = Server.MapPath("~/Upload/ExcelError/") + httpPostedFileBase.FileName;
                    if (System.IO.File.Exists(fileErrorLocation))
                    {
                        System.IO.File.Delete(fileErrorLocation);
                    }
                    httpPostedFileBase.SaveAs(fileErrorLocation);

                    //Insert errorExcel Notice
                    var excelnotice = new ExcelNotice();
                    excelnotice.Phone = User.Identity.Name;
                    excelnotice.Location = fileErrorLocation;
                    excelnotice.Name = httpPostedFileBase.FileName;
                    excelnotice.Status = ExcelNoticeStatus.UnDone.ToString();
                    excelnotice.LastUpdate = DateTime.Now;
                    excelnotice.Note = "Không tìm thấy sheet: File đặt hàng của khách";
                    db.ExcelNotices.Add(excelnotice);
                    db.SaveChanges();

                    return Json(new { success = false, message = ex.ToString() });
                }
            }

            return Json(new { success = false, message = "File không đúng mẫu" });
        }

        //
        // GET: /Account/CreateOrder

        [AllowAnonymous]
        public ActionResult CreateOrder()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult CreateOrder(NewOrderModel model, FormCollection ortype)
        {
            var ortype1 = ortype["OrderType"];
            //thực hiện tạo order dựa tên orderdetail
            if (Request.IsAuthenticated)
            {
                using (DbContextTransaction dbContextTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        Rate rate = db.Rates.FirstOrDefault();
                        UserProfile user = db.UserProfiles.First(a => a.Phone == User.Identity.Name);
                        var order = new Order();
                        if (ortype1 == "1")
                        {
                            if (model.ListOrderDetail.Count() == 1)
                            {
                                var orderInternal = new OrderInternal
                                {
                                    CreateDate = DateTime.Now,
                                    UpdateDate = DateTime.Now,
                                    TotalPrice = model.ListOrderDetail.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0)),
                                    TotalPriceClient = model.ListOrderDetail.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0)),
                                    TotalPriceReality = model.ListOrderDetail.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0)),

                                    Status = OrderStatus.New.ToString(),
                                    Rate = rate != null ? rate.Price : 0,
                                    Fee = 5,
                                    Phone = user.Phone,
                                    UserName = user.Email,
                                    OrderType = OrderType.Order.ToString(),
                                    IsShop = true
                                };
                                // tạo mã đơn theo ngày
                                var orderinternalinday = db.OrderInternalInDays.First(a => a.ID == 1);
                                if (orderinternalinday.CreateDate.Date == DateTime.Now.Date)
                                {
                                    orderinternalinday.OrderInternalInDayID = orderinternalinday.OrderInternalInDayID + 1;
                                }
                                else
                                {
                                    orderinternalinday.OrderInternalInDayID = 1;
                                    orderinternalinday.CreateDate = DateTime.Now;
                                }
                                db.SaveChanges();

                                var customer_code = db.UserProfiles.FirstOrDefault(a => a.Phone == orderInternal.Phone).customercode;
                                orderInternal.OrderInternalInDayID = DateTime.Now.ToString("yyMMdd") + orderInternal.OrderType.ToUpper() + "0" + orderinternalinday.OrderInternalInDayID + customer_code;

                                //
                                orderInternal.TotalPriceConvert = orderInternal.TotalPrice * orderInternal.Rate;

                                //get downpayment
                                orderInternal.DownPaymentRate = user.DownPaymentRate;

                                // get fee
                                int orderValueInt = Convert.ToInt32(orderInternal.TotalPriceConvert / 1000000);
                                var fee =
                                    db.Fees.Where(a => (a.From <= orderValueInt && a.To >= orderValueInt) || (a.To == 0 && a.From <= orderValueInt))
                                        .OrderByDescending(a => a.Value).First();
                                order.Fee = fee != null ? fee.Value : 5;

                                SaleManageClient saleManage =
                                    db.SaleManageClients.FirstOrDefault(a => a.User_Client == user.Phone);
                                if (saleManage != null) orderInternal.SaleManager = saleManage.User_Sale;
                                db.Entry(orderInternal).State = EntityState.Added;
                                db.OrderInternals.Add(orderInternal);
                                db.SaveChanges();

                                int orderInternalId = orderInternal.OrderInternalId;

                                var orderDetail = new OrderInternalDetail();
                                orderDetail.OrderInternalId = orderInternalId;
                                orderDetail.Phone = user.Phone;
                                orderDetail.OrderDetailStatus = OrderDetailStatus.Active.ToString();
                                orderDetail.Link = model.ListOrderDetail.First().Link;
                                orderDetail.Shop = model.ListOrderDetail.First().Shop;
                                orderDetail.Description = model.ListOrderDetail.First().Description;
                                orderDetail.Color = model.ListOrderDetail.First().Color;
                                orderDetail.Size = model.ListOrderDetail.First().Size;
                                orderDetail.Quantity = model.ListOrderDetail.First().Quantity;
                                orderDetail.Price = model.ListOrderDetail.First().Price;
                                try
                                {
                                    var image = Request.Files["Image_" + model.ListOrderDetail.First().stt];
                                    if (image != null && !string.IsNullOrEmpty(image.FileName))
                                    {
                                        orderDetail.Image = "/Images/" + image.FileName;
                                        image.SaveAs(Server.MapPath("~/Images/" + image.FileName));
                                    }
                                }
                                catch (Exception)
                                {
                                    //no image
                                }
                                db.Entry(orderDetail).State = EntityState.Added;
                                db.OrderInternalDetails.Add(orderDetail);
                                db.SaveChanges();

                                dbContextTransaction.Commit();

                                ViewData["message"] = "Tạo đơn hàng thành công.";
                                if (user.UserType.IndexOf(((int)UserType.Client).ToString()) >= 0)
                                {
                                    return RedirectToAction("AllOrder", "Account");
                                }
                                return RedirectToAction("Manage", "Account");


                            }
                            else
                            {
                                order = new Order
                                {
                                    CreateDate = DateTime.Now,
                                    UpdateDate = DateTime.Now,
                                    TotalPrice = model.ListOrderDetail.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0)),
                                    Status = OrderStatus.New.ToString(),
                                    Rate = rate != null ? rate.Price : 0,
                                    Fee = 5,
                                    Phone = user.Phone,
                                    UserName = user.Email,
                                    OrderType = OrderType.Order.ToString(),
                                    IsShop = false
                                };
                                order.TotalPriceConvert = order.TotalPrice * order.Rate;

                                //get downpayment
                                order.DownPaymentRate = user.DownPaymentRate;

                                //get fee
                                double orderValueInt = order.TotalPriceConvert / 1000000;
                                var fee =
                                    db.Fees.Where(a => (a.From <= orderValueInt && a.To >= orderValueInt) || (a.To == 0 && a.From <= orderValueInt))
                                        .OrderByDescending(a => a.Value).First();
                                order.Fee = fee != null ? fee.Value : 5;

                                SaleManageClient saleManage =
                                    db.SaleManageClients.FirstOrDefault(a => a.User_Client == user.Phone);
                                if (saleManage != null) order.SaleManager = saleManage.User_Sale;
                                db.Entry(order).State = EntityState.Added;
                                db.Orders.Add(order);
                                db.SaveChanges();

                                int orderId = order.OrderId;
                                foreach (OrderDetail orderDetail in model.ListOrderDetail)
                                {
                                    orderDetail.OrderId = orderId;
                                    orderDetail.Phone = user.Phone;
                                    orderDetail.OrderDetailStatus = OrderDetailStatus.Active.ToString();
                                    try
                                    {
                                        var image = Request.Files["Image_" + orderDetail.stt];
                                        if (image != null && !string.IsNullOrEmpty(image.FileName))
                                        {
                                            orderDetail.Image = "/Images/" + image.FileName;
                                            image.SaveAs(Server.MapPath("~/Images/" + image.FileName));
                                        }

                                    }
                                    catch (Exception)
                                    {
                                        //no image
                                    }
                                    db.Entry(orderDetail).State = EntityState.Added;
                                    db.OrderDetails.Add(orderDetail);
                                    db.SaveChanges();
                                }

                                dbContextTransaction.Commit();

                                ViewData["message"] = "Tạo đơn hàng thành công.";
                                if (user.UserType.IndexOf(((int)UserType.Client).ToString()) >= 0)
                                {
                                    return RedirectToAction("OrderNoShop", "Account");
                                }
                                return RedirectToAction("Manage", "Account");
                            }

                        }
                        else
                        {
                            var orderInternal = new OrderInternal
                            {
                                CreateDate = DateTime.Now,
                                UpdateDate = DateTime.Now,
                                TotalPrice = model.ListOrderDetails.Sum(a => a.TotalPrice),
                                Status = OrderStatus.New.ToString(),
                                Rate = rate != null ? rate.Price : 0,
                                DownPayment = model.ListOrderDetails.Sum(a => a.DownPayment),
                                Fee = 0,
                                Phone = user.Phone,
                                UserName = user.Email,
                                OrderType = OrderType.Order.ToString(),
                                IsShop = false
                            };
                            if (orderInternal.TotalPrice > orderInternal.DownPayment)
                            {
                                orderInternal.TotalPriceConvert = (orderInternal.TotalPrice - orderInternal.DownPayment);

                                //get downpayment
                                orderInternal.DownPaymentRate = user.DownPaymentRate;

                                //get fee
                                //int orderValueInt = Convert.ToInt32(orderInternal.TotalPriceConvert / 1000000);
                                //var fee =
                                //    db.Fees.Where(a => a.From <= orderValueInt && a.To >= orderValueInt)
                                //        .OrderByDescending(a => a.Value).First();
                                orderInternal.Fee = 0;

                                SaleManageClient saleManage =
                                    db.SaleManageClients.FirstOrDefault(a => a.User_Client == user.Phone);
                                if (saleManage != null) orderInternal.SaleManager = saleManage.User_Sale;
                                orderInternal.OrderType = OrderType.AllInOne.ToString();
                                orderInternal.Status = OrderStatus.SaleConfirm.ToString();
                                db.Entry(orderInternal).State = EntityState.Added;
                                db.OrderInternals.Add(orderInternal);
                                db.SaveChanges();

                                int orderInternalId = orderInternal.OrderInternalId;

                                foreach (OrderInternalDetail orderDetail in model.ListOrderInternalDetails)
                                {
                                    orderDetail.OrderInternalId = orderInternalId;
                                    orderDetail.Phone = user.Phone;
                                    orderDetail.OrderDetailStatus = OrderDetailStatus.Active.ToString();

                                    try
                                    {
                                        var image = Request.Files["Image_" + orderDetail.stt];
                                        if (image != null && !string.IsNullOrEmpty(image.FileName))
                                        {
                                            orderDetail.Image = "/Images/" + image.FileName;
                                            image.SaveAs(Server.MapPath("~/Images/" + image.FileName));
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        //no image
                                    }
                                    db.Entry(orderDetail).State = EntityState.Added;
                                    db.OrderInternalDetails.Add(orderDetail);
                                    db.SaveChanges();
                                }

                                dbContextTransaction.Commit();

                                ViewData["message"] = "Tạo đơn hàng thành công.";
                                if (user.UserType.IndexOf(((int)UserType.Client).ToString()) >= 0)
                                {
                                    return RedirectToAction("OrderNoShop", "Account");
                                }
                                return RedirectToAction("Manage", "Account");
                            }
                            else
                            {
                                dbContextTransaction.Commit();

                                ViewData["message"] = "Tổng tiền phải lớn hơn tiền đặt cọc";
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                    }
                }
            }
            else
            {
                using (DbContextTransaction dbContextTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        Rate rate = db.Rates.FirstOrDefault();
                        if (ortype1 == "1")
                        {
                            var order = new Order
                            {
                                CreateDate = DateTime.Now,
                                TotalPrice = model.ListOrderDetail.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0)),
                                Status = OrderStatus.New.ToString(),
                                Rate = rate != null ? rate.Price : 0,
                                Fee = 5,
                                Phone = model.ListOrderDetail.First().Phone,
                                UpdateDate = DateTime.Now,
                                OrderType = OrderType.Order.ToString(),
                                IsShop = false

                            };
                            db.Entry(order).State = EntityState.Added;

                            order.TotalPriceConvert = order.TotalPrice * order.Rate;

                            //get fee
                            double orderValueInt = order.TotalPriceConvert / 1000000;
                            var fee =
                                db.Fees.Where(a => (a.From <= orderValueInt && a.To >= orderValueInt) || (a.To == 0 && a.From <= orderValueInt))
                                    .OrderByDescending(a => a.Value).First();
                            order.Fee = fee != null ? fee.Value : 5;

                            db.Orders.Add(order);
                            db.SaveChanges();

                            int orderId = order.OrderId;

                            foreach (OrderDetail orderDetail in model.ListOrderDetail)
                            {
                                orderDetail.OrderId = orderId;
                                orderDetail.Phone = order.Phone;
                                orderDetail.OrderDetailStatus = OrderDetailStatus.Active.ToString();
                                try
                                {
                                    var image = Request.Files["Image_" + orderDetail.stt];
                                    if (image != null && !string.IsNullOrEmpty(image.FileName))
                                    {
                                        orderDetail.Image = "/Images/" + image.FileName;
                                        image.SaveAs(Server.MapPath("~/Images/" + image.FileName));
                                    }
                                }
                                catch (Exception)
                                {
                                    //no image
                                }
                                db.Entry(orderDetail).State = EntityState.Added;
                                db.OrderDetails.Add(orderDetail);
                                db.SaveChanges();
                            }

                            dbContextTransaction.Commit();

                            ViewData["message"] = "Tạo đơn hàng thành công, chờ nhân viên kinh doanh liên hệ với bạn.";

                            return View();
                        }
                        else
                        {
                            var orderInternal = new OrderInternal
                            {
                                CreateDate = DateTime.Now,
                                UpdateDate = DateTime.Now,
                                TotalPrice = model.ListOrderDetails.Sum(a => a.TotalPrice),
                                DownPayment = model.ListOrderDetails.First().DownPayment,
                                Status = OrderStatus.New.ToString(),
                                Rate = rate != null ? rate.Price : 0,
                                Phone = model.ListOrderDetails.First().Phone,
                                Fee = 0,
                                OrderType = OrderType.Order.ToString(),
                                IsShop = false
                            };
                            if (orderInternal.TotalPrice > orderInternal.DownPayment)
                            {
                                orderInternal.TotalPriceConvert = orderInternal.TotalPrice - orderInternal.DownPayment;
                                //get downpayment                        
                                //get fee
                                //int orderValueInt = Convert.ToInt32(orderInternal.TotalPriceConvert / 1000000);
                                //var fee =
                                //    db.Fees.Where(a => a.From <= orderValueInt && a.To >= orderValueInt)
                                //        .OrderByDescending(a => a.Value).First();
                                orderInternal.Fee = 0;

                                //SaleManageClient saleManage =
                                //    db.SaleManageClients.FirstOrDefault(a => a.User_Client == user.Phone);
                                //if (saleManage != null) orderInternal.SaleManager = saleManage.User_Sale;
                                orderInternal.OrderType = OrderType.AllInOne.ToString();
                                orderInternal.Status = OrderStatus.SaleConfirm.ToString();
                                db.Entry(orderInternal).State = EntityState.Added;
                                db.OrderInternals.Add(orderInternal);
                                db.SaveChanges();

                                int orderInternalId = orderInternal.OrderInternalId;

                                foreach (OrderInternalDetail orderDetail in model.ListOrderInternalDetails)
                                {
                                    orderDetail.OrderInternalId = orderInternalId;
                                    orderDetail.Phone = orderInternal.Phone;
                                    orderDetail.OrderDetailStatus = OrderDetailStatus.Active.ToString();
                                    try
                                    {
                                        var image = Request.Files["Image_" + orderDetail.stt];
                                        if (image != null && !string.IsNullOrEmpty(image.FileName))
                                        {
                                            orderDetail.Image = "/Images/" + image.FileName;
                                            image.SaveAs(Server.MapPath("~/Images/" + image.FileName));
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        //no image
                                    }

                                    db.Entry(orderDetail).State = EntityState.Added;
                                    db.OrderInternalDetails.Add(orderDetail);
                                    db.SaveChanges();
                                }

                                dbContextTransaction.Commit();

                                ViewData["message"] = "Tạo đơn hàng thành công đợi sale liên hệ với bạn ";
                                return View();
                            }
                            else
                            {
                                // dbContextTransaction.Commit();

                                ViewData["message"] = "Tổng tiền phải lớn hơn tiền đặt cọc";
                            }

                        }

                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                    }
                }
            }
            return View();
        }



        //
        // GET: /Account/CreateOrder

        [AllowAnonymous]
        public ActionResult CreateOrderOfSale()
        {

            if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Sale))
            {
                List<string> listUserManage =
                    db.SaleManageClients.Where(a => a.User_Sale == User.Identity.Name)
                        .Select(a => a.User_Client)
                        .ToList();
                var listClient = new List<object>();
                foreach (string client in listUserManage)
                {
                    UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Phone == client);
                    if (user != null)
                    {
                        listClient.Add(new { value = user.Phone, display = user.Phone + "-" + user.Name });
                    }
                }
                ViewBag.ListClient = new SelectList(listClient, "value", "display");
                return View();
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult CreateOrderOfSale(NewOrderWithPhoneModel model, FormCollection coleec)
        {
            var OrderType1 = coleec["OrderType"];
            var IDOrderCompain = coleec["IDOrderCompain"];
            //thực hiện tạo order dựa tên orderdetail
            using (DbContextTransaction dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    Rate rate = db.Rates.FirstOrDefault();
                    UserProfile user = db.UserProfiles.First(a => a.Phone == model.Phone);
                    var order = new Order();
                    var orderinter = new OrderInternal();
                    if (OrderType1 == "1")
                    {
                        if (model.ListOrderDetail.Count() == 1)
                        {
                            var orderInternal = new OrderInternal
                            {
                                CreateDate = DateTime.Now,
                                TotalPrice = model.ListOrderDetail.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0)),
                                TotalPriceReality = model.ListOrderDetail.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0)),
                                TotalPriceClient = model.ListOrderDetail.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0)),
                                Status = OrderStatus.New.ToString(),
                                Rate = rate != null ? rate.Price : 0,
                                Fee = 5,
                                Phone = user.Phone,
                                UserName = user.Email,
                                UpdateDate = DateTime.Now,

                            };


                            orderInternal.TotalPriceConvert = orderInternal.TotalPrice * orderInternal.Rate;
                            orderInternal.SaleManager = User.Identity.Name;

                            //get downpayment
                            orderInternal.DownPaymentRate = user.DownPaymentRate;

                            //get fee

                            double orderValueInt = Convert.ToInt32(orderInternal.TotalPriceConvert / 1000000);
                            var fee =
                                     db.Fees.Where(a => (a.From <= orderValueInt && a.To >= orderValueInt) || (a.To == 0 && a.From <= orderValueInt))
                                         .OrderByDescending(a => a.Value).First();

                            orderInternal.Fee = fee != null ? fee.Value : 5;
                            orderInternal.OrderType = OrderType.Order.ToString();
                            db.Entry(orderInternal).State = EntityState.Added;
                            db.OrderInternals.Add(orderInternal);
                            db.SaveChanges();

                            int orderInternalId = orderInternal.OrderInternalId;

                            var orderDetail = new OrderInternalDetail();
                            orderDetail.OrderInternalId = orderInternalId;
                            orderDetail.Phone = user.Phone;
                            orderDetail.OrderDetailStatus = OrderDetailStatus.Active.ToString();
                            orderDetail.Link = model.ListOrderDetail.First().Link;
                            orderDetail.Shop = model.ListOrderDetail.First().Shop;
                            orderDetail.Description = model.ListOrderDetail.First().Description;
                            orderDetail.Color = model.ListOrderDetail.First().Color;
                            orderDetail.Size = model.ListOrderDetail.First().Size;
                            orderDetail.Quantity = model.ListOrderDetail.First().Quantity;
                            orderDetail.Price = model.ListOrderDetail.First().Price;
                            try
                            {
                                var image = Request.Files["Image_" + model.ListOrderDetail.First().stt];
                                if (image != null && !string.IsNullOrEmpty(image.FileName))
                                {
                                    orderDetail.Image = "/Images/" + image.FileName;
                                    image.SaveAs(Server.MapPath("~/Images/" + image.FileName));
                                }
                            }
                            catch (Exception)
                            {
                                //no image
                            }

                            db.Entry(orderDetail).State = EntityState.Added;
                            db.OrderInternalDetails.Add(orderDetail);
                            db.SaveChanges();

                            dbContextTransaction.Commit();

                            return RedirectToAction("Manage", "Account");


                        }
                        else
                        {
                            order = new Order
                            {
                                CreateDate = DateTime.Now,
                                TotalPrice = model.ListOrderDetail.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0)),
                                Status = OrderStatus.New.ToString(),
                                Rate = rate != null ? rate.Price : 0,
                                Fee = 5,
                                Phone = user.Phone,
                                UserName = user.Email,
                                UpdateDate = DateTime.Now,

                            };

                            order.TotalPriceConvert = order.TotalPrice * order.Rate;
                            order.SaleManager = User.Identity.Name;

                            //get downpayment
                            order.DownPaymentRate = user.DownPaymentRate;

                            //get fee

                            double orderValueInt = Convert.ToInt32(order.TotalPriceConvert / 1000000);
                            var fee =
                                    db.Fees.Where(a => (a.From <= orderValueInt && a.To >= orderValueInt) || (a.To == 0 && a.From <= orderValueInt))
                                        .OrderByDescending(a => a.Value).First();
                            order.Fee = fee != null ? fee.Value : 5;
                            order.OrderType = OrderType.Order.ToString();
                            db.Entry(order).State = EntityState.Added;
                            db.Orders.Add(order);
                            db.SaveChanges();

                            int orderId = order.OrderId;

                            foreach (OrderDetail orderDetail in model.ListOrderDetail)
                            {
                                orderDetail.OrderId = orderId;
                                orderDetail.Phone = user.Phone;
                                orderDetail.OrderDetailStatus = OrderDetailStatus.Active.ToString();
                                try
                                {
                                    var image = Request.Files["Image_" + orderDetail.stt];
                                    if (image != null && !string.IsNullOrEmpty(image.FileName))
                                    {
                                        orderDetail.Image = "/Images/" + image.FileName;
                                        image.SaveAs(Server.MapPath("~/Images/" + image.FileName));
                                    }
                                }
                                catch (Exception)
                                {
                                    //no image
                                }

                                db.Entry(orderDetail).State = EntityState.Added;
                                db.OrderDetails.Add(orderDetail);
                                db.SaveChanges();
                            }

                            dbContextTransaction.Commit();

                            return RedirectToAction("ManageNoShop", "Account");
                        }

                    }
                    if (OrderType1 == "0")
                    {
                        var orderInternal = new OrderInternal
                        {
                            CreateDate = DateTime.Now,
                            TotalPrice = model.ListOrderDetails.Sum(a => a.TotalPrice),
                            Status = OrderStatus.New.ToString(),
                            Rate = rate != null ? rate.Price : 0,
                            DownPayment = model.ListOrderDetails.Sum(a => a.DownPayment),
                            Fee = 0,
                            Phone = user.Phone,
                            UserName = user.Email,
                            UpdateDate = DateTime.Now,

                        };

                        if (orderInternal.TotalPrice > orderInternal.DownPayment)
                        {
                            orderInternal.TotalPriceConvert = orderInternal.TotalPrice - orderInternal.DownPayment;
                            orderInternal.SaleManager = User.Identity.Name;

                            //get downpayment
                            orderInternal.DownPaymentRate = user.DownPaymentRate;

                            //get fee

                            //double orderValueInt = Convert.ToInt32(order.TotalPriceConvert / 1000000);
                            //var fee =
                            //    db.Fees.Where(a => a.From <= orderValueInt && a.To >= orderValueInt)
                            //        .OrderByDescending(a => a.Value).First();
                            orderInternal.Fee = 0;
                            orderInternal.OrderType = OrderType.AllInOne.ToString();
                            db.Entry(orderInternal).State = EntityState.Added;
                            db.OrderInternals.Add(orderInternal);
                            db.SaveChanges();

                            int orderId = orderInternal.OrderInternalId;

                            foreach (OrderInternalDetail orderDetail in model.ListOrderInternalDetails)
                            {
                                orderDetail.OrderInternalId = orderId;
                                orderDetail.Phone = user.Phone;
                                orderDetail.OrderDetailStatus = OrderDetailStatus.Active.ToString();
                                try
                                {
                                    var image = Request.Files["Image_" + orderDetail.stt];
                                    if (image != null && !string.IsNullOrEmpty(image.FileName))
                                    {
                                        orderDetail.Image = "/Images/" + image.FileName;
                                        image.SaveAs(Server.MapPath("~/Images/" + image.FileName));
                                    }
                                }
                                catch (Exception)
                                {
                                    //no image
                                }

                                db.Entry(orderDetail).State = EntityState.Added;
                                db.OrderInternalDetails.Add(orderDetail);
                                db.SaveChanges();
                            }

                            dbContextTransaction.Commit();

                            return RedirectToAction("Manage", "Account");
                        }
                        else
                        {
                            dbContextTransaction.Commit();

                            ViewData["message"] = "Tổng tiền phải lớn hơn tiền đặt cọc";
                        }

                    }
                    if (OrderType1 == "2")
                    {
                        if (model.ListOrderDetail.Count() == 1)
                        {
                            var orderInternal = new OrderInternal
                            {
                                IDOrderCompain = IDOrderCompain,
                                CreateDate = DateTime.Now,
                                TotalPrice = model.ListOrderDetail.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0)),
                                Status = OrderStatus.New.ToString(),
                                Rate = rate != null ? rate.Price : 0,
                                Fee = 0,
                                Phone = user.Phone,
                                UserName = user.Email,
                                UpdateDate = DateTime.Now,
                            };
                            orderInternal.TotalPriceConvert = orderInternal.TotalPrice * orderInternal.Rate;
                            orderInternal.SaleManager = User.Identity.Name;

                            //get downpayment
                            orderInternal.DownPaymentRate = 0;

                            //get fee

                            //  double orderValueInt = Convert.ToInt32(orderInternal.TotalPriceConvert / 1000000);
                            //var fee =
                            //    db.Fees.Where(a => a.From <= orderValueInt && a.To >= orderValueInt)
                            //        .OrderByDescending(a => a.Value).First();
                            orderInternal.Fee = 0;
                            orderInternal.OrderType = OrderType.Complaint.ToString();
                            db.Entry(orderInternal).State = EntityState.Added;
                            db.OrderInternals.Add(orderInternal);
                            db.SaveChanges();

                            int orderId = orderInternal.OrderInternalId;
                            var orderDetail = new OrderInternalDetail();
                            orderDetail.OrderId = orderId;
                            orderDetail.Phone = user.Phone;
                            orderDetail.OrderDetailStatus = OrderDetailStatus.Active.ToString();
                            orderDetail.Link = model.ListOrderDetail.First().Link;
                            orderDetail.Shop = model.ListOrderDetail.First().Shop;
                            orderDetail.Description = model.ListOrderDetail.First().Description;
                            orderDetail.Color = model.ListOrderDetail.First().Color;
                            orderDetail.Size = model.ListOrderDetail.First().Size;
                            orderDetail.Quantity = model.ListOrderDetail.First().Quantity;
                            orderDetail.Price = model.ListOrderDetail.First().Price;
                            try
                            {
                                var image = Request.Files["Image_" + model.ListOrderDetail.First().stt];
                                if (image != null && !string.IsNullOrEmpty(image.FileName))
                                {
                                    orderDetail.Image = "/Images/" + image.FileName;
                                    image.SaveAs(Server.MapPath("~/Images/" + image.FileName));
                                }
                            }
                            catch (Exception)
                            {
                                //no image
                            }
                            db.Entry(orderDetail).State = EntityState.Added;
                            db.OrderInternalDetails.Add(orderDetail);
                            db.SaveChanges();

                            dbContextTransaction.Commit();

                            return RedirectToAction("Manage", "Account");
                        }
                        else
                        {
                            order = new Order
                            {
                                IDOrderCompain = IDOrderCompain,
                                CreateDate = DateTime.Now,
                                TotalPrice = model.ListOrderDetail.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0)),
                                Status = OrderStatus.New.ToString(),
                                Rate = rate != null ? rate.Price : 0,
                                Fee = 0,
                                Phone = user.Phone,
                                UserName = user.Email,
                                UpdateDate = DateTime.Now,


                            };

                            order.TotalPriceConvert = order.TotalPrice * order.Rate;
                            order.SaleManager = User.Identity.Name;

                            //get downpayment
                            order.DownPaymentRate = 0;

                            //get fee

                            double orderValueInt = Convert.ToInt32(order.TotalPriceConvert / 1000000);
                            //var fee =
                            //    db.Fees.Where(a => a.From <= orderValueInt && a.To >= orderValueInt)
                            //        .OrderByDescending(a => a.Value).First();
                            order.Fee = 0;
                            order.OrderType = OrderType.Complaint.ToString();
                            db.Entry(order).State = EntityState.Added;
                            db.Orders.Add(order);
                            db.SaveChanges();

                            int orderId = order.OrderId;

                            foreach (OrderDetail orderDetail in model.ListOrderDetail)
                            {
                                orderDetail.OrderId = orderId;
                                orderDetail.Phone = user.Phone;
                                orderDetail.OrderDetailStatus = OrderDetailStatus.Active.ToString();
                                try
                                {
                                    var image = Request.Files["Image_" + orderDetail.stt];
                                    if (image != null && !string.IsNullOrEmpty(image.FileName))
                                    {
                                        orderDetail.Image = "/Images/" + image.FileName;
                                        image.SaveAs(Server.MapPath("~/Images/" + image.FileName));
                                    }
                                }
                                catch (Exception)
                                {
                                    //no image
                                }
                                db.Entry(orderDetail).State = EntityState.Added;
                                db.OrderDetails.Add(orderDetail);
                                db.SaveChanges();
                            }

                            dbContextTransaction.Commit();

                            return RedirectToAction("ManageNoShop", "Account");
                        }

                    }
                }
                catch (Exception)
                {
                    dbContextTransaction.Rollback();
                }
            }

            return View();
        }


        [AllowAnonymous]
        public ActionResult CreateOrderFromExtensions()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult CreateOrderFromExtensions(NewOrderModel model)
        {
            //thực hiện tạo order dựa tên orderdetail
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Keys.SelectMany(k => ModelState[k].Errors)
                    .Select(m => m.ErrorMessage).Distinct().ToArray();
                string message = errors.Aggregate(string.Empty, (current, error) => current + error + "<br />");

                return Json(new { success = false, message = message });

            }
            if (Request.IsAuthenticated)
            {
                using (DbContextTransaction dbContextTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        Rate rate = db.Rates.FirstOrDefault();
                        UserProfile user = db.UserProfiles.First(a => a.Phone == User.Identity.Name);
                        var order = new Order
                        {
                            CreateDate = DateTime.Now,
                            UpdateDate = DateTime.Now,
                            TotalPrice = model.ListOrderDetail.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0)),
                            Status = OrderStatus.New.ToString(),
                            Rate = rate != null ? rate.Price : 0,
                            Fee = 5,
                            Phone = user.Phone,
                            UserName = user.Email
                        };
                        SaleManageClient saleManage =
                            db.SaleManageClients.FirstOrDefault(a => a.User_Client == user.Phone);
                        if (saleManage != null) order.SaleManager = saleManage.User_Sale;

                        order.TotalPriceConvert = order.TotalPrice * order.Rate;

                        //get fee
                        int orderValueInt = Convert.ToInt32(order.TotalPriceConvert / 1000000);
                        var fee =
                            db.Fees.Where(a => a.From <= orderValueInt && a.To >= orderValueInt)
                                .OrderByDescending(a => a.Value).First();
                        order.Fee = fee != null ? fee.Value : 5;

                        db.Entry(order).State = EntityState.Added;
                        db.Orders.Add(order);
                        db.SaveChanges();

                        int orderId = order.OrderId;

                        foreach (OrderDetail orderDetail in model.ListOrderDetail)
                        {
                            orderDetail.OrderId = orderId;
                            orderDetail.Phone = user.Phone;
                            orderDetail.OrderDetailStatus = OrderDetailStatus.Active.ToString();
                            db.Entry(orderDetail).State = EntityState.Added;
                            db.OrderDetails.Add(orderDetail);
                            db.SaveChanges();
                        }

                        dbContextTransaction.Commit();

                        return Json(new { success = true, url = Url.Action("Manage", "Account") });

                    }
                    catch (Exception)
                    {
                        dbContextTransaction.Rollback();
                    }
                }
            }
            else
            {
                using (DbContextTransaction dbContextTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        Rate rate = db.Rates.FirstOrDefault();
                        var order = new Order
                        {
                            CreateDate = DateTime.Now,
                            TotalPrice = model.ListOrderDetail.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0)),
                            Status = OrderStatus.New.ToString(),
                            Rate = rate != null ? rate.Price : 0,
                            Fee = 5,
                            Phone = model.ListOrderDetail.First().Phone,
                            UpdateDate = DateTime.Now
                        };
                        db.Entry(order).State = EntityState.Added;

                        order.TotalPriceConvert = order.TotalPrice * order.Rate;

                        //get fee
                        int orderValueInt = Convert.ToInt32(order.TotalPriceConvert / 1000000);
                        var fee =
                            db.Fees.Where(a => a.From <= orderValueInt && a.To >= orderValueInt)
                                .OrderByDescending(a => a.Value).First();
                        order.Fee = fee != null ? fee.Value : 5;

                        db.Orders.Add(order);
                        db.SaveChanges();

                        int orderId = order.OrderId;

                        foreach (OrderDetail orderDetail in model.ListOrderDetail)
                        {
                            orderDetail.OrderId = orderId;
                            orderDetail.Phone = order.Phone;
                            orderDetail.OrderDetailStatus = OrderDetailStatus.Active.ToString();

                            db.Entry(orderDetail).State = EntityState.Added;
                            db.OrderDetails.Add(orderDetail);
                            db.SaveChanges();
                        }

                        dbContextTransaction.Commit();

                        return Json(new { success = true });

                    }
                    catch (Exception)
                    {
                        dbContextTransaction.Rollback();
                    }
                }
            }
            return Json(new { success = false, message = "Tạo đơn hàng không thành công" });
        }

        public ActionResult ViewOrderDetail(int id)
        {
            var model = new ViewDetailOrderModel();
            var order = db.OrderInternals.FirstOrDefault(a => a.OrderInternalId == id);
            var orderDetail = db.OrderInternalDetails.Where(a => a.OrderInternalId == id);
            model.OrderInternalId = id;
            if (order != null)
            {

                model.Phone = order.Phone;
                model.Rate = order.Rate;
                model.Status = order.Status;
                model.StatusText = order.getStatusText();
                model.TotalPriceConvert = order.TotalPriceConvert;
                model.TotalPrice = order.TotalPrice;
                model.SaleManager = order.SaleManager;
                model.UserName = order.UserName;
                model.Phone = order.Phone;
                model.FeeShip = order.FeeShip;
                model.FeeShipChina = order.FeeShipChina;
                model.CreateDate = order.CreateDate;
                model.Fee = order.Fee;
                model.Weight = order.Weight;
                model.DownPayment = order.DownPayment;
                model.AccountingCollected = order.AccountingCollected;


                if (!string.IsNullOrEmpty(order.SaleManager))
                {
                    UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Phone == order.SaleManager);
                    if (user != null)
                    {
                        model.SaleManageInfo = new SaleManageInfo
                        {
                            SaleName = user.Name,
                            SalePhone = user.Phone,
                            SalePhoneCompany = user.PhoneCompany
                        };
                    }
                }

                if (!string.IsNullOrEmpty(order.Phone))
                {
                    UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Phone == order.Phone);
                    if (user != null)
                    {
                        model.Client = user;
                    }
                }
            }
            model.ListOrderDetails = orderDetail;
            return View(model);
        }

        public ActionResult ViewOrderDetailForSale(int id)
        {

            var model = new ViewDetailOrderNonShopModel();
            Order order = db.Orders.FirstOrDefault(a => a.OrderId == id);
            var orderDetail = db.OrderDetails.Where(a => a.OrderId == id).ToList();
            model.OrderId = id;
            UserProfile users = db.UserProfiles.FirstOrDefault(a => a.Phone == order.Phone);
            if (users != null)
            {
                ViewBag.Name = users.Name;
                ViewBag.Email = users.Email;
                ViewBag.Phone = users.Phone;
                ViewBag.Address = users.Address;
            }
            ViewBag.Update = order.UpdateDate;
            var listShop = orderDetail.Where(a => !string.IsNullOrEmpty(a.Shop)).Select(a => a.Shop).Distinct();
            foreach (var shopText in listShop)
            {
                if (order != null)
                {
                    //check limit 50 detail
                    var listOrderDetails = orderDetail.Where(a => a.Shop == shopText).ToList();
                    if (listOrderDetails.Count > 50)
                    {
                        //thuc hien tach shop
                        var loop = (listOrderDetails.Count / 50) + 1;
                        var endIndex = 0;
                        var max = 50;
                        if (listOrderDetails.Count % 50 != 0)
                        {
                            loop = loop + 1;
                        }

                        for (int i = 0; i < loop; i++)
                        {
                            var startIndex = endIndex;
                            endIndex = startIndex + (max - 1);

                            var shop = new ListShopModel
                            {
                                Phone = order.Phone,
                                Name = shopText,
                                Fee = order.Fee,
                                Weight = 0,
                                Rate = order.Rate,
                            };
                            shop.ListOrderDetails = listOrderDetails.GetRange(startIndex, endIndex > listOrderDetails.Count ? listOrderDetails.Count : endIndex);
                            shop.TotalPrice = shop.ListOrderDetails.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0));
                            shop.TotalPriceConvert = shop.TotalPrice * shop.Rate;
                            model.ListShop.Add(shop);
                        }

                    }
                    else
                    {
                        var shop = new ListShopModel
                        {
                            Phone = order.Phone,
                            Name = shopText,
                            Fee = order.Fee,
                            Weight = 0,
                            Rate = order.Rate,

                        };

                        //
                        shop.ListOrderDetails = listOrderDetails;
                        shop.TotalPrice = shop.ListOrderDetails.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0));
                        shop.TotalPriceConvert = shop.TotalPrice * shop.Rate;

                        model.ListShop.Add(shop);
                    }

                }
            }
            if (order != null)
            {
                model.Rate = order.Rate;
                model.Status = order.Status;
                model.StatusText = order.getStatusText();
                model.Fee = order.Fee;
                model.TotalPriceConvert = order.TotalPriceConvert;
                model.TotalPrice = order.TotalPrice;
                model.IsShop = order.IsShop;
                model.Phone = order.Phone;
                if (!string.IsNullOrEmpty(order.Phone))
                {
                    UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Phone == order.Phone);
                    if (user != null)
                    {
                        model.Client = user;
                    }
                }
            }
            model.ListOrderDetails = orderDetail.Where(a => string.IsNullOrEmpty(a.Shop)).ToList();
            return View(model);
        }


        [Authorize]
        public ActionResult ViewOrderNoShopDetail(int id)
        {
            string phone = Session["Phone"].ToString();
            UserProfile users = db.UserProfiles.FirstOrDefault(a => a.Phone == phone);
            ViewBag.Name = users.Name;
            ViewBag.Email = users.Email;
            ViewBag.Address = users.Address;
            Order donhang = db.Orders.FirstOrDefault(b => b.OrderId == id);
            ViewBag.UpdateDate = donhang.UpdateDate;
            var model = new ViewDetailOrderNonShopModel();
            var order = db.Orders.FirstOrDefault(a => a.OrderId == id);
            var orderDetail = db.OrderDetails.Where(a => a.OrderId == id).ToList();
            model.OrderId = id;

            if (order != null)
            {

                model.Phone = order.Phone;
                model.Rate = order.Rate;
                model.Status = order.Status;
                model.StatusText = order.getStatusText();
                model.TotalPriceConvert = order.TotalPriceConvert;
                model.TotalPrice = order.TotalPrice;
                model.FeeShip = order.FeeShip;
                model.FeeShipChina = order.FeeShipChina;
                model.Fee = order.Fee;
                model.DownPayment = order.DownPayment;
                model.AccountingCollected = order.AccountingCollected;


                if (!string.IsNullOrEmpty(order.SaleManager))
                {
                    UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Phone == order.SaleManager);
                    if (user != null)
                    {
                        model.SaleManageInfo = new SaleManageInfo
                        {
                            SaleName = user.Name,
                            SalePhone = user.Phone,
                            SalePhoneCompany = user.PhoneCompany
                        };
                    }
                }

                if (!string.IsNullOrEmpty(order.Phone))
                {
                    UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Phone == order.Phone);
                    if (user != null)
                    {
                        model.Client = user;
                    }
                }
            }
            model.ListOrderDetails = orderDetail;
            return View(model);
        }

        public ActionResult SaleConfirmOrder(int id)
        {
            try
            {
                var model = db.OrderInternals.FirstOrDefault(a => a.OrderInternalId == id);
                if (model != null)
                {
                    ViewBag.ListOrderer = GetListOrderer();

                    return PartialView("_SaleConfirmOrderPartial", model);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.ToString());
            }
            return RedirectToAction("Error", "Home");
        }

        [HttpPost]
        [AllowAnonymous]

        public ActionResult SaleConfirmOrder(ViewDetailOrderModelNew model, string Reason)
        {

            var order = db.OrderInternals.FirstOrDefault(m => m.OrderInternalId == model.OrderInternalId);
            if (order != null)
            {
                if (order.OrderType == OrderType.Complaint.ToString())
                {
                    order.Status = OrderStatus.Paid.ToString();
                    order.OrdererManager = model.OrdererManager;
                    order.IDOrderCompain = model.IDOrderCompain;
                    db.SaveChanges();
                    return Json(
                                new
                                {
                                    success = true
                                });
                }
                //check thay doi fee and downpaymentRate
                if (order.DownPaymentRate != model.DownPaymentRate || order.Fee != model.Fee || !string.IsNullOrEmpty(model.OrdererManager))
                {
                    var userOrderer = db.UserProfiles.FirstOrDefault(a => a.Phone == model.OrdererManager);
                    //switch to approval
                    var objectApproval = new ApprovalSaleChangeFee
                    {
                        OrderInternalId = order.OrderInternalId,
                        DownPaymentRate_New = model.DownPaymentRate,
                        DownPaymentRate_Old = order.DownPaymentRate,
                        OrdererManager = userOrderer != null ? userOrderer.Phone : "",
                        OrdererManagerName = userOrderer != null ? userOrderer.Name : "",
                        Fee_New = model.Fee,
                        Fee_Old = order.Fee,
                        Reason = Reason
                    };
                    var approval = new Approval
                    {
                        OrderInternalId = order.OrderInternalId,
                        ApprovalType = ApprovalType.SaleChangeFee.ToString(),
                        ApprovalValue = JsonConvert.SerializeObject(objectApproval),
                        CreateBy = User.Identity.Name,
                        CreateDate = DateTime.Now
                    };

                    //chuyen order
                    order.Status = OrderStatus.WaitingApprovalSaleConfirm.ToString();
                    order.UpdateDate = DateTime.Now;
                    order.Fee = model.Fee;
                    order.FeeShip = model.FeeShip;
                    order.FeeShipChina = model.FeeShipChina;
                    order.DownPaymentRate = model.DownPaymentRate;
                    order.UpdateDate = DateTime.Now;
                    order.Status = OrderStatus.SaleConfirm.ToString();
                    db.Approvals.Add(approval);
                    db.SaveChanges();

                    return Json(new { success = true, isApproval = true });

                }
                order.Fee = model.Fee;
                order.FeeShip = model.FeeShip;
                order.FeeShipChina = model.FeeShipChina;
                order.DownPaymentRate = model.DownPaymentRate;
                order.UpdateDate = DateTime.Now;
                order.Status = OrderStatus.SaleConfirm.ToString();
                //Kiểm tra thông tin tiền trong ví và thực hiện thu tiền
                //get wallet for client
                var walletClient = db.Wallets.FirstOrDefault(a => a.Client == order.Phone && a.Currency == Utils.VND);
                if (walletClient != null)
                {
                    var money = walletClient.Money;
                    //total price for order
                    //(Model.TotalPriceConvert + Math.Round((Model.TotalPriceConvert * Model.Fee) / 100, MidpointRounding.ToEven) + Model.FeeShipChina + Model.FeeShip).ToString("##,###")
                    var totalMoney = order.TotalPriceConvert +
                                     Math.Round((order.TotalPriceConvert * order.Fee) / 100, MidpointRounding.ToEven) +
                                     order.FeeShipChina + order.FeeShip;

                    //tính toán số tiền đặt cọc
                    //if client collect full money, set downpayment rate = 100
                    var moneyHold = (totalMoney * order.DownPaymentRate) / 100;
                    if (moneyHold <= money)
                    {
                        //collect money and switch status of Order
                        walletClient.Money = walletClient.Money - moneyHold;
                        order.DownPayment = moneyHold;

                        var wallerHistory = new WalletHistory
                        {
                            WalletId = walletClient.Id,
                            Money = moneyHold,
                            UpdateType = WalletUpdateType.Subtract.ToString(),
                            Client = walletClient.Client,
                            Currency = walletClient.Currency,
                            User_Update = User.Identity.Name,
                            LastUpdate = DateTime.Now,
                            Reason =
                                string.Format(
                                    "Thu {5}% của tổng đơn hàng {6}. Chi tiết: /n Tiền hàng: {0}; Phí dịch vụ({1} %): {2}; Ship nội địa TQ : {3}; Cước vận chuyển: {4}",
                                    order.TotalPriceConvert.ToString("##,###"), order.Fee,
                                    Math.Round((order.TotalPriceConvert * order.Fee) / 100, MidpointRounding.ToEven)
                                        .ToString("##,###"), order.FeeShipChina.ToString("##,###"),
                                    order.FeeShip.ToString("##,###"), order.DownPaymentRate,
                                    totalMoney.ToString("##,###"))
                        };
                        db.WalletHistorys.Add(wallerHistory);
                        order.OrdererManager = model.OrdererManager;
                        //switch order
                        order.Status = OrderStatus.Paid.ToString();
                    }
                    else
                    {
                        //check money in rank
                        moneyHold = totalMoney * (order.DownPaymentRate - 5) / 100;

                        if (moneyHold <= money)
                        {
                            //subtruct full money in wallet
                            //collect money and switch status of Order
                            walletClient.Money = walletClient.Money - walletClient.Money;
                            order.DownPayment = walletClient.Money;

                            var wallerHistory = new WalletHistory
                            {
                                WalletId = walletClient.Id,
                                Money = walletClient.Money,
                                UpdateType = WalletUpdateType.Subtract.ToString(),
                                Client = walletClient.Client,
                                Currency = walletClient.Currency,
                                User_Update = User.Identity.Name,
                                LastUpdate = DateTime.Now,
                                Reason =
                                    string.Format(
                                        "Thu {5}% của tổng đơn hàng {6}. Chi tiết: /n Tiền hàng: {0}; Phí dịch vụ({1} %): {2}; Ship nội địa TQ : {3}; Cước vận chuyển: {4}",
                                        order.TotalPriceConvert.ToString("##,###"), order.Fee,
                                        Math.Round((order.TotalPriceConvert * order.Fee) / 100, MidpointRounding.ToEven)
                                            .ToString("##,###"), order.FeeShipChina.ToString("##,###"),
                                        order.FeeShip.ToString("##,###"), order.DownPaymentRate,
                                        totalMoney.ToString("##,###"))
                            };
                            db.WalletHistorys.Add(wallerHistory);
                            order.OrdererManager = model.OrdererManager;
                            order.Status = OrderStatus.Paid.ToString();

                        }
                    }

                }

                db.SaveChanges();
            }

            return Json(
                new
                {
                    success = true
                });
        }


        [HttpPost]
        [AllowAnonymous]
        public ActionResult OrdererRejectOrder(string orderid)
        {
            Order model = db.Orders.FirstOrDefault(a => a.OrderId.ToString() == orderid);
            if (model != null)
            {
                model.Status = OrderStatus.OrdererReject.ToString();
                db.SaveChanges();

                return Json(new { success = true });
            }
            return Json(new { success = false });
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult OrdererConfirmOrder(string orderid)
        {
            Order model = db.Orders.FirstOrDefault(a => a.OrderId.ToString() == orderid);
            if (model != null)
            {
                model.Status = OrderStatus.Order.ToString();
                db.SaveChanges();

                return Json(new { success = true });
            }
            return Json(new { success = false });
        }
        private void InsertWalletHistory(Wallet walletClient, double money, string reason, string note = "")
        {
            try
            {
                if (walletClient == null)
                    return;
                var wallerHistory = new WalletHistory
                {
                    WalletId = walletClient.Id,
                    Money = money,
                    UpdateType = money >= 0 ? WalletUpdateType.Addition.ToString() : WalletUpdateType.Subtract.ToString(),
                    Client = walletClient.Client,
                    Currency = walletClient.Currency,
                    User_Update = User.Identity.Name,
                    LastUpdate = DateTime.Now,
                    Reason = reason,
                    Note = note,
                };
                db.WalletHistorys.Add(wallerHistory);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult ConfirmReceive(string orderid)
        {
            var model = db.OrderInternals.FirstOrDefault(a => a.OrderInternalId.ToString() == orderid);
            if (model != null)
            {
                if (model.OrderType == OrderType.Complaint.ToString())
                {
                    model.Status = OrderStatus.FullCollect.ToString();
                    db.SaveChanges();
                    //gửi mail
                    SendMail(model.UserName, model.OrderId);

                    // thuc hien lay thong tin tokenId va push notification
                    var user = db.UserProfiles.FirstOrDefault(c => c.Email == model.UserName);
                    if (user != null && !string.IsNullOrWhiteSpace(user.TokenId))
                    {
                        var listToken = new List<string> { user.TokenId };
                        var objectNotification = new NotificationObject
                        {
                            Message = "Xin trân trọng kính báo đơn hàng: " + orderid + " đã về đến kho của chúng tôi",
                            Title = "Thay đổi trạng thái đơn hàng"
                        };
                        PushNotification.Push(listToken, objectNotification);

                    }
                    return Json(new { success = true });
                }
                model.Status = OrderStatus.Receive.ToString();

                // xu ly tru sau khi confirm hang da ve kho
                var walletClient = db.Wallets.FirstOrDefault(a => a.Client == model.Phone && a.Currency == Utils.VND);
                if (walletClient != null)
                {
                    var money = walletClient.Money;
                    //total price for order
                    //(Model.TotalPriceConvert + Math.Round((Model.TotalPriceConvert * Model.Fee) / 100, MidpointRounding.ToEven) + Model.FeeShipChina + Model.FeeShip).ToString("##,###")
                    var totalMoney = model.TotalPriceConvert +
                                     Math.Round((model.TotalPriceConvert * model.Fee) / 100, MidpointRounding.ToEven) +
                                     model.FeeShipChina + model.FeeShip;

                    var meneySub = totalMoney - model.DownPayment;
                    //truong hop tien trong tai khoan lon hon hoac bang so tien con lai thi tu dong tru tien trong tai khoan
                    if (money >= meneySub)
                    {
                        walletClient.Money -= meneySub;
                        var reason = string.Format("Thanh toán số tiền: {0} còn thiếu của đơn hàng có mã: {1}", meneySub, model.OrderId);
                        InsertWalletHistory(walletClient, meneySub * -1, reason);
                    }
                }

                db.SaveChanges();

                //gửi mail
                SendMail(model.UserName, model.OrderId);

                // thuc hien lay thong tin tokenId va push notification
                var userProfile = db.UserProfiles.FirstOrDefault(c => c.Email == model.UserName);
                if (userProfile != null && !string.IsNullOrWhiteSpace(userProfile.TokenId))
                {
                    var listToken = new List<string> { userProfile.TokenId };
                    var objectNotification = new NotificationObject
                    {
                        Message = "Xin trân trọng kính báo đơn hàng: " + orderid + " đã về đến kho của chúng tôi",
                        Title = "Thay đổi trạng thái đơn hàng"
                    };
                    PushNotification.Push(listToken, objectNotification);

                }
                return Json(new { success = true });
            }
            return Json(new { success = false });
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult FinishOrder(string orderid)
        {
            OrderInternal model = db.OrderInternals.FirstOrDefault(a => a.OrderInternalId.ToString() == orderid);
            if (model != null)
            {
                model.Status = OrderStatus.Finish.ToString();
                db.SaveChanges();

                return Json(new { success = true });
            }
            return Json(new { success = false });
        }

        public ActionResult UpdateOrder(int id)
        {
            ViewBag.BankOrder = "";
            List<SelectListItem> BankOrder = new List<SelectListItem>();
            foreach (var b in db.Wallets.Where(a => a.Client == User.Identity.Name))
            {
                BankOrder.Add(new SelectListItem() { Value = b.Id.ToString(), Text = b.Name.ToString() });
            }
            ViewBag.BankOrder = BankOrder;
            OrderInternal model = db.OrderInternals.FirstOrDefault(a => a.OrderInternalId == id);
            return PartialView("_UpdateOrderPartial", model);
        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateOrder(OrderInternal model, FormCollection collection)
        {
            OrderInternal order = db.OrderInternals.FirstOrDefault(m => m.OrderInternalId == model.OrderInternalId);
            if (order != null)
            {
                if (order.OrderType == OrderType.Complaint.ToString())
                {
                    var bankorder = collection["BankOrder"];
                    Wallet bank = db.Wallets.FirstOrDefault(a => a.Id.ToString() == bankorder);
                    if (model.AccountingCollected == 0)
                    {
                        order.TransId = model.TransId;
                        order.AccountingCollected = model.AccountingCollected;
                        order.UpdateDate = DateTime.Now;
                        order.Content = model.Content;
                        order.Status = OrderStatus.Order.ToString();
                        db.SaveChanges();
                        return RedirectToAction("ViewOrderComplaintDetail", new { id = model.OrderInternalId });
                    }
                    if (model.AccountingCollected > 0 && bank == null)
                    {
                        return RedirectToAction("ViewOrderComplaintDetail", new { id = model.OrderInternalId });
                    }
                    if (model.AccountingCollected > 0 && bank != null)
                    {
                        if (model.AccountingCollected > bank.Money)
                        {
                            return RedirectToAction("ViewOrderComplaintDetail", new { id = model.OrderInternalId });
                        }
                        var wallerHistory = new WalletHistory
                        {
                            WalletId = bank.Id,
                            Money = bank.Money,
                            UpdateType = WalletUpdateType.Subtract.ToString(),
                            Client = bank.Client,
                            Currency = bank.Currency,
                            User_Update = User.Identity.Name,
                            LastUpdate = DateTime.Now,
                            Reason = "Trừ: " + model.AccountingCollected.ToString("##,###") + " Tệ vào ví , trả tiền mua thêm sản phẩm của đơn khiếu nại"
                        };
                        db.WalletHistorys.Add(wallerHistory);

                        bank.Money -= model.AccountingCollected;
                        order.TransId = model.TransId;
                        order.AccountingCollected = model.AccountingCollected;
                        order.UpdateDate = DateTime.Now;
                        order.Content = model.Content;
                        order.Status = OrderStatus.Order.ToString();
                        db.SaveChanges();
                        return RedirectToAction("ViewOrderComplaintDetail", new { id = model.OrderInternalId });
                    }
                }
                else
                {
                    order.FeeShip = model.FeeShip;
                    order.Weight = model.Weight;
                    order.FeeShipChina = model.FeeShipChina;
                    order.UpdateDate = DateTime.Now;

                    order.Status = OrderStatus.Order.ToString();
                    db.SaveChanges();
                    return RedirectToAction("ViewOrderDetail", new { id = model.OrderInternalId });
                }

            }

            return RedirectToAction("ViewOrderDetail", new { id = model.OrderInternalId });
        }

        public ActionResult AccountingConfirmOrder(int id)
        {
            var model = db.OrderInternals.FirstOrDefault(a => a.OrderInternalId == id);
            ViewBag.ListAccount = GetListBankName(null, "VND");
            return PartialView("_AccountingConfirmOrderPartial", model);

        }


        [HttpPost]
        [AllowAnonymous]
        public ActionResult AccountingConfirmOrder(OrderInternal model, int id)
        {
            var isOk = true;
            var errorText = "Cập nhật tiền cọc thành công";
            var order = db.OrderInternals.FirstOrDefault(m => m.OrderInternalId == model.OrderInternalId);
            var order1 = db.OrderInternals.FirstOrDefault(m => m.OrderInternalId == id && m.OrderType == OrderType.Complaint.ToString());

            if (order != null)
            {
                if (model.Status == OrderStatus.SaleConfirm.ToString())
                {

                    if (model.DownPaymentRate < 0)
                    {
                        isOk = false;
                        errorText = "Số tiền Đặt cọc phải> 0";
                        return Json(new { success = isOk, url = Url.Action("ViewOrderDetailNew", "Account", new { id = order.OrderInternalId }), ErrorText = errorText });
                    }

                    var totalMoney = order.TotalPriceConvert +
                                        Math.Round((order.TotalPriceConvert * order.Fee) / 100, MidpointRounding.ToEven) +
                                        order.FeeShipChina + order.FeeShip;


                    if (model.DownPaymentRate > totalMoney)
                        return Json(new { success = false, url = Url.Action("ViewOrderDetailNew", "Account", new { id = order.OrderInternalId }), ErrorText = "Số tiền đặt cọc không được lớn hơn số tiền đơn hàng" });
                    #region Check Account Info


                    var paymentPercent = model.DownPaymentRate * 100 / totalMoney;
                    var walletClient = db.Wallets.FirstOrDefault(a => a.Client == order.Phone && a.Currency == Utils.VND);
                    //var walletSystem = GetWallet(null, model.Content);

                    if (walletClient != null)
                    {
                        var money = walletClient.Money;

                        //tính toán số tiền đặt cọc
                        //if client collect full money, set downpayment rate = 100
                        var moneyHold = model.DownPaymentRate;
                        if (moneyHold <= money)
                        {
                            //collect money and switch status of Order
                            walletClient.Money = walletClient.Money - moneyHold;
                            order.DownPayment = moneyHold;
                            order.Reason = model.Reason;
                            order.DownPaymentRate = paymentPercent;
                            //walletSystem.Money += moneyHold;
                            InsertWalletHistory(walletClient, -1 * moneyHold, string.Format(
                                        "Thanh toán {5} của tổng đơn hàng {6}. Chi tiết: /n Tiền hàng: {0}; Phí dịch vụ({1} %): {2}; Ship nội địa TQ : {3}; Cước vận chuyển: {4}",
                                        order.TotalPriceConvert.ToString("##,###"), order.Fee,
                                        Math.Round((order.TotalPriceConvert * order.Fee) / 100, MidpointRounding.ToEven)
                                            .ToString("##,###"), order.FeeShipChina.ToString("##,###"),
                                        order.FeeShip.ToString("##,###"), moneyHold.ToString("N0"),
                                        totalMoney.ToString("##,###")));

                            //switch order
                            order.Status = OrderStatus.Paid.ToString();
                        }
                        else
                        {
                            //check money in rank
                            moneyHold = totalMoney * (order.DownPaymentRate - 5) / 100;

                            if (moneyHold <= money)
                            {
                                //subtruct full money in wallet
                                //collect money and switch status of Order
                                walletClient.Money = walletClient.Money - moneyHold;
                                order.DownPayment = moneyHold;
                                //walletSystem.Money += moneyHold;
                                order.DownPaymentRate = paymentPercent;
                                order.Reason = model.Reason;
                                InsertWalletHistory(walletClient, -1 * moneyHold, string.Format(
                                            "Thanh toán {5} của tổng đơn hàng {6}. Chi tiết: /n Tiền hàng: {0}; Phí dịch vụ({1} %): {2}; Ship nội địa TQ : {3}; Cước vận chuyển: {4}",
                                            order.TotalPriceConvert.ToString("##,###"), order.Fee,
                                            Math.Round((order.TotalPriceConvert * order.Fee) / 100, MidpointRounding.ToEven)
                                                .ToString("##,###"), order.FeeShipChina.ToString("##,###"),
                                            order.FeeShip.ToString("##,###"), moneyHold.ToString("N0"),
                                            totalMoney.ToString("##,###")));

                                order.Status = OrderStatus.Paid.ToString();
                            }
                            else
                            {
                                isOk = false;
                                errorText = "Tài khoản của khách hàng không đủ tiền";
                            }
                        }
                    }
                    else
                    {
                        isOk = false;
                        errorText = "Không có Tài khoản của khách hàng";
                    }
                    #endregion
                }
                else if (model.Status == OrderStatus.Receive.ToString())
                {
                    errorText = "Cập nhật tiền thu thêm thành công";

                    var walletClient = db.Wallets.FirstOrDefault(a => a.Client == order.Phone && a.Currency == Utils.VND);
                    if (walletClient == null)
                        return Json(new { success = false, url = Url.Action("ViewOrderDetailNew", "Account", new { id = order.OrderInternalId }), ErrorText = "Không có Tài khoản của khách hàng" });

                    walletClient.Money -= model.AccountingCollected;
                    InsertWalletHistory(walletClient, -1 * model.AccountingCollected, string.Format("Thanh toán tiền thu thêm cho đơn hàng có mã: {0} số tiền: {1}", order.OrderInternalId, model.AccountingCollected.ToString("N0")));
                    order.AccountingCollected = model.AccountingCollected;
                    order.Status = OrderStatus.FullCollect.ToString();
                }
                else if (model.Status == OrderStatus.WaitOrder.ToString())
                {
                    order.Status = OrderStatus.SaleConfirm.ToString();
                }
                else if (model.Status != OrderStatus.New.ToString()
                    && model.Status != OrderStatus.Receive.ToString()
                     && model.Status != OrderStatus.SaleConfirm.ToString())
                {

                    if (model.Owe < 0)
                    {
                        isOk = false;
                        errorText = "Số tiền nợ không được phép nhỏ hơn 0";
                        return Json(new { success = isOk, url = Url.Action("ViewOrderDetailNew", "Account", new { id = order.OrderInternalId }), ErrorText = errorText });
                    }
                    var totalMoney = order.TotalPriceConvert +
                                        Math.Round((order.TotalPriceConvert * order.Fee) / 100, MidpointRounding.ToEven) +
                                        order.FeeShipChina + order.FeeShip;
                    if (model.Owe > totalMoney)
                    {
                        isOk = false;
                        errorText = "Số tiền nợ không được phép lớn hơn tổng tiền đơn hàng";
                        return Json(new { success = isOk, url = Url.Action("ViewOrderDetailNew", "Account", new { id = order.OrderInternalId }), ErrorText = errorText });
                    }

                    // trong truong hop cho nhan hang va nhan hang xong ke toan van co the set no
                    // neu so no moi set nho hon so no cu tuc la tra no => -tkkh +tkkt
                    // neu so no moi lon hon so no cu tuc la set them no => +tkkh -tkkt
                    var oweOld = order.Owe;
                    order.Owe = model.Owe;
                    var diffValue = model.Owe - oweOld;
                    var walletClient = db.Wallets.FirstOrDefault(a => a.Client == order.Phone && a.Currency == Utils.VND);
                    //var bankName = model.Content;
                    //var walletSystem = GetWallet(null, bankName);

                    if (walletClient != null)
                    {
                        walletClient.Money += diffValue;
                        walletClient.Owe += diffValue;
                        //walletSystem.Money -= diffValue;

                        // sinh thong tin giao dich
                        //InsertWalletHistory(walletSystem, -1 * diffValue, string.Format("Sét nợ đơn hàng: {0} số tiền: {1}", order.OrderId, diffValue.ToString("N2")));
                        InsertWalletHistory(walletClient, diffValue, string.Format("Sét nợ đơn hàng: {0} số tiền: {1}", order.OrderId, diffValue.ToString("N2")));
                        errorText = "Sét nợ đơn hàng thành công";
                    }
                    else
                    {
                        isOk = false;
                        errorText = "Không có Tài khoản của khách hàng";
                    }
                }
                order.UpdateDate = DateTime.Now;
                db.SaveChanges();
            }
            if (order1 != null)
            {
                if (order1.OrderType == OrderType.Complaint.ToString())
                {
                    if (order1.Status == OrderStatus.SaleConfirm.ToString())
                    {
                        order1.Status = OrderStatus.Paid.ToString();
                    }
                    else if (order1.Status == OrderStatus.Receive.ToString())
                    {
                        order1.AccountingCollected = model.AccountingCollected;
                        order1.Status = OrderStatus.FullCollect.ToString();
                    }
                    else if (order1.Status == OrderStatus.WaitOrder.ToString())
                    {
                        order1.Status = OrderStatus.SaleConfirm.ToString();
                    }
                    order1.UpdateDate = DateTime.Now;
                    db.SaveChanges();

                }
                return Json(new { success = isOk, url = Url.Action("ViewOrderComplaintDetail", "Account", new { id = order1.OrderInternalId }), ErrorText = errorText });
            }
            return Json(new { success = isOk, url = Url.Action("ViewOrderDetailNew", "Account", new { id = model.OrderInternalId }), ErrorText = errorText });

        }


        public ActionResult AddEditOrderDetail(int? id, int? orderId)
        {
            if (id != null)
            {
                ViewBag.IsUpdate = true;
                OrderDetail model = db.OrderDetails.FirstOrDefault(m => m.OrderDetailId == id);
                return PartialView("_AddOrderDetailPartial", model);
            }
            ViewBag.IsUpdate = false;
            var modelEmpty = new OrderDetail { OrderId = orderId ?? 0 };
            return PartialView("_AddOrderDetailPartial", modelEmpty);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddEditOrderDetail(OrderDetail model, string cmd, int id)
        {
            using (DbContextTransaction dbContextTransaction = db.Database.BeginTransaction())
            {
                if (cmd == "Save")
                {
                    try
                    {

                        var user = db.Orders.FirstOrDefault(a => a.OrderId == id);

                        if (user != null) model.Phone = user.Phone;
                        model.OrderDetailStatus = OrderDetailStatus.Active.ToString();
                        db.OrderDetails.Add(model);
                        db.SaveChanges();

                        //tính toán lại order
                        Order ord = db.Orders.FirstOrDefault(a => a.OrderId == model.OrderId);
                        double totalPrice = 0;
                        var listOrderDetail = db.OrderDetails.Where(
                                    a =>
                                        a.OrderId == ord.OrderId &&
                                        a.OrderDetailStatus == OrderDetailStatus.Active.ToString()).ToList();
                        if (listOrderDetail.Count > 0)
                        {
                            totalPrice = listOrderDetail.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0));
                        }
                        Order order = db.Orders.FirstOrDefault(a => a.OrderId == ord.OrderId);

                        if (order != null)
                        {
                            order.TotalPrice = totalPrice;
                            order.TotalPriceConvert = order.Rate * order.TotalPrice;
                            db.SaveChanges();
                        }
                        dbContextTransaction.Commit();

                        ViewData["message"] = "Thêm mới link hàng thành công";

                        if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Sale, false))
                        {
                            return RedirectToAction("ViewOrderDetailForSale", new { id = model.OrderId });
                        }
                        else
                        {
                            if (ord.IsShop == false)
                            {
                                return RedirectToAction("ViewOrderNoShopDetail",
                                new { id = model.OrderId, message = "Cập nhật link hàng thành công" });
                            }
                            else
                            {
                                return RedirectToAction("ViewOrderDetail",
                                new { id = model.OrderId, message = "Cập nhật link hàng thành công" });
                            }
                        }
                    }
                    catch
                    {
                        ViewData["message"] = "Thêm mới link hàng thất bại";
                        dbContextTransaction.Rollback();
                    }
                }
                else
                {
                    try
                    {
                        OrderDetail orderDetail =
                            db.OrderDetails.FirstOrDefault(m => m.OrderDetailId == model.OrderDetailId);
                        if (orderDetail != null)
                        {
                            try
                            {
                                var image = Request.Files["Image_0"];
                                if (image != null && !string.IsNullOrEmpty(image.FileName))
                                {
                                    orderDetail.Image = "/Images/" + image.FileName;
                                    image.SaveAs(Server.MapPath("~/Images/" + image.FileName));
                                }
                                else
                                {
                                    orderDetail.Image = model.Image;
                                }

                            }
                            catch (Exception)
                            {
                                //no image
                            }

                            orderDetail.Link = model.Link;
                            orderDetail.Shop = model.Shop;
                            orderDetail.Color = model.Color;
                            orderDetail.Size = model.Size;
                            orderDetail.Description = model.Description;
                            orderDetail.Quantity = model.Quantity;
                            orderDetail.Price = model.Price;
                            orderDetail.OrderDetailStatus = OrderDetailStatus.Active.ToString();
                            db.SaveChanges();

                            double totalPrice = 0;
                            var listOrderDetail = db.OrderDetails.Where(
                                        a =>
                                            a.OrderId == orderDetail.OrderId &&
                                            a.OrderDetailStatus == OrderDetailStatus.Active.ToString()).ToList();
                            if (listOrderDetail.Count > 0)
                            {
                                totalPrice = listOrderDetail.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0));
                            }
                            Order ord = db.Orders.FirstOrDefault(a => a.OrderId == orderDetail.OrderId);

                            if (ord != null)
                            {
                                ord.TotalPrice = totalPrice;
                                ord.TotalPriceConvert = ord.Rate * ord.TotalPrice;
                                db.SaveChanges();
                            }
                            dbContextTransaction.Commit();
                            if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Sale, false))
                            {
                                return RedirectToAction("ViewOrderDetailForSale",
                                new { id = model.OrderId, message = "Cập nhật link hàng thành công" });
                            }
                            else
                            {
                                if (ord.IsShop == false)
                                {
                                    return RedirectToAction("ViewOrderNoShopDetail",
                                    new { id = model.OrderId, message = "Cập nhật link hàng thành công" });
                                }
                                else
                                {
                                    return RedirectToAction("ViewOrderDetail",
                                    new { id = model.OrderId, message = "Cập nhật link hàng thành công" });
                                }
                            }
                        }

                    }
                    catch
                    {
                        ViewData["message"] = "Cập nhật link hàng thất bại";
                        dbContextTransaction.Rollback();
                    }
                }
            }

            return RedirectToAction("Manage");
        }

        public ActionResult AddEditOrderInternalDetail(int? id, int? orderId)
        {
            if (id != null)
            {
                ViewBag.IsUpdate = true;
                OrderInternalDetail model = db.OrderInternalDetails.FirstOrDefault(m => m.OrderInternalDetailId == id);
                return PartialView("_AddOrderInternalDetailPartial", model);
            }
            ViewBag.IsUpdate = false;
            var modelEmpty = new OrderInternalDetail { OrderInternalId = orderId ?? 0 };
            return PartialView("_AddOrderInternalDetailPartial", modelEmpty);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult AddEditOrderInternalDetail(OrderInternalDetail model, string cmd)
        {
            using (DbContextTransaction dbContextTransaction = db.Database.BeginTransaction())
            {
                if (cmd == "Save")
                {
                    try
                    {
                        //
                        //var user = db.UserProfiles.FirstOrDefault(a => a.Phone == User.Identity.Name);
                        //if (user != null) model.em = user.Phone;
                        model.OrderDetailStatus = OrderDetailStatus.Active.ToString();
                        db.OrderInternalDetails.Add(model);
                        db.SaveChanges();

                        //tính toán lại order
                        OrderInternal ord = db.OrderInternals.FirstOrDefault(a => a.OrderInternalId == model.OrderInternalId);
                        double totalPrice = 0;
                        var listOrderDetail = db.OrderDetails.Where(
                                    a =>
                                        a.OrderId == ord.OrderId &&
                                        a.OrderDetailStatus == OrderDetailStatus.Active.ToString()).ToList();
                        if (listOrderDetail.Count > 0)
                        {
                            totalPrice = listOrderDetail.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0));
                        }
                        Order order = db.Orders.FirstOrDefault(a => a.OrderId == ord.OrderId);

                        if (order != null)
                        {
                            order.TotalPrice = totalPrice;
                            order.TotalPriceConvert = order.Rate * order.TotalPrice;
                            db.SaveChanges();
                        }
                        dbContextTransaction.Commit();

                        ViewData["message"] = "Thêm mới link hàng thành công";

                        if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Sale, false))
                        {
                            return RedirectToAction("ViewOrderNoShopDetail", new { id = model.OrderId });
                        }
                        else
                        {
                            return RedirectToAction("ViewOrderNoShopDetail", new { id = model.OrderId });
                        }
                    }
                    catch
                    {
                        ViewData["message"] = "Thêm mới link hàng thất bại";
                        dbContextTransaction.Rollback();
                    }
                }
                else
                {
                    try
                    {
                        OrderInternalDetail orderDetail =
                            db.OrderInternalDetails.FirstOrDefault(m => m.OrderInternalDetailId == model.OrderInternalDetailId);
                        if (orderDetail != null)
                        {
                            try
                            {
                                var image = Request.Files["Image_0"];
                                if (image != null && !string.IsNullOrEmpty(image.FileName))
                                {
                                    orderDetail.Image = "/Images/" + image.FileName;
                                    image.SaveAs(Server.MapPath("~/Images/" + image.FileName));
                                }
                                else
                                {
                                    orderDetail.Image = model.Image;
                                }

                            }
                            catch (Exception)
                            {
                                //no image
                            }
                            orderDetail.Link = model.Link;
                            orderDetail.Shop = model.Shop;
                            orderDetail.Color = model.Color;
                            orderDetail.Size = model.Size;
                            orderDetail.Description = model.Description;
                            orderDetail.Quantity = model.Quantity;
                            orderDetail.Price = model.Price;
                            orderDetail.OrderDetailStatus = OrderDetailStatus.Active.ToString();

                            db.SaveChanges();

                            double totalPrice = 0;
                            var listOrderDetail = db.OrderDetails.Where(
                                        a =>
                                            a.OrderId == orderDetail.OrderId &&
                                            a.OrderDetailStatus == OrderDetailStatus.Active.ToString()).ToList();
                            if (listOrderDetail.Count > 0)
                            {
                                totalPrice = listOrderDetail.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0));
                            }
                            Order ord = db.Orders.FirstOrDefault(a => a.OrderId == orderDetail.OrderId);

                            if (ord != null)
                            {
                                ord.TotalPrice = totalPrice;
                                ord.TotalPriceConvert = ord.Rate * ord.TotalPrice;
                                db.SaveChanges();
                            }

                        }

                        dbContextTransaction.Commit();
                        if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Sale, false))
                        {
                            return RedirectToAction("ViewOrderDetailNew",
                            new { id = model.OrderInternalId, message = "Cập nhật link hàng thành công" });
                        }
                        else
                        {
                            return RedirectToAction("ViewOrderDetailNew",
                            new { id = model.OrderInternalId, message = "Cập nhật link hàng thành công" });
                        }
                    }
                    catch
                    {
                        ViewData["message"] = "Cập nhật link hàng thất bại";
                        dbContextTransaction.Rollback();
                    }
                }
            }

            return RedirectToAction("Manage");
        }

        public ActionResult UpdateOrderDetail(int? id, bool IsOrderer)
        {
            OrderInternalDetail model = db.OrderInternalDetails.FirstOrDefault(m => m.OrderInternalDetailId == id);
            ViewBag.IsOrderer = IsOrderer;
            return PartialView("_UpdateOrderDetail", model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateOrderDetail(OrderInternalDetail model)
        {
            using (DbContextTransaction dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    OrderInternalDetail orderDetail = db.OrderInternalDetails.FirstOrDefault(m => m.OrderDetailId == model.OrderDetailId);
                    if (orderDetail != null)
                    {
                        orderDetail.DeliveryDate = model.DeliveryDate;
                        orderDetail.QuantityInWarehouse = model.QuantityInWarehouse;
                        orderDetail.QuantitySellPlace = model.QuantitySellPlace;
                        orderDetail.Rate_Real = model.Rate_Real;
                        db.SaveChanges();
                    }

                    dbContextTransaction.Commit();
                }
                catch
                {
                    ViewData["message"] = "Cập nhật link hàng thất bại";
                    dbContextTransaction.Rollback();
                }
            }
            if (ValidateRequest)
            {

            }
            return RedirectToAction("ViewOrderDetailNew", new { id = model.OrderInternalId });
        }

        public ActionResult DeletedOrderDetail(int id)
        {
            OrderDetail orderDetail = db.OrderDetails.FirstOrDefault(m => m.OrderDetailId == id);
            int orderId = 0;
            if (orderDetail != null)
            {
                orderId = orderDetail.OrderId;
                orderDetail.OrderDetailStatus = OrderDetailStatus.Inactive.ToString();
                //db.OrderDetails.Remove(orderDetail);
                db.SaveChanges();

                double totalPrice = 0;
                var listOrderDetail = db.OrderDetails.Where(
                            a =>
                                a.OrderId == orderDetail.OrderId &&
                                a.OrderDetailStatus == OrderDetailStatus.Active.ToString()).ToList();
                if (listOrderDetail.Count > 0)
                {
                    totalPrice = listOrderDetail.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0));
                }
                Order ord = db.Orders.FirstOrDefault(a => a.OrderId == orderDetail.OrderId);

                if (ord != null)
                {
                    ord.TotalPrice = totalPrice;
                    ord.TotalPriceConvert = ord.Rate * ord.TotalPrice;
                    db.SaveChanges();
                }
            }

            if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Sale))
            {
                return RedirectToAction("ViewOrderDetailForSale", new { id = orderId });
            }
            return RedirectToAction("ViewOrderNoShopDetail", new { id = orderId });
        }


        public ActionResult DeletedOrderInternalDetail(int id)
        {
            OrderInternalDetail orderDetail = db.OrderInternalDetails.FirstOrDefault(m => m.OrderInternalDetailId == id);
            int orderId = 0;
            if (orderDetail != null)
            {
                orderId = orderDetail.OrderInternalId;
                orderDetail.OrderDetailStatus = OrderDetailStatus.Inactive.ToString();
                db.OrderInternalDetails.Remove(orderDetail);
                db.SaveChanges();


                double totalPrice = 0;
                var listOrderDetail = db.OrderDetails.Where(
                            a =>
                                a.OrderId == orderDetail.OrderId &&
                                a.OrderDetailStatus == OrderDetailStatus.Active.ToString()).ToList();
                if (listOrderDetail.Count > 0)
                {
                    totalPrice = listOrderDetail.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0));
                }
                Order ord = db.Orders.FirstOrDefault(a => a.OrderId == orderDetail.OrderId);

                if (ord != null)
                {
                    ord.TotalPrice = totalPrice;
                    ord.TotalPriceConvert = ord.Rate * ord.TotalPrice;
                    db.SaveChanges();
                }
            }
            return RedirectToAction("ViewOrderDetailNew", new { id = orderId });
        }
        public ActionResult DeletedOrderInternalDetailOrder(int id)
        {
            OrderInternalDetail orderDetail = db.OrderInternalDetails.FirstOrDefault(m => m.OrderInternalDetailId == id);
            int orderId = 0;
            if (orderDetail != null)
            {
                orderId = orderDetail.OrderInternalId;
                orderDetail.OrderDetailStatus = OrderDetailStatus.Inactive.ToString();
                db.OrderInternalDetails.Remove(orderDetail);
                db.SaveChanges();


                double totalPrice = 0;
                var listOrderDetail = db.OrderDetails.Where(
                            a =>
                                a.OrderId == orderDetail.OrderId &&
                                a.OrderDetailStatus == OrderDetailStatus.Active.ToString()).ToList();
                if (listOrderDetail.Count > 0)
                {
                    totalPrice = listOrderDetail.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0));
                }
                OrderInternal ord = db.OrderInternals.FirstOrDefault(a => a.OrderId == orderDetail.OrderId);
                if (ord != null)
                {
                    ord.TotalPriceClient = totalPrice;
                    ord.TotalPriceConvert = ord.Rate * ord.TotalPrice;
                    db.SaveChanges();
                }
            }
            return Json(new { success = true });
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Cancel_Order(string orderId)
        {
            var order = db.OrderInternals.FirstOrDefault(m => m.OrderInternalId.ToString() == orderId);
            if (order != null)
            {
                order.Status = OrderStatus.Cancel.ToString();
                db.SaveChanges();
            }
            return Json(new { success = true });
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Cancel_Order_Order(string orderId)
        {
            var order = db.Orders.FirstOrDefault(m => m.OrderId.ToString() == orderId);
            if (order != null)
            {
                order.Status = OrderStatus.Cancel.ToString();
                db.SaveChanges();
            }
            return Json(new { success = true });
        }


        [HttpPost]
        [AllowAnonymous]
        public ActionResult Cancel_OrderNoShop(string orderId)
        {
            var order = db.Orders.FirstOrDefault(m => m.OrderId.ToString() == orderId);
            if (order != null)
            {
                order.Status = OrderStatus.Cancel.ToString();
                db.SaveChanges();
            }
            return Json(new { success = true });
        }


        [HttpPost]
        [AllowAnonymous]
        public ActionResult Confirm_Order(string orderId)
        {
            Order order = db.Orders.FirstOrDefault(m => m.OrderId.ToString() == orderId);
            if (order != null)
            {
                order.Status = OrderStatus.ClientConfirm.ToString();
                db.SaveChanges();
            }
            return Json(new { success = true });
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult SaleConfirmShop(int orderId)
        {
            //tạo ra 
            var orderDetail = db.OrderDetails.Where(a => a.OrderId == orderId).ToList();
            var order = db.Orders.FirstOrDefault(a => a.OrderId == orderId);
            if (order == null)
            {
                return Json(new { success = false, msg = "Không tồn tại đơn hàng!" });
            }
            if (!orderDetail.Select(a => string.IsNullOrEmpty(a.Shop)).Any())
            {
                return Json(new { success = false, msg = "Tồn tại link chưa phân shop!" });
            }

            if (order.IsShop)
            {
                return Json(new { success = false, msg = "Đơn hàng đã được sale khác xử lý!" });
            }

            var listShop = orderDetail.Select(a => a.Shop).Distinct();

            using (DbContextTransaction dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    //chiem quyen
                    //get sale manage
                    if (!string.IsNullOrEmpty(order.SaleManager))
                    {
                        if (order.SaleManager != User.Identity.Name)
                        {
                            return Json(new { success = false, msg = "Không được xử lý đơn hàng của sale khác!" });
                        }
                    }
                    else
                    {
                        //truong hop kh tu tạo tai khoản
                        var saleManage = db.SaleManageClients.FirstOrDefault(a => a.User_Client == order.Phone);
                        if (saleManage == null)
                        {
                            //tạo manage
                            var saleManageInsert = new SaleManageClient
                            {
                                User_Client = order.Phone,
                                User_Sale = User.Identity.Name,
                                LastUpdate = DateTime.Now,
                                User_Update = User.Identity.Name
                            };
                            db.SaleManageClients.Add(saleManageInsert);

                            order.SaleManager = User.Identity.Name;
                        }
                        else
                        {
                            if (saleManage.User_Sale != User.Identity.Name)
                            {
                                return Json(new { success = false, msg = "Không được xử lý đơn hàng của sale khác!" });
                            }
                        }
                    }
                    foreach (var shopText in listShop)
                    {
                        //check limit 50 detail
                        var listOrderDetails = orderDetail.Where(a => a.Shop == shopText).ToList();
                        if (listOrderDetails.Count > 50)
                        {
                            //thuc hien tach shop
                            var loop = (listOrderDetails.Count / 50) + 1;
                            var endIndex = 0;
                            var max = 50;
                            if ((listOrderDetails.Count % 50) != 0)
                            {
                                loop = loop + 1;
                            }
                            for (int i = 0; i < loop; i++)
                            {
                                var startIndex = endIndex;
                                endIndex = startIndex + (max - 1);

                                var listOrdDetailForShop = listOrderDetails.GetRange(startIndex, endIndex > listOrderDetails.Count ? listOrderDetails.Count : endIndex);


                                var orderInternal = order.CloneOrderInternal();
                                orderInternal.CreateDate = DateTime.Now;

                                // tạo biến đếm mới cho ngày mới
                                var orderinternalinday = db.OrderInternalInDays.First(a => a.ID == 1);
                                if (orderinternalinday.CreateDate.Date == DateTime.Now.Date)
                                {
                                    orderinternalinday.OrderInternalInDayID = orderinternalinday.OrderInternalInDayID + 1;
                                }
                                else
                                {
                                    orderinternalinday.OrderInternalInDayID = 1;
                                    orderinternalinday.CreateDate = DateTime.Now;
                                }
                                db.SaveChanges();

                                //

                                orderInternal.UpdateDate = DateTime.Now;
                                orderInternal.TotalPrice = listOrdDetailForShop.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0));
                                orderInternal.TotalPriceReality = listOrdDetailForShop.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0));
                                orderInternal.TotalPriceClient = listOrdDetailForShop.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0));
                                orderInternal.Status = OrderStatus.New.ToString();
                                orderInternal.Content = order.Content;
                                orderInternal.IDOrderCompain = order.IDOrderCompain;
                                orderInternal.TotalPriceConvert = orderInternal.TotalPrice * orderInternal.Rate;

                                var customer_code = db.UserProfiles.FirstOrDefault(a => a.Phone == orderInternal.Phone).customercode;
                                orderInternal.OrderInternalInDayID = DateTime.Now.ToString("yyMMdd") + orderInternal.OrderType.ToUpper() + "0" + orderinternalinday.OrderInternalInDayID + customer_code;


                                db.Entry(orderInternal).State = EntityState.Added;
                                db.OrderInternals.Add(orderInternal);
                                db.SaveChanges();

                                foreach (OrderDetail ordDetail in listOrdDetailForShop)
                                {
                                    var orderInternalDetail = ordDetail.CloneInternal();
                                    orderInternalDetail.OrderInternalId = orderInternal.OrderInternalId;
                                    db.Entry(orderInternalDetail).State = EntityState.Added;
                                    db.OrderInternalDetails.Add(orderInternalDetail);
                                }

                            }

                        }
                        else
                        {
                            // tạo biến đếm mới cho ngày mới
                            var orderinternalinday = db.OrderInternalInDays.First(a => a.ID == 1);
                            if (orderinternalinday.CreateDate.Date == DateTime.Now.Date)
                            {
                                orderinternalinday.OrderInternalInDayID = orderinternalinday.OrderInternalInDayID + 1;
                            }
                            else
                            {
                                orderinternalinday.OrderInternalInDayID = 1;
                                orderinternalinday.CreateDate = DateTime.Now;
                            }
                            db.SaveChanges();

                            //
                            var orderInternal = order.CloneOrderInternal();
                            orderInternal.CreateDate = DateTime.Now;
                            orderInternal.UpdateDate = DateTime.Now;
                            orderInternal.TotalPrice = listOrderDetails.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0));
                            orderInternal.TotalPriceReality = listOrderDetails.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0));
                            orderInternal.TotalPriceClient = listOrderDetails.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0));
                            orderInternal.Content = order.Content;
                            orderInternal.IDOrderCompain = order.IDOrderCompain;
                            if (order.OrderType == OrderType.AllInOne.ToString())
                            {
                                orderInternal.Status = OrderStatus.WaitOrder.ToString();
                            }
                            else
                            {
                                orderInternal.Status = OrderStatus.New.ToString();
                            }

                            orderInternal.TotalPriceConvert = orderInternal.TotalPrice * orderInternal.Rate;

                            var customer_code = db.UserProfiles.FirstOrDefault(a => a.Phone == orderInternal.Phone).customercode;
                            orderInternal.OrderInternalInDayID = DateTime.Now.ToString("yyMMdd") + orderInternal.OrderType.ToUpper() + orderinternalinday.OrderInternalInDayID + customer_code;


                            db.Entry(orderInternal).State = EntityState.Added;
                            db.OrderInternals.Add(orderInternal);
                            //db.OrderInternalInDays.Add(orderinternalinday);
                            db.SaveChanges();

                            foreach (OrderDetail ordDetail in listOrderDetails)
                            {
                                var orderInternalDetail = new OrderInternalDetail
                                {
                                    OrderId = orderId,
                                    OrderInternalId = orderInternal.OrderInternalId,
                                    Phone = ordDetail.Phone,
                                    Shop = ordDetail.Shop,
                                    Link = ordDetail.Link,
                                    Color = ordDetail.Color,
                                    Size = ordDetail.Size,
                                    Image = ordDetail.Image,
                                    Description = ordDetail.Description,
                                    Note = ordDetail.Note,
                                    OrderDetailId = ordDetail.OrderDetailId,
                                    Price = ordDetail.Price,
                                    Quantity = ordDetail.Quantity,
                                    OrderDetailStatus = OrderDetailStatus.Active.ToString()
                                };
                                db.Entry(orderInternalDetail).State = EntityState.Added;
                                db.OrderInternalDetails.Add(orderInternalDetail);
                            }
                        }
                    }

                    order.IsShop = true;

                    db.SaveChanges();
                    dbContextTransaction.Commit();

                    return Json(new { success = true });

                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback();
                    return Json(new { success = false, msg = e.ToString() });
                }
            }
        }
        #endregion

        #region Administrator

        public ActionResult ListClient(string userName, int? page)
        {
            var userProfiles = new List<UserProfile>();
            if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Sale, false))// Thêm
            {
                List<string> listUserManage =
                        db.SaleManageClients.Where(a => a.User_Sale == User.Identity.Name)
                            .Select(a => a.User_Client).Distinct()
                            .ToList();
                userProfiles.AddRange(
                    db.UserProfiles.Where(a => listUserManage.Contains(a.Phone))
                        .ToList());
            }
            else
            {

                userProfiles = (from user in db.UserProfiles
                                where user.UserType.Contains(((int)UserType.Client).ToString())
                                select user).ToList();
            }
            if (!string.IsNullOrEmpty(userName))
            {
                userProfiles =
                    userProfiles.FindAll(
                        a =>
                            !String.Equals(a.Phone, User.Identity.Name, StringComparison.CurrentCultureIgnoreCase) &&
                            a.Phone.ToLower().Contains(userName.ToLower()));
            }
            foreach (UserProfile userProfile in userProfiles)
            {
                //get sale
                SaleManageClient salemanage =
                    db.SaleManageClients.FirstOrDefault(a => a.User_Client == userProfile.Phone);
                if (salemanage != null)
                {
                    userProfile.SaleManage = salemanage.User_Sale;
                }
            }

            ViewBag.CurrentUserName = userName;

            int pageNumber = (page ?? 1);

            return View(userProfiles.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult ListUser(string userType, string userName, int? page)
        {
            ViewBag.ListUserType = GetListUserType(userType);
            var userProfiles = new List<UserProfile>();

            userProfiles = (from user in db.UserProfiles
                            where
                                !user.UserType.Contains(((int)UserType.Client).ToString()) &&
                                user.UserType.Contains(userType ?? user.UserType)
                            select user).ToList();

            if (!string.IsNullOrEmpty(userName))
            {
                userProfiles =
                    userProfiles.FindAll(
                        a =>
                            !String.Equals(a.Phone, User.Identity.Name, StringComparison.CurrentCultureIgnoreCase) &&
                            a.Phone.ToLower().Contains(userName.ToLower()));
            }

            ViewBag.CurrentUserType = userType;
            ViewBag.CurrentUserName = userName;

            int pageNumber = (page ?? 1);

            return View(userProfiles.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult AssignSaleForClient(string id, string userSale)
        {
            var model = new UpdateSetingClientUserModel();
            model.Phone = id;
            model.SaleManage = new SaleManageClient { User_Client = id, User_Sale = userSale };
            var userClient =
                            db.UserProfiles.FirstOrDefault(a => a.Phone == model.Phone);
            if (userClient != null)
            {
                model.DownPaymentRate = userClient.DownPaymentRate;
            }
            var listSale =
                db.UserProfiles.Where(a => a.UserType.Contains(((int)UserType.Sale).ToString()))
                    .Select(a => new { phone = a.Phone, display = a.Name });
            ViewBag.listSale = new SelectList(listSale, "phone", "display", userSale);
            return PartialView("_AssignSalePartial", model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult AssignSaleForClient(UpdateSetingClientUserModel model, string listSale)
        {
            SaleManageClient update = db.SaleManageClients.FirstOrDefault(a => a.User_Client == model.Phone);

            if (update == null)
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        //update rate down payment
                        var userClient =
                            db.UserProfiles.FirstOrDefault(a => a.Phone == model.Phone);
                        if (userClient != null)
                        {
                            userClient.DownPaymentRate = model.DownPaymentRate ?? 80;
                        }

                        model.SaleManage.User_Update = User.Identity.Name;
                        model.SaleManage.LastUpdate = DateTime.Now;
                        db.SaleManageClients.Add(model.SaleManage);

                        //truong hop khach hang moi duoc gan sale
                        //update sale vao don hang moi chua co sale manage
                        List<Order> listOrder =
                            db.Orders.Where(a => a.Phone == model.SaleManage.User_Client && string.IsNullOrEmpty(a.SaleManager))
                                .ToList();
                        foreach (Order order in listOrder)
                        {
                            order.SaleManager = model.SaleManage.User_Sale;
                        }
                        db.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                    }
                }
            }
            else
            {
                //update rate down payment
                var userClient =
                    db.UserProfiles.FirstOrDefault(a => a.Phone == model.Phone);
                if (userClient != null)
                {
                    userClient.DownPaymentRate = model.DownPaymentRate ?? 80;
                }

                update.User_Update = User.Identity.Name;
                update.LastUpdate = DateTime.Now;
                update.User_Sale = model.SaleManage.User_Sale;
                db.SaveChanges();
            }

            return RedirectToAction("ListClient");
        }

        public ActionResult ChangeUserType(string id, string userType)
        {
            UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Phone == id);
            if (user == null)
                return RedirectToAction("ListUser");
            var model = new ChangeUserTypeModel
            {
                Phone = user.Phone
            };
            string[] select = userType.Split(',');
            ViewBag.listUserType = GetListUserType(select);

            return PartialView("_ChangeUserTypePartial", model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ChangeUserType(ChangeUserTypeModel model)
        {
            UserProfile userUpdate = db.UserProfiles.FirstOrDefault(a => a.Phone == model.Phone);
            if (userUpdate != null)
            {
                if (model.UserType == null || !model.UserType.Any())
                {
                    userUpdate.UserType = ((int)UserType.Client).ToString(CultureInfo.InvariantCulture);
                }
                else
                {
                    string userType = model.UserType.Aggregate(string.Empty, (current, s) => current + "," + s);
                    userType = userType.Substring(1, userType.Length - 1);
                    userUpdate.UserType = userType;
                }

                db.SaveChanges();
            }
            return RedirectToAction("ListUser");
        }

        public ActionResult UpdateRate(string fromDate, string toDate, int? page)
        {
            int pageNumber = (page ?? 1);

            var model = new RateModel();
            Rate rate = db.Rates.First();
            if (rate != null)
            {
                model.Rate = rate;
                if (!string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate))
                {
                    DateTime fromdate = DateTime.ParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    DateTime todate = DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(1);
                    List<RateHistory> rateHistories = (from rateHistory in db.RateHistorys
                                                       where fromdate <= rateHistory.LastUpdate && rateHistory.LastUpdate <= todate
                                                       select rateHistory).ToList();
                    model.ListRateHistory = rateHistories.ToPagedList(pageNumber, pageSize);
                }

                model.ListRateHistory =
                    db.RateHistorys.OrderByDescending(a => a.LastUpdate).Take(10).ToPagedList(pageNumber, pageSize);
            }
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateRate(RateModel model)
        {
            if (model.Rate.RateId > 0)
            {
                Rate modelUpdate = db.Rates.FirstOrDefault(a => a.RateId == model.Rate.RateId);

                if (modelUpdate != null)
                {
                    modelUpdate.Price = model.Rate.Price;
                    modelUpdate.fee1 = model.Rate.fee1;
                    modelUpdate.fee2 = model.Rate.fee2;
                    modelUpdate.fee3 = model.Rate.fee3;
                    modelUpdate.rate_forex = model.Rate.rate_forex;
                    modelUpdate.userUpdate = User.Identity.Name;
                    modelUpdate.lastUpdated = DateTime.Now;

                    db.RateHistorys.Add(modelUpdate.CloneHistory());
                    db.SaveChanges();

                    Session["Price"] = modelUpdate.Price.ToString("##,###");
                    Session["fee1"] = modelUpdate.FormatPrice(modelUpdate.fee1);
                    Session["fee2"] = modelUpdate.FormatPrice(modelUpdate.fee2);
                    Session["fee3"] = modelUpdate.FormatPrice(modelUpdate.fee3);
                    Session["Forex"] = modelUpdate.FormatPrice(modelUpdate.rate_forex);
                }
            }
            else
            {
                model.Rate.userUpdate = User.Identity.Name;
                model.Rate.lastUpdated = DateTime.Now;
                db.Rates.Add(model.Rate);

                db.RateHistorys.Add(model.Rate.CloneHistory());
                db.SaveChanges();

                Session["Price"] = model.Rate.Price.ToString("##,###");
                Session["fee1"] = model.Rate.FormatPrice(model.Rate.fee1);
                Session["fee2"] = model.Rate.FormatPrice(model.Rate.fee2);
                Session["fee3"] = model.Rate.FormatPrice(model.Rate.fee3);
                Session["Forex"] = model.Rate.FormatPrice(model.Rate.rate_forex);
            }

            return RedirectToAction("UpdateRate");
        }
        [Authorize]
        public ActionResult HistoryChat(int id)
        {
            var lischat = new List<LisChat>();
            DateTime today = DateTime.Now;
            DateTime daydetail = today.AddDays(-5);
            var chat = db.MessageDetails.Where(a => a.FromUserId == id.ToString() && a.DateTime >= daydetail).GroupBy(a => a.ToUserId).ToList();
            if (chat.Count > 0)
            {
                foreach (var model in chat)
                {
                    var userto = db.UserProfiles.FirstOrDefault(a => a.UserId == id);
                    var userfrom = db.UserProfiles.FirstOrDefault(a => a.UserId.ToString() == model.Key);
                    if (userfrom == null)
                    {
                        var NewView = new LisChat
                        {
                            FromName = model.Key,
                            ToName = userto.Email,
                            ToId = id.ToString(),
                            FromId = model.Key
                        };
                        lischat.Add(NewView);
                    }
                    else
                    {
                        var NewView = new LisChat
                        {
                            FromName = userfrom.Email,
                            ToName = userto.Email,
                            ToId = id.ToString(),
                            FromId = model.Key
                        };
                        lischat.Add(NewView);
                    }
                }
            }
            return View(lischat);
        }
        [HttpPost]
        public JsonResult ViewDetailchat(string ToId, string FromId)
        {
            var lischat = new List<LisChat>();
            var html = "";
            DateTime today = DateTime.Now;
            DateTime daydetail = today.AddDays(-5);
            var chat = db.MessageDetails.Where(a => (a.FromUserId == FromId && a.ToUserId == ToId) || (a.FromUserId == ToId && a.ToUserId == FromId)).ToList();
            chat = chat.Where(a => a.DateTime >= daydetail).ToList();
            if (chat.Count > 0)
            {

                foreach (var i in chat)
                {
                    if (i.FromUserId == FromId)
                    {
                        html += "<p style='color:blue'> " + i.UserName + " nói:" + i.Message + "<br />" +
                            "<i>" + i.DateTime + "</i>" +
                            "</p>";
                    }
                    else
                    {
                        html += "<p style='text-align:right'> " + i.Message + "<br />" +
                            "<i>" + i.DateTime + "</i>" +
                            "</p>";
                    }

                }

            }

            return Json(new { success = true, html });
        }
        #endregion

        #region Personal
        [AllowAnonymous]
        public ActionResult Personal(string id)
        {
            UserProfile user = db.UserProfiles.First(m => m.Phone == User.Identity.Name);
            //db.UserProfiles.Find(id)
            return View(user);
        }

        [HttpPost]
        public ActionResult Personal(string id, FormCollection collection, HttpPostedFileBase Image)
        {
            if (ModelState.IsValid)
            {
                UserProfile user = db.UserProfiles.First(m => m.Phone == User.Identity.Name);
                string name = collection["Name"];
                string phone = collection["Phone"];
                string mail = collection["Email"];
                string gerna = collection["Gender"];
                string addres = collection["Address"];
                string pic = "";
                if (Image != null)
                {
                    pic = Path.GetFileName(Image.FileName);
                    string path = System.IO.Path.Combine(
                                           Server.MapPath("~/Images/"), pic);
                    // file is uploaded
                    Image.SaveAs(path);

                    // save the image path path to the database or you can send image 
                    // directly to database
                    // in-case if you want to store byte[] ie. for DB
                    using (MemoryStream ms = new MemoryStream())
                    {
                        Image.InputStream.CopyTo(ms);
                        byte[] array = ms.GetBuffer();
                    }
                }
                user.Image = pic;
                Session["Image"] = user.Image;
                user.Name = name;
                user.Phone = phone;
                user.Email = mail;
                user.Gender = gerna;
                user.Address = addres;
                db.SaveChanges();
            }
            return RedirectToAction("Personal");
        }

        #endregion

        #region ChangePass
        [AllowAnonymous]
        public ActionResult ChangePassOfAdmin(string id)
        {
            UserProfile user = db.UserProfiles.First(m => m.Phone == id);
            return View(user);
        }
        [HttpPost]
        public ActionResult ChangePassOfAdminone(string pass, string newspass, string reppass, string id)
        {
            if (ModelState.IsValid)
            {
                UserProfile user = db.UserProfiles.First(m => m.Phone == id);
                UserProfile admin = db.UserProfiles.First(m => m.Phone == User.Identity.Name);
                if (pass != null)
                {
                    if (admin.Password != pass)
                    {
                        return Json(new
                        {
                            success = false,
                            htlm = "Mật khẩu quản lý không đúng"
                        });
                    }
                    else
                    {

                        user.Password = newspass;
                        db.SaveChanges();
                        return Json(new
                        {
                            success = true,
                            htlm = "Đổi mật khẩu user thành công"
                        });
                    }
                }
            }
            return View();
        }
        [AllowAnonymous]
        public ActionResult ChangePass()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangePass(string id, FormCollection collection)
        {
            if (ModelState.IsValid)
            {
                UserProfile user = db.UserProfiles.First(m => m.Phone == id);
                string oldpass = collection["Password"];
                string newpass = collection["NewPassword"];
                if (oldpass != null)
                {
                    if (user.Password != oldpass)
                    {
                        ModelState.AddModelError("", "Mật khẩu cũ sai mời bạn nhập lại đầy đủ thông tin");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Bạn đã đổi mật khẩu thành công");
                        user.Password = newpass;
                        db.SaveChanges();
                        return RedirectToAction("ChangePass");
                    }
                }
            }
            return View();
        }

        #endregion

        #region DepositOrders

        [AllowAnonymous]
        public ActionResult DepositOrders()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult DepositOrders(NewDepositOrders model, FormCollection collec)
        {
            var Content = collec["Content"];
            if (Request.IsAuthenticated)
            {
                using (DbContextTransaction dbContextTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        Rate rate = db.Rates.FirstOrDefault();
                        UserProfile user = db.UserProfiles.First(a => a.Phone == User.Identity.Name);

                        foreach (Order orderDetail in model.ListDepositOrders)
                        {
                            var order = new Order
                            {
                                Content = "Khách hàng:" + Content,
                                CreateDate = DateTime.Now,
                                UpdateDate = DateTime.Now,
                                Status = OrderStatus.Order.ToString(),
                                Rate = 0,
                                Fee = 5,
                                Phone = user.Phone,
                                UserName = user.Email,
                                OrderType = OrderType.Ship.ToString(),
                                ShippingCode = orderDetail.ShippingCode,
                                Bucket = orderDetail.Bucket,
                            };
                            db.Orders.Add(order);
                            db.SaveChanges();
                        }
                        dbContextTransaction.Commit();

                        ViewData["message"] = "Tạo đơn hàng thành công.";
                        if (user.UserType == "5")
                        {
                            return RedirectToAction("OrderTransport", "Account");
                        }
                        else
                        {

                            return RedirectToAction("MangeDepositOrders", "Account");
                        }
                    }
                    catch (Exception)
                    {
                        dbContextTransaction.Rollback();
                    }
                }
            }
            else
            {
                using (DbContextTransaction dbContextTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        Rate rate = db.Rates.FirstOrDefault();

                        foreach (Order orderDetail in model.ListDepositOrders)
                        {
                            var order = new Order
                            {
                                Content = "Khách hàng:" + Content,
                                CreateDate = DateTime.Now,
                                UpdateDate = DateTime.Now,
                                Status = OrderStatus.Order.ToString(),
                                Rate = 0,
                                Fee = 5,
                                Phone = model.ListDepositOrders.First().Phone,
                                OrderType = OrderType.Ship.ToString(),
                                ShippingCode = orderDetail.ShippingCode,
                                Bucket = orderDetail.Bucket,
                            };
                            db.Orders.Add(order);
                            db.SaveChanges();
                        }
                        dbContextTransaction.Commit();

                        ViewData["message"] = "Tạo đơn hàng thành công, chờ nhân viên kinh doanh liên hệ với bạn.";

                        return View();
                    }
                    catch (Exception)
                    {
                        dbContextTransaction.Rollback();
                    }
                }
            }
            return View();
        }
        [AllowAnonymous]
        public ActionResult MangeDepositOrders(string username, string listStatus, string fromDate, string toDate, string OrderId,
            int? page)
        {
            var lismnager = new List<ViewManager>();
            var listOrder = new List<Order>();
            bool isFilter = false;
            listOrder = db.Orders.Where(a => a.OrderType == OrderType.Ship.ToString()).ToList();
            if (!string.IsNullOrEmpty(username))
            {
                isFilter = true;
                listOrder = db.Orders.Where(a => a.Phone == username).ToList();
            }

            if (!string.IsNullOrEmpty(fromDate))
            {
                DateTime fromdate = DateTime.ParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                if (isFilter)
                {
                    listOrder = (from order in listOrder
                                 where fromdate <= order.CreateDate
                                 select order).ToList();
                }
                else
                {
                    isFilter = true;
                    listOrder = (from order in db.Orders
                                 where fromdate <= order.CreateDate
                                 select order).ToList();
                }
            }

            if (!string.IsNullOrEmpty(toDate))
            {
                DateTime todate = DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(1);
                if (isFilter)
                {
                    listOrder = (from order in listOrder
                                 where order.CreateDate <= todate
                                 select order).ToList();
                }
                else
                {
                    isFilter = true;
                    listOrder = (from order in db.Orders
                                 where order.CreateDate <= todate
                                 select order).ToList();
                }
            }
            if (!string.IsNullOrEmpty(OrderId))
            {
                if (isFilter)
                {
                    listOrder = listOrder.FindAll(a => a.ShippingCode.ToString() == OrderId);
                }
                else
                {
                    isFilter = true;
                    listOrder = db.Orders.Where(a => a.ShippingCode.ToString() == OrderId).ToList();
                }
            }
            var listOrderDisplay = new List<Order>();

            if (isFilter)
            {
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Sale, false))
                {
                    List<string> listUserManage =
                        db.SaleManageClients.Where(a => a.User_Sale == User.Identity.Name)
                            .Select(a => a.User_Client)
                            .ToList();
                    listOrderDisplay.AddRange(
                        listOrder.Where(a => listUserManage.Contains(a.Phone) || string.IsNullOrEmpty(a.SaleManager))
                            .ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Client, false))
                {
                    listOrderDisplay.AddRange(listOrder.Where(a => a.Phone == User.Identity.Name).ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"]))
                {
                    listOrderDisplay = listOrder.ToList();
                }
            }
            else
            {
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Sale, false))
                {
                    List<string> listUserManage =
                        db.SaleManageClients.Where(a => a.User_Sale == User.Identity.Name)
                            .Select(a => a.User_Client)
                            .ToList();
                    listOrderDisplay.AddRange(
                       listOrder.Where(a => listUserManage.Contains(a.Phone) || string.IsNullOrEmpty(a.SaleManager))
                            .ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Client, false))
                {
                    listOrderDisplay.AddRange(listOrder.Where(a => a.Phone == User.Identity.Name).ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Recieve, false))
                {
                    listOrderDisplay = listOrder.ToList();
                }
                if (Utils.CheckRole((string)Session["UserType"]))
                {
                    listOrderDisplay = listOrder.ToList();
                }
            }

            listOrderDisplay = listOrderDisplay.Distinct().OrderByDescending(a => a.UpdateDate).ToList();
            if (listOrderDisplay.Count >= 0)
            {
                var lisorder = (from a in listOrderDisplay select a.OrderId).ToList();
                foreach (int orderid in lisorder)
                {
                    Order order = db.Orders.First(a => a.OrderId == orderid);
                    UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Phone == order.Phone);
                    if (user != null)
                    {
                        var NewView = new ViewManager
                        {
                            Orders = order,
                            UserName = user.Name,
                            IdUser = user.customercode
                        };
                        lismnager.Add(NewView);
                    }
                    else
                    {
                        var NewView = new ViewManager
                        {
                            Orders = order,
                            UserName = "Khách hàng chưa lập TK",
                            IdUser = "Khách hàng chưa lập TK"
                        };
                        lismnager.Add(NewView);
                    }
                }
            }
            ViewBag.CurrentToDate = toDate;
            ViewBag.CurrentFromDate = fromDate;
            ViewBag.CurrentOrderId = OrderId;
            int pageNumber = (page ?? 1);
            return View(lismnager.ToPagedList(pageNumber, pageSize));
        }
        [HttpGet]
        public ActionResult ViewDepositOrder(int id)
        {

            var model = new ViewDetailOrderModel();
            var order = db.Orders.FirstOrDefault(a => a.OrderId == id);
            if (order != null)
            {
                model.Content = order.Content;
                model.OrderId = order.OrderId;
                model.Phone = order.Phone;
                model.Rate = order.Rate;
                model.Status = order.Status;
                model.StatusText = order.getStatusText();
                model.Incurred = order.Incurred;
                model.Inland = order.Inland;
                model.Pulling = order.Pulling;
                model.SaleManager = order.SaleManager;
                model.UserName = order.UserName;
                model.Phone = order.Phone;
                model.FeeShip = order.FeeShip;
                model.FeeShipChina = order.FeeShipChina;
                model.CreateDate = order.CreateDate;
                model.Fee = order.Fee;
                model.Weight = order.Weight;
                model.DownPayment = order.DownPayment;
                model.AccountingCollected = order.AccountingCollected;
                model.OrderType = order.OrderType;
                model.Bucket = order.Bucket;
                model.ReceiveBucket = order.ReceiveBucket;
                if (!string.IsNullOrEmpty(order.SaleManager))
                {
                    UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Phone == order.SaleManager);
                    if (user != null)
                    {
                        model.SaleManageInfo = new SaleManageInfo
                        {
                            SaleName = user.Name,
                            SalePhone = user.Phone,
                            SalePhoneCompany = user.PhoneCompany
                        };
                    }
                }

                if (!string.IsNullOrEmpty(order.Phone))
                {
                    UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Phone == order.Phone);
                    if (user != null)
                    {
                        model.Client = user;
                    }
                }
            }
            if (order.ReceiveBucket == order.Bucket)
            {
                ViewBag.Message = "Đã đủ số lượng thùng";
            }
            else
            {
                int m = order.Bucket - order.ReceiveBucket;
                ViewBag.Message = "Số thùng còn thiếu: " + m + " thùng.";
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult ViewDepositOrder(int id, string MissingBucket)
        {
            if (MissingBucket != "")
            {
                if (Convert.ToInt32(MissingBucket) > 0)
                {
                    int BucketReceive = Convert.ToInt32(MissingBucket);
                    var order = db.Orders.FirstOrDefault(a => a.OrderId == id);

                    if (BucketReceive == order.Bucket)
                    {
                        order.Status = OrderStatus.Receive.ToString();
                        order.ReceiveBucket = BucketReceive;
                    }
                    else if (BucketReceive < order.Bucket)
                    {
                        order.ReceiveBucket = BucketReceive;
                        order.Status = OrderStatus.MissingBucket.ToString();
                    }
                }
            }


            db.SaveChanges();
            return RedirectToAction("ViewDepositOrder", new { id = id });
        }
        [AllowAnonymous]
        public ActionResult UpdateDepositOrder(int? id)
        {
            Order model = db.Orders.FirstOrDefault(m => m.OrderId == id);
            return View(model);
        }
        [HttpPost]
        public ActionResult UpdateDepositOrder(Order model, int? id, FormCollection collec)
        {
            if (Request.IsAuthenticated)
            {
                int m = Int16.Parse(collec["m"]);
                Order order = db.Orders.FirstOrDefault(a => a.OrderId == id);
                Wallet wallet = db.Wallets.FirstOrDefault(a => a.Client == order.Phone);
                Freight kg = db.Freights.FirstOrDefault(a => (model.Weight >= a.From && model.Weight <= a.To) || (model.Weight >= a.From && a.To == 0));
                Clot m3 = db.Clots.FirstOrDefault(a => m >= a.From && m <= a.To);
                order.Weight = model.Weight;
                order.Incurred = model.Incurred;
                order.FeeShipChina = model.FeeShipChina;
                order.FeeShip = model.FeeShip;

                if (kg == null && m3 == null)
                {
                    order.Inland = model.Weight * 22000;
                }
                else if (kg == null && m3 != null)
                {
                    order.Inland = model.Weight * m3.Value;
                }
                else if (kg != null && m3 == null)
                {
                    order.Inland = model.Weight * kg.Value;
                }
                else if ((model.Weight * kg.Value) <= (m * m3.Value))
                {
                    order.Inland = m * m3.Value;
                }
                else
                {
                    order.Inland = model.Weight * kg.Value;
                }
                double sum = order.Pulling + order.Incurred + order.Inland + order.FeeShipChina + order.FeeShip;
                if (wallet != null)
                {
                    if (wallet.Money <= sum)
                    {
                        order.Status = OrderStatus.Receive.ToString();

                    }
                    else
                    {
                        order.Status = OrderStatus.FullCollect.ToString();
                        wallet.Money -= sum;
                        InsertWalletHistory(wallet, -1 * sum, string.Format("Trừ tiền đơn hàng vận chuyển số: {0}, số tiền: {1}", order.OrderId, sum.ToString("N0")));
                        db.SaveChanges();
                    }
                }

                UpdateModel(order);
                db.SaveChanges();

            }
            return RedirectToAction("MangeDepositOrders", "Account");
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult FinishOrderDepo(string orderid)// chưa trừ tiền
        {
            Order model = db.Orders.FirstOrDefault(a => a.OrderId.ToString() == orderid);
            double sum = model.Pulling + model.Incurred + model.Inland + model.FeeShipChina + model.FeeShip;
            Wallet wallet = db.Wallets.FirstOrDefault(a => a.Client == model.Phone);
            if (model != null)
            {
                if (wallet != null)
                {
                    if (wallet.Money <= sum)
                    {
                        return Json(new { success = false });
                    }
                    else
                    {
                        model.Status = OrderStatus.Finish.ToString();
                        wallet.Money -= sum;
                        db.SaveChanges();
                        return Json(new { success = true });
                    }
                }
            }
            return Json(new { success = false });
        }
        public ActionResult OrderDepoFinish(string orderid)// đã trừ tiền
        {
            Order model = db.Orders.FirstOrDefault(a => a.OrderId.ToString() == orderid);
            if (model != null)
            {

                model.Status = OrderStatus.Finish.ToString();
                db.SaveChanges();

                return Json(new { success = true });
            }
            return Json(new { success = false });
        }
        #endregion

        #region LoginManager

        [AllowAnonymous]
        public ActionResult LoginManager(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult LoginManager(LoginModel model, string returnUrl)
        {
            if (IsEmail(model.Email))
            {
                if (IsValid(model.Email, model.Password))
                {
                    UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Email == model.Email);
                    if (user.UserType.IndexOf(((int)UserType.Client).ToString()) >= 0)
                    {
                        ModelState.AddModelError("", "Bạn không phải quản lý quản lý bạn không thể đăng nhập ở đây");
                    }
                    else
                    {
                        FormsAuthentication.SetAuthCookie(user.Phone, model.RememberMe);
                        //var authTicket = new FormsAuthenticationTicket(model.Email, model.RememberMe, 1);
                        //var EncryptedTicket = FormsAuthentication.Encrypt(authTicket);
                        //var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, EncryptedTicket);

                        //Response.Cookies.Add(authCookie);
                        if (!string.IsNullOrEmpty(returnUrl))
                        {
                            if (db.Rates.Any())
                            {
                                Rate rate = db.Rates.FirstOrDefault();
                                if (rate != null)
                                {
                                    Session["Price"] = rate.Price.ToString("##,###");
                                    Session["fee1"] = rate.FormatPrice(rate.fee1);
                                    Session["fee2"] = rate.FormatPrice(rate.fee2);
                                    Session["fee3"] = rate.FormatPrice(rate.fee3);
                                }
                            }

                        }
                        Session["Name"] = user.Name;
                        Session["ID"] = user.UserId;
                        Session["UserType"] = user.UserType;
                        Session["Email"] = user.Email;
                        Session["Phone"] = user.Phone;
                        return RedirectToAction("Manage", "Account");
                    }

                }
            }
            else
            {
                //phone
                if (IsValidPhoneManager(model.Email, model.Password))
                {
                    UserProfile userProfile = db.UserProfiles.FirstOrDefault(a => a.Phone == model.Email);
                    FormsAuthentication.SetAuthCookie(model.Email, model.RememberMe);
                    //var authTicket = new FormsAuthenticationTicket(model.Email, model.RememberMe, 1);
                    //var EncryptedTicket = FormsAuthentication.Encrypt(authTicket);
                    //var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, EncryptedTicket);

                    //Response.Cookies.Add(authCookie);
                    if (db.Rates.Any())
                    {
                        Rate rate = db.Rates.FirstOrDefault();
                        if (rate != null)
                        {
                            Session["Price"] = rate.Price.ToString("##,###");
                            Session["fee1"] = rate.FormatPrice(rate.fee1);
                            Session["fee2"] = rate.FormatPrice(rate.fee2);
                            Session["fee3"] = rate.FormatPrice(rate.fee3);
                        }

                    }

                    if (userProfile != null)
                    {
                        Session["Name"] = userProfile.Name;
                        Session["ID"] = userProfile.UserId;
                        Session["UserType"] = userProfile.UserType;
                        Session["Email"] = userProfile.Email;
                        Session["Phone"] = userProfile.Phone;
                        return RedirectToAction("Manage", "Account");
                    }

                }
            }


            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "Tên người dùng hoặc mật khẩu được cung cấp là không chính xác .");
            return View(model);
        }

        private bool IsValidPhoneManager(string phone, string password)
        {
            bool IsValid = false;

            UserProfile user = db.UserProfiles.FirstOrDefault(u => u.Phone == phone);
            if (user != null)
            {
                if (user.UserType.IndexOf(((int)UserType.Client).ToString()) >= 0)
                {
                    ModelState.AddModelError("", "Bạn không phải quản lý quản lý bạn không thể đăng nhập ở đây");
                }
                else if (user.Password == password)
                {
                    IsValid = true;
                }

            }

            return IsValid;
        }

        #endregion

        #region wallet

        #endregion


        public bool SendMail(string address, int orderid, List<string> listLink = null)
        {
            try
            {
                UserProfile userProfile = db.UserProfiles.FirstOrDefault(a => a.Email == address);
                if (userProfile != null)
                {
                    var fromAddress = new MailAddress(WebConfigurationManager.AppSettings["Email"],
                        WebConfigurationManager.AppSettings["Email_Name"]);
                    var toAddress = new MailAddress(userProfile.Email, userProfile.Name);

                    string fromPassword = WebConfigurationManager.AppSettings["Password"];
                    string subject = WebConfigurationManager.AppSettings["Subject"];
                    string body = "Công ty abc xin trân trọng kính báo đơn hàng : " + orderid +
                                  " đã về đến kho của chúng tôi. \n Kính mới quý khách đến lấy. Chúc quý khách làm ăn phát đạt. \n Regards.";

                    var smtp = new SmtpClient
                    {
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                    };
                    using (
                        var message = new MailMessage(fromAddress, toAddress)
                        {
                            Subject = subject,
                            Body = body,
                            IsBodyHtml = true
                        })
                    {
                        smtp.Send(message);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
            }
            return false;
        }

        #region ViewUser
        [AllowAnonymous]
        public ActionResult OrderNoShop(string username, string fromDate, string toDate, string listStatus, string sapxep
            , int? page)
        {
            var dsDonHang = new List<Order>();
            var dsDonHangDisplay = new List<Order>();

            bool isFilter = false;

            if (Session["Phone"].ToString() != null)
            {
                username = Session["Phone"].ToString();
            }
            dsDonHang = (from a in db.Orders
                         where a.Phone == username && a.IsShop == false && a.OrderType != OrderType.Ship.ToString()
                         orderby a.UpdateDate descending
                         select a).ToList();
            if (!string.IsNullOrEmpty(username))
            {
                isFilter = true;
                dsDonHang = dsDonHang.Where(a => a.Phone == username).ToList();
            }


            if (!string.IsNullOrEmpty(listStatus))
            {
                if (isFilter)
                {
                    dsDonHang = dsDonHang.FindAll(a => a.Status == listStatus
                                                  && a.Phone == username);
                }
                else
                {
                    isFilter = true;
                    dsDonHang = dsDonHang.Where(a => a.Status == listStatus
                                                        && a.Phone == username).ToList();
                }
            }
            if (listStatus == "" || listStatus == "New")
            {
                if (!string.IsNullOrEmpty(sapxep))
                {
                    if (isFilter == true)
                    {
                        if (sapxep == "0")
                        {
                            dsDonHang = (from a in dsDonHang
                                         where a.Phone == username && a.IsShop == false && a.OrderType != OrderType.Ship.ToString()
                                         orderby a.UpdateDate ascending
                                         select a)
                                        .ToList();
                        }

                        else
                        {
                            dsDonHang = (from a in dsDonHang
                                         where a.Phone == username && a.IsShop == false && a.OrderType != OrderType.Ship.ToString()
                                         orderby a.UpdateDate descending
                                         select a)
                                        .ToList();
                        }
                    }
                }
                else
                {
                    dsDonHang = (from a in dsDonHang
                                 where a.Phone == username && a.IsShop == false && a.OrderType != OrderType.Ship.ToString()
                                 orderby a.UpdateDate descending
                                 select a)
                                .ToList();
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(sapxep))
                {
                    if (isFilter == true)
                    {
                        if (sapxep == "0")
                        {
                            dsDonHang = (from a in dsDonHang
                                         where a.Phone == username
                                         orderby a.UpdateDate ascending
                                         select a)
                                        .ToList();
                        }

                        else
                        {
                            dsDonHang = (from a in dsDonHang
                                         where a.Phone == username
                                         orderby a.UpdateDate descending
                                         select a)
                                        .ToList();
                        }
                    }
                }
            }
            if (!string.IsNullOrEmpty(fromDate))
            {
                DateTime fromdate = DateTime.ParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                if (isFilter)
                {
                    dsDonHang = (from order in dsDonHang
                                 where fromdate <= order.UpdateDate
                                                                 && order.Phone == username
                                 select order).ToList();
                }
                else
                {
                    isFilter = true;
                    dsDonHang = (from order in dsDonHang
                                 where fromdate <= order.UpdateDate
                                                                 && order.Phone == username
                                 select order).ToList();
                }
            }
            if (!string.IsNullOrEmpty(toDate))
            {
                DateTime todate = DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(1);
                if (isFilter)
                {
                    dsDonHang = (from order in dsDonHang
                                 where order.UpdateDate <= todate
                                                                 && order.Phone == username
                                 select order).ToList();
                }
                else
                {
                    isFilter = true;
                    dsDonHang = (from order in dsDonHang
                                 where order.UpdateDate <= todate
                                                                 && order.Phone == username
                                 select order).ToList();
                }
            }
            ViewBag.ListStatus = GetListStatus(listStatus);



            dsDonHangDisplay.AddRange(dsDonHang.ToList());

            string countOrder = dsDonHangDisplay.Count().ToString();
            if (Convert.ToInt32(countOrder) == 0)
            {
                ViewBag.NoOrder = "Không có đơn hàng nào thỏa mãn điều kiện tìm kiếm !";
            }
            else
            {
                ViewBag.NoOrder = "";
            }
            string name = "";
            if (Session["Name"].ToString() != null)
            {
                name = Session["Name"].ToString();
            }

            ViewBag.Name = name;
            ViewBag.CurrentToDate = toDate;
            ViewBag.CountOrder = countOrder;
            ViewBag.CurrentSapXep = sapxep;
            ViewBag.CurrentFromDate = fromDate;
            ViewBag.CurrentStatus = listStatus;
            ViewBag.CurrentUserName = username;

            int pageNumber = (page ?? 1);

            return View(dsDonHangDisplay.ToPagedList(pageNumber, pageSize));
        }
        [AllowAnonymous]
        public ActionResult OrderChange(string username, string fromDate, string toDate, string listStatus, string sapxep
            , int? page)
        {
            var dsDonHang = new List<OrderInternal>();


            bool isFilter = false;
            if (Session["Phone"].ToString() != null)
            {
                username = Session["Phone"].ToString();
            }
            dsDonHang = (from a in db.OrderInternals
                         where a.Phone == username && a.Status == "OrdererReject"
                         orderby a.UpdateDate descending
                         select a).ToList();
            if (!string.IsNullOrEmpty(username))
            {
                isFilter = true;
                dsDonHang = dsDonHang.Where(a => a.Phone == username).ToList();
            }


            if (!string.IsNullOrEmpty(listStatus))
            {
                if (isFilter)
                {
                    dsDonHang = dsDonHang.FindAll(a => a.Status == listStatus
                                                  && a.Phone == username);
                }
                else
                {
                    isFilter = true;
                    dsDonHang = dsDonHang.Where(a => a.Status == listStatus
                                                        && a.Phone == username).ToList();
                }
            }
            if (!string.IsNullOrEmpty(sapxep))
            {
                if (isFilter == true)
                {
                    if (sapxep == "0")
                    {
                        dsDonHang = (from a in dsDonHang
                                     where a.Phone == username && a.Status == "OrdererReject"
                                     orderby a.UpdateDate ascending
                                     select a)
                                    .ToList();
                    }

                    else
                    {
                        dsDonHang = (from a in dsDonHang
                                     where a.Phone == username && a.Status == "OrdererReject"
                                     orderby a.UpdateDate descending
                                     select a)
                                    .ToList();
                    }
                }

                else
                {
                    dsDonHang = (from a in dsDonHang
                                 where a.Phone == username && a.Status == "OrdererReject"
                                 orderby a.UpdateDate descending
                                 select a)
                                .ToList();
                }
            }
            if (!string.IsNullOrEmpty(fromDate))
            {
                DateTime fromdate = DateTime.ParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                if (isFilter)
                {
                    dsDonHang = (from order in dsDonHang
                                 where fromdate <= order.UpdateDate
                                                                 && order.Phone == username
                                 select order).ToList();
                }
                else
                {
                    isFilter = true;
                    dsDonHang = (from order in dsDonHang
                                 where fromdate <= order.UpdateDate
                                                                 && order.Phone == username
                                 select order).ToList();
                }
            }
            if (!string.IsNullOrEmpty(toDate))
            {
                DateTime todate = DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(1);
                if (isFilter)
                {
                    dsDonHang = (from order in dsDonHang
                                 where order.UpdateDate <= todate
                                                                 && order.Phone == username
                                 select order).ToList();
                }
                else
                {
                    isFilter = true;
                    dsDonHang = (from order in dsDonHang
                                 where order.UpdateDate <= todate
                                                                 && order.Phone == username
                                 select order).ToList();
                }
            }

            ViewBag.ListStatus = GetListStatus(listStatus);

            var dsDonHangDisplay = new List<OrderInternal>();

            dsDonHangDisplay.AddRange(dsDonHang.ToList());


            string countOrder = dsDonHangDisplay.Count().ToString();

            if (Convert.ToInt32(countOrder) == 0)
            {
                ViewBag.NoOrder = "Không có đơn hàng nào thỏa mãn điều kiện tìm kiếm !";
            }
            else
            {
                ViewBag.NoOrder = "";
            }
            string name = "";
            if (Session["Name"].ToString() != null)
            {
                name = Session["Name"].ToString();
            }

            ViewBag.Name = name;
            ViewBag.CurrentToDate = toDate;
            ViewBag.CountOrder = countOrder;
            ViewBag.CurrentSapXep = sapxep;
            ViewBag.CurrentFromDate = fromDate;
            ViewBag.CurrentStatus = listStatus;
            ViewBag.CurrentUserName = username;

            int pageNumber = (page ?? 1);

            return View(dsDonHangDisplay.ToPagedList(pageNumber, pageSize));
        }
        [AllowAnonymous]
        public ActionResult OrderRecevice(string username, string fromDate, string toDate, string listStatus, string sapxep
            , int? page)
        {
            var dsDonHang = new List<OrderInternal>();


            bool isFilter = false;
            if (Session["Phone"].ToString() != null)
            {
                username = Session["Phone"].ToString();
            }
            dsDonHang = (from a in db.OrderInternals
                         where a.Phone == username && a.Status == "Receive"
                         orderby a.UpdateDate descending
                         select a).ToList();
            if (!string.IsNullOrEmpty(username))
            {
                isFilter = true;
                dsDonHang = dsDonHang.Where(a => a.Phone == username).ToList();
            }

            if (!string.IsNullOrEmpty(listStatus))
            {
                if (isFilter)
                {
                    dsDonHang = dsDonHang.FindAll(a => a.Status == listStatus
                                                  && a.Phone == username);
                }
                else
                {
                    isFilter = true;
                    dsDonHang = dsDonHang.Where(a => a.Status == listStatus
                                                        && a.Phone == username).ToList();
                }
            }
            if (!string.IsNullOrEmpty(sapxep))
            {
                if (isFilter == true)
                {
                    if (sapxep == "0")
                    {
                        dsDonHang = (from a in dsDonHang
                                     where a.Phone == username && a.Status == "Receive"
                                     orderby a.UpdateDate ascending
                                     select a)
                                    .ToList();
                    }

                    else
                    {
                        dsDonHang = (from a in dsDonHang
                                     where a.Phone == username && a.Status == "Receive"
                                     orderby a.UpdateDate descending
                                     select a)
                                    .ToList();
                    }
                }
                else
                {
                    dsDonHang = (from a in dsDonHang
                                 where a.Phone == username && a.Status == "Receive"
                                 orderby a.UpdateDate descending
                                 select a)
                                .ToList();
                }
            }

            if (!string.IsNullOrEmpty(fromDate))
            {
                DateTime fromdate = DateTime.ParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                if (isFilter)
                {
                    dsDonHang = (from OrderInternals in dsDonHang
                                 where fromdate <= OrderInternals.UpdateDate
                                                                 && OrderInternals.Phone == username
                                 select OrderInternals).ToList();
                }
                else
                {
                    isFilter = true;
                    dsDonHang = (from OrderInternals in dsDonHang
                                 where fromdate <= OrderInternals.UpdateDate
                                                                 && OrderInternals.Phone == username
                                 select OrderInternals).ToList();
                }
            }
            if (!string.IsNullOrEmpty(toDate))
            {
                DateTime todate = DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(1);
                if (isFilter)
                {
                    dsDonHang = (from OrderInternals in dsDonHang
                                 where OrderInternals.UpdateDate <= todate
                                                                 && OrderInternals.Phone == username
                                 select OrderInternals).ToList();
                }
                else
                {
                    isFilter = true;
                    dsDonHang = (from OrderInternals in dsDonHang
                                 where OrderInternals.UpdateDate <= todate
                                                                 && OrderInternals.Phone == username
                                 select OrderInternals).ToList();
                }
            }
            ViewBag.ListStatus = GetListStatus(listStatus);

            var dsDonHangDisplay = new List<OrderInternal>();

            dsDonHangDisplay.AddRange(dsDonHang.ToList());


            string countOrder = dsDonHangDisplay.Count().ToString();

            if (Convert.ToInt32(countOrder) == 0)
            {
                ViewBag.NoOrder = "Không có đơn hàng nào thỏa mãn điều kiện tìm kiếm !";
            }
            else
            {
                ViewBag.NoOrder = "";
            }
            string name = "";
            if (Session["Name"].ToString() != null)
            {
                name = Session["Name"].ToString();
            }

            ViewBag.Name = name;
            ViewBag.CurrentToDate = toDate;
            ViewBag.CountOrder = countOrder;
            ViewBag.CurrentSapXep = sapxep;
            ViewBag.CurrentFromDate = fromDate;
            ViewBag.CurrentStatus = listStatus;
            ViewBag.CurrentUserName = username;

            int pageNumber = (page ?? 1);

            return View(dsDonHangDisplay.ToPagedList(pageNumber, pageSize));
        }

        [AllowAnonymous]
        public ActionResult OrderPaid(string username, string fromDate, string toDate, string listStatus, string sapxep
            , int? page)
        {
            var dsDonHang = new List<OrderInternal>();


            bool isFilter = false;
            if (Session["Phone"].ToString() != null)
            {
                username = Session["Phone"].ToString();
            }
            dsDonHang = (from a in db.OrderInternals
                         where a.Phone == username && a.Status == OrderStatus.Order.ToString()
                         orderby a.UpdateDate descending
                         select a)
                                .ToList();

            if (!string.IsNullOrEmpty(username))
            {
                isFilter = true;
                dsDonHang = dsDonHang.Where(a => a.Phone == username).ToList();
            }

            if (!string.IsNullOrEmpty(listStatus))
            {
                if (isFilter)
                {
                    dsDonHang = dsDonHang.FindAll(a => a.Status == listStatus
                                                  && a.Phone == username);
                }
                else
                {
                    isFilter = true;
                    dsDonHang = dsDonHang.Where(a => a.Status == listStatus
                                                        && a.Phone == username).ToList();
                }
            }
            if (!string.IsNullOrEmpty(sapxep))
            {
                if (isFilter == true)
                {
                    if (sapxep == "0")
                    {
                        dsDonHang = (from a in dsDonHang
                                     where a.Phone == username
                                     orderby a.UpdateDate ascending
                                     select a)
                                    .ToList();
                    }

                    else
                    {
                        dsDonHang = (from a in dsDonHang
                                     where a.Phone == username
                                     orderby a.UpdateDate descending
                                     select a)
                                    .ToList();
                    }
                }
                else
                {
                    dsDonHang = (from a in dsDonHang
                                 where a.Phone == username
                                 orderby a.UpdateDate descending
                                 select a)
                                .ToList();
                }
            }

            if (!string.IsNullOrEmpty(fromDate))
            {
                DateTime fromdate = DateTime.ParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                if (isFilter)
                {
                    dsDonHang = (from OrderInternals in dsDonHang
                                 where fromdate <= OrderInternals.UpdateDate
                                                                 && OrderInternals.Phone == username
                                 select OrderInternals).ToList();
                }
                else
                {
                    isFilter = true;
                    dsDonHang = (from OrderInternals in dsDonHang
                                 where fromdate <= OrderInternals.UpdateDate
                                                                 && OrderInternals.Phone == username
                                 select OrderInternals).ToList();
                }
            }
            if (!string.IsNullOrEmpty(toDate))
            {
                DateTime todate = DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(1);
                if (isFilter)
                {
                    dsDonHang = (from OrderInternals in dsDonHang
                                 where OrderInternals.UpdateDate <= todate
                                                                 && OrderInternals.Phone == username
                                 select OrderInternals).ToList();
                }
                else
                {
                    isFilter = true;
                    dsDonHang = (from OrderInternals in dsDonHang
                                 where OrderInternals.UpdateDate <= todate
                                                                 && OrderInternals.Phone == username
                                 select OrderInternals).ToList();
                }
            }
            ViewBag.ListStatus = GetListStatus(listStatus);

            var dsDonHangDisplay = new List<OrderInternal>();

            dsDonHangDisplay.AddRange(dsDonHang.ToList());


            string countOrder = dsDonHangDisplay.Count().ToString();

            if (Convert.ToInt32(countOrder) == 0)
            {
                ViewBag.NoOrder = "Không có đơn hàng nào thỏa mãn điều kiện tìm kiếm !";
            }
            else
            {
                ViewBag.NoOrder = "";
            }
            string name = "";
            if (Session["Name"].ToString() != null)
            {
                name = Session["Name"].ToString();
            }

            ViewBag.Name = name;
            ViewBag.CurrentToDate = toDate;
            ViewBag.CountOrder = countOrder;
            ViewBag.CurrentSapXep = sapxep;
            ViewBag.CurrentFromDate = fromDate;
            ViewBag.CurrentStatus = listStatus;
            ViewBag.CurrentUserName = username;

            int pageNumber = (page ?? 1);

            return View(dsDonHangDisplay.ToPagedList(pageNumber, pageSize));
        }

        [AllowAnonymous]
        public ActionResult BangChung_User(int? page, string username)
        {
            string name = "";
            if (Session["Name"].ToString() != null)
            {
                name = Session["Name"].ToString();
            }
            var dsDonHang = new List<OrderInternal>();
            string phone = "";

            if (Session["Phone"].ToString() != null)
            {
                phone = Session["Phone"].ToString();
                dsDonHang = (from a in db.OrderInternals
                             where a.Phone == phone
                             orderby a.UpdateDate descending
                             select a).ToList();
            }
            ViewBag.Name = name;
            int pageNumber = (page ?? 1);
            return View(dsDonHang.ToPagedList(pageNumber, pageSize));
        }
        [AllowAnonymous]
        public ActionResult OrderTransport(string username, string fromDate, string toDate, string listStatus, string sapxep
            , int? page)
        {
            var listOrder = new List<Order>();
            bool isFilter = false;
            if (Session["Phone"].ToString() != null)
            {
                username = Session["Phone"].ToString();
            }
            listOrder = (from a in db.Orders
                         where a.Phone == username && a.IsShop == false && a.OrderType == OrderType.Ship.ToString()
                         orderby a.UpdateDate descending
                         select a)
                                .ToList();
            if (!string.IsNullOrEmpty(username))
            {
                isFilter = true;
                listOrder = listOrder.Where(a => a.Phone == username).ToList();
            }

            if (!string.IsNullOrEmpty(fromDate))
            {
                DateTime fromdate = DateTime.ParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                if (isFilter)
                {
                    listOrder = (from a in listOrder
                                 where fromdate <= a.CreateDate
                                 select a).ToList();
                }
                else
                {
                    isFilter = true;
                    listOrder = (from a in listOrder
                                 where fromdate <= a.CreateDate
                                 select a).ToList();
                }
            }
            if (!string.IsNullOrEmpty(toDate))
            {
                DateTime todate = DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(1);
                if (isFilter)
                {
                    listOrder = (from a in listOrder
                                 where a.CreateDate <= todate
                                 select a).ToList();
                }
                else
                {
                    isFilter = true;
                    listOrder = (from a in listOrder
                                 where a.CreateDate <= todate
                                 select a).ToList();
                }
            }
            if (!string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate))
            {

                DateTime fromdate = DateTime.ParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime todate = DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(1);
                if (isFilter)
                {
                    listOrder = (from order in listOrder
                                 where fromdate <= order.CreateDate && order.CreateDate <= todate
                                 select order).ToList();
                }
            }
            if (!string.IsNullOrEmpty(sapxep))
            {
                if (isFilter == true)
                {
                    if (sapxep == "0")
                    {
                        listOrder = (from a in listOrder
                                     where a.Phone == username && a.IsShop == false && a.OrderType != OrderType.Ship.ToString()
                                     orderby a.UpdateDate ascending
                                     select a)
                                    .ToList();
                    }

                    else
                    {
                        listOrder = (from a in listOrder
                                     where a.Phone == username && a.IsShop == false && a.OrderType != OrderType.Ship.ToString()
                                     orderby a.UpdateDate descending
                                     select a)
                                    .ToList();
                    }
                }
                else
                {
                    listOrder = (from a in listOrder
                                 where a.Phone == username && a.IsShop == false && a.OrderType != OrderType.Ship.ToString()
                                 orderby a.UpdateDate descending
                                 select a)
                                .ToList();
                }
            }

            ViewBag.ListStatus = GetListStatus(listStatus);
            string countOrder = listOrder.Count().ToString();

            if (Convert.ToInt32(countOrder) == 0)
            {
                ViewBag.NoOrder = "Không có đơn hàng nào thỏa mãn điều kiện tìm kiếm !";
            }
            else
            {
                ViewBag.NoOrder = "";
            }
            string name = "";
            if (Session["Name"].ToString() != null)
            {
                name = Session["Name"].ToString();
            }
            ViewBag.Name = name;
            ViewBag.CurrentToDate = toDate;
            ViewBag.CountOrder = countOrder;
            ViewBag.CurrentSapXep = sapxep;
            ViewBag.CurrentFromDate = fromDate;
            ViewBag.CurrentStatus = listStatus;
            ViewBag.CurrentUserName = username;
            int pageNumber = (page ?? 1);
            return View(listOrder.ToPagedList(pageNumber, pageSize));
        }

        [AllowAnonymous]
        public ActionResult AllOrder(string username, string fromDate, string toDate, string listStatus, string sapxep
            , int? page)
        {
            var dsDonHang = new List<OrderInternal>();


            bool isFilter = false;
            if (Session["Phone"].ToString() != null)
            {
                username = Session["Phone"].ToString();
            }
            dsDonHang = (from a in db.OrderInternals
                         where a.Phone == username
                         orderby a.UpdateDate descending
                         select a)
                                  .ToList();
            if (!string.IsNullOrEmpty(username))
            {
                isFilter = true;
                dsDonHang = dsDonHang.Where(a => a.Phone == username).ToList();
            }

            if (!string.IsNullOrEmpty(listStatus))
            {
                if (isFilter)
                {
                    dsDonHang = dsDonHang.FindAll(a => a.Status == listStatus
                                                  && a.Phone == username);
                }
                else
                {
                    isFilter = true;
                    dsDonHang = dsDonHang.Where(a => a.Status == listStatus
                                                        && a.Phone == username).ToList();
                }
            }
            if (!string.IsNullOrEmpty(sapxep))
            {
                if (isFilter == true)
                {
                    if (sapxep == "0")
                    {
                        dsDonHang = (from a in dsDonHang
                                     where a.Phone == username
                                     orderby a.UpdateDate ascending
                                     select a)
                                    .ToList();
                    }

                    else
                    {
                        dsDonHang = (from a in dsDonHang
                                     where a.Phone == username
                                     orderby a.UpdateDate descending
                                     select a)
                                    .ToList();
                    }
                }
                else
                {
                    dsDonHang = (from a in dsDonHang
                                 where a.Phone == username
                                 orderby a.UpdateDate descending
                                 select a)
                                   .ToList();
                }
            }

            if (!string.IsNullOrEmpty(fromDate))
            {
                DateTime fromdate = DateTime.ParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                if (isFilter)
                {
                    dsDonHang = (from order in dsDonHang
                                 where fromdate <= order.UpdateDate
                                                                 && order.Phone == username
                                 select order).ToList();
                }
                else
                {
                    isFilter = true;
                    dsDonHang = (from order in dsDonHang
                                 where fromdate <= order.UpdateDate
                                                                 && order.Phone == username
                                 select order).ToList();
                }
            }
            if (!string.IsNullOrEmpty(toDate))
            {

                DateTime todate = DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(1);
                if (isFilter)
                {
                    dsDonHang = (from order in dsDonHang
                                 where order.UpdateDate <= todate
                                                                 && order.Phone == username
                                 select order).ToList();
                }
                else
                {
                    isFilter = true;
                    dsDonHang = (from order in dsDonHang
                                 where order.UpdateDate <= todate
                                                                 && order.Phone == username
                                 select order).ToList();
                }
            }
            ViewBag.ListStatus = GetListStatus(listStatus);

            var dsDonHangDisplay = new List<OrderInternal>();

            dsDonHangDisplay.AddRange(dsDonHang.ToList());

            string countOrder = dsDonHangDisplay.Count().ToString();

            if (Convert.ToInt32(countOrder) == 0)
            {
                ViewBag.NoOrder = "Không có đơn hàng nào thỏa mãn điều kiện tìm kiếm !";
            }
            else
            {
                ViewBag.NoOrder = "";
            }
            string name = "";
            if (Session["Name"].ToString() != null)
            {
                name = Session["Name"].ToString();
            }

            ViewBag.Name = name;
            ViewBag.CurrentToDate = toDate;
            ViewBag.CountOrder = countOrder;
            ViewBag.CurrentSapXep = sapxep;
            ViewBag.CurrentFromDate = fromDate;
            ViewBag.CurrentStatus = listStatus;
            ViewBag.CurrentUserName = username;

            int pageNumber = (page ?? 1);

            return View(dsDonHangDisplay.ToPagedList(pageNumber, pageSize));
        }
        [AllowAnonymous]
        public ActionResult YeuCauGiaoHang_User()
        {
            string name = "";
            if (Session["Name"].ToString() != null)
            {
                name = Session["Name"].ToString();
            }

            ViewBag.Name = name;
            return View();
        }
        [AllowAnonymous]
        public ActionResult PhieuGiaoHang_User(string username, string fromDate, string toDate, string trangthai,
            string transId, string orderId, int? page)
        {
            var dsDonHang = new List<Order>();
            bool isFilter = false;
            username = Session["Phone"].ToString();

            if (!string.IsNullOrEmpty(username))
            {
                isFilter = true;
                dsDonHang = db.Orders.Where(a => a.Phone == username).ToList();
            }
            if (!string.IsNullOrEmpty(orderId) && !string.IsNullOrEmpty(transId))
            {
                if (isFilter == true)
                {
                    dsDonHang = (from order in db.Orders
                                 where order.OrderId == Convert.ToInt32(orderId)
                                 && order.TransId == transId
                                 && order.Phone == username
                                 select order).ToList();
                }
            }

            if (!string.IsNullOrEmpty(trangthai))
            {
                if (isFilter == true)
                {
                    if (trangthai == "ChuaGiao")
                    {
                        dsDonHang = db.Orders.Where(a => a.Status == "Order" && a.Phone == username).ToList();
                    }
                    else if (trangthai == "DaGiao")
                    {
                        dsDonHang = db.Orders.Where(a => a.Status == "Finish" && a.Phone == username).ToList();
                    }
                }
            }
            if (!string.IsNullOrEmpty(fromDate))
            {

                DateTime fromdate = DateTime.ParseExact(fromDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                if (isFilter)
                {
                    dsDonHang = (from order in dsDonHang
                                 where fromdate <= order.UpdateDate && order.Phone == username
                                 select order).ToList();
                }
                else
                {
                    isFilter = true;
                    dsDonHang = (from order in db.Orders
                                 where fromdate <= order.UpdateDate && order.Phone == username
                                 select order).ToList();
                }
            }

            if (!string.IsNullOrEmpty(toDate))
            {

                DateTime todate = DateTime.ParseExact(toDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                if (isFilter)
                {
                    dsDonHang = (from order in dsDonHang
                                 where order.UpdateDate <= todate
                                 && order.Phone == username
                                 select order).ToList();
                }
                else
                {
                    isFilter = true;
                    dsDonHang = (from order in db.Orders
                                 where order.UpdateDate <= todate
                                 && order.Phone == username
                                 select order).ToList();
                }
            }
            ViewBag.ListStatus = GetListStatus(trangthai);

            var dsDonHangDisplay = new List<Order>();

            dsDonHangDisplay.AddRange(dsDonHang.ToList());


            string count = dsDonHangDisplay.Count.ToString();
            if (Convert.ToInt32(count) == 0)
            {
                ViewBag.NoOrder = "Hiện tại quý khách chưa có phiếu chờ giao.";
            }
            else
            {
                ViewBag.NoOrder = "";
            }
            string name = Session["Name"].ToString();

            ViewBag.Name = name;
            ViewBag.CurrentToDate = toDate;
            ViewBag.CurrentOrderId = orderId;
            ViewBag.CurrentTransId = transId;
            ViewBag.CurrentFromDate = fromDate;
            ViewBag.CurrentTrangThai = trangthai;
            ViewBag.CurrentUserName = username;

            int pageNumber = (page ?? 1);

            return View(dsDonHangDisplay.ToPagedList(pageNumber, pageSize));
        }
        [AllowAnonymous]
        public ActionResult KienHang_User(string username, string fromDate, string toDate, string trangthaihang,
            string transId, string orderId, string sapxep, string listStatus, int? page)
        {
            var dsDonHang = new List<Order>();
            bool isFilter = false;
            username = Session["Phone"].ToString();
            if (!string.IsNullOrEmpty(username))
            {
                isFilter = true;
                dsDonHang = db.Orders.Where(a => a.Phone == username).ToList();
            }
            if (!string.IsNullOrEmpty(orderId) && !string.IsNullOrEmpty(transId))
            {
                if (isFilter == true)
                {

                    dsDonHang = (from order in db.Orders
                                 where order.OrderId == Convert.ToInt32(orderId)
                                 && order.TransId == transId
                                 && order.Phone == username
                                 select order).ToList();
                }
            }
            if (!string.IsNullOrEmpty(sapxep))
            {
                if (isFilter == true)
                {
                    if (sapxep == "0")
                    {
                        dsDonHang = (from a in db.Orders
                                     where a.Phone == username
                                     orderby a.UpdateDate ascending
                                     select a)
                                             .ToList();
                    }

                    else
                    {
                        dsDonHang = (from a in db.Orders
                                     where a.Phone == username
                                     orderby a.UpdateDate descending
                                     select a)
                                             .ToList();
                    }
                }
            }

            if (!string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate))
            {
                DateTime fromdate = DateTime.ParseExact(fromDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                DateTime todate = DateTime.ParseExact(toDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                if (isFilter)
                {
                    dsDonHang = (from order in dsDonHang
                                 where fromdate <= order.UpdateDate && order.UpdateDate <= todate
                                 && order.Phone == username
                                 select order).ToList();
                }
                else
                {
                    isFilter = true;
                    dsDonHang = (from order in db.Orders
                                 where fromdate <= order.UpdateDate && order.UpdateDate <= todate
                                 && order.Phone == username
                                 select order).ToList();
                }
            }
            ViewBag.ListStatus = GetListStatus(listStatus);
            var dsDonHangDisplay = new List<Order>();

            dsDonHangDisplay.AddRange(dsDonHang.ToList());

            string countorderId = dsDonHangDisplay.Count.ToString();
            if (Convert.ToInt32(countorderId) == 0)
            {
                ViewBag.NoOrder = "Không có đơn hàng nào thỏa mãn điều kiện tìm kiếm !";
            }
            else
            {
                ViewBag.NoOrder = "";
            }
            string name = Session["Name"].ToString();

            ViewBag.Name = name;
            ViewBag.CurrentToDate = toDate;
            ViewBag.CurrentOrderId = orderId;
            ViewBag.CurrentTransId = transId;
            ViewBag.CurrentFromDate = fromDate;
            ViewBag.CurrentSapXep = sapxep;
            ViewBag.CurrentUserName = username;
            ViewBag.CountOrderId = countorderId;


            int pageNumber = (page ?? 1);

            return View(dsDonHangDisplay.ToPagedList(pageNumber, pageSize));
        }
        [AllowAnonymous]
        public ActionResult LichSuGiaoDich()
        {
            string name = "";
            if (Session["Name"].ToString() != null)
            {
                name = Session["Name"].ToString();
            }
            ViewBag.Name = name;

            var ds = new List<WalletHistory>();
            string phone = Session["Phone"].ToString();


            ds = db.WalletHistorys.Where(a => a.Client == phone).ToList();
            if (phone != null)
            {

                ds = (from a in ds
                      where a.Client == phone
                      orderby a.LastUpdate descending
                      select a).Take(5).ToList();
            }

            return View(ds);
        }
        [AllowAnonymous]
        public ActionResult DiemTichLuy()
        {
            return View();
        }
        public ActionResult LienHe()
        {
            return View();
        }
        public JsonResult GetListComplaintClient()
        {
            if (!Request.IsAuthenticated)
            {
                return Json(new { success = false });
            }
            if (Session["UserType"] == null || (!Utils.CheckRole((string)Session["UserType"], (int)UserType.Client)))
            {
                return Json(new { success = false });
            }
            var customer = db.UserProfiles.First(a => a.Phone == User.Identity.Name);
            var saleclient = db.SaleManageClients.FirstOrDefault(a => a.User_Client == User.Identity.Name);
            var user = db.Wallets.First(a => a.Client == User.Identity.Name);
            var rate = db.Rates.First();
            if (user != null)
            {
                string html = "<div class=\"exchange\">" +
                    "<ul class='list-unstyled list-inline'>" +
                    "<li>Mã khách hàng:  <b>" + customer.customercode + "</b></li>";
                if (saleclient != null)
                {
                    html += "<li>Nhân viên hỗ trợ:  <b>" + saleclient.User_Sale + "</b></li>";
                }

                html += " <li> Tổng nợ:  <b>" + @user.Owe + " </b></li>" +

                 "<li>Ví: <strong class=\"money-text\"><b>" + @user.Money.ToString("##,###") + "<sup>VNĐ</sup></strong></li></ul></div>";

                return Json(new
                {
                    success = true,
                    data = html
                });
            }
            return Json(new
            {
                success = false
            });
        }
        #endregion

        #region Staff
        public ActionResult StaffOrderInternalID(string username, string listStatus, string fromDate, string toDate, string OrderId, string OrderType,
            int? page)
        {
            int pageSize = 50;
            int pageNum = (page ?? 1);
            List<OrderInternal> listOrder = new List<OrderInternal>();
            listOrder = (from a in db.OrderInternals where a.Status == OrderStatus.Order.ToString() select a).ToList();
            //var html = "";
            bool isFilter = false;
            if (!string.IsNullOrEmpty(OrderType))
            {
                if (OrderType == "1")
                {
                    listOrder = listOrder.Where(a => a.ShippingCode != null && a.ShippingCode != "").ToList();
                }
                if (OrderType == "0")
                {
                    listOrder = listOrder.Where(a => a.ShippingCode == null || a.ShippingCode == "").ToList();
                }
            }
            if (!string.IsNullOrEmpty(username))
            {
                isFilter = true;
                listOrder = listOrder.Where(a => a.Phone == username).ToList();
            }

            if (!string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate))
            {
                DateTime fromdate = DateTime.ParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime todate = DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(1);
                if (isFilter)
                {
                    listOrder = (from order in listOrder
                                 where fromdate <= order.CreateDate && order.CreateDate <= todate
                                 select order).ToList();
                }
                else
                {
                    isFilter = true;
                    listOrder = (from order in listOrder
                                 where fromdate <= order.CreateDate && order.CreateDate <= todate
                                 select order).ToList();
                }
            }
            if (!string.IsNullOrEmpty(OrderId))
            {
                if (isFilter)
                {
                    listOrder = (from a in listOrder where a.OrderInternalId.ToString() == OrderId select a).ToList();
                }
                else
                {
                    isFilter = true;
                    listOrder = listOrder.Where(a => a.OrderId.ToString() == OrderId).ToList();
                }
            }
            listOrder = listOrder.Distinct().OrderByDescending(a => a.UpdateDate).ToList();

            //for (int i = 0; i < listOrder.Count; i++)
            //{
            //    List<OrderInternal> ordership = new List<OrderInternal>();
            //    ordership = (from a in db.OrderInternals select a).ToList();
            //    if (ordership != null)
            //    {
            //        ordership = ordership.Where(a => a.Status == OrderStatus.Finish.ToString()).ToList();
            //        ordership = ordership.Where(a => a.ShippingCode == listOrder[i].ShippingCode && a.OrderInternalId != listOrder[i].OrderInternalId).ToList();
            //        if (ordership.Count != 0)
            //        {
            //            html += "<h4>Mã vận đơn <b style=\"color:red\">" + listOrder[i].ShippingCode + "</b>của đơn hàng có mã" + listOrder[i].OrderInternalId + "trùng:</h4>" +
            //                "<table class=\"table table-bordered table-striped\" style='font-size: 12px;'>" +
            //                 "<th class='text-center'>Khách hàng</th>" +
            //                "<th class='text-center'>Mã đơn</th>" +
            //                    "<th class=\"text-center\">Mã giao dịch</th>" +
            //                    "<th class=\"text-center\">Tỷ giá</th>" +
            //                    "<th class=\"text-center\">Trạng thái</th>" +
            //                    "<th class=\"text-center\">Ngày tạo</th>";
            //            foreach (var a in ordership)
            //            {
            //                html += "<tr>" +
            //                "<td class=\"text-center\">" +
            //                a.UserName +
            //                "</td>" +
            //                "<td class=\"text-center\">" +
            //                a.OrderInternalId +
            //                "</td>" +
            //                "<td class=\"text-center\">" +
            //                a.TransId +
            //                "</td>" +
            //                "<td class=\"text-center\">" +
            //                a.Rate.ToString("###,##") +
            //                "</td>" +
            //                "<td class=\"text-center\">" +
            //                a.getStatusText() +
            //                "</td>" +
            //                "<td class=\"text-center\">" +
            //                a.CreateDate.ToString("dd/MM/yyyy") +
            //                "</td>" +
            //                "</tr>" +
            //                "</table>";
            //            }
            //            html += "</table>";
            //        }
            //    }
            //}
            //ViewBag.Shingpingcode = html;
            return View(listOrder.ToPagedList(pageNum, pageSize));
        }



        public ActionResult ChangeOrderInternalID(int id)
        {
            return View(db.OrderInternals.Find(id));
        }

        [HttpPost]
        public ActionResult ChangeOrderInternalID(int id, FormCollection collection)
        {
            string OrderIDPain = collection["OrderPainId"];

            var order = db.OrderInternals.Find(id);

            order.ShippingCode = OrderIDPain;
            db.SaveChanges();
            return RedirectToAction("StaffOrderInternalID", "Account");
        }
        #endregion

        #region Fee
        public ActionResult ChangeFeeClient(string id)
        {
            var model = new WatchListModel { ListFeeClient = db.FeeClients.Where(a => a.ClientID == id).ToList(), ListClotClient = db.ClotClients.Where(a => a.IdClient == id).ToList(), ListFreightClient = db.FreightClients.Where(a => a.IdClient == id).ToList(), ListCNY = db.CNYs.ToList() };
            ViewBag.IDClient = id;
            return View(model);
        }
        public ActionResult ChangeFee()
        {
            var model = new WatchListModel { ListFee = db.Fees.ToList(), ListClot = db.Clots.ToList(), ListFreight = db.Freights.ToList(), ListCNY = db.CNYs.ToList() };
            return View(model);
        }
        public ActionResult CreateChangeFee(string id)
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateChangeFee(string id, Fee model, FeeClient modelclient)
        {
            var day = DateTime.Now;
            if (id != "" && id != null)
            {
                modelclient.LastUpdate = day;
                modelclient.ClientID = id;
                db.FeeClients.Add(modelclient);
                db.SaveChanges();
                return RedirectToAction("ChangeFeeClient/" + id);
            }
            model.LastUpdate = day;
            db.Fees.Add(model);
            db.SaveChanges();
            return RedirectToAction("ChangeFee");
        }
        public ActionResult EditChangeFee(int id, string client)
        {
            if (client != null)
            {
                var editclient = db.FeeClients.FirstOrDefault(a => a.Id == id);
                ViewBag.Client = client;
                return View(editclient);
            }
            return View(db.Fees.Find(id));
        }
        [HttpPost]
        public ActionResult EditChangeFee(int id, FormCollection collection, Fee modelid)
        {
            var day = DateTime.Now;
            //if ((collection["From"]) != null)
            //{
            //    from = int.Parse(collection["From"]);
            //}

            //var to = 0;
            //if ((collection["To"]) != null)
            //{
            //    to = int.Parse(collection["To"]);
            //}

            //var value = 0.1;
            //if (collection["Value"] != null)
            //{
            //    value = double.Parse(collection["Value"]);
            //}
            string client = collection["Client"];
            if (client != "" && client != null)
            {
                var modelclient = db.FeeClients.Find(id);
                modelclient.From = modelid.From;
                modelclient.To = modelid.To;
                modelclient.Value = modelid.Value;
                modelclient.LastUpdate = day;
                db.SaveChanges();
                return RedirectToAction("ChangeFeeClient/" + client);
            }

            var model = db.Fees.Find(id);
            model.From = modelid.From;
            model.To = modelid.To;
            model.Value = modelid.Value;
            model.LastUpdate = day;
            db.SaveChanges();
            return RedirectToAction("ChangeFee");
        }
        [HttpPost]
        public JsonResult DeleteFee(int id)
        {
            Fee fee = db.Fees.FirstOrDefault(a => a.Id == id);
            db.Fees.Remove(fee);
            db.SaveChanges();
            return Json(new { success = true });
        }
        public JsonResult DeleteFeeClient(int id)
        {
            FeeClient fee = db.FeeClients.FirstOrDefault(a => a.Id == id);
            db.FeeClients.Remove(fee);
            db.SaveChanges();
            return Json(new { success = true });
        }
        #endregion

        #region FeeHill
        #region SizeHill

        public ActionResult CreateChangeClotsHill()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateChangeClotsHill(Feeshipment model)
        {
            var day = DateTime.Now;
            model.LastUpdate = day;
            model.ShipmentType = (int)ShipmentType.Hill;
            model.UnitType = (int)UnitType.Size;
            db.Feeshipments.Add(model);
            db.SaveChanges();
            return RedirectToAction("ChangeFeeHill");
        }
        public ActionResult EditChangeClotsHill(int id)
        {
            return View(db.Feeshipments.Find(id));
        }
        [HttpPost]
        public ActionResult EditChangeClotsHill(int id, FormCollection collection)
        {
            var day = DateTime.Now;
            var model = db.Feeshipments.Find(id);

            var from = int.Parse(collection["From"]);
            var to = int.Parse(collection["To"]);
            var value = int.Parse(collection["Value"]);
            model.From = from;
            model.To = to;
            model.Value = value;
            model.LastUpdate = day;
            db.SaveChanges();
            return RedirectToAction("ChangeFeeHill");
        }
        #endregion

        #region FreightsHill
        public ActionResult ChangeFeeHill()
        {
            var model = new WatchListModel
            {
                FeeshipmentWeight = db.Feeshipments.Where(c => c.ShipmentType == (int)ShipmentType.Hill && c.UnitType == (int)UnitType.Weight).ToList(),
                FeeshipmentSize = db.Feeshipments.Where(c => c.ShipmentType == (int)ShipmentType.Hill && c.UnitType == (int)UnitType.Size).ToList()
            };
            return View(model);
        }
        public ActionResult CreateChangeFreightsHill()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateChangeFreightsHill(Feeshipment model)
        {
            var day = DateTime.Now;
            model.LastUpdate = day;
            model.ShipmentType = (int)ShipmentType.Hill;
            model.UnitType = (int)UnitType.Weight;
            db.Feeshipments.Add(model);
            db.SaveChanges();
            return RedirectToAction("ChangeFeeHill");
        }
        public ActionResult EditChangeFreightsHill(int id)
        {
            return View(db.Feeshipments.Find(id));
        }
        [HttpPost]
        public ActionResult EditChangeFreightsHill(int id, FormCollection collection)
        {
            var day = DateTime.Now;
            var model = db.Feeshipments.Find(id);

            var from = int.Parse(collection["From"]);
            var to = int.Parse(collection["To"]);
            var value = int.Parse(collection["Value"]);
            model.From = from;
            model.To = to;
            model.Value = value;
            model.LastUpdate = day;
            db.SaveChanges();
            return RedirectToAction("ChangeFeeHill");
        }
        #endregion
        #endregion


        #region SizeByroad

        public ActionResult CreateChangeClotsByroad()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateChangeClotsByroad(Feeshipment model)
        {
            var day = DateTime.Now;
            model.LastUpdate = day;
            model.ShipmentType = (int)ShipmentType.Byroad;
            model.UnitType = (int)UnitType.Size;
            db.Feeshipments.Add(model);
            db.SaveChanges();
            return RedirectToAction("ChangeFeeByroad");
        }
        public ActionResult EditChangeClotsByroad(int id)
        {
            return View(db.Feeshipments.Find(id));
        }
        [HttpPost]
        public ActionResult EditChangeClotsByroad(int id, FormCollection collection)
        {
            var day = DateTime.Now;
            var model = db.Feeshipments.Find(id);

            var from = int.Parse(collection["From"]);
            var to = int.Parse(collection["To"]);
            var value = int.Parse(collection["Value"]);
            model.From = from;
            model.To = to;
            model.Value = value;
            model.LastUpdate = day;
            db.SaveChanges();
            return RedirectToAction("ChangeFeeByroad");
        }
        #endregion



        public ActionResult ListOweAccountDetail(string phone, int? page)
        {
            var listApproval = db.OrderInternals.Where(c => c.Phone == phone && c.Owe != 0).OrderByDescending(c => c.OrderInternalId).ToList();

            ViewBag.Phone = "";
            ViewBag.Email = "";
            ViewBag.FullName = "";

            var user = db.UserProfiles.FirstOrDefault(c => c.Phone == phone);
            if (user != null)
            {
                ViewBag.Phone = user.Phone;
                ViewBag.Email = user.Email;
                ViewBag.FullName = user.Name;
            }

            const int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(listApproval.ToPagedList(pageNumber, pageSize));
        }
        #region  OrderAccount
        public ActionResult ListOrderAccountDetail(int orderId, int? page)
        {

            var listApproval = new List<OrderAccount>();

            ViewBag.OrderAccount = db.OrderAccounts.FirstOrDefault(a => a.OrderAccountId == orderId);
            if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Recieve))
            {
                listApproval = db.OrderAccounts.Where(a => a.ParentOrderAccountId == orderId && a.Status == OrderStatus.Receive.ToString()).OrderByDescending(c => c.UpdateDate).ToList();
            }
            if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Accounting)
                || Utils.CheckRole((string)Session["UserType"], (int)UserType.Sale) || Utils.CheckRole((string)Session["UserType"], (int)UserType.Admin))
            {
                listApproval = db.OrderAccounts.Where(a => a.ParentOrderAccountId == orderId).OrderByDescending(c => c.UpdateDate).ToList();
            }


            const int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(listApproval.ToPagedList(pageNumber, pageSize));
        }



        public ActionResult CreateOrderAccountDetail(int orderId)
        {
            var listCustomer = db.UserProfiles.Where(c => c.UserType == ((int)UserType.Client).ToString()).ToList();
            var orderInfo = db.OrderAccounts.FirstOrDefault(c => c.OrderAccountId == orderId);
            if (orderInfo == null)
                return View();
            var viewModel = new CreateAccountDetailModel
            {
                OrderId = orderInfo.OrderAccountId,
                LeaveQuantity = orderInfo.LeaveQuantity,
            };
            ViewBag.ListCustomer = GetListDropdown(listCustomer);
            return View(viewModel);
        }
        public ActionResult ProcessOrderAccountDetail(OrderAccount orderAccount)
        {
            ViewBag.OrderAccount = db.OrderAccounts.FirstOrDefault(a => a.OrderAccountId == orderAccount.OrderAccountId);
            ViewBag.ListBankName = GetListBankName(null, "VND");
            return View(orderAccount);
        }


        [HttpPost]
        public ActionResult ProcessOrderAccountDetailAction(FormCollection collection)
        {
            var id = collection["OrderAccountId"];
            var orderId = 0;
            int.TryParse(id, out orderId);

            var itemOrder = db.OrderAccounts.FirstOrDefault(c => c.OrderAccountId == orderId);
            if (itemOrder != null)
            {
                itemOrder.UpdateDate = DateTime.Now;
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Accounting) && itemOrder.Status == OrderStatus.New.ToString())
                {
                    double valueSys = 0;
                    var val = collection["ValueSystem"];
                    double.TryParse(val, out valueSys);

                    var bankName = collection["ExtandField"];
                    if (string.IsNullOrWhiteSpace(bankName))
                        return RedirectToAction("ListOrderAccount", "Account");

                    //var walletSys = GetWallet(null, bankName);
                    //walletSys.Money += valueSys;
                    //InsertWalletHistory(walletSys, valueSys, string.Format("Khách hàng thanh toán số tiền: {0} Id: {1}", valueSys.ToString("N0"), orderId));
                    //TODO: thực hiện trừ tiền ở đây hiện tại chưa biết trừ như thế nào

                    var customerWallet = GetWallet(itemOrder.Phone, null);
                    if (customerWallet == null)
                        return RedirectToAction("ListOrderAccount", "Account");
                    customerWallet.Money -= valueSys;
                    InsertWalletHistory(customerWallet, -1 * valueSys, string.Format("Thanh toán số tiền: {0} Id: {1}", valueSys.ToString("N0"), orderId));

                    itemOrder.ValueRemain = itemOrder.TotalValue - valueSys;
                    itemOrder.ValueSystem = valueSys;
                    itemOrder.Status = OrderStatus.Receive.ToString();
                }

                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Recieve)
                    && itemOrder.Status == OrderStatus.Receive.ToString())
                {
                    itemOrder.Status = OrderStatus.Finish.ToString();

                    var orderParent =
                        db.OrderAccounts.FirstOrDefault(c => c.OrderAccountId == itemOrder.ParentOrderAccountId);

                    if (orderParent != null)
                    {
                        var listOrder = db.OrderAccounts.Where(c => c.ParentOrderAccountId == itemOrder.ParentOrderAccountId && c.Status == OrderStatus.Finish.ToString()).ToList().Sum(c => c.Quantity);
                        if (orderParent.LeaveQuantity == 0 && listOrder + itemOrder.Quantity == orderParent.Quantity)
                        {
                            orderParent.Status = OrderStatus.Finish.ToString();
                        }
                    }
                }
            }
            db.SaveChanges();
            return RedirectToAction("ListOrderAccountDetail", "Account", new { orderId = itemOrder.ParentOrderAccountId });
        }

        [HttpPost]
        public ActionResult CreateOrderAccountDetail(OrderAccount objectApproval)
        {
            if (string.IsNullOrWhiteSpace(objectApproval.ExtandField))
                return RedirectToAction("ListOrderAccountDetail", "Account", new { orderId = objectApproval.ParentOrderAccountId });

            var totalValue = objectApproval.Price * objectApproval.Quantity;

            //var wallet =
            //    db.Wallets.FirstOrDefault(
            //        c => c.Client == objectApproval.ExtandField && c.Currency == Utils.VND.ToString());
            //if (wallet == null)
            //    return Json(new { success = false, url = Url.Action("ListOrderAccount", "Account"), ErrorText = "Không có tài khoản VNĐ của khách hàng" });
            //if (wallet.Money < totalValue)
            //    return Json(new { success = false, url = Url.Action("ListOrderAccount", "Account"), ErrorText = "Tài khoản VNĐ của khách hàng không đủ tiền" });

            // thục hiện trừ tiền của tài khoản KH
            //wallet.Money -= totalValue;
            //wallet.LastUpdate = DateTime.Now;

            //InsertWalletHistory(wallet, -1 * totalValue,string.Format("Mua hàng từ hệ thống số tiền: {0}",totalValue.ToString("N0")));


            var orderParent =
                db.OrderAccounts.FirstOrDefault(c => c.OrderAccountId == objectApproval.ParentOrderAccountId);

            if (orderParent == null)
                return Json(new { success = false, url = Url.Action("ListOrderAccount", "Account"), ErrorText = "Không lấy được thông tin order cha" });
            if (orderParent.LeaveQuantity < objectApproval.Quantity)
                return Json(new { success = false, url = Url.Action("ListOrderAccount", "Account"), ErrorText = "Số lượng không được lớn hơn số lượng còn lại" });
            orderParent.LeaveQuantity -= objectApproval.Quantity;

            objectApproval.CreateBy = User.Identity.Name;
            objectApproval.CreateDate = DateTime.Now;
            objectApproval.UpdateDate = DateTime.Now;
            objectApproval.Phone = objectApproval.ExtandField;
            objectApproval.TotalValue = totalValue;
            objectApproval.Status = OrderStatus.New.ToString();
            db.OrderAccounts.Add(objectApproval);

            db.SaveChanges();
            return Json(new { success = true, url = Url.Action("ListOrderAccountDetail", "Account", new { orderId = objectApproval.ParentOrderAccountId }), ErrorText = "Tạo đơn hàng xuất thành công" });
        }


        public ActionResult ListOrderAccount(int? page)
        {

            var listApproval = new List<OrderAccount>();


            if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Accounting)
                || Utils.CheckRole((string)Session["UserType"], (int)UserType.Recieve) || Utils.CheckRole((string)Session["UserType"], (int)UserType.Sale))
            {
                listApproval = db.OrderAccounts.Where(c => c.ParentOrderAccountId == 0).OrderByDescending(c => c.UpdateDate).ToList();
            }

            const int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(listApproval.ToPagedList(pageNumber, pageSize));
        }
        public ActionResult ListOweAccount(int? page)
        {

            var listApproval = db.OrderInternals.Where(c => c.Owe != 0);


            var listUser = db.UserProfiles.ToList();

            var diUser = new Dictionary<string, UserProfile>();
            foreach (var userProfile in listUser)
                diUser[userProfile.Phone] = userProfile;

            var dicOrderPhone = new Dictionary<string, List<OrderInternal>>();
            foreach (var orderInternal in listApproval)
            {
                var listOrder = new List<OrderInternal>();
                if (dicOrderPhone.ContainsKey(orderInternal.Phone))
                    listOrder = dicOrderPhone[orderInternal.Phone];
                listOrder.Add(orderInternal);
                dicOrderPhone[orderInternal.Phone] = listOrder;
            }


            var listTopage = new List<OweModel>();

            foreach (var order in dicOrderPhone)
            {
                if (!diUser.ContainsKey(order.Key))
                    continue;
                var userInfo = diUser[order.Key];
                var listOrder = order.Value;
                var owe = listOrder.Sum(orderInternal => orderInternal.Owe);
                listTopage.Add(new OweModel
                {
                    Phone = userInfo.Phone,
                    Email = userInfo.Email,
                    FullName = userInfo.Name,
                    OweValue = owe
                });
            }

            const int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(listTopage.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult CreateOrderAccount()
        {
            ViewBag.ListBankName = GetListBankName(null, "VND");
            return View();
        }
        public ActionResult OrderAccountDetail(OrderAccount orderAccount)
        {
            ViewBag.ListBankName = GetListBankName(null, "VND");
            return View(orderAccount);
        }

        [HttpPost]
        public ActionResult CreateOrderAccount(OrderAccount objectApproval)
        {
            var bankName = objectApproval.ExtandField;

            var walletSys = GetWallet(null, bankName);
            var walletChina = GetWallet(FeeOrderChinaSystem.FeeOrderChinaVND.ToString(), null);

            if (walletSys == null)
                return Json(new { success = false, url = Url.Action("ListOrderAccount", "Account"), ErrorText = "Không có tài khoản hệ thống" });
            if (walletChina == null)
                return Json(new { success = false, url = Url.Action("ListOrderAccount", "Account"), ErrorText = "Không có tài khoản phí trung quốc" });
            if (walletSys.Money < objectApproval.ValueSystem)
                return Json(new { success = false, url = Url.Action("ListOrderAccount", "Account"), ErrorText = "Tài khoản hệ thống không đủ tiền" });

            if (objectApproval.ValueSystem > objectApproval.TotalValue)
                return Json(new { success = false, url = Url.Action("ListOrderAccount", "Account"), ErrorText = "Số tiền thanh toán không được vượt quá số tiền đơn hàng" });

            walletSys.Money -= objectApproval.ValueSystem;
            walletChina.Money += objectApproval.ValueSystem;
            InsertWalletHistory(walletSys, -1 * objectApproval.ValueSystem, string.Format("Thanh toán đơn hàng nhập, Số tiền: {0}", objectApproval.ValueSystem));
            InsertWalletHistory(walletChina, objectApproval.ValueSystem, string.Format("Thanh toán đơn hàng nhập, Số tiền: {0}", objectApproval.ValueSystem));
            objectApproval.CreateBy = User.Identity.Name;
            objectApproval.CreateDate = DateTime.Now;
            objectApproval.UpdateDate = DateTime.Now;
            objectApproval.LeaveQuantity = objectApproval.Quantity;

            objectApproval.ValueRemain = objectApproval.TotalValue - objectApproval.ValueSystem;
            objectApproval.Status = OrderStatus.Order.ToString();
            db.OrderAccounts.Add(objectApproval);


            db.SaveChanges();
            return Json(new { success = true, url = Url.Action("ListOrderAccount", "Account"), ErrorText = "Tạo đơn hàng nhập thành công" });
        }

        [HttpPost]

        public ActionResult OrderAccountAction(FormCollection collection)
        {

            var id = collection["OrderAccountId"];
            var orderId = 0;
            int.TryParse(id, out orderId);

            var itemOrder = db.OrderAccounts.FirstOrDefault(c => c.OrderAccountId == orderId);
            if (itemOrder != null)
            {
                itemOrder.UpdateDate = DateTime.Now;
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Accounting, false))
                {
                    var bankName = collection["ExtandField"];
                    var walletSys = GetWallet(null, bankName);
                    var walletChina = GetWallet(FeeOrderChinaSystem.FeeOrderChinaVND.ToString(), null);

                    if (walletSys == null || walletChina == null)
                        return RedirectToAction("ListOrderAccount", "Account");

                    double valueSys = 0;
                    var val = collection["ValueSystem"];
                    double.TryParse(val, out valueSys);

                    var valueDiff = valueSys - itemOrder.ValueSystem;

                    walletSys.Money -= valueDiff;
                    walletChina.Money += valueDiff;
                    InsertWalletHistory(walletSys, -1 * valueDiff, string.Format("Set lại trạng thái Thanh toán đơn hàng nhập, Số tiền: {0}", (-1 * valueDiff).ToString("N0")));
                    InsertWalletHistory(walletChina, valueDiff, string.Format("Set lại trạng thái Thanh toán đơn hàng nhập, Số tiền: {0}", valueDiff));

                    itemOrder.ValueRemain = itemOrder.TotalValue - valueSys;
                    itemOrder.ValueSystem = valueSys;
                }
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Recieve))
                {
                    itemOrder.Status = OrderStatus.PendingExport.ToString();
                }
            }
            db.SaveChanges();
            return RedirectToAction("ListOrderAccount", "Account");
        }



        #endregion

        #region Create Request Amount

        public ActionResult ListRequestAmount(int? page)
        {

            var listApproval = new List<Approval>();
            if ((Utils.CheckRole((string)Session["UserType"], (int)UserType.Orderer, false)))
            {
                listApproval = db.Approvals.Where(c => c.CreateBy == User.Identity.Name).ToList();
            }

            if ((Utils.CheckRole((string)Session["UserType"], (int)UserType.Accounting, true)))
            {
                listApproval = db.Approvals.Where(c => c.OrderInternalId == (int)ApprovalStatus.Request).ToList();
            }

            const int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(listApproval.ToPagedList(pageNumber, pageSize));
        }
        public ActionResult OrderCreateRequestAmount()
        {
            return View();
        }

        public ActionResult RequestAmountReject(Approval approval)
        {

            var approvalItem = db.Approvals.FirstOrDefault(c => c.ApprovalId == approval.ApprovalId);
            if (approvalItem != null)
            {
                approvalItem.OrderInternalId = (int)ApprovalStatus.Reject;
                approvalItem.ApprovalBy = User.Identity.Name;
                approvalItem.ApprovalDate = DateTime.Now;
            }
            db.SaveChanges();
            return RedirectToAction("ListRequestAmount", "Account");
        }
        public ActionResult AccountProcessRequestAmount(Approval approval)
        {
            ViewBag.ListBankName = GetListBankName(null, "");
            return PartialView(approval);
        }

        private SelectList GetListDropdown(List<UserProfile> listData)
        {
            var list = new List<Object>();

            foreach (var a in listData)
            {
                list.Add(new { value = a.Phone, display = a.Name });
            }

            return new SelectList(list, "value", "display");
        }

        private SelectList GetListBankName(string client, string currency)
        {
            var listWallet = new List<Wallet>();

            listWallet = string.IsNullOrWhiteSpace(currency) ?
                db.Wallets.Where(a => a.Client == client).ToList() :
                db.Wallets.Where(a => a.Client == client && a.Currency == currency).ToList();

            var list = new List<Object>();

            var isEmptyCurrency = string.IsNullOrWhiteSpace(currency);

            foreach (var a in listWallet)
            {
                list.Add(isEmptyCurrency
                             ? new { value = a.Currency + "#" + a.Name + "#" + a.Id, display = a.Currency + " - " + a.Name }
                             : new { value = a.Name, display = a.Name });
            }

            return new SelectList(list, "value", "display");
        }

        private Wallet GetWallet(string client, string bankName, string currency = Utils.VND)
        {
            if (string.IsNullOrWhiteSpace(bankName))
                return db.Wallets.FirstOrDefault(a => a.Client == client && a.Currency == currency);

            return db.Wallets.FirstOrDefault(a => a.Client == client && a.Currency == currency && a.Name == bankName);

        }

        [HttpPost]
        public ActionResult AccountProcessRequestAmount(FormCollection collection)
        {
            var approvalId = collection["ApprovalId"];
            var amount = collection["Amount"];
            var bankName = collection["BankName"];
            var rate = collection["Rate"];

            if (!string.IsNullOrWhiteSpace(approvalId) && !string.IsNullOrWhiteSpace(bankName))
            {
                var bankNameSplit = bankName.Split('#').ToList();
                var approvalIdNum = 0;

                int.TryParse(approvalId, out approvalIdNum);
                double rateNum = 0;
                double.TryParse(rate, out rateNum);
                var approvalItem = db.Approvals.FirstOrDefault(c => c.ApprovalId == approvalIdNum);
                if (approvalItem != null && bankNameSplit.Count >= 2)
                {
                    var idTemp = 0;
                    var itText = bankNameSplit[2];
                    var currencyText = bankNameSplit[0];
                    int.TryParse(itText, out idTemp);
                    var objectJson = JsonConvert.DeserializeObject<RequestAmount>(approvalItem.ApprovalValue);
                    var walletSystem = db.Wallets.FirstOrDefault(c => c.Id == idTemp);
                    var userWallet = GetWallet(approvalItem.CreateBy, null, Utils.CNY);
                    if (walletSystem != null
                        && userWallet != null
                        && !string.IsNullOrWhiteSpace(currencyText)
                        && objectJson != null
                        && rateNum != 0)
                    {

                        #region Set thong tin approval
                        approvalItem.OrderInternalId = (int)ApprovalStatus.Approval;
                        approvalItem.ApprovalBy = User.Identity.Name;
                        approvalItem.ApprovalDate = DateTime.Now;
                        #endregion


                        double amountNumber;
                        double.TryParse(amount, out amountNumber);
                        if (amountNumber > objectJson.Amount)
                            // trong truong hop set vo van hoac bi by pass qua thi set dung bang Amount
                            amountNumber = objectJson.Amount;

                        userWallet.Money += amountNumber;
                        walletSystem.Money -= currencyText.ToLower() == Utils.CNY.ToLower() ? amountNumber : amountNumber * rateNum;

                        InsertWalletHistory(userWallet, amountNumber, string.Format("Nhận tiền theo yêu cầu tiền có mã: {0}, Số tiền: {1} CNY, Người xử lý: {2}, Thời gian: {3}", approvalIdNum, amountNumber.ToString("N0"), approvalItem.ApprovalBy, approvalItem.ApprovalDate.Value.ToString("dd/MM/yyyy HH:mm")));
                        InsertWalletHistory(walletSystem, amountNumber, string.Format("Chuyển tiền theo yêu cầu tiền có mã: {0}, Số tiền: {1} CNY, Tỷ giá {4},Thành Tiền: {5} VND, Người xử lý: {2}, Thời gian: {3}", approvalIdNum, amountNumber, approvalItem.ApprovalBy, approvalItem.ApprovalDate.Value.ToString("dd/MM/yyyy HH:mm"), rateNum.ToString("N0"), (rateNum * amountNumber).ToString("N0")));
                    }
                }
            }

            db.SaveChanges();
            return RedirectToAction("ListRequestAmount", "Account");
        }


        [HttpPost]
        public ActionResult OrderCreateRequestAmount(RequestAmount objectApproval)
        {
            var model = new Approval
            {
                ApprovalType = ApprovalType.RequestAmount.ToString(),
                CreateBy = User.Identity.Name,
                CreateDate = DateTime.Now,
                ApprovalValue = JsonConvert.SerializeObject(objectApproval),
                OrderInternalId = (int)ApprovalStatus.Request
            };

            db.Approvals.Add(model);
            db.SaveChanges();
            return RedirectToAction("ListRequestAmount", "Account");
        }
        #endregion

        #region FreightsByroad
        public ActionResult ChangeFeeByroad()
        {
            var model = new WatchListModel
            {
                FeeshipmentWeight = db.Feeshipments.Where(c => c.ShipmentType == (int)ShipmentType.Byroad && c.UnitType == (int)UnitType.Weight).ToList(),
                FeeshipmentSize = db.Feeshipments.Where(c => c.ShipmentType == (int)ShipmentType.Byroad && c.UnitType == (int)UnitType.Size).ToList()
            };
            return View(model);
        }
        public ActionResult CreateChangeFreightsByroad()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateChangeFreightsByroad(Feeshipment model)
        {
            var day = DateTime.Now;
            model.LastUpdate = day;
            model.ShipmentType = (int)ShipmentType.Byroad;
            model.UnitType = (int)UnitType.Weight;
            db.Feeshipments.Add(model);
            db.SaveChanges();
            return RedirectToAction("ChangeFeeByroad");
        }
        public ActionResult EditChangeFreightsByroad(int id)
        {
            return View(db.Feeshipments.Find(id));
        }
        [HttpPost]
        public ActionResult EditChangeFreightsByroad(int id, FormCollection collection)
        {
            var day = DateTime.Now;
            var model = db.Feeshipments.Find(id);

            var from = int.Parse(collection["From"]);
            var to = int.Parse(collection["To"]);
            var value = int.Parse(collection["Value"]);
            model.From = from;
            model.To = to;
            model.Value = value;
            model.LastUpdate = day;
            db.SaveChanges();
            return RedirectToAction("ChangeFeeByroad");
        }
        #endregion

        #region Freights
        public ActionResult CreateChangeFreights(string id)
        {
            if (id != null)
            {
                var fre = db.FreightClients.Where(a => a.Name == NameType.Common.ToString() && a.IdClient == id).ToList();
                var html = "";
                if (fre.Count() == 0)
                {
                    html = "<div class='col-sm-7'>" +
                          "<label class='control-label'>Chọn loại hàng:</label>" +
                           "<select class='form-control flat valid' id='OrderType_ChangeFreights' name='OrderType_ChangeFreights' aria-invalid='false'>" +
                                "<option value=" + NameType.Batch.ToString() + "> Hàng hóa theo lô</option>" +
                                 "<option value=" + NameType.Common.ToString() + "> Hàng hóa thông thường</option>" +
                            "</select>" +
                        "</div>";
                }
                ViewBag.OrderType = html;
            }
            return View();
        }
        [HttpPost]
        public ActionResult CreateChangeFreights(Freight model, string id, FreightClient modelclient, FormCollection collec)
        {
            var OrderType = collec["OrderType_ChangeFreights"];
            if (OrderType == null)
            {
                OrderType = NameType.Batch.ToString();
            }
            var day = DateTime.Now;
            if (id != "" && id != null)
            {
                modelclient.Name = OrderType;
                modelclient.LastUpdate = day;
                modelclient.IdClient = id;
                db.FreightClients.Add(modelclient);
                db.SaveChanges();
                return RedirectToAction("ChangeFeeClient/" + id);
            }
            model.Name = NameType.Batch.ToString();
            model.LastUpdate = day;
            db.Freights.Add(model);
            db.SaveChanges();
            return RedirectToAction("ChangeFee");
        }
        public ActionResult EditChangeFreights(int id, string client)
        {
            if (client != null)
            {

                var editclient = db.FreightClients.FirstOrDefault(a => a.FreightId == id);
                ViewBag.Client = client;
                return View(editclient);
            }
            return View(db.Freights.Find(id));
        }
        [HttpPost]
        public ActionResult EditChangeFreights(int id, FormCollection collection, Freight model, FreightClient modelclient)
        {
            var day = DateTime.Now;
            var modelid = db.Freights.Find(id);

            //var from = double.Parse(collection["From"]);
            //var to = double.Parse(collection["To"]);
            //var value = int.Parse(collection["Value"]);
            var name = collection["Name"];
            string client = collection["Client"];
            if (client != "" && client != null)
            {
                var modelclientid = db.FreightClients.Find(id);
                modelclientid.From = modelclient.From;
                modelclientid.To = modelclient.To;
                modelclientid.Value = modelclient.Value;
                modelclientid.LastUpdate = day;
                db.SaveChanges();
                return RedirectToAction("ChangeFeeClient/" + client);
            }
            modelid.From = model.From;
            modelid.To = model.To;
            modelid.Value = model.Value;
            modelid.LastUpdate = day;
            db.SaveChanges();
            return RedirectToAction("ChangeFee");
        }
        [HttpPost]
        public JsonResult DeleteFre(int id)
        {
            Freight fre = db.Freights.First(a => a.FreightId == id);
            db.Freights.Remove(fre);
            db.SaveChanges();
            return Json(new { success = true });
        }
        public JsonResult DeleteFreClient(int id)
        {
            FreightClient fre = db.FreightClients.First(a => a.FreightId == id);
            db.FreightClients.Remove(fre);
            db.SaveChanges();
            return Json(new { success = true });
        }
        #endregion

        #region Clots
        public ActionResult CreateChangeClots()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateChangeClots(Clot model, string id, ClotClient modelclient)
        {

            var day = DateTime.Now;
            if (id != "" && id != null)
            {
                modelclient.LastUpdate = day;
                modelclient.IdClient = id;
                db.ClotClients.Add(modelclient);
                db.SaveChanges();
                return RedirectToAction("ChangeFeeClient/" + id);
            }
            model.LastUpdate = day;
            db.Clots.Add(model);
            db.SaveChanges();
            return RedirectToAction("ChangeFee");
        }
        public ActionResult EditChangeClots(int id, string client)
        {
            if (client != null)
            {

                var editclient = db.FreightClients.FirstOrDefault(a => a.FreightId == id);
                ViewBag.Client = client;
                return View(editclient);
            }
            return View(db.Clots.Find(id));
        }
        [HttpPost]
        public ActionResult EditChangeClots(int id, FormCollection collection)
        {
            var day = DateTime.Now;
            var model = db.Clots.Find(id);

            //var from = int.Parse(collection["From"]);
            //var to = int.Parse(collection["To"]);
            var value = double.Parse(collection["Value"]);
            //var name = collection["Name"];
            string client = collection["Client"];
            if (client != "" && client != null)
            {
                var modelclient = db.ClotClients.Find(id);
                //modelclient.From = from;
                //modelclient.To = to;
                modelclient.Value = value;
                modelclient.LastUpdate = day;
                db.SaveChanges();
                return RedirectToAction("ChangeFeeClient/" + client);
            }
            //model.Name = name;
            //model.From = from;
            //model.To = to;
            model.Value = value;
            model.LastUpdate = day;
            db.SaveChanges();
            return RedirectToAction("ChangeFee");
        }
        [HttpPost]
        public ActionResult DeleteClot(int id)
        {
            Clot clo = db.Clots.Find(id);
            db.Clots.Remove(clo);
            db.SaveChanges();
            return Json(new { success = true });
        }

        #endregion

        #region CNYS
        public ActionResult CreateChangeCNYS()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateChangeCNYS(CNY model, FormCollection collection)
        {
            var name = collection["Bank"];
            if (name == "1")
            {
                model.NameType = NameType.Alipay.ToString();
            }
            else
            {
                model.NameType = NameType.Bank.ToString();
            }
            model.LastUpdate = DateTime.Now;
            db.CNYs.Add(model);
            db.SaveChanges();
            return RedirectToAction("ChangeFee");
        }
        public ActionResult EditChangeCNYS(int id)
        {
            return View(db.CNYs.Find(id));
        }
        [HttpPost]
        public ActionResult EditChangeCNYS(int id, FormCollection collection)
        {
            var day = DateTime.Now;
            var model = db.CNYs.Find(id);

            var from = int.Parse(collection["From"]);
            var to = int.Parse(collection["To"]);
            var value = int.Parse(collection["Value"]);

            model.From = from;
            model.To = to;
            model.Value = value;
            model.LastUpdate = day;
            db.SaveChanges();
            return RedirectToAction("ChangeFee");
        }
        [HttpPost]
        public JsonResult DeleteCNY(int id)
        {
            CNY cny = db.CNYs.First(a => a.CNYId == id);
            db.CNYs.Remove(cny);
            db.SaveChanges();
            return Json(new { success = true });
        }

        #endregion


        #region Approval
        [HttpPost]
        public JsonResult GetListMenuApproval()
        {
            if (!Request.IsAuthenticated)
            {
                return Json(new { success = false });
            }
            if (Session["UserType"] == null || (!Utils.CheckRole((string)Session["UserType"], (int)UserType.Accounting) && !Utils.CheckRole((string)Session["UserType"], (int)UserType.Manager)))
            {
                return Json(new { success = false });
            }

            var getlistApproval = db.Approvals.Where(a => string.IsNullOrEmpty(a.ApprovalBy) && a.ApprovalType == ApprovalType.SaleChangeFee.ToString()).ToList();

            if (getlistApproval != null && getlistApproval.Count > 0)
            {
                string html = "<a class=\"dropdown-toggle face\" data-toggle=\"dropdown\" aria-expanded=\"true\">" +
                              "<span class=\"fa fa-check\"></span>" +
                            "<sup><b style =\"color:red\" >" + getlistApproval.Count + "</b></sup>" +
                        "</a>" +
                        "<ul class=\"dropdown-menu flat panel-notify\">";

                for (int i = 0; i < getlistApproval.Count; i++)
                {
                    var approval = getlistApproval[i];
                    if (approval.ApprovalType == ApprovalType.RequestAmount.ToString())
                    {
                        html = html + " <li>" +
                          " <a title =\"" + "Order yêu cầu cấp thêm tiền mã phê duyệt: " + approval.ApprovalId + "\" href =\"" + @Url.Action("AccountProcessRequestAmount", "Account", approval) + "\" >" +
                           "Order yêu cầu cấp thêm tiền mã phê duyệt : " + approval.ApprovalId + " </a>" + " </li>";
                    }
                    else
                    {
                        if (!Utils.CheckRole((string)Session["UserType"], (int)UserType.Manager))
                            continue;
                        html = html + " <li>" +
                      " <a title =\"" + "Sale thay đổi phí dịch vụ của đơn hàng : " + approval.OrderInternalId + "\" href =\"" + @Url.Action("ApprovalDetail", "Account", new { id = approval.ApprovalId }) + "\" >" +
                       "Sale thay đổi phí dịch vụ của đơn hàng : " + approval.OrderInternalId + " </a>" + " </li>";
                    }

                }

                html = html + " </ul>";

                return Json(new
                {
                    success = true,
                    data = html
                });
            }

            return Json(new
            {
                success = false
            });
        }



        public ActionResult ApprovalDetail(int id)
        {
            var approval = db.Approvals.FirstOrDefault(a => a.ApprovalId == id);
            if (approval == null)
            {
                return RedirectToAction("Error", "Home");
            }
            var user = db.UserProfiles.FirstOrDefault(a => a.Phone == approval.CreateBy);
            var model = new ApprovalDetailModel
            {
                ApprovalId = approval.ApprovalId,
                ApprovalType = approval.ApprovalType,
                ApprovalTypeText = approval.getApprovalType(),
                OrderInternalId = approval.OrderInternalId,
                ApprovalSaleChangeFee = JsonConvert.DeserializeObject<ApprovalSaleChangeFee>(approval.ApprovalValue),
                CreateBy = user != null ? user.Name : approval.CreateBy,
                CreateDate = approval.CreateDate
            };
            return View(model);
        }

        public ActionResult ExcelErrorDetail(int id)
        {
            var excelerror = db.ExcelNotices.FirstOrDefault(a => a.ExcelNoticeID == id);
            if (excelerror == null)
            {
                return RedirectToAction("Error", "Home");
            }
            var userProfile = db.UserProfiles.FirstOrDefault(a => a.Phone == excelerror.Phone);
            var model = new ExcelErrorDetailModel
            {
                ExcelNoticeID = excelerror.ExcelNoticeID,
                Name = excelerror.Name,
                Phone = excelerror.Phone,
                Note = excelerror.Note,
                Status = excelerror.Status,
                Location = excelerror.Location,
                IsHaveSaleManage = excelerror.IsHaveSaleManage,
                ClientName = userProfile != null ? userProfile.Name : ""
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult ExcelErrorDetail(ExcelErrorDetailModel model)
        {
            var approval = db.ExcelNotices.FirstOrDefault(a => a.ExcelNoticeID == model.ExcelNoticeID);
            if (approval == null)
            {
                return Json(new { success = false, msg = "Không tìm thấy bản ghi file excel lỗi" });
            }

            if (approval.Status != ExcelNoticeStatus.UnDone.ToString())
            {
                return Json(new { success = false, msg = "Đã được người khác xử lý" });
            }

            if (!model.IsHaveSaleManage)
            {
                var saleManage = new SaleManageClient
                {
                    User_Sale = User.Identity.Name,
                    User_Client = model.Phone,
                    LastUpdate = DateTime.Now,
                    User_Update = User.Identity.Name
                };
                db.SaleManageClients.Add(saleManage);
            }
            approval.Status = ExcelNoticeStatus.Done.ToString();
            db.SaveChanges();

            return Json(new { success = true });
        }

        [HttpPost]
        public ActionResult Approval(string approvalId)
        {
            var approval = db.Approvals.FirstOrDefault(a => a.ApprovalId.ToString() == approvalId);
            if (approval == null)
            {
                return Json(new { success = false, msg = "Không tìm thấy bản ghi phê duyệt!" });
            }

            if (!string.IsNullOrEmpty(approval.ApprovalBy))
            {
                var user = db.UserProfiles.FirstOrDefault(a => a.Phone == approval.ApprovalBy);
                return Json(new { success = false, msg = string.Format("Yêu cầu đã được phê duyệt bởi {0} !", user != null ? user.Name : approval.ApprovalBy) });
            }

            if (approval.ApprovalType == ApprovalType.SaleChangeFee.ToString())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        approval.ApprovalBy = User.Identity.Name;
                        approval.ApprovalDate = DateTime.Now;

                        var objectValue = JsonConvert.DeserializeObject<ApprovalSaleChangeFee>(approval.ApprovalValue);

                        var order = db.OrderInternals.FirstOrDefault(m => m.OrderInternalId == approval.OrderInternalId);
                        if (order != null)
                        {
                            order.DownPaymentRate = objectValue.DownPaymentRate_New;
                            order.Fee = objectValue.Fee_New;
                            order.OrdererManager = objectValue.OrdererManager;
                            order.Status = OrderStatus.SaleConfirm.ToString();

                            //Kiểm tra thông tin tiền trong ví và thực hiện thu tiền
                            //get wallet for client
                            var walletClient = db.Wallets.FirstOrDefault(a => a.Client == order.Phone && a.Currency == Utils.VND);
                            if (walletClient != null)
                            {
                                var money = walletClient.Money;
                                //total price for order
                                //(Model.TotalPriceConvert + Math.Round((Model.TotalPriceConvert * Model.Fee) / 100, MidpointRounding.ToEven) + Model.FeeShipChina + Model.FeeShip).ToString("##,###")
                                var totalMoney = order.TotalPriceConvert +
                                                 Math.Round((order.TotalPriceConvert * order.Fee) / 100, MidpointRounding.ToEven) +
                                                 order.FeeShipChina + order.FeeShip;

                                //tính toán số tiền đặt cọc
                                //if client collect full money, set downpayment rate = 100
                                var moneyHold = (totalMoney * order.DownPaymentRate) / 100;
                                if (moneyHold <= money)
                                {
                                    //collect money and switch status of Order
                                    walletClient.Money = walletClient.Money - moneyHold;
                                    order.DownPayment = moneyHold;

                                    var wallerHistory = new WalletHistory
                                    {
                                        WalletId = walletClient.Id,
                                        Money = moneyHold,
                                        UpdateType = WalletUpdateType.Subtract.ToString(),
                                        Client = walletClient.Client,
                                        Currency = walletClient.Currency,
                                        User_Update = User.Identity.Name,
                                        LastUpdate = DateTime.Now,
                                        Reason =
                                            string.Format(
                                                "Thu {5}% của tổng đơn hàng {6}. Chi tiết: /n Tiền hàng: {0}; Phí dịch vụ({1} %): {2}; Ship nội địa TQ : {3}; Cước vận chuyển: {4}",
                                                order.TotalPriceConvert.ToString("##,###"), order.Fee,
                                                Math.Round((order.TotalPriceConvert * order.Fee) / 100, MidpointRounding.ToEven)
                                                    .ToString("##,###"), order.FeeShipChina.ToString("##,###"),
                                                order.FeeShip.ToString("##,###"), order.DownPaymentRate,
                                                totalMoney.ToString("##,###"))
                                    };
                                    db.WalletHistorys.Add(wallerHistory);

                                    //switch order
                                    order.Status = OrderStatus.Paid.ToString();
                                }
                                else
                                {
                                    //check money in rank
                                    moneyHold = totalMoney * (order.DownPaymentRate - 5) / 100;

                                    if (moneyHold <= money)
                                    {
                                        //subtruct full money in wallet
                                        //collect money and switch status of Order
                                        walletClient.Money = walletClient.Money - walletClient.Money;
                                        order.DownPayment = walletClient.Money;

                                        var wallerHistory = new WalletHistory
                                        {
                                            WalletId = walletClient.Id,
                                            Money = walletClient.Money,
                                            UpdateType = WalletUpdateType.Subtract.ToString(),
                                            Client = walletClient.Client,
                                            Currency = walletClient.Currency,
                                            User_Update = User.Identity.Name,
                                            LastUpdate = DateTime.Now,
                                            Reason =
                                                string.Format(
                                                    "Thu {5}% của tổng đơn hàng {6}. Chi tiết: /n Tiền hàng: {0}; Phí dịch vụ({1} %): {2}; Ship nội địa TQ : {3}; Cước vận chuyển: {4}",
                                                    order.TotalPriceConvert.ToString("##,###"), order.Fee,
                                                    Math.Round((order.TotalPriceConvert * order.Fee) / 100, MidpointRounding.ToEven)
                                                        .ToString("##,###"), order.FeeShipChina.ToString("##,###"),
                                                    order.FeeShip.ToString("##,###"), order.DownPaymentRate,
                                                    totalMoney.ToString("##,###"))
                                        };
                                        db.WalletHistorys.Add(wallerHistory);

                                        order.Status = OrderStatus.Paid.ToString();

                                    }
                                }

                            }
                        }

                        db.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        return Json(new { success = false, ex.InnerException });
                    }

                }
            }
            return Json(new { success = true });
        }
        #endregion

        #region Plugin
        [HttpPost]
        [AllowAnonymous]
        public ActionResult AddItemExtension(string value)
        {
            if (!string.IsNullOrEmpty(value) && value != "underfined")
            {
                var cookie = Request.Cookies["ci_sessions_plugin"];
                string cookiePlugin = string.Empty;
                if (cookie == null)
                {
                    cookiePlugin = Guid.NewGuid().ToString().Replace("-", "");
                    HttpCookie chatcookie = new HttpCookie("ci_sessions_plugin", cookiePlugin);
                    Response.Cookies.Add(chatcookie);
                }
                else
                {
                    cookiePlugin = cookie.Value;
                }


                var listDetail = JsonConvert.DeserializeObject<List<OrderDetailExtensions>>(value);
                if (listDetail != null && listDetail.Count > 0)
                {
                    var rate = db.Rates.FirstOrDefault();

                    foreach (var item in listDetail)
                    {
                        item.cookie = cookiePlugin;
                        item.exch_rate = rate != null ? rate.Price : 0;

                        db.ListOrderDetailExtensions.Add(item);
                    }

                    db.SaveChanges();
                    return Json(new
                    {
                        data = ""
                    });
                }
            }

            return Json(new
            {
                data = ""

            });
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult DeleteItemExtension(int value)
        {
            if (!string.IsNullOrEmpty(value.ToString()) && value.ToString() != "underfined")
            {
                var cookie = Request.Cookies["ci_sessions_plugin"];
                string cookiePlugin = string.Empty;
                if (cookie == null)
                {
                    cookiePlugin = Guid.NewGuid().ToString().Replace("-", "");
                    HttpCookie chatcookie = new HttpCookie("ci_sessions_plugin", cookiePlugin);
                    Response.Cookies.Add(chatcookie);
                }
                else
                {
                    cookiePlugin = cookie.Value;
                }
                var listOrder = db.ListOrderDetailExtensions.First(a => a.cookie == cookiePlugin && a.id == value);
                if (listOrder != null)
                {
                    db.ListOrderDetailExtensions.Remove(listOrder);
                    db.SaveChanges();
                    return Json(new
                    {
                        data = ""
                    });
                }
            }

            return Json(new
            {
                data = ""

            });
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult GetDataDisplayExtension(string value)
        {
            if (!string.IsNullOrEmpty(value) && value != "underfined")
            {
                var cookie = Request.Cookies["ci_sessions_plugin"];
                if (cookie == null)
                {
                    return Json(new
                    {
                        data = ""
                    });
                }

                string cookiePlugin = cookie.Value;

                var listOrder = db.ListOrderDetailExtensions.Where(a => a.cookie == cookiePlugin).ToList();
                if (listOrder != null && listOrder.Count > 0)
                {
                    var listObject = new List<Object>();
                    for (int i = 0; i < listOrder.Count; i++)
                    {
                        listObject.Add(new
                        {
                            amount = listOrder[i].amount,
                            check_price = listOrder[i].item_check_price,
                            exch_rate = listOrder[i].exch_rate,
                            express_price = listOrder[i].item_express_price,
                            packed_box_price = listOrder[i].item_packed_box_price,
                            price = listOrder[i].price,
                            property = listOrder[i].size,
                            shop = listOrder[i].item_shop,
                            src = listOrder[i].item_src,
                            src_img = listOrder[i].image,
                            web = listOrder[i].item_web,
                            id = listOrder[i].id,
                            stt = i
                        });
                    }

                    return Json(listObject);
                }
            }

            return Json(new
            {
                data = ""
            });
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult get_exch_rate(string get_exch_rate)
        {
            if (get_exch_rate == "1")
            {
                var cookie = Request.Cookies["ci_sessions_plugin"];
                if (cookie == null)
                {
                    var value = Guid.NewGuid().ToString().Replace("-", "");
                    HttpCookie chatcookie = new HttpCookie("ci_sessions_plugin", value);
                    Response.Cookies.Add(chatcookie);
                }

                var rate = db.Rates.FirstOrDefault();
                if (rate != null)
                {
                    return Json(new { exch_rate = rate.Price });
                }
            }

            return Json(new { exch_rate = 0 });
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult is_login(string is_login)
        {
            if (is_login == "1")
            {
                var cookie = Request.Cookies["ci_sessions_plugin"];
                if (cookie == null)
                {
                    var value = Guid.NewGuid().ToString().Replace("-", "");
                    HttpCookie chatcookie = new HttpCookie("ci_sessions_plugin", value);
                    Response.Cookies.Add(chatcookie);
                }

                if (Request.IsAuthenticated)
                {
                    var userProfile = db.UserProfiles.FirstOrDefault(a => a.Phone == User.Identity.Name);
                    return Json(new { login_status = "success", user_name = userProfile.Name, user_avatar = "", user_id = userProfile.UserId });
                }
            }

            return Json(new { login_status = "invalid" });
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult CreateOrderExtensions(string value, OrderDetail orderDetail)
        {
            if (!string.IsNullOrEmpty(value) && value != "underfined")
            {
                var cookie = Request.Cookies["ci_sessions_plugin"];
                string cookiePlugin = string.Empty;
                if (cookie == null)
                {
                    cookiePlugin = Guid.NewGuid().ToString().Replace("-", "");
                    HttpCookie chatcookie = new HttpCookie("ci_sessions_plugin", cookiePlugin);
                    Response.Cookies.Add(chatcookie);
                }
                else
                {
                    cookiePlugin = cookie.Value;
                }
                if (Request.IsAuthenticated)
                {
                    using (DbContextTransaction dbContextTransaction = db.Database.BeginTransaction())
                    {
                        Rate rate = db.Rates.FirstOrDefault();
                        UserProfile user = db.UserProfiles.First(a => a.Phone == User.Identity.Name);
                        var ListOrderDetail = db.ListOrderDetailExtensions.Where(a => a.cookie == cookiePlugin);
                        if (ListOrderDetail.Count() < 0)
                        {
                            return Json(new
                            {
                                success = false,
                                data = "",
                            });
                        }
                        if (ListOrderDetail.Count() == 1)
                        {
                            var orderinternal = new OrderInternal
                            {
                                CreateDate = DateTime.Now,
                                UpdateDate = DateTime.Now,
                                TotalPrice = ListOrderDetail.Sum(a => (a.amount ?? 0) * (a.price ?? 0)),
                                Status = OrderStatus.New.ToString(),
                                OrderType = OrderType.Order.ToString(),
                                Rate = rate != null ? rate.Price : 0,
                                Phone = user.Phone,
                                UserName = user.Email
                            };
                            int orderInternalValueInt = Convert.ToInt32(orderinternal.TotalPriceConvert / 1000000);
                            var feeinternal =
                                         db.Fees.Where(a => (a.From <= orderInternalValueInt && a.To >= orderInternalValueInt) || (a.To == 0 && a.From <= orderInternalValueInt))
                                             .OrderByDescending(a => a.Value).First();
                            orderinternal.Fee = feeinternal != null ? feeinternal.Value : 5;
                            SaleManageClient saleManageinternal =
                                db.SaleManageClients.FirstOrDefault(a => a.User_Client == user.Phone);
                            orderinternal.TotalPriceConvert = orderinternal.TotalPrice * orderinternal.Rate;
                            if (saleManageinternal != null) orderinternal.SaleManager = saleManageinternal.User_Sale;
                            db.OrderInternals.Add(orderinternal);
                            db.SaveChanges();

                            int orderinternalId = orderinternal.OrderId;
                            //var ord = db.ListOrderDetailExtensions.First(a => a.cookie == cookiePlugin);
                            foreach (var ord in db.ListOrderDetailExtensions.Where(a => a.cookie == cookiePlugin))
                            {
                                var detail = new OrderInternalDetail();
                                detail.OrderInternalId = orderinternalId;
                                detail.Phone = user.Phone;
                                detail.OrderDetailStatus = OrderDetailStatus.Active.ToString();

                                detail.Image = ord.image;
                                detail.Link = ord.item_src;
                                detail.Price = ord.price;
                                detail.Shop = ord.item_shop;
                                detail.Size = ord.size;
                                detail.Quantity = ord.amount;
                                db.OrderInternalDetails.Add(detail);
                                db.ListOrderDetailExtensions.Remove(ord);
                            }
                            db.SaveChanges();
                            dbContextTransaction.Commit();

                            return Json(new
                            {
                                data = ""
                            });
                        }
                        var order = new Order
                        {
                            CreateDate = DateTime.Now,
                            UpdateDate = DateTime.Now,
                            TotalPrice = ListOrderDetail.Sum(a => (a.amount ?? 0) * (a.price ?? 0)),
                            Status = OrderStatus.New.ToString(),
                            OrderType = OrderType.Order.ToString(),
                            Rate = rate != null ? rate.Price : 0,
                            Phone = user.Phone,
                            UserName = user.Email
                        };
                        int orderValueInt = Convert.ToInt32(order.TotalPriceConvert / 1000000);
                        var fee =
                             db.Fees.Where(a => (a.From <= orderValueInt && a.To >= orderValueInt) || (a.To == 0 && a.From <= orderValueInt))
                                             .OrderByDescending(a => a.Value).First();
                        order.Fee = fee != null ? fee.Value : 5;
                        SaleManageClient saleManage =
                            db.SaleManageClients.FirstOrDefault(a => a.User_Client == user.Phone);
                        order.TotalPriceConvert = order.TotalPrice * order.Rate;
                        if (saleManage != null) order.SaleManager = saleManage.User_Sale;
                        db.Orders.Add(order);
                        db.SaveChanges();

                        int orderId = order.OrderId;
                        //var ord = db.ListOrderDetailExtensions.First(a => a.cookie == cookiePlugin);
                        foreach (var ord in db.ListOrderDetailExtensions.Where(a => a.cookie == cookiePlugin))
                        {
                            var detail = new OrderDetail();
                            detail.OrderId = orderId;
                            detail.Phone = user.Phone;
                            detail.OrderDetailStatus = OrderDetailStatus.Active.ToString();

                            detail.Image = ord.image;
                            detail.Link = ord.item_src;
                            detail.Price = ord.price;
                            detail.Shop = ord.item_shop;
                            detail.Size = ord.size;
                            detail.Quantity = ord.amount;
                            db.OrderDetails.Add(detail);
                            db.ListOrderDetailExtensions.Remove(ord);
                        }
                        db.SaveChanges();
                        dbContextTransaction.Commit();

                        return Json(new
                        {
                            data = ""
                        });
                    }
                }
            }
            return Json(new
            {
                data = ""
            });
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult CreateOrderExtensionsNoUser(string value, OrderDetail orderDetail)
        {
            if (!string.IsNullOrEmpty(value) && value != "underfined")
            {
                var cookie = Request.Cookies["ci_sessions_plugin"];
                string cookiePlugin = string.Empty;
                if (cookie == null)
                {
                    cookiePlugin = Guid.NewGuid().ToString().Replace("-", "");
                    HttpCookie chatcookie = new HttpCookie("ci_sessions_plugin", cookiePlugin);
                    Response.Cookies.Add(chatcookie);
                }
                else
                {
                    cookiePlugin = cookie.Value;
                }
                using (DbContextTransaction dbContextTransaction = db.Database.BeginTransaction())
                {
                    var list = JsonConvert.DeserializeObject<CreateOrderExtensionsNoUser>(value);
                    Rate rate = db.Rates.FirstOrDefault();
                    var ListOrderDetail = db.ListOrderDetailExtensions.Where(a => a.cookie == cookiePlugin);
                    if (ListOrderDetail.Count() < 0)
                    {
                        return Json(new
                        {
                            success = false,
                            data = "",
                        });
                    }
                    if (ListOrderDetail.Count() == 1)
                    {
                        var orderinternal = new OrderInternal
                        {
                            CreateDate = DateTime.Now,
                            UpdateDate = DateTime.Now,
                            TotalPrice = ListOrderDetail.Sum(a => (a.amount ?? 0) * (a.price ?? 0)),
                            Status = OrderStatus.New.ToString(),
                            OrderType = OrderType.Order.ToString(),
                            Rate = rate != null ? rate.Price : 0,
                        };
                        int orderInternalValueInt = Convert.ToInt32(orderinternal.TotalPriceConvert / 1000000);
                        var feeinternal =
                                     db.Fees.Where(a => (a.From <= orderInternalValueInt && a.To >= orderInternalValueInt) || (a.To == 0 && a.From <= orderInternalValueInt))
                                         .OrderByDescending(a => a.Value).First();
                        orderinternal.Fee = feeinternal != null ? feeinternal.Value : 5;
                        orderinternal.TotalPriceConvert = orderinternal.TotalPrice * orderinternal.Rate;
                        db.OrderInternals.Add(orderinternal);
                        db.SaveChanges();

                        int orderinternalId = orderinternal.OrderId;
                        //var ord = db.ListOrderDetailExtensions.First(a => a.cookie == cookiePlugin);
                        foreach (var ord in db.ListOrderDetailExtensions.Where(a => a.cookie == cookiePlugin))
                        {
                            var detail = new OrderInternalDetail();
                            detail.OrderInternalId = orderinternalId;
                            detail.OrderDetailStatus = OrderDetailStatus.Active.ToString();

                            detail.Image = ord.image;
                            detail.Link = ord.item_src;
                            detail.Price = ord.price;
                            detail.Shop = ord.item_shop;
                            detail.Size = ord.size;
                            detail.Quantity = ord.amount;
                            db.OrderInternalDetails.Add(detail);
                            db.ListOrderDetailExtensions.Remove(ord);
                        }
                        db.SaveChanges();
                        dbContextTransaction.Commit();

                        return Json(new
                        {
                            data = ""
                        });
                    }
                    var order = new Order
                    {
                        CreateDate = DateTime.Now,
                        UpdateDate = DateTime.Now,
                        TotalPrice = ListOrderDetail.Sum(a => (a.amount ?? 0) * (a.price ?? 0)),
                        Status = OrderStatus.New.ToString(),
                        OrderType = OrderType.Order.ToString(),
                        Rate = rate != null ? rate.Price : 0,
                        Phone = list.phone,
                        UserName = list.email,

                    };
                    order.TotalPriceConvert = order.TotalPrice * order.Rate;

                    //get fee
                    int orderValueInt = Convert.ToInt32(order.TotalPriceConvert / 1000000);
                    var fee =
                         db.Fees.Where(a => (a.From <= orderValueInt && a.To >= orderValueInt) || (a.To == 0 && a.From <= orderValueInt))
                                         .OrderByDescending(a => a.Value).First();
                    order.Fee = fee != null ? fee.Value : 5;

                    db.Orders.Add(order);
                    db.SaveChanges();

                    int orderId = order.OrderId;
                    foreach (var ord in db.ListOrderDetailExtensions.Where(a => a.cookie == cookiePlugin))
                    {
                        var detail = new OrderDetail();
                        detail.OrderId = orderId;
                        detail.Phone = order.Phone;
                        detail.OrderDetailStatus = OrderDetailStatus.Active.ToString();

                        detail.Image = ord.image;
                        detail.Link = ord.item_src;
                        detail.Price = ord.price;
                        detail.Shop = ord.item_shop;
                        detail.Size = ord.size;
                        detail.Quantity = ord.amount;
                        db.OrderDetails.Add(detail);
                        db.ListOrderDetailExtensions.Remove(ord);
                    }
                    db.SaveChanges();
                    dbContextTransaction.Commit();
                    return Json(new
                    {
                        data = ""
                    });
                }
            }
            return Json(new
            {
                data = ""
            });
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult LoginExtensions(string value, string returnUrl)
        {
            var list = JsonConvert.DeserializeObject<LoginExtensions>(value);
            if (IsEmail(list.email))
            {
                if (IsValid(list.email, list.password))
                {
                    UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Email == list.email);
                    if (user != null)
                    {
                        FormsAuthentication.SetAuthCookie(user.Phone, list.rememberme == "on" ? true : false);
                        //var authTicket = new FormsAuthenticationTicket(model.Email, model.RememberMe, 1);
                        //var EncryptedTicket = FormsAuthentication.Encrypt(authTicket);
                        //var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, EncryptedTicket);

                        //Response.Cookies.Add(authCookie);
                        if (db.Rates.Any())
                        {
                            Rate rate = db.Rates.FirstOrDefault();
                            if (rate != null)
                            {
                                Session["Price"] = rate.Price.ToString("##,###");
                                Session["fee1"] = rate.FormatPrice(rate.fee1);
                                Session["fee2"] = rate.FormatPrice(rate.fee2);
                                Session["fee3"] = rate.FormatPrice(rate.fee3);
                            }
                        }

                        Session["Name"] = user.Name;
                        Session["ID"] = user.UserId;
                        Session["UserType"] = user.UserType;
                        Session["Email"] = user.Email;
                        Session["Phone"] = user.Phone;
                    }
                    return Json(new { login_status = "success" });
                }
            }
            else
            {
                //phone
                if (IsValidPhone(list.email, list.password))
                {
                    UserProfile userProfile = db.UserProfiles.FirstOrDefault(a => a.Phone == list.email);
                    if (userProfile != null)
                    {
                        FormsAuthentication.SetAuthCookie(userProfile.Phone, list.rememberme == "on" ? true : false);
                        //var authTicket = new FormsAuthenticationTicket(model.Email, model.RememberMe, 1);
                        //var EncryptedTicket = FormsAuthentication.Encrypt(authTicket);
                        //var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, EncryptedTicket);

                        //Response.Cookies.Add(authCookie);

                        if (db.Rates.Any())
                        {
                            Rate rate = db.Rates.FirstOrDefault();
                            if (rate != null)
                            {
                                Session["Price"] = rate.Price.ToString("##,###");
                                Session["fee1"] = rate.FormatPrice(rate.fee1);
                                Session["fee2"] = rate.FormatPrice(rate.fee2);
                                Session["fee3"] = rate.FormatPrice(rate.fee3);
                            }
                        }

                        Session["Name"] = userProfile.Name;
                        Session["ID"] = userProfile.UserId;
                        Session["UserType"] = userProfile.UserType;
                        Session["Email"] = userProfile.Email;
                        Session["Phone"] = userProfile.Phone;

                        return Json(new { login_status = "success" });

                    }
                }
            }


            // If we got this far, something failed, redisplay form
            return Json(new
            {
                login_status = "invalid"
            });
        }
        public ActionResult LogOffExtensions(string value)
        {
            if (!string.IsNullOrEmpty(value.ToString()) && value.ToString() != "underfined")
            {
                var cookie = Request.Cookies["ci_sessions_plugin"];
                string cookiePlugin = string.Empty;
                if (cookie == null)
                {
                    cookiePlugin = Guid.NewGuid().ToString().Replace("-", "");
                    HttpCookie chatcookie = new HttpCookie("ci_sessions_plugin", cookiePlugin);
                    Response.Cookies.Add(chatcookie);
                }
                else
                {
                    cookiePlugin = cookie.Value;
                }
                var listOrder = db.ListOrderDetailExtensions.FirstOrDefault(a => a.cookie == cookiePlugin);
                if (listOrder != null)
                {
                    db.ListOrderDetailExtensions.Remove(listOrder);
                    db.SaveChanges();
                    WebSecurity.Logout();
                    FormsAuthentication.SignOut();

                    Session.Remove("Name");
                    Session.Remove("ID");
                    Session.Remove("UserType");
                    Session.Remove("Phone");
                    Session.Remove("Email");
                    return Json(new
                    {
                        login_status = "success"
                    });
                }
                else
                {
                    WebSecurity.Logout();
                    FormsAuthentication.SignOut();

                    Session.Remove("Name");
                    Session.Remove("ID");
                    Session.Remove("UserType");
                    Session.Remove("Phone");
                    Session.Remove("Email");
                    return Json(new
                    {
                        login_status = "success"
                    });
                }
            }

            return Json(new
            {
                login_status = "invalid"

            });
        }
        #endregion

        #region Oderupdate
        [Authorize]
        public ActionResult ListOdrer(string username, string listStatus, string fromDate, string toDate, string OrderId,
             int? page)
        {
            var listOrder = new List<OrderInternal>();
            bool isFilter = false;
            if (!string.IsNullOrEmpty(username))
            {
                isFilter = true;
                listOrder = db.OrderInternals.Where(a => a.Phone == username).ToList();

            }

            double totalPrice = 0;
            var listOrderDetail = db.OrderInternals.Where(
                        a => a.Status == OrderStatus.Order.ToString()).ToList();
            if (listOrderDetail.Count > 0)
            {
                totalPrice = listOrderDetail.Sum(a => ((a.TotalPrice) - (a.TotalPriceReality)));
            }
            if (!string.IsNullOrEmpty(fromDate))
            {
                DateTime fromdate = DateTime.ParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                if (isFilter)
                {
                    listOrder = (from order in listOrder
                                 where fromdate <= order.CreateDate
                                 select order).ToList();
                }
                else
                {
                    isFilter = true;
                    listOrder = (from order in db.OrderInternals
                                 where fromdate <= order.CreateDate
                                 select order).ToList();
                }
            }

            if (!string.IsNullOrEmpty(toDate))
            {

                DateTime todate = DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(1);
                if (isFilter)
                {
                    listOrder = (from order in listOrder
                                 where order.CreateDate <= todate
                                 select order).ToList();
                }
                else
                {
                    isFilter = true;
                    listOrder = (from order in db.OrderInternals
                                 where order.CreateDate <= todate
                                 select order).ToList();
                }
            }

            if (!string.IsNullOrEmpty(listStatus))
            {
                if (isFilter)
                {
                    listOrder = listOrder.FindAll(a => a.Status == listStatus);
                }
                else
                {
                    isFilter = true;
                    listOrder = db.OrderInternals.Where(a => a.Status == listStatus).ToList();
                }
            }
            if (!string.IsNullOrEmpty(OrderId))
            {
                if (isFilter)
                {
                    listOrder = listOrder.FindAll(a => a.OrderInternalId.ToString() == OrderId);
                }
                else
                {
                    isFilter = true;
                    listOrder = db.OrderInternals.Where(a => a.OrderInternalId.ToString() == OrderId).ToList();
                }
            }

            ViewBag.ListStatus = GetListStatus(listStatus);

            var listOrderDisplay = new List<OrderInternal>();

            if (isFilter)
            {
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Sale, false))
                {
                    List<string> listUserManage =
                        db.SaleManageClients.Where(a => a.User_Sale == User.Identity.Name)
                            .Select(a => a.User_Client)
                            .ToList();
                    listOrderDisplay.AddRange(
                        listOrder.Where(a => listUserManage.Contains(a.Phone) || string.IsNullOrEmpty(a.SaleManager))
                            .ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Client, false))
                {
                    listOrderDisplay.AddRange(listOrder.Where(a => a.Phone == User.Identity.Name).ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Accounting, false))
                {
                    listOrderDisplay.AddRange(
                        listOrder.Where(
                            a =>
                                a.Status == OrderStatus.SaleConfirm.ToString() ||
                                a.Status == OrderStatus.Receive.ToString())
                            .ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Orderer, false))
                {
                    listOrderDisplay.AddRange(listOrder.Where(a => a.Status == OrderStatus.Paid.ToString() && (a.StatusOder == username || a.StatusOder == null)).ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Recieve, false))
                {
                    listOrderDisplay.AddRange(
                        listOrder.Where(
                            a =>
                                a.Status == OrderStatus.Order.ToString() ||
                                a.Status == OrderStatus.FullCollect.ToString()).ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"]))
                {
                    listOrderDisplay = listOrder.ToList();
                }
            }
            else
            {
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Sale, false))
                {
                    List<string> listUserManage =
                        db.SaleManageClients.Where(a => a.User_Sale == User.Identity.Name)
                            .Select(a => a.User_Client)
                            .ToList();
                    listOrderDisplay.AddRange(
                        db.OrderInternals.Where(a => listUserManage.Contains(a.Phone) || string.IsNullOrEmpty(a.SaleManager))
                            .ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Client, false))
                {
                    listOrderDisplay.AddRange(db.OrderInternals.Where(a => a.Phone == User.Identity.Name).ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Accounting, false))
                {
                    listOrderDisplay.AddRange(
                        db.OrderInternals.Where(
                            a =>
                                a.Status == OrderStatus.SaleConfirm.ToString() ||
                                a.Status == OrderStatus.Receive.ToString())
                            .ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Orderer, false))
                {
                    listOrderDisplay.AddRange(db.OrderInternals.Where(a => a.Status == OrderStatus.Paid.ToString()).ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Recieve, false))
                {
                    listOrderDisplay.AddRange(
                        db.OrderInternals.Where(
                            a =>
                                a.Status == OrderStatus.Order.ToString() ||
                                a.Status == OrderStatus.FullCollect.ToString()).ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"]))
                {
                    listOrderDisplay = db.OrderInternals.ToList();
                }
            }

            listOrderDisplay = listOrderDisplay.Distinct().OrderByDescending(a => a.UpdateDate).ToList();

            ViewBag.CurrentToDate = toDate;
            ViewBag.CurrentFromDate = fromDate;
            ViewBag.CurrentStatus = listStatus;
            ViewBag.CurrentOrderId = OrderId;
            ViewBag.CurrentUserName = username;


            int pageNumber = (page ?? 1);

            return View(listOrderDisplay.ToPagedList(pageNumber, pageSize));
        }
        [HttpPost]
        public ActionResult GetModelOrderById(string orderInternalId)
        {
            if (string.IsNullOrEmpty(orderInternalId))
            {
                return Json(new { success = false });
            }
            var model = new ViewDetailOrderModel();
            var order = db.OrderInternals.FirstOrDefault(a => a.OrderInternalId.ToString() == orderInternalId);
            var orderDetail = db.OrderInternalDetails.Where(a => a.OrderInternalId.ToString() == orderInternalId);
            model.OrderInternalId = Convert.ToInt32(orderInternalId);
            if (order != null)
            {
                model.Phone = order.Phone;
                model.Rate = order.Rate;
                model.Status = order.Status;
                model.StatusText = order.getStatusText();
                model.TotalPriceConvert = order.TotalPriceConvert;
                model.TotalPrice = order.TotalPrice;
                model.SaleManager = order.SaleManager;
                model.UserName = order.UserName;
                model.Phone = order.Phone;
                model.FeeShip = order.FeeShip;
                model.FeeShipChina = order.FeeShipChina;
                model.CreateDate = order.CreateDate;
                model.Fee = order.Fee;
                model.Weight = order.Weight;
                model.DownPayment = order.DownPayment;
                model.AccountingCollected = order.AccountingCollected;

                if (!string.IsNullOrEmpty(order.Phone))
                {
                    UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Phone == order.Phone);
                    if (user != null)
                    {
                        model.Client = user;
                    }
                }
                if (ModelState.IsValid)
                {
                    var news = db.OrderInternals.First(m => m.OrderInternalId.ToString() == orderInternalId);
                    var phone = Session["Phone"];
                    news.StatusOder = phone.ToString();
                    UpdateModel(news);
                    db.SaveChanges();
                }
            }
            model.ListOrderDetails = orderDetail;

            return Json(new { success = true, data = model });

        }
        public ActionResult ViewOrderDetailNew(int id)
        {
            var model = new ViewDetailOrderModelNew();
            var order = db.OrderInternals.FirstOrDefault(a => a.OrderInternalId == id);
            var orderDetail = db.OrderInternalDetails.Where(a => a.OrderInternalId == id);
            model.OrderInternalId = id;
            if (order != null)
            {
                var UserDetail = db.UserProfiles.FirstOrDefault(a => a.Phone == order.Phone);
                var wallet = db.Wallets.FirstOrDefault(a => a.Client == order.Phone);
                if (wallet != null)
                {
                    model.Money = wallet.Money;
                    model.Currency = wallet.Currency;

                }
                if (UserDetail != null)
                {
                    model.Name = UserDetail.Name;
                    model.Email = UserDetail.Email;
                    model.Address = UserDetail.Address;
                }
                //model.Image = orderDetail.First().Image;
                model.Phone = order.Phone;
                model.Rate = order.Rate;
                model.Status = order.Status;
                model.StatusText = order.getStatusText();
                model.TotalPriceConvert = order.TotalPriceConvert;
                model.TotalPrice = order.TotalPrice;
                model.SaleManager = order.SaleManager;
                model.UserName = order.UserName;
                model.Phone = order.Phone;
                model.FeeShip = order.FeeShip;
                model.FeeShipChina = order.FeeShipChina;
                model.CreateDate = order.CreateDate;
                model.Fee = order.Fee;
                model.Weight = order.Weight;
                model.DownPayment = order.DownPayment;
                model.AccountingCollected = order.AccountingCollected;
                model.DownPaymentRate = order.DownPaymentRate;
                model.OrdererManager = order.OrdererManager;
                model.OrderType = order.OrderType;
                model.Owe = order.Owe;
                if (!string.IsNullOrEmpty(order.SaleManager))
                {
                    UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Phone == order.SaleManager);
                    if (user != null)
                    {
                        model.SaleManageInfo = new SaleManageInfo
                        {
                            SaleUserId = user.UserId,
                            SaleName = user.Name,
                            SalePhone = user.Phone,
                            SalePhoneCompany = user.PhoneCompany
                        };
                    }
                }

                if (!string.IsNullOrEmpty(order.Phone))
                {
                    UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Phone == order.Phone);
                    if (user != null)
                    {
                        model.Client = user;
                    }
                }

                if (!string.IsNullOrEmpty(order.OrdererManager))
                {
                    UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Phone == order.OrdererManager);
                    if (user != null)
                    {
                        model.Orderer = user;
                    }
                }

                ViewBag.ListOrderer = GetListOrderer(model.OrdererManager);

            }
            model.ListOrderDetails = orderDetail;
            return View(model);
        }
        public ActionResult ViewOrderDetailNewForUser(int id)
        {
            var model = new ViewDetailOrderModelNew();
            var order = db.OrderInternals.FirstOrDefault(a => a.OrderInternalId == id);
            var orderDetail = db.OrderInternalDetails.Where(a => a.OrderInternalId == id);

            model.OrderInternalId = id;
            if (order != null)
            {
                var UserDetail = db.UserProfiles.FirstOrDefault(a => a.Phone == order.Phone);
                if (UserDetail != null)
                {
                    model.Name = UserDetail.Name;
                    model.Email = UserDetail.Email;
                    model.Address = UserDetail.Address;
                }
                model.Phone = order.Phone;
                model.Rate = order.Rate;
                model.Status = order.Status;
                model.StatusText = order.getStatusText();
                model.TotalPriceConvert = order.TotalPriceConvert;
                model.TotalPrice = order.TotalPrice;
                model.SaleManager = order.SaleManager;
                model.UserName = order.UserName;
                model.Phone = order.Phone;
                model.FeeShip = order.FeeShip;
                model.FeeShipChina = order.FeeShipChina;
                model.CreateDate = order.CreateDate;
                model.Fee = order.Fee;
                model.Weight = order.Weight;
                model.DownPayment = order.DownPayment;
                model.AccountingCollected = order.AccountingCollected;
                model.DownPaymentRate = order.DownPaymentRate;
                model.OrdererManager = order.OrdererManager;

                if (!string.IsNullOrEmpty(order.SaleManager))
                {
                    UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Phone == order.SaleManager);
                    if (user != null)
                    {
                        model.SaleManageInfo = new SaleManageInfo
                        {
                            SaleName = user.Name,
                            SalePhone = user.Phone,
                            SalePhoneCompany = user.PhoneCompany
                        };
                    }
                }

                if (!string.IsNullOrEmpty(order.Phone))
                {
                    UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Phone == order.Phone);
                    if (user != null)
                    {
                        model.Client = user;
                    }
                }

                ViewBag.ListOrderer = GetListOrderer(model.OrdererManager);

            }
            model.ListOrderDetails = orderDetail;
            return View(model);
        }
        #endregion


        #region
        [HttpPost]
        public JsonResult GetListOrder(string KH, string NBD, string NKT, string MaDon, string TrangThai)
        {
            if (!Request.IsAuthenticated)
            {
                return Json(new { success = false });
            }
            if (Session["UserType"] == null || !Utils.CheckRole((string)Session["UserType"], (int)UserType.Orderer))
            {
                return Json(new { success = false });
            }
            var getlistApproval = new List<OrderInternal>();
            var Phone = Session["Phone"];
            getlistApproval = db.OrderInternals.Where(a => (a.Status == OrderStatus.Paid.ToString() && (a.StatusOder == null || a.StatusOder == User.Identity.Name))).ToList();
            if (KH != "" & KH != null)
            {
                getlistApproval = (from a in getlistApproval where a.Phone.Contains(KH) select a).ToList();
            }

            if (MaDon != "" & MaDon != null)
            {
                getlistApproval = (from a in getlistApproval where a.OrderInternalId.ToString() == MaDon select a).ToList();
            }

            if (NBD != null & NBD != "")
            {
                DateTime fromdate = DateTime.ParseExact(NBD, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                getlistApproval = (from order in getlistApproval where fromdate <= order.CreateDate select order).ToList();
            }

            if (NKT != null & NKT != "")
            {
                DateTime todate = DateTime.ParseExact(NKT, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                getlistApproval = (from order in getlistApproval where order.CreateDate <= todate select order).ToList();
            }
            if ((NBD != null & NBD != "") & (NKT != null & NKT != ""))
            {
                DateTime fromdate = DateTime.ParseExact(NBD, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                DateTime todate = DateTime.ParseExact(NKT, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                getlistApproval = (from order in getlistApproval where fromdate <= order.CreateDate && order.CreateDate <= todate select order).ToList();
            }
            if (getlistApproval != null)
            {

                string html = "<table class='table table-bordered table-striped' style='font-size: 10px'>" +
                      "<tr>" +
                           "<th >   <input class='form-control' id='searchKhachhang'  placeholder ='Khách hàng' data-placement='top' data - toggle = 'tooltip' rel = 'tooltip' data - original - title = 'Khách hàng' name = 'username' type = 'text' value ='" + KH + "' ></th>" +
                           " <th >  <input class='form-control' id='searchNBD' placeholder='Từ ngày' data-placement='top' data - toggle = 'tooltip' rel = 'tooltip' data - original - title = 'Khách hàng' name = 'username' type = 'date' value ='" + NBD + "' ></th>" +
                            " <th >  <input class='form-control'  id='searchNKT' placeholder='Đến ngày' data-placement='top' data - toggle = 'tooltip' rel = 'tooltip' data - original - title = 'Khách hàng' name = 'username' type = 'date' value ='" + NKT + "' ></th>" +
                           " <th >  <input class='form-control'  id='searchMaDon' placeholder='Mã đơn' data-placement='top' data - toggle = 'tooltip' rel = 'tooltip' data - original - title = 'Khách hàng' name = 'username' type = 'text' value ='" + MaDon + "'  ></th>" +
                  "  </tr>" +
                    "</ table>" +
                             " <table class='table table-bordered table-striped' style='font-size: 12px'>" +
                          "<li>" +
                            "<tr>" +
                               "<th class='text-center'>Mã đơn</th>" +
                               " <th class='text-center'>Tên Khách</th>" +
                                " <th class='text-center'>Shop</th>" +
                               " <th class='text-center'>Trạng thái</th>" +
                               "<th class='text-center'>Tổng tiền</th>" +
                                " <th class='text-center'>Ngày tạo</th>" +
                              "  </tr>" +
                       " </li>" +
                       " <li> " +
                       "<tbody>" +
                       "<tr>";

                for (int i = 0; i < getlistApproval.Count; i++)
                {
                    html = html +
                                "<td class='text-center'>" + getlistApproval[i].OrderInternalId + "</ td >" +
                                "<td class='text-center'>" + getlistApproval[i].UserName + "</ td >" +
                                "<td class='text-center'>" + getlistApproval[i].IsShop + "</ td >" +
                                "<td class='text-center'>" + getlistApproval[i].getStatusText() + "</ td >" +
                                "<td class='text-center'>" + getlistApproval[i].TotalPriceConvert.ToString("##,###") + "</ td >" +
                                "<td class='text-center'>" + getlistApproval[i].CreateDate.ToString("dd/MM/yyyy") + "</ td >" +
                                " <td class='text-center'>" +
                                "<a rel = 'tooltip' id=" + getlistApproval[i].OrderInternalId + " title='Xem' href='javascript:void(0)' onClick='addtabdemo(" + getlistApproval[i].OrderInternalId + ",false,true);' class='btn btn-xs btn-primary'>" +
                                "Giao dịch đơn hàng</a>" +
                                " </ td >" +
                    " </ tr >" +
                    "</tbody>";
                }

                html = html + " </li> </ table >";




                return Json(new
                {
                    success = true,
                    data = html
                });

            }



            return Json(new
            {
                success = false
            });
        }
        [HttpPost]
        public ActionResult GetModelOrderByIdNew(string orderInternalId)
        {
            if (string.IsNullOrEmpty(orderInternalId))
            {
                return Json(new { success = false });
            }

            var model = new ViewDetailOrderModelNew();
            var order = db.OrderInternals.FirstOrDefault(a => a.OrderInternalId.ToString() == orderInternalId);
            var orderDetail = db.OrderInternalDetails.Where(a => a.OrderInternalId.ToString() == orderInternalId).ToList();
            model.OrderInternalId = Convert.ToInt32(orderInternalId);
            var OderlistWallets = db.Wallets.Where(a => a.Client == User.Identity.Name);
            if (order != null)
            {
                var UserDetail = db.UserProfiles.FirstOrDefault(a => a.Phone == order.Phone);
                if (UserDetail != null)
                {
                    model.Name = UserDetail.Name;
                    model.Email = UserDetail.Email;
                    model.Address = UserDetail.Address;
                }
                if (order.TotalPriceReality == 0 || order.TotalPriceReality > order.TotalPrice)
                {
                    model.PriceReality = 0;
                }
                else
                {
                    model.PriceReality = order.TotalPrice - order.TotalPriceReality;
                }
                model.Phone = order.Phone;
                model.Rate = order.Rate;
                model.Status = order.Status;
                model.StatusText = order.getStatusText();
                model.TotalPriceConvert = order.TotalPriceConvert;
                model.TotalPrice = order.TotalPrice;
                model.SaleManager = order.SaleManager;
                model.UserName = order.UserName;
                model.Phone = order.Phone;
                model.FeeShip = order.FeeShip;
                model.FeeShipChina = order.FeeShipChina;
                model.CreateDate = order.CreateDate;
                model.Fee = order.Fee;
                model.Weight = order.Weight;
                model.DownPayment = order.DownPayment;
                model.TotalPriceReality = order.TotalPriceReality;
                model.AccountingCollected = order.AccountingCollected;
                model.OrdererManager = order.OrdererManager;
                model.TotalPriceClient = order.TotalPriceClient;
                if (!string.IsNullOrEmpty(order.Phone))
                {
                    UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Phone == order.Phone);
                    if (user != null)
                    {
                        model.Client = user;
                    }
                }
                if (ModelState.IsValid)
                {
                    var news = db.OrderInternals.First(m => m.OrderInternalId.ToString() == orderInternalId);
                    var phone = Session["Phone"];
                    if (news.StatusOder == null)
                    {
                        news.StatusOder = phone.ToString();
                        news.OrdererManager = phone.ToString();
                        news.CheckView = true;
                        UpdateModel(news);
                        db.SaveChanges();
                    }

                }
            }
            model.ListOrderDetails = orderDetail.ToList();
            model.ListWallets = OderlistWallets;

            return Json(new { success = true, data = model });

        }
        [HttpPost]
        public ActionResult UpdateOrderLink(string value)
        {
            var list = JsonConvert.DeserializeObject<UpdateOrderLink>(value);

            if (string.IsNullOrEmpty(value))
            {
                return Json(new { success = false });
            }
            if (ModelState.IsValid)
            {
                var news = db.OrderInternalDetails.FirstOrDefault(m => m.OrderInternalDetailId == list.UorderId);


                if (news != null)
                {
                    news.Link = list.ULink;
                    news.Color = list.UColor;
                    news.Size = list.USize;
                    news.NoteClient = list.UGhichuKhach;
                    news.NoteSale = list.UGhichuSale;
                    news.QuantitySellPlace = list.USLTT;
                    news.PriceReality = list.UDonGiaTT;
                    UpdateModel(news);
                    db.SaveChanges();
                    OrderInternalDetail orderDetail = db.OrderInternalDetails.FirstOrDefault(m => m.OrderInternalDetailId == list.UorderId);
                    double totalPrice = 0;
                    var listOrderDetail = db.OrderInternalDetails.Where(
                                a =>
                                    a.OrderInternalId == list.UOrderInternalId &&
                                    a.OrderDetailStatus == OrderDetailStatus.Active.ToString()).ToList();
                    if (listOrderDetail.Count > 0)
                    {
                        totalPrice = listOrderDetail.Sum(a => (a.QuantitySellPlace ?? 0) * (a.PriceReality ?? 0));
                    }
                    OrderInternal ord = db.OrderInternals.FirstOrDefault(a => a.OrderInternalId == list.UOrderInternalId);

                    if (ord != null)
                    {
                        ord.TotalPriceClient = totalPrice;
                        ord.TotalPriceReality = totalPrice;
                        ord.TotalPriceConvert = ord.Rate * (ord.TotalPriceReality);
                        db.SaveChanges();

                    }
                    return Json(new { success = true });

                }


                return Json(new { success = false });

            }

            return Json(new { success = false });
        }
        public ActionResult ComfrimOrder(string value)
        {
            var list = JsonConvert.DeserializeObject<ComfirmOrder>(value);

            if (string.IsNullOrEmpty(value))
            {
                return Json(new { success = false });
            }

            if (ModelState.IsValid)
            {
                var news = db.OrderInternals.First(m => m.OrderInternalId == list.CIdOrder);
                if (list.CTotalMoney == list.CIDPriceRealityEdit)
                {
                    var mmdmdmdmdm = (list.CIDPriceRealityEdit + Math.Round(((list.CIDPriceRealityEdit) * (list.CIDFee)) / 100) + list.CIDFeeShipChina);
                    var checks = db.Wallets.FirstOrDefault(m => m.Id == list.IdW);
                    if (checks.Money >= mmdmdmdmdm)
                    {
                        checks.Money = checks.Money - mmdmdmdmdm;
                        UpdateModel(checks);
                        db.SaveChanges();
                        var wallerHistory = new WalletHistory
                        {
                            WalletId = checks.Id,
                            Money = checks.Money,
                            UpdateType = checks.Money <= 0 ? WalletUpdateType.Addition.ToString() : WalletUpdateType.Subtract.ToString(),
                            Client = checks.Client,
                            Currency = checks.Currency,
                            User_Update = User.Identity.Name,
                            LastUpdate = DateTime.Now,
                            Reason = "Trừ tiền"
                        };
                        db.WalletHistorys.Add(wallerHistory);

                    }
                    else
                    {
                        return Json(new { success = false });
                    }
                }
                if (list.CTotalMoney > list.CIDPriceRealityEdit)
                {
                    var mmdmdmdmdm = (list.CIDPriceRealityEdit + Math.Round(((list.CIDPriceRealityEdit) * (list.CIDFee)) / 100) + list.CIDFeeShipChina);
                    var checks = db.Wallets.FirstOrDefault(m => m.Id == list.IdW);
                    if (checks.Money >= mmdmdmdmdm)
                    {
                        checks.Money = checks.Money - mmdmdmdmdm;
                        UpdateModel(checks);
                        db.SaveChanges();
                        var wallerHistory = new WalletHistory
                        {
                            WalletId = checks.Id,
                            Money = checks.Money,
                            UpdateType = checks.Money >= 0 ? WalletUpdateType.Addition.ToString() : WalletUpdateType.Subtract.ToString(),
                            Client = checks.Client,
                            Currency = checks.Currency,
                            User_Update = User.Identity.Name,
                            LastUpdate = DateTime.Now,
                            Reason = "Trừ tiền"
                        };
                        db.WalletHistorys.Add(wallerHistory);

                    }
                    else
                    {
                        return Json(new { success = false });
                    }
                }
                if (list.CTotalMoney < list.CIDPriceRealityEdit)
                {
                    var mmdmdmdmdm = (list.CIDPriceRealityEdit + Math.Round(((list.CIDPriceRealityEdit) * (list.CIDFee)) / 100) + list.CIDFeeShipChina);
                    var checks = db.Wallets.FirstOrDefault(m => m.Id == list.IdW);
                    if (checks.Money >= mmdmdmdmdm)
                    {
                        checks.Money = checks.Money - mmdmdmdmdm;
                        UpdateModel(checks);
                        db.SaveChanges();
                        var wallerHistory = new WalletHistory
                        {
                            WalletId = checks.Id,
                            Money = checks.Money,
                            UpdateType = checks.Money >= 0 ? WalletUpdateType.Addition.ToString() : WalletUpdateType.Subtract.ToString(),
                            Client = checks.Client,
                            Currency = checks.Currency,
                            User_Update = User.Identity.Name,
                            LastUpdate = DateTime.Now,
                            Reason = "Trừ tiền"
                        };
                        db.WalletHistorys.Add(wallerHistory);

                    }
                    else
                    {
                        return Json(new { success = false });
                    }
                }

                if (news != null)
                {
                    if (list.CTotalMoney == list.CIDPriceRealityEdit)
                    {
                        news.TotalPriceReality = list.CTotalMoney;
                    }
                    if (list.CTotalMoney > list.CIDPriceRealityEdit)
                    {
                        news.TotalPriceReality = list.CIDPriceRealityEdit;
                    }
                    var moneyclient = (list.CTotalMoney + Math.Round(((list.CTotalMoney) * (list.CIDFee)) / 100) + list.CIDFeeShipChina);
                    if (list.CIDPriceRealityEdit == list.CTotalMoney)
                    {
                        double moneyc = list.CIDPriceRealityEdit;

                        news.Status = "Order";
                        news.FeeShipChina = list.CIDFeeShipChina;
                        news.Fee = list.CIDFee;
                        news.ShippingCode = list.CIDShippingCode;
                        news.TransId = list.CIDTransId;
                        news.NoteOrder = list.COrderComment;
                        news.UpdateDate = DateTime.Now;
                        news.TotalPriceClient = moneyclient;
                        news.TotalPriceConvert = moneyclient * news.Rate;
                        news.TotalPriceClient = moneyc;
                        UpdateModel(news);
                        db.SaveChanges();
                    }
                    return Json(new { success = true });
                }

                return Json(new { success = false });
            }

            return Json(new { success = false });
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult MisOrderOrder(string orderId)
        {
            var order = db.OrderInternals.FirstOrDefault(m => m.OrderInternalId.ToString() == orderId);
            if (order != null)
            {
                order.Status = OrderStatus.MisOrder.ToString();
                db.SaveChanges();
            }
            return Json(new { success = true });
        }
        [Authorize]
        public ActionResult ListOdrerFinish(string username, string listStatus, string fromDate, string toDate, string OrderId,
       int? page)
        {
            var listOrder = new List<OrderInternal>();
            bool isFilter = false;
            if (!string.IsNullOrEmpty(username))
            {
                isFilter = true;
                listOrder = db.OrderInternals.Where(a => a.Phone == username).ToList();
            }

            if (!string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate))
            {
                DateTime fromdate = DateTime.ParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime todate = DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                if (isFilter)
                {
                    listOrder = (from order in listOrder
                                 where fromdate <= order.CreateDate && order.CreateDate <= todate
                                 select order).ToList();
                }
                else
                {
                    isFilter = true;
                    listOrder = (from order in db.OrderInternals
                                 where fromdate <= order.CreateDate && order.CreateDate <= todate
                                 select order).ToList();
                }
            }

            if (!string.IsNullOrEmpty(listStatus))
            {
                if (isFilter)
                {
                    listOrder = listOrder.FindAll(a => a.Status == listStatus);
                }
                else
                {
                    isFilter = true;
                    listOrder = db.OrderInternals.Where(a => a.Status == listStatus).ToList();
                }
            }
            if (!string.IsNullOrEmpty(OrderId))
            {
                if (isFilter)
                {
                    listOrder = listOrder.FindAll(a => a.OrderInternalId.ToString() == OrderId);
                }
                else
                {
                    isFilter = true;
                    listOrder = db.OrderInternals.Where(a => a.OrderInternalId.ToString() == OrderId).ToList();
                }
            }

            ViewBag.ListStatus = GetListStatus(listStatus);

            var listOrderDisplay = new List<OrderInternal>();

            if (!Request.IsAuthenticated)
            {
                return Json(new { success = false });
            }
            if (Session["UserType"] == null || !Utils.CheckRole((string)Session["UserType"], (int)UserType.Orderer))
            {
                return Json(new { success = false });
            }
            var Phone = Session["Phone"];

            listOrderDisplay = db.OrderInternals.Where(a => a.Status == OrderStatus.Order.ToString() && (a.StatusOder == Phone) && (a.ShippingCode != null)).ToList();

            if (listOrderDisplay != null)
                listOrderDisplay = listOrderDisplay.Distinct().OrderByDescending(a => a.UpdateDate).ToList();

            ViewBag.CurrentToDate = toDate;
            ViewBag.CurrentFromDate = fromDate;
            ViewBag.CurrentStatus = listStatus;
            ViewBag.CurrentOrderId = OrderId;
            ViewBag.CurrentUserName = username;


            int pageNumber = (page ?? 1);

            return View(listOrderDisplay.ToPagedList(pageNumber, pageSize));
        }


        public ActionResult ComfrimOrderApply(string value)
        {
            var list = JsonConvert.DeserializeObject<ComfirmOrderApply>(value);

            if (string.IsNullOrEmpty(value))
            {
                return Json(new { success = false });
            }

            if (ModelState.IsValid)
            {
                var news = db.OrderInternals.First(m => m.OrderInternalId == list.CIdOrder);
                if (news != null)
                {
                    news.ShippingCode = list.CIDShippingCode;
                    news.TransId = list.CIDTransId;
                    news.NoteOrder = list.COrderComment;
                    UpdateModel(news);
                    db.SaveChanges();
                    return Json(new { success = true });
                }
                return Json(new { success = false });
            }

            return Json(new { success = false });
        }
        public ActionResult DeleteOrderLink(string value)
        {
            var list = JsonConvert.DeserializeObject<UpdateOrderLink>(value);

            if (string.IsNullOrEmpty(value))
            {
                return Json(new { success = false });
            }
            if (ModelState.IsValid)
            {
                var news = db.OrderInternalDetails.First(m => m.OrderInternalId == list.UOrderInternalId);


                if (news != null)
                {

                    db.OrderInternalDetails.Remove(news);
                    db.SaveChanges();
                    return Json(new { success = true });
                }
                return Json(new { success = false });
            }

            return Json(new { success = false });
        }

        [HttpPost]
        public JsonResult GetListOrderMoney(string NBD, string NKT)
        {
            if (!Request.IsAuthenticated)
            {
                return Json(new { success = false });
            }
            if (Session["UserType"] == null || !Utils.CheckRole((string)Session["UserType"], (int)UserType.Orderer))
            {
                return Json(new { success = false });
            }
            double totalPrice = 0;

            var getlistApproval = new List<OrderInternal>();
            var Phone = Session["Phone"];
            getlistApproval = db.OrderInternals.Where(a => (a.Status == OrderStatus.Order.ToString() && a.StatusOder == User.Identity.Name && a.TotalPriceReality < a.TotalPriceClient)).ToList();

            totalPrice = getlistApproval.Sum(a => (a.TotalPriceClient) - (a.TotalPriceReality));

            if (NBD != null & NBD != "")
            {
                DateTime fromdate = DateTime.ParseExact(NBD, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                getlistApproval = (from order in getlistApproval where fromdate <= order.CreateDate select order).ToList();

                totalPrice = getlistApproval.Sum(a => (a.TotalPriceClient) - (a.TotalPriceReality));
            }

            if (NKT != null & NKT != "")
            {
                DateTime todate = DateTime.ParseExact(NKT, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                getlistApproval = (from order in getlistApproval where order.CreateDate <= todate select order).ToList();

                totalPrice = getlistApproval.Sum(a => (a.TotalPriceClient) - (a.TotalPriceReality));
            }
            if ((NBD != null & NBD != "") & (NKT != null & NKT != ""))
            {
                DateTime fromdate = DateTime.ParseExact(NBD, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                DateTime todate = DateTime.ParseExact(NKT, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                getlistApproval = (from order in getlistApproval where fromdate <= order.CreateDate && order.CreateDate <= todate select order).ToList();
                totalPrice = getlistApproval.Sum(a => (a.TotalPriceClient) - (a.TotalPriceReality));
            }
            if (getlistApproval != null)
            {

                string html = "<table class='table table-bordered role='dialog' table-striped' style='font-size: 10px'>" +
                      "<tr>" +

                           " <th >  <input class='form-control' id='searchNBD' placeholder='Từ ngày' data-placement='top' data - toggle = 'tooltip' rel = 'tooltip' data - original - title = 'Khách hàng' name = 'username' type = 'date' value ='" + NBD + "' ></th>" +
                            " <th >  <input class='form-control'  id='searchNKT' placeholder='Đến ngày' data-placement='top' data - toggle = 'tooltip' rel = 'tooltip' data - original - title = 'Khách hàng' name = 'username' type = 'date' value ='" + NKT + "' ></th>" +
                        "<th>Tổng tiền: " + totalPrice.ToString("##,###") + "   </th>" +
                  "  </tr>" +
                    "</ table>" +
                             " <table class='table table-bordered table-striped' style='font-size: 12px'>" +
                          "<li>" +
                            "<tr>" +
                               "<th class='text-center'>Mã đơn</th>" +
                               " <th class='text-center'>Tên Khách</th>" +
                               " <th class='text-center'>Trạng thái</th>" +
                               "<th class='text-center'>Tổng tiền Khách trả</th>" +
                                    "<th class='text-center'>Tổng Tiền thực tế</th>" +
                                "<th class='text-center'>Số dư</th>" +
                                " <th class='text-center'>Ngày giao dịch</th>" +
                              "  </tr>" +
                       " </li>" +
                       " <li> " +
                       "<tbody>" +
                       "<tr>";

                for (int i = 0; i < getlistApproval.Count; i++)
                {
                    double money = ((getlistApproval[i].TotalPriceReality) - (getlistApproval[i].TotalPriceReality));
                    html = html +
                                "<td class='text-center'>" + getlistApproval[i].OrderId + "</ td >" +
                                "<td class='text-center'>" + getlistApproval[i].UserName + "</ td >" +
                                "<td class='text-center'>" + getlistApproval[i].getStatusText() + "</ td >" +
                                "<td class='text-center'>" + getlistApproval[i].TotalPriceClient.ToString("##,###") + "</ td >" +
                                "<td class='text-center'>" + getlistApproval[i].TotalPriceReality.ToString("##,###") + "</ td >" +
                                "<td class='text-center'>" + money + "</ td >" +
                                "<td class='text-center'>" + getlistApproval[i].UpdateDate.ToString("dd/MM/yyyy") + "</ td >" +
                                " <td class='text-center'>" +
                                "<a rel = 'tooltip' id=" + getlistApproval[i].OrderInternalId + " title='Xem' href='ViewOrderDetailUpdate/" + getlistApproval[i].OrderInternalId + "' onClick='addtabdemo(" + getlistApproval[i].OrderInternalId + ",false,true);' class='btn btn-xs btn-primary'>" +
                                "Xem chi tiết</a>" +
                                " </ td >" +
                    " </ tr >" +
                    "</tbody>";
                }

                html = html + " </li> </ table >";




                return Json(new
                {
                    success = true,
                    data = html
                });

            }



            return Json(new
            {
                success = false
            });
        }
        public ActionResult ViewOrderDetailUpdate(string id)
        {

            var model = new ViewDetailOrderModelNew();
            var order = db.OrderInternals.FirstOrDefault(a => a.OrderInternalId.ToString() == id);
            var orderDetail = db.OrderInternalDetails.Where(a => a.OrderInternalId.ToString() == id);
            model.OrderInternalId = Convert.ToInt32(id);
            var OderlistWallets = db.Wallets.Where(a => a.Client == User.Identity.Name);
            if (order != null)
            {
                var UserDetail = db.UserProfiles.FirstOrDefault(a => a.Phone == order.Phone);
                if (UserDetail != null)
                    if (order.TotalPriceReality == 0 || order.TotalPriceReality > order.TotalPrice)
                    {
                        model.PriceReality = 0;
                    }
                    else
                    {
                        model.PriceReality = order.TotalPrice - order.TotalPriceReality;
                    }
                {
                    model.Name = UserDetail.Name;
                    model.Email = UserDetail.Email;
                    model.Address = UserDetail.Address;
                }
                model.Phone = order.Phone;
                model.Rate = order.Rate;
                model.Status = order.Status;
                model.StatusText = order.getStatusText();
                model.TotalPriceConvert = order.TotalPriceConvert;
                model.TotalPrice = order.TotalPrice;
                model.SaleManager = order.SaleManager;
                model.UserName = order.UserName;
                model.Phone = order.Phone;
                model.FeeShip = order.FeeShip;
                model.FeeShipChina = order.FeeShipChina;
                model.CreateDate = order.CreateDate;
                model.Fee = order.Fee;
                model.Weight = order.Weight;
                model.DownPayment = order.DownPayment;
                model.AccountingCollected = order.AccountingCollected;
                model.TotalPriceReality = order.TotalPriceReality;
                model.TotalPriceClient = order.TotalPriceClient;
                model.TransId = order.TransId;
                if (!string.IsNullOrEmpty(order.Phone))
                {
                    UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Phone == order.Phone);
                    if (user != null)
                    {
                        model.Client = user;
                    }
                }

            }
            model.ListOrderDetails = orderDetail;
            model.ListWallets = OderlistWallets;

            return View(model);

        }
        public ActionResult ViewPriceReality()
        {

            var model = new ViewDetailOrderMoney();
            double totalPrice = 0;
            var Phone1 = Session["Phone"];

            var ListOrderInternalDetails1 = db.OrderInternals.Where(a => a.StatusOder == User.Identity.Name && a.TotalPriceClient > a.TotalPriceReality).ToList();
            if (ListOrderInternalDetails1.Count > 0)
            {
                totalPrice = ListOrderInternalDetails1.Sum(a => (a.TotalPriceClient) - (a.TotalPriceReality));
                model.Money = totalPrice;
            }
            return View(model);

        }
        [HttpPost]
        public JsonResult GetListOrderNotification()
        {
            if (!Request.IsAuthenticated)
            {
                return Json(new { success = false });
            }
            if (Session["UserType"] == null || !Utils.CheckRole((string)Session["UserType"], (int)UserType.Orderer))
            {
                return Json(new { success = false });
            }
            DateTime today = DateTime.Now;
            DateTime answer = today.AddDays(-5);
            var getlistApproval = new List<OrderInternal>();
            var Phone = Session["Phone"];
            getlistApproval = db.OrderInternals.Where(a => (a.Status == OrderStatus.Order.ToString() && a.UpdateDate <= answer && a.TransId != null && a.StatusOder == User.Identity.Name)).ToList();
            if (getlistApproval != null)
            {

                string html = "<table class='table table-bordered table-striped'  style='font-size: 10px'>" +
                    "</ table>" +
                             " <table class='table table-bordered table-striped' style='font-size: 12px'>" +
                          "<li>" +
                            "<tr>" +
                               "<th class='text-center'>Mã đơn</th>" +
                               " <th class='text-center'>Tên Khách</th>" +
                                " <th class='text-center'>Shop</th>" +
                               " <th class='text-center'>Trạng thái</th>" +
                               "<th class='text-center'>Tổng tiền</th>" +
                                " <th class='text-center'>Ngày đặt</th>" +
                              "  </tr>" +
                       " </li>" +
                       " <li> " +
                       "<tbody>" +
                       "<tr>";

                for (int i = 0; i < getlistApproval.Count; i++)
                {
                    html = html +
                                "<td class='text-center'>" + getlistApproval[i].OrderId + "</ td >" +
                                "<td class='text-center'>" + getlistApproval[i].UserName + "</ td >" +
                                "<td class='text-center'>" + getlistApproval[i].IsShop + "</ td >" +
                                "<td class='text-center'>" + getlistApproval[i].getStatusText() + "</ td >" +
                                "<td class='text-center'>" + getlistApproval[i].TotalPriceConvert.ToString("##,###") + "</ td >" +
                                "<td class='text-center'>" + getlistApproval[i].UpdateDate.ToString("dd/MM/yyyy") + "</ td >" +
                                " <td class='text-center'>" +
                                "<a rel = 'tooltip' id=" + getlistApproval[i].OrderInternalId + " title='Xem' href='ViewOrderDetailUpdate/" + getlistApproval[i].OrderInternalId + "' onClick='addtabdemo(,false,true);' class='btn btn-xs btn-primary'>" +
                                "Giao dịch đơn hàng</a>" +
                                " </ td >" +
                    " </ tr >" +
                    "</tbody>";
                }

                html = html + " </li> </ table >";
                return Json(new
                {
                    success = true,
                    data = html
                });

            }
            return Json(new
            {
                success = false
            });
        }
        [HttpPost]
        public JsonResult GetListOrderWallet(int IDO)
        {
            if (!Request.IsAuthenticated)
            {
                return Json(new { success = false });
            }
            if (Session["UserType"] == null || !Utils.CheckRole((string)Session["UserType"], (int)UserType.Orderer))
            {
                return Json(new { success = false });
            }
            var getlistApproval = new List<Wallet>();
            getlistApproval = db.Wallets.Where(a => a.Client == User.Identity.Name).ToList();


            if (getlistApproval != null)
            {

                string html = "<table class='table table-bordered table-striped' style='font-size: 10px'>" +
                    "</ table>" +
                             " <table class='table table-bordered table-striped' style='font-size: 12px'>" +

                            "<tr>" +
                               "<th class='text-center'>Tên Ngân Hàng</th>" +
                               " <th class='text-center'>Số tài khoản</th>" +
                                " <th class='text-center'>Số tiền</th>" +
                                " <th class='text-center'></th>" +
                              "  </tr>" +


                       "<tbody>" +
                       "<tr>";

                for (int i = 0; i < getlistApproval.Count; i++)
                {
                    html = html +
                                "<td class='text-center'>" + getlistApproval[i].Name + "</ td >" +
                                "<td class='text-center'>" + getlistApproval[i].Account + "</ td >" +
                                "<td class='text-center'>" + getlistApproval[i].Money.ToString("##,###") + "</ td >" +
                                 "<td class='text-center'>" +
                                "<button class='btn btn-primary' onclick='ComfrimOrder(" + getlistApproval[i].Id + ", " + IDO + "); '>Giao Dịch</button>" +

                                " </ td >" +
                    " </ tr >" +
                    "</tbody>";
                }

                html = html + " </ table >";




                return Json(new
                {
                    success = true,
                    data = html
                });

            }



            return Json(new
            {
                success = false
            });
        }

        [Authorize]
        public ActionResult ListOdrerComfirm(string username, string listStatus, string fromDate, string toDate, string OrderId,
          int? page)
        {
            var listOrder = new List<OrderInternal>();
            bool isFilter = false;
            if (!string.IsNullOrEmpty(username))
            {
                isFilter = true;
                listOrder = db.OrderInternals.Where(a => a.Phone == username).ToList();
            }

            if (!string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate))
            {
                DateTime fromdate = DateTime.ParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime todate = DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(1);
                if (isFilter)
                {
                    listOrder = (from order in listOrder
                                 where fromdate <= order.CreateDate && order.CreateDate <= todate
                                 select order).ToList();
                }
                else
                {
                    isFilter = true;
                    listOrder = (from order in db.OrderInternals
                                 where fromdate <= order.CreateDate && order.CreateDate <= todate
                                 select order).ToList();
                }
            }

            if (!string.IsNullOrEmpty(listStatus))
            {
                if (isFilter)
                {
                    listOrder = listOrder.FindAll(a => a.Status == listStatus);
                }
                else
                {
                    isFilter = true;
                    listOrder = db.OrderInternals.Where(a => a.Status == listStatus).ToList();
                }
            }
            if (!string.IsNullOrEmpty(OrderId))
            {
                if (isFilter)
                {
                    listOrder = listOrder.FindAll(a => a.OrderInternalId.ToString() == OrderId);
                }
                else
                {
                    isFilter = true;
                    listOrder = db.OrderInternals.Where(a => a.OrderInternalId.ToString() == OrderId).ToList();
                }
            }

            ViewBag.ListStatus = GetListStatus(listStatus);

            var listOrderDisplay = new List<OrderInternal>();

            if (!Request.IsAuthenticated)
            {
                return Json(new { success = false });
            }
            if (Session["UserType"] == null || !Utils.CheckRole((string)Session["UserType"], (int)UserType.Orderer))
            {
                return Json(new { success = false });
            }
            var Phone = Session["Phone"];

            listOrderDisplay = db.OrderInternals.Where(a => a.Status == OrderStatus.Order.ToString() && (a.StatusOder == Phone)).ToList();

            if (listOrderDisplay != null)
                listOrderDisplay = listOrderDisplay.Distinct().OrderByDescending(a => a.UpdateDate).ToList();

            ViewBag.CurrentToDate = toDate;
            ViewBag.CurrentFromDate = fromDate;
            ViewBag.CurrentStatus = listStatus;
            ViewBag.CurrentOrderId = OrderId;
            ViewBag.CurrentUserName = username;


            int pageNumber = (page ?? 1);

            return View(listOrderDisplay.ToPagedList(pageNumber, pageSize));
        }
        public ActionResult ListOdrerCancel(string username, string listStatus, string fromDate, string toDate, string OrderId,
         int? page)
        {
            var listOrder = new List<OrderInternal>();
            bool isFilter = false;
            if (!string.IsNullOrEmpty(username))
            {
                isFilter = true;
                listOrder = db.OrderInternals.Where(a => a.Phone == username).ToList();
            }

            if (!string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate))
            {
                DateTime fromdate = DateTime.ParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime todate = DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(1);
                if (isFilter)
                {
                    listOrder = (from order in listOrder
                                 where fromdate <= order.CreateDate && order.CreateDate <= todate
                                 select order).ToList();
                }
                else
                {
                    isFilter = true;
                    listOrder = (from order in db.OrderInternals
                                 where fromdate <= order.CreateDate && order.CreateDate <= todate
                                 select order).ToList();
                }
            }

            if (!string.IsNullOrEmpty(listStatus))
            {
                if (isFilter)
                {
                    listOrder = listOrder.FindAll(a => a.Status == listStatus);
                }
                else
                {
                    isFilter = true;
                    listOrder = db.OrderInternals.Where(a => a.Status == listStatus).ToList();
                }
            }
            if (!string.IsNullOrEmpty(OrderId))
            {
                if (isFilter)
                {
                    listOrder = listOrder.FindAll(a => a.OrderInternalId.ToString() == OrderId);
                }
                else
                {
                    isFilter = true;
                    listOrder = db.OrderInternals.Where(a => a.OrderInternalId.ToString() == OrderId).ToList();
                }
            }

            ViewBag.ListStatus = GetListStatus(listStatus);

            var listOrderDisplay = new List<OrderInternal>();

            if (!Request.IsAuthenticated)
            {
                return Json(new { success = false });
            }
            if (Session["UserType"] == null || !Utils.CheckRole((string)Session["UserType"], (int)UserType.Orderer))
            {
                return Json(new { success = false });
            }
            var Phone = Session["Phone"];

            listOrderDisplay = db.OrderInternals.Where(a => a.Status == OrderStatus.Cancel.ToString() && (a.StatusOder == Phone)).ToList();

            if (listOrderDisplay != null)
                listOrderDisplay = listOrderDisplay.Distinct().OrderByDescending(a => a.UpdateDate).ToList();

            ViewBag.CurrentToDate = toDate;
            ViewBag.CurrentFromDate = fromDate;
            ViewBag.CurrentStatus = listStatus;
            ViewBag.CurrentOrderId = OrderId;
            ViewBag.CurrentUserName = username;


            int pageNumber = (page ?? 1);

            return View(listOrderDisplay.ToPagedList(pageNumber, pageSize));
        }
        private SelectList GetListOrderer(string valueSelect = "")
        {
            var list = db.UserProfiles.Where(a => a.UserType.Contains(((int)UserType.Orderer).ToString())).Select(a => new { a.Phone, a.Name });

            return new SelectList(list, "Phone", "Name", valueSelect);
        }

        #endregion

        #region ManagerUserofSale
        public ActionResult ChangeUserofSale(string id)
        {
            UserProfile user = db.UserProfiles.First(m => m.Phone == id);
            return View(user);
        }
        [HttpPost]
        public ActionResult ChangeUserofSale(string id, FormCollection collection, Historychangeuser model)
        {
            if (ModelState.IsValid)
            {
                UserProfile user = db.UserProfiles.First(m => m.Phone == id);
                UserProfile sale = db.UserProfiles.First(a => a.Phone == User.Identity.Name);
                UpdateModel(user);
                db.SaveChanges();

                if (user != null)
                {
                    model.Datetime = DateTime.Now;
                    model.SaleChange = sale.Email;
                    model.UserName = user.Email;
                    model.TypeChange = TypeChange.ChangeUser.ToString();
                    db.Historychangeusers.Add(model);
                    db.SaveChanges();
                }
            }
            return RedirectToAction("ListClient");
        }
        public ActionResult HistoryChanageUserofSale(string userName, int? page)
        {
            var history = (from a in db.Historychangeusers select a).ToList();
            if (userName != null)
            {
                history = (from a in db.Historychangeusers where a.UserName == userName select a).ToList();

            }
            int pageNumber = (page ?? 1);
            return View(history.ToPagedList(pageNumber, pageSize));
        }
        #endregion


        #region Shipment
        public ActionResult ManageShipment(int? page, string Message)
        {
            var listShipment = db.Shipments.ToList();
            int pageNumber = (page ?? 1);
            ViewBag.MessageError = Message;
            return View(listShipment.ToPagedList(pageNumber, pageSize));
        }
        public ActionResult CreateShipmentDetail()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateShipmentDetail(Shipment model, string MessageError)
        {
            var day = DateTime.Now;

            model.LastUpdate = day;
            model.Volume = model.Length * model.Height * model.Width;

            double weight = model.Weight;
            double volume = model.Volume;
            if (model.getShipmentTypeText() != null)
            {
                if (model.ShipmentType == 1)
                {
                    if (db.Feeshipments.Count() != 0)
                    {
                        if (db.Feeshipments.Where(a => a.From <= weight && a.To >= weight && a.ShipmentType == 1).Count() > 0)
                        {
                            var feekg =
                           db.Feeshipments.Where(a => a.From <= weight && a.To >= weight && a.ShipmentType == 1)
                               .OrderByDescending(a => a.Value).First();
                            double PriceKg = model.Weight * Convert.ToDouble(feekg.Value);
                            if (db.Feeshipments.Where(a => a.From <= volume && a.To >= volume && a.ShipmentType == 1).Count() > 0)
                            {
                                var feeVolume =
                                    db.Feeshipments.Where(a => a.From <= volume && a.To >= volume && a.ShipmentType == 1)
                                        .OrderByDescending(a => a.Value).First();

                                double PriceVolume = model.Volume * Convert.ToDouble(feeVolume.Value);
                                if (PriceKg > PriceVolume)
                                {
                                    model.PriceFee = PriceKg;
                                }
                                else
                                {
                                    model.PriceFee = PriceVolume;
                                }
                                db.Shipments.Add(model);
                                db.SaveChanges();

                            }
                            else
                            {
                                MessageError = "Thể tích của lô hàng không nằm trong khoảng tính phí lô hàng. Hãy thêm phí lô hàng.";
                            }
                        }
                        else
                        {
                            MessageError = "Khối lượng của lô hàng không nằm trong khoảng tính phí lô hàng. Hãy thêm phí lô hàng.";
                        }
                    }
                }
                else if (model.ShipmentType == 2)
                {
                    if (db.Feeshipments.Count() != 0)
                    {
                        if (db.Feeshipments.Where(a => a.From <= weight && a.To >= weight && a.ShipmentType == 2).Count() > 0)
                        {
                            var feekg =
                           db.Feeshipments.Where(a => a.From <= weight && a.To >= weight && a.ShipmentType == 2)
                               .OrderByDescending(a => a.Value).First();
                            double PriceKg = model.Weight * Convert.ToDouble(feekg.Value);
                            if (db.Feeshipments.Where(a => a.From <= volume && a.To >= volume && a.ShipmentType == 2).Count() > 0)
                            {
                                var feeVolume =
                                    db.Feeshipments.Where(a => a.From <= volume && a.To >= volume && a.ShipmentType == 2)
                                        .OrderByDescending(a => a.Value).First();

                                double PriceVolume = model.Volume * Convert.ToDouble(feeVolume.Value);
                                if (PriceKg > PriceVolume)
                                {
                                    model.PriceFee = PriceKg;
                                }
                                else
                                {
                                    model.PriceFee = PriceVolume;
                                }
                                db.Shipments.Add(model);
                                db.SaveChanges();

                            }
                            else
                            {
                                MessageError = "Thể tích của lô hàng không nằm trong khoảng tính phí lô hàng. Hãy thêm phí lô hàng.";
                            }
                        }
                        else
                        {
                            MessageError = "Khối lượng của lô hàng không nằm trong khoảng tính phí lô hàng. Hãy thêm phí lô hàng.";
                        }
                    }
                }

                else
                {
                    MessageError = "Không có phí lô hàng. Hãy thêm phí lô hàng trước khi tạo mới lô hàng.";
                }
            }

            return RedirectToAction("ManageShipment", new { Message = MessageError }); ;
        }

        private double CalcualtorShipment(double weight, double size, int shipType)
        {
            try
            {
                var listShipmentType = db.Feeshipments.Where(c => c.ShipmentType == shipType).ToList();
                var listShipmentWeight = listShipmentType.FindAll(c => c.UnitType == (int)UnitType.Weight).OrderBy(c => c.From).ToList();
                var listShipmentSize = listShipmentType.FindAll(c => c.UnitType == (int)UnitType.Size).OrderBy(c => c.From).ToList();

                double weightFee = 0;
                double sizeFee = 0;

                for (var i = 0; i < listShipmentWeight.Count; i++)
                {
                    var shipment = listShipmentWeight[i];
                    if (shipment.From <= weight && shipment.To >= weight)
                    {
                        weightFee = shipment.Value;
                        break;
                    }
                }

                for (var i = 0; i < listShipmentSize.Count; i++)
                {
                    var shipment = listShipmentSize[i];
                    if (shipment.From <= size && shipment.To >= size)
                    {
                        sizeFee = shipment.Value;
                        break;
                    }
                }

                return weightFee > sizeFee ? weightFee : sizeFee;

            }
            catch (Exception ex)
            {
            }
            return 0;
        }
        [HttpGet]
        public ActionResult AccountShipmentDetail(int id)
        {
            var model = new AccountShipmentModel();
            var shipment = db.Shipments.FirstOrDefault(a => a.ShipmentId == id);
            if (shipment == null)
            {

            }
            else
            {
                var amountWeight = CalcualtorShipment(shipment.Weight, shipment.Width * shipment.Height * shipment.Length, shipment.ShipmentType);

                model = new AccountShipmentModel
                {
                    IdShipment = shipment.ShipmentId,
                    Name = shipment.Name,
                    Height = shipment.Height,
                    Weight = shipment.Weight,
                    Width = shipment.Width,
                    Length = shipment.Length,
                    ShipType = shipment.ShipmentType == (int)ShipmentType.Byroad ? "Tiều ngạch" : "Đồi",
                    Amount = amountWeight.ToString("N0"),
                    Explain = string.Format("Thanh toán {0} tiền phí vận chuyển cho lô hàng có tên: {1} và Mã: {2}", amountWeight.ToString("N0"), shipment.Name, shipment.ShipmentId)
                };
                ViewBag.ListAccount = GetListBankName(null, "VND");
            }


            return View(model);
        }
        [HttpPost]
        public ActionResult AccountShipmentDetail(AccountShipmentModel model)
        {
            if (string.IsNullOrWhiteSpace(model.AccountType))
                return RedirectToAction("ManageShipment", new { id = model.IdShipment });

            #region Xu ly thanh toan Fee Lo hang
            var m = db.Shipments.FirstOrDefault(b => b.ShipmentId == model.IdShipment);
            if (m == null)
                return RedirectToAction("ManageShipment", new { id = model.IdShipment });

            var accountSelect = GetWallet(null, model.AccountType);
            if (accountSelect == null)
                return RedirectToAction("ManageShipment", new { id = model.IdShipment });

            var accountSystemSelect = GetWallet(FeeOrderChinaSystem.FeeOrderChinaVND.ToString(), FeeOrderChinaSystem.FeeOrderChinaVND.ToString(), "VND");
            if (accountSystemSelect == null)
                return RedirectToAction("ManageShipment", new { id = model.IdShipment });


            m.PaymentStatus = (int)PaymentStatus.Done;
            m.LastUpdate = DateTime.Now;


            #region Thuc hien tru tien tai khoan Account
            double amountShip = 0;
            double.TryParse(model.Amount, out amountShip);
            accountSelect.Money -= amountShip;
            accountSelect.LastUpdate = DateTime.Now;
            accountSelect.User_Update = User.Identity.Name;
            #endregion
            #region Cong tai khoan He thong

            accountSystemSelect.Money += amountShip;
            accountSystemSelect.LastUpdate = DateTime.Now;
            accountSystemSelect.User_Update = User.Identity.Name;
            InsertWalletHistory(accountSystemSelect, amountShip, model.Explain);
            #endregion
            InsertWalletHistory(accountSelect, -1 * amountShip, model.Explain);
            db.SaveChanges();




            return RedirectToAction("ManageShipment", new { id = model.IdShipment });
            #endregion
        }

        [HttpGet]
        public ActionResult ShipmentDetail(int id)
        {

            var model = new ShipmentDetailModel();
            Shipment Shipment = db.Shipments.FirstOrDefault(a => a.ShipmentId == id);
            var packageOrder = db.PackageOrders.Where(b => b.ShipmentId == Shipment.ShipmentId);
            model.ListPackageOrder = packageOrder.ToList();
            model.sumWeight = 0;
            model.sumVolume = 0;
            if (packageOrder != null)
            {
                foreach (var h in model.ListPackageOrder)
                {
                    model.sumWeight += h.Weight;
                    model.sumVolume += h.Volume;
                }

            }
            if (model.sumWeight < Shipment.Weight)
            {
                Shipment.Status = "Lô hàng chưa chốt.";
                ViewBag.Message = "Thiếu gói hàng.Trọng lượng chưa đủ.";
            }
            else
            {
                if (model.sumVolume < Shipment.Volume)
                {
                    Shipment.Status = "Lô hàng chưa chốt.";
                    ViewBag.Message = "Thiếu gói hàng.Thể tích chưa đủ.";
                }
                else
                {
                    if (Shipment.Status == "Lô hàng đã chốt")
                    {
                        ViewBag.Message = "";
                    }
                    else
                    {
                        ViewBag.Message = "Lô hàng đã đủ trọng lượng.Hãy chốt đơn hàng.";
                    }
                }


            }
            db.SaveChanges();
            model.ShipmentId = id;
            model.Name = Shipment.Name;
            model.Length = Shipment.Length;
            model.Width = Shipment.Width;
            model.Height = Shipment.Height;
            model.Weight = Shipment.Weight;

            model.LastUpdate = Shipment.LastUpdate;
            if (Shipment.Status == null)
            {
                model.Status = "Lô hàng chưa chốt.";
            }
            else
            {
                model.Status = Shipment.Status;
            }
            Session["ShipmentId"] = id;


            return View(model);
        }
        [HttpPost]
        public ActionResult ShipmentDetail(Shipment model, int id)
        {
            Shipment m = db.Shipments.FirstOrDefault(b => b.ShipmentId == id);

            m.Status = "Lô hàng đã chốt";
            db.SaveChanges();
            return RedirectToAction("ShipmentDetail", new { id = id });
        }
        public ActionResult DeletedPackage(int PackageOrderId, int ShipmentId)
        {
            var m = db.PackageOrders.FirstOrDefault(a => a.PackageOrderId == PackageOrderId);
            m.Status = "";
            m.ShipmentId = 0;
            db.SaveChanges();
            return RedirectToAction("ShipmentDetail", new { id = ShipmentId });
        }
        public ActionResult DeletedPackageInAddPackage(int PackageOrderId)
        {
            int ShipmentId = Convert.ToInt32(Session["ShipmentId"]);
            var m = db.PackageOrders.FirstOrDefault(a => a.PackageOrderId == PackageOrderId);
            db.PackageOrders.Remove(m);
            db.SaveChanges();
            return RedirectToAction("ShipmentDetail", new { id = ShipmentId });
        }

        public ActionResult DeletedPackageInManagePakageOrder(int PackageOrderId)
        {
            var m = db.PackageOrders.FirstOrDefault(a => a.PackageOrderId == PackageOrderId);
            db.PackageOrders.Remove(m);
            db.SaveChanges();
            return RedirectToAction("ManagePakageOrder");
        }
        public ActionResult DeletedShipment(int id)
        {

            var m = db.Shipments.FirstOrDefault(a => a.ShipmentId == id);
            db.Shipments.Remove(m);
            db.SaveChanges();
            return RedirectToAction("ManageShipment");
        }
        [HttpPost]
        public ActionResult ShipmentDetails(string ShippingCode)
        {
            int id = Convert.ToInt32(Session["ShipmentId"]);
            int PackageOrderId = Convert.ToInt32(Session["PackageOrderId"]);

            var order = db.Orders.FirstOrDefault(a => a.ShippingCode == ShippingCode);
            if (order != null)
            {
                order.ShipmentId = id;
                db.SaveChanges();
            }

            var package = db.PackageOrders.FirstOrDefault(b => b.PackageOrderId == PackageOrderId);
            if (package != null)
            {
                package.ShipmentId = id;
                package.Status = "choose";
                db.SaveChanges();
            }
            Session["PackageOrderId"] = null;
            return Json(new
            {
                data = id
            });
        }
        public ActionResult AddPackageOrder()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddPackageOrders(string ShippingCode)
        {
            if (ShippingCode != "")
            {
                PackageOrder packageOrder = db.PackageOrders.FirstOrDefault(a => a.ShippingCode == ShippingCode);
                string html = "";
                if (packageOrder != null)
                {
                    if (packageOrder.Status == "choose")
                    {
                        ViewBag.Message = "Gói hàng đã thuộc lô hàng khác.Hãy chọn gói hàng khác.";
                    }
                    else
                    {
                        Session["PackageOrderId"] = packageOrder.PackageOrderId.ToString();

                        if (packageOrder != null)
                        {
                            html =

                                      "<tr>" +
                                        "<td class='text-center'>" + packageOrder.PackageOrderId + "</ td >" +
                                           "<td class='text-center'>" + packageOrder.ShippingCode + "</ td >" +
                                           "<td class='text-center'>" + packageOrder.Length + "</ td >" +
                                           "<td class='text-center'>" + packageOrder.Width + "</ td >" +
                                           "<td class='text-center'>" + packageOrder.Height + "</ td >" +
                                           "<td class='text-center'>" + packageOrder.Volume + "</ td >" +
                                           "<td class='text-center'>" + packageOrder.Weight + "</ td >" +
                                   " </ tr >";

                        }
                        return Json(new
                        {
                            data = html
                        });
                    }
                }
                else
                {
                    ViewBag.Message = "Gói hàng không tồn tại.";
                }
                return Json(new
                {
                    data = ""
                });
            }
            return Json(new
            {
                data = ""
            });
        }


        public ActionResult CreatePakageOrder()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreatePakageOrder(string ShippingCode, string Length, string Width,
            string Height, string Weight)
        {
            PackageOrder model = new PackageOrder();
            if (ShippingCode != null)
            {
                if (Length != "")
                {
                    if (Width != null)
                    {
                        if (Height != null)
                        {
                            if (Weight != null)
                            {
                                model.ShippingCode = ShippingCode;
                                model.Length = Convert.ToDouble(Length);
                                model.Width = Convert.ToDouble(Width);
                                model.Height = Convert.ToDouble(Height);
                                model.Weight = Convert.ToDouble(Weight);
                                model.Volume = Convert.ToDouble(Length) * Convert.ToDouble(Width) * Convert.ToDouble(Height);
                                db.PackageOrders.Add(model);
                                db.SaveChanges();
                            }
                        }
                    }
                }
            }

            return RedirectToAction("ManagePakageOrder");
        }
        public ActionResult ManagePakageOrder(int? page)
        {

            var list = db.PackageOrders.ToList();
            int pageNumber = (page ?? 1);
            return View(list.ToPagedList(pageNumber, pageSize));
        }



        #endregion

        #region ManagerForex
        public ActionResult ManageForex(string username, string fromDate, string toDate,
            int? page)
        {
            var list = new List<Forex>();
            list = db.Forexs.ToList();
            bool isFilter = false;
            if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Client, false))
            {
                list = list.Where(a => a.Forex_Phone == User.Identity.Name).ToList();
            }
            else
            {
                list = list.ToList();
            }
            if (!string.IsNullOrEmpty(username))
            {
                list = db.Forexs.Where(a => a.Forex_Phone == username).ToList();
            }

            if (!string.IsNullOrEmpty(fromDate))
            {
                DateTime fromdate = DateTime.ParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                if (isFilter)
                {
                    list = (from a in list
                            where fromdate <= a.Date
                            select a).ToList();
                }
                else
                {
                    isFilter = true;
                    list = (from a in db.Forexs
                            where fromdate <= a.Date
                            select a).ToList();
                }
            }

            if (!string.IsNullOrEmpty(toDate))
            {

                DateTime todate = DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(1);
                if (isFilter)
                {
                    list = (from a in list
                            where a.Date <= todate
                            select a).ToList();
                }
                else
                {
                    isFilter = true;
                    list = (from a in db.Forexs
                            where a.Date <= todate
                            select a).ToList();
                }
            }

            var listOrderDisplay = new List<Forex>();
            if (isFilter)
            {
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Sale, false))
                {
                    List<string> listUserManage =
                        db.SaleManageClients.Where(a => a.User_Sale == User.Identity.Name)
                            .Select(a => a.User_Client)
                            .ToList();
                    listOrderDisplay.AddRange(
                        list.Where(a => listUserManage.Contains(a.Forex_Phone) || string.IsNullOrEmpty(a.Catalogy))
                            .ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Client, false))
                {
                    listOrderDisplay.AddRange(list.Where(a => a.Forex_Phone == User.Identity.Name).ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"]))
                {
                    listOrderDisplay = list.ToList();
                }
            }
            else
            {
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Sale, false))
                {
                    List<string> listUserManage =
                        db.SaleManageClients.Where(a => a.User_Sale == User.Identity.Name)
                            .Select(a => a.User_Client)
                            .ToList();
                    listOrderDisplay.AddRange(
                        list.Where(a => listUserManage.Contains(a.Forex_Phone) || string.IsNullOrEmpty(a.Catalogy))
                            .ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Client, false))
                {
                    listOrderDisplay.AddRange(list.Where(a => a.Forex_Phone == User.Identity.Name).ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"]))
                {
                    listOrderDisplay = list.ToList();
                }
            }

            listOrderDisplay = listOrderDisplay.Distinct().OrderByDescending(a => a.Date).ToList();

            ViewBag.CurrentToDate = toDate;
            ViewBag.CurrentFromDate = fromDate;
            int pageNumber = (page ?? 1);
            return View(listOrderDisplay.ToPagedList(pageNumber, pageSize));
        }
        public ActionResult CreateForex(string lis)
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateForex(Forex model, string amount, string BankUserName, string BankNumber, string BankName)
        {
            var eror = "";
            if (Request.IsAuthenticated)
            {
                var rate = db.Rates.FirstOrDefault();
                UserProfile user = db.UserProfiles.First(a => a.Phone == User.Identity.Name);
                Wallet wal = db.Wallets.FirstOrDefault(a => a.Client == User.Identity.Name);

                model.Rate = rate.rate_forex;
                model.Amount = Int32.Parse(amount);
                model.Forex_Phone = user.Phone;
                model.Date = DateTime.Now;
                model.TypeForex = TypeForex.New.ToString();
                model.BankName = BankUserName;
                model.BankNumber = BankNumber;
                if (wal != null)
                {
                    if (BankName != null && BankName != "")
                    {
                        model.BankName = BankName;
                        var cny = new List<CNY>();
                        cny = db.CNYs.Where(a => a.NameType == NameType.Bank.ToString()).ToList();
                        if (cny != null)
                        {
                            for (int i = 0; i < cny.Count; i++)
                            {
                                if (model.Amount >= cny[i].From && model.Amount <= cny[i].To)
                                {
                                    model.Service = cny[i].Value;

                                    double sum = model.Amount * model.Rate + model.Service;
                                    if (sum > wal.Money)
                                    {
                                        ModelState.AddModelError("CustomError", "Số tiền hiện tại trong tài khoản không đủ");
                                        eror = "Số tiền hiện tại trong tài khoản không đủ";
                                        return Json(new
                                        {
                                            success = false,
                                            html = eror
                                        });
                                    }
                                    wal.Money = wal.Money - sum;
                                    db.Forexs.Add(model);
                                    db.SaveChanges();
                                    return Json(new
                                    {
                                        success = true,
                                    }); ;
                                }
                                if (model.Amount > cny[i].From && cny[i].To == 0)
                                {
                                    model.Service = cny[i].Value;
                                    double sum = model.Amount * model.Rate + model.Service;
                                    if (sum > wal.Money)
                                    {
                                        ModelState.AddModelError("CustomError", "Số tiền hiện tại trong trài khoản không đủ");
                                        eror = "Số tiện hiện tại trong trài khoản không đủ";
                                        return Json(new
                                        {
                                            success = false,
                                            html = eror
                                        });
                                    }
                                    wal.Money = wal.Money - sum;
                                    db.Forexs.Add(model);
                                    db.SaveChanges();
                                    var wallerHistory = new WalletHistory
                                    {
                                        WalletId = wal.Id,
                                        Money = wal.Money,
                                        UpdateType = WalletUpdateType.Subtract.ToString(),
                                        Client = wal.Client,
                                        Currency = wal.Currency,
                                        User_Update = User.Identity.Name,
                                        LastUpdate = DateTime.Now,
                                        Reason = "Trừ: " + sum.ToString("##,###") + " VNĐ vào ví , bạn vừa nạp tệ" + model.Amount.ToString("##,###")

                                    };
                                    db.WalletHistorys.Add(wallerHistory);
                                    db.SaveChanges();
                                    return Json(new
                                    {
                                        success = true,
                                    }); ;
                                }
                            }
                            ModelState.AddModelError("CustomError", "Số tệ của bạn không có trong định phí mời bạn liên lạc lại với nhân viên");
                            eror = "Số tệ của bạn không có trong định phí mời bạn liên lạc lại với nhân viên";
                            return Json(new
                            {
                                success = false,
                                html = eror
                            });
                        }
                    }
                    else
                    {

                        model.BankName = NameType.Alipay.ToString();
                        var cny = new List<CNY>();
                        cny = db.CNYs.Where(a => a.NameType == NameType.Alipay.ToString()).ToList();
                        for (int i = 0; i < cny.Count; i++)
                        {
                            if (model.Amount >= cny[i].From && model.Amount <= cny[i].To)
                            {
                                model.Service = cny[i].Value;

                                double sum = model.Amount * model.Rate + model.Service;
                                if (sum > wal.Money)
                                {
                                    ModelState.AddModelError("CustomError", "Số tiền hiện tại trong trài khoản không đủ");
                                    eror = "Số tiền hiện tại trong trài khoản không đủ";
                                    return Json(new
                                    {
                                        success = false,
                                        html = eror
                                    });
                                }
                                wal.Money = wal.Money - sum;
                                db.Forexs.Add(model);
                                db.SaveChanges();
                                var wallerHistory = new WalletHistory
                                {
                                    WalletId = wal.Id,
                                    Money = wal.Money,
                                    UpdateType = WalletUpdateType.Subtract.ToString(),
                                    Client = wal.Client,
                                    Currency = wal.Currency,
                                    User_Update = User.Identity.Name,
                                    LastUpdate = DateTime.Now,
                                    Reason = "Trừ: " + sum.ToString("##,###") + " VNĐ vào ví , bạn vừa nạp tệ:" + model.Amount.ToString("##,###")

                                };
                                db.WalletHistorys.Add(wallerHistory);
                                db.SaveChanges();
                                return Json(new
                                {
                                    success = true,
                                }); ;
                            }
                            if (model.Amount > cny[i].From && cny[i].To == 0)
                            {
                                model.Service = cny[i].Value;
                                double sum = model.Amount * model.Rate + model.Service;
                                if (sum > wal.Money)
                                {
                                    ModelState.AddModelError("CustomError", "Số tiền hiện tại trong trài khoản không đủ");
                                    eror = "Số tiền hiện tại trong trài khoản không đủ";
                                    return Json(new
                                    {
                                        success = false,
                                        html = eror
                                    });
                                }
                                wal.Money = wal.Money - sum;
                                db.Forexs.Add(model);
                                db.SaveChanges();
                                return Json(new
                                {
                                    success = true,
                                }); ;
                            }
                        }
                        ModelState.AddModelError("CustomError", "Số tệ của bạn không có trong định phí mời bạn liên lạc lại với nhân viên");
                        eror = "Số tệ của bạn không có trong định phí mời bạn liên lạc lại với nhân viên";
                        return Json(new
                        {
                            success = false,
                            html = eror
                        });
                    }
                }
                else
                {
                    ModelState.AddModelError("CustomError", "Hiện tại bạn ko có tài khoản");
                    eror = "Hiện tại bạn ko có tài khoản";
                    return Json(new
                    {
                        success = false,
                        html = eror
                    });

                }
            }
            eror = "Có lỗi xảy ra";
            return Json(new
            {
                success = false,
                html = eror
            });
        }

        [HttpPost]
        public JsonResult ViewForex(string ForexID)
        {
            string html = "";
            if (ForexID != null)
            {
                var forex = db.Forexs.First(a => a.ForexID.ToString() == ForexID);
                if (forex.Forex_Orderer == null || forex.Forex_Orderer == User.Identity.Name)
                {
                    var Sum = forex.Amount * forex.Rate + forex.Service;
                    forex.Forex_Orderer = User.Identity.Name;
                    forex.Forex_Orderer = TypeForex.Orderer.ToString();
                    db.SaveChanges();
                    html = "<div class='modal-content'>" +
                                "<div class='modal-header'>" +
                                    "<h4 class='modal-title' id='myModalLabel'>Chốt đơn nạp tệ</h4>" +
                                "</div>" +
                                "<div class='modal-body'>" +
                                    "<div class='form-group row' style='width: 100%;font-size: 14px'>" +
                                        "<div class='col-lg-6'>" +
                                        "Tiền nạp:" +
                                        "<br />" +
                                        "<br />" +
                                        "Tên tài khoản:" +
                                        "<br />" +
                                        "<br />" +
                                        "Số tài khoản:" +
                                        "<br />" +
                                        "<br />" +
                                        "Tên ngân hàng:" +
                                        "<br />" +
                                        "<br />" +
                                        "Thời gian tạo đơn:" +
                                        "<br />" +
                                        "<br />" +
                                        "Số tiền Orderer cần thành toán:" +
                                        "<br />" +
                                        "</div>" +
                                        "<div class='col-lg-6'>" +
                                        forex.Amount.ToString("##,###") + "(Tệ)" +
                                        "<br />" +
                                        "<br />" +
                                        forex.BankUserName +
                                        "<br />" +
                                        "<br />" +
                                        forex.BankNumber +
                                        "<br />" +
                                        "<br />" +
                                        forex.BankName +
                                        "<br />" +
                                        "<br />" +
                                        forex.Date +
                                        "<br />" +
                                        "<br />" +
                                        "<b style='color:red'>" + Sum.ToString("##,###") + "VNĐ</b>" +
                                        "<br />" +
                                        "</div>" +
                                    "</div>" +
                                    "<div class='modal-footer'>" +
                                        "<button type = 'submit' value='Save' onclick='edit(" + forex.ForexID + ")'  class='btn btn-primary'>Chốt đơn</button>" +
                                        "<a hrep='/Account/ManageForex'  class='btn btn-info'>Cập nhật sau</a>" +
                                        "<button type = 'button' class='btn btn-default' data-dismiss='modal'>Đóng</button>" +
                                    "</div>" +
                                "</div>" +
                           "</div>";
                    return Json(new
                    {
                        success = true,
                        data = html
                    });
                }
            }
            return Json(new
            {
                success = false,
                data = "<div class='modal-content'>" +
                                "<div class='modal-header'>" +
                                    "<h4 class='modal-title' id='myModalLabel'>Đơn đang được chốt</h4>" +
                                 "</div>" +
                                 "<div class='modal-body'>" +
                                     "<div class='modal-footer'>" +
                                     "</div>" +
                                "</div>" +
                         "</div>"
            });
        }
        [HttpPost]
        public JsonResult UpdateForex(string ForexID)
        {
            if (ForexID != null)
            {
                var forex = db.Forexs.First(a => a.ForexID.ToString() == ForexID);
                var use = db.Wallets.FirstOrDefault(a => a.Client == User.Identity.Name);
                if (use != null)
                {
                    if (use.Currency == "CNY")
                    {
                        double sum = forex.Amount * forex.Rate + forex.Service;
                        if (sum <= use.Money)
                        {
                            use.Money = use.Money - forex.Amount;
                            forex.TypeForex = TypeForex.Done.ToString();
                            return Json(new { success = true });
                        }
                    }
                }
            }
            return Json(new { success = false });
        }
        #endregion

        #region Manage Complaint
        [AllowAnonymous]
        public ActionResult CreateOrderComplaint()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateOrderComplaint(NewOrderModel model, FormCollection collec)
        {
            //thực hiện tạo order dựa tên orderdetail
            var conten = collec["Content"];
            var orderid = collec["OrderID"];
            if (Request.IsAuthenticated)
            {
                using (DbContextTransaction dbContextTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        Rate rate = db.Rates.FirstOrDefault();
                        UserProfile user = db.UserProfiles.First(a => a.Phone == User.Identity.Name);
                        if (model.ListOrderDetail.Count() == 1)
                        {
                            var orderInternal = new OrderInternal
                            {
                                CreateDate = DateTime.Now,
                                UpdateDate = DateTime.Now,
                                TotalPrice = model.ListOrderDetail.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0)),
                                TotalPriceClient = model.ListOrderDetail.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0)),
                                TotalPriceReality = model.ListOrderDetail.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0)),
                                Status = OrderStatus.New.ToString(),
                                Rate = rate != null ? rate.Price : 0,
                                Fee = 5,
                                Phone = user.Phone,
                                UserName = user.Email,
                                OrderType = OrderType.Complaint.ToString(),
                                IsShop = true
                            };
                            // tạo mã đơn theo ngày
                            var orderinternalinday = db.OrderInternalInDays.First(a => a.ID == 1);
                            if (orderinternalinday.CreateDate.Date == DateTime.Now.Date)
                            {
                                orderinternalinday.OrderInternalInDayID = orderinternalinday.OrderInternalInDayID + 1;
                            }
                            else
                            {
                                orderinternalinday.OrderInternalInDayID = 1;
                                orderinternalinday.CreateDate = DateTime.Now;
                            }
                            db.SaveChanges();

                            var customer_code = db.UserProfiles.FirstOrDefault(a => a.Phone == orderInternal.Phone).customercode;
                            orderInternal.OrderInternalInDayID = DateTime.Now.ToString("yyMMdd") + orderInternal.OrderType.ToUpper() + "0" + orderinternalinday.OrderInternalInDayID + customer_code;

                            //
                            orderInternal.TotalPriceConvert = orderInternal.TotalPrice * orderInternal.Rate;

                            //get downpayment
                            orderInternal.DownPaymentRate = user.DownPaymentRate;

                            // get fee
                            int orderValueInt = Convert.ToInt32(orderInternal.TotalPriceConvert / 1000000);
                            var fee =
                                    db.Fees.Where(a => (a.From <= orderValueInt && a.To >= orderValueInt) || (a.To == 0 && a.From <= orderValueInt))
                                        .OrderByDescending(a => a.Value).First();
                            orderInternal.Fee = 0;

                            SaleManageClient saleManage =
                                db.SaleManageClients.FirstOrDefault(a => a.User_Client == user.Phone);
                            if (saleManage != null) orderInternal.SaleManager = saleManage.User_Sale;
                            db.Entry(orderInternal).State = EntityState.Added;
                            db.OrderInternals.Add(orderInternal);
                            db.SaveChanges();

                            int orderInternalId = orderInternal.OrderInternalId;

                            var orderDetail = new OrderInternalDetail();
                            orderDetail.OrderInternalId = orderInternalId;
                            orderDetail.Phone = user.Phone;
                            orderDetail.OrderDetailStatus = OrderDetailStatus.Active.ToString();
                            orderDetail.Link = model.ListOrderDetail.First().Link;
                            orderDetail.Shop = model.ListOrderDetail.First().Shop;
                            orderDetail.Description = model.ListOrderDetail.First().Description;
                            orderDetail.Color = model.ListOrderDetail.First().Color;
                            orderDetail.Size = model.ListOrderDetail.First().Size;
                            orderDetail.Quantity = model.ListOrderDetail.First().Quantity;
                            orderDetail.Price = model.ListOrderDetail.First().Price;
                            try
                            {
                                var image = Request.Files["Image_" + model.ListOrderDetail.First().stt];
                                if (image != null && !string.IsNullOrEmpty(image.FileName))
                                {
                                    orderDetail.Image = "/Images/" + image.FileName;
                                    image.SaveAs(Server.MapPath("~/Images/" + image.FileName));
                                }
                            }
                            catch (Exception)
                            {
                                //no image
                            }
                            db.Entry(orderDetail).State = EntityState.Added;
                            db.OrderInternalDetails.Add(orderDetail);
                            db.SaveChanges();

                            dbContextTransaction.Commit();

                            ViewData["message"] = "Tạo đơn hàng thành công.";
                            if (user.UserType.IndexOf(((int)UserType.Client).ToString()) >= 0)
                            {
                                return RedirectToAction("AllOrder", "Account");
                            }
                            return RedirectToAction("Manage", "Account");


                        }
                        else
                        {
                            var order = new Order
                            {
                                IDOrderCompain = orderid,
                                Content = "Khách hàng:" + conten,
                                CreateDate = DateTime.Now,
                                UpdateDate = DateTime.Now,
                                TotalPrice = model.ListOrderDetail.Sum(a => (a.Quantity ?? 0) * (a.Price ?? 0)),
                                Status = OrderStatus.New.ToString(),
                                Rate = rate != null ? rate.Price : 0,
                                Fee = 0,
                                Phone = user.Phone,
                                UserName = user.Email,
                                OrderType = OrderType.Complaint.ToString(),
                                IsShop = false
                            };
                            order.TotalPriceConvert = order.TotalPrice * order.Rate;

                            //get downpayment
                            order.DownPaymentRate = user.DownPaymentRate;

                            //get fee
                            int orderValueInt = Convert.ToInt32(order.TotalPriceConvert / 1000000);
                            order.Fee = 0;

                            SaleManageClient saleManage =
                                db.SaleManageClients.FirstOrDefault(a => a.User_Client == user.Phone);
                            if (saleManage != null) order.SaleManager = saleManage.User_Sale;
                            db.Entry(order).State = EntityState.Added;
                            db.Orders.Add(order);
                            db.SaveChanges();

                            int orderId = order.OrderId;

                            foreach (OrderDetail orderDetail in model.ListOrderDetail)
                            {
                                orderDetail.OrderId = orderId;
                                orderDetail.Phone = user.Phone;
                                try
                                {
                                    var image = Request.Files["Image_" + orderDetail.stt];
                                    if (image != null && !string.IsNullOrEmpty(image.FileName))
                                    {
                                        orderDetail.Image = "/Images/" + image.FileName;
                                        image.SaveAs(Server.MapPath("~/Images/" + image.FileName));
                                    }

                                }
                                catch (Exception)
                                {
                                    //no image
                                }
                                orderDetail.OrderDetailStatus = OrderDetailStatus.Active.ToString();
                                db.Entry(orderDetail).State = EntityState.Added;
                                db.OrderDetails.Add(orderDetail);
                                db.SaveChanges();
                            }

                            dbContextTransaction.Commit();

                            ViewData["message"] = "Tạo đơn khiếu nại thành công.";
                            if (user.UserType == "5")
                            {
                                return RedirectToAction("ManagerOrderComplaint", "Account");
                            }
                            return RedirectToAction("Manage", "Account");
                        }
                    }

                    catch (Exception)
                    {
                        dbContextTransaction.Rollback();
                    }
                }
            }

            return View();
        }
        [AllowAnonymous]
        public ActionResult ManagerOrderComplaint(string username, string listStatus, string fromDate, string toDate, string OrderId,
            int? page)
        {
            var lismnager = new List<ViewManager>();
            var listOrder = new List<OrderInternal>();
            listOrder = db.OrderInternals.Where(a => a.OrderType == OrderType.Complaint.ToString()).ToList();
            bool isFilter = false;
            if (!string.IsNullOrEmpty(username))
            {
                isFilter = true;
                listOrder = listOrder.Where(a => a.Phone == username).ToList();
            }

            if (!string.IsNullOrEmpty(fromDate))
            {
                DateTime fromdate = DateTime.ParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                if (isFilter)
                {
                    listOrder = (from order in listOrder
                                 where fromdate <= order.CreateDate
                                 select order).ToList();
                }
                else
                {
                    isFilter = true;
                    listOrder = (from order in listOrder
                                 where fromdate <= order.CreateDate
                                 select order).ToList();
                }
            }

            if (!string.IsNullOrEmpty(toDate))
            {

                DateTime todate = DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(1);
                if (isFilter)
                {
                    listOrder = (from order in listOrder
                                 where order.CreateDate <= todate
                                 select order).ToList();
                }
                else
                {
                    isFilter = true;
                    listOrder = (from order in listOrder
                                 where order.CreateDate <= todate
                                 select order).ToList();
                }
            }

            if (!string.IsNullOrEmpty(listStatus))
            {
                if (isFilter)
                {
                    listOrder = listOrder.FindAll(a => a.Status == listStatus);
                }
                else
                {
                    isFilter = true;
                    listOrder = listOrder.Where(a => a.Status == listStatus).ToList();
                }
            }
            if (!string.IsNullOrEmpty(OrderId))
            {
                if (isFilter)
                {
                    listOrder = listOrder.FindAll(a => a.OrderId.ToString() == OrderId);
                }
                else
                {
                    isFilter = true;
                    listOrder = listOrder.Where(a => a.OrderId.ToString() == OrderId).ToList();
                }
            }

            ViewBag.ListStatus = GetListStatus(listStatus);

            var listOrderDisplay = new List<OrderInternal>();

            if (isFilter)
            {
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Sale, false))
                {
                    List<string> listUserManage =
                        db.SaleManageClients.Where(a => a.User_Sale == User.Identity.Name)
                            .Select(a => a.User_Client)
                            .ToList();
                    listOrderDisplay.AddRange(
                        listOrder.Where(a => listUserManage.Contains(a.Phone) || string.IsNullOrEmpty(a.SaleManager))
                            .ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Client, false))
                {
                    listOrderDisplay.AddRange(listOrder.Where(a => a.Phone == User.Identity.Name).ToList());
                }

                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Orderer, false))
                {
                    listOrderDisplay.AddRange(listOrder.Where(a => a.Status == OrderStatus.Paid.ToString()).ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Accounting, false))
                {
                    listOrderDisplay.AddRange(
                        listOrder.Where(
                            a =>
                                a.Status != OrderStatus.New.ToString())
                            .ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Recieve, false))
                {
                    listOrderDisplay.AddRange(
                        listOrder.Where(
                            a =>
                                a.Status == OrderStatus.Order.ToString() ||
                                a.Status == OrderStatus.FullCollect.ToString()).ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"]))
                {
                    listOrderDisplay = listOrder.ToList();
                }
            }
            else
            {
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Sale, false))
                {
                    List<string> listUserManage =
                        db.SaleManageClients.Where(a => a.User_Sale == User.Identity.Name)
                            .Select(a => a.User_Client)
                            .ToList();
                    listOrderDisplay.AddRange(
                       listOrder.Where(a => listUserManage.Contains(a.Phone) || string.IsNullOrEmpty(a.SaleManager))
                            .ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Client, false))
                {
                    listOrderDisplay.AddRange(listOrder.Where(a => a.Phone == User.Identity.Name).ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Accounting, false))
                {
                    listOrderDisplay.AddRange(
                        listOrder.Where(
                            a =>
                                a.Status != OrderStatus.New.ToString())
                            .ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Orderer, false))
                {
                    listOrderDisplay.AddRange(listOrder.Where(a => a.Status == OrderStatus.Paid.ToString()).ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"], (int)UserType.Recieve, false))
                {
                    listOrderDisplay.AddRange(
                        listOrder.Where(
                            a =>
                                a.Status == OrderStatus.Order.ToString() ||
                                a.Status == OrderStatus.FullCollect.ToString()).ToList());
                }
                if (Utils.CheckRole((string)Session["UserType"]))
                {
                    listOrderDisplay = listOrder.ToList();
                }
            }

            listOrderDisplay = listOrderDisplay.Distinct().OrderByDescending(a => a.UpdateDate).ToList();
            if (listOrderDisplay.Count >= 0)
            {
                var lisorder = (from a in listOrderDisplay select a.OrderInternalId).ToList();
                foreach (int orderid in lisorder)
                {
                    OrderInternal order = db.OrderInternals.First(a => a.OrderInternalId == orderid);
                    UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Phone == order.Phone);
                    if (user != null)
                    {
                        var NewView = new ViewManager
                        {
                            OrderInternals = order,
                            UserName = user.Name,
                            IdUser = user.customercode
                        };
                        lismnager.Add(NewView);
                    }
                    else
                    {
                        var NewView = new ViewManager
                        {
                            OrderInternals = order,
                            UserName = "Khách hàng chưa lập TK",
                            IdUser = "Khách hàng chưa lập TK"
                        };
                        lismnager.Add(NewView);
                    }

                }
            }
            ViewBag.CurrentToDate = toDate;
            ViewBag.CurrentFromDate = fromDate;
            ViewBag.CurrentStatus = listStatus;
            ViewBag.CurrentOrderId = OrderId;
            ViewBag.CurrentUserName = username;


            int pageNumber = (page ?? 1);

            return View(lismnager.ToPagedList(pageNumber, pageSize));
        }
        [AllowAnonymous]
        public ActionResult ViewOrderComplaintDetail(int id)
        {
            var model = new ViewDetailOrderModelNew();
            var order = db.OrderInternals.FirstOrDefault(a => a.OrderInternalId == id);
            var orderDetail = db.OrderInternalDetails.Where(a => a.OrderInternalId == id);

            model.OrderInternalId = id;
            if (order != null)
            {
                var UserDetail = db.UserProfiles.FirstOrDefault(a => a.Phone == order.Phone);
                if (UserDetail != null)
                {
                    model.Name = UserDetail.Name;
                    model.Email = UserDetail.Email;
                    model.Address = UserDetail.Address;
                }
                model.IDOrderCompain = order.IDOrderCompain;
                model.TransId = order.TransId;
                model.OrderInternalId = id;
                model.Phone = order.Phone;
                model.Rate = order.Rate;
                model.Status = order.Status;
                model.StatusText = order.getStatusText();
                model.TotalPriceConvert = order.TotalPriceConvert;
                model.TotalPrice = order.TotalPrice;
                model.SaleManager = order.SaleManager;
                model.UserName = order.UserName;
                model.Phone = order.Phone;
                model.FeeShip = order.FeeShip;
                model.FeeShipChina = order.FeeShipChina;
                model.CreateDate = order.CreateDate;
                model.Fee = order.Fee;
                model.Weight = order.Weight;
                model.DownPayment = order.DownPayment;
                model.AccountingCollected = order.AccountingCollected;
                model.DownPaymentRate = order.DownPaymentRate;
                model.OrdererManager = order.OrdererManager;
                model.Content = order.Content;
                model.AccountingCollected = order.AccountingCollected;

                if (!string.IsNullOrEmpty(order.SaleManager))
                {
                    UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Phone == order.SaleManager);
                    if (user != null)
                    {
                        model.SaleManageInfo = new SaleManageInfo
                        {
                            SaleName = user.Name,
                            SalePhone = user.Phone,
                            SalePhoneCompany = user.PhoneCompany
                        };
                    }
                }

                if (!string.IsNullOrEmpty(order.Phone))
                {
                    UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Phone == order.Phone);
                    if (user != null)
                    {
                        model.Client = user;
                    }
                }



                ViewBag.ListOrderer = GetListOrderer(model.OrdererManager);

            }
            model.ListOrderDetails = orderDetail;
            return View(model);
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult UpdateOrderComplaint(int OrderInternalId)
        {
            var order = db.OrderInternals.First(a => a.OrderInternalId == OrderInternalId);
            if (order != null)
            {
                order.Status = OrderStatus.MisOrder.ToString();
                db.SaveChanges();
            }
            return Json(new
            {
                success = true
            });
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult CombackComplaint(int OrderInternalId)
        {
            var order = db.OrderInternals.First(a => a.OrderInternalId == OrderInternalId);
            if (order != null)
            {
                var user = db.UserProfiles.First(a => a.Phone == order.Phone);
                if (user != null)
                {
                    var wallet = db.Wallets.First(a => a.Client == user.Phone);
                    if (wallet != null)
                    {
                        wallet.Money += order.TotalPriceConvert;
                        var wallerHistory = new WalletHistory
                        {
                            WalletId = wallet.Id,
                            Money = wallet.Money,
                            UpdateType = WalletUpdateType.Addition.ToString(),
                            Client = wallet.Client,
                            Currency = wallet.Currency,
                            User_Update = User.Identity.Name,
                            LastUpdate = DateTime.Now,
                            Reason = "Cộng: " + order.TotalPriceConvert.ToString("##,###") + " VNĐ vào ví , trả lại tiền đơn khiếu nại vì không mua được sản phẩm"

                        };
                        db.WalletHistorys.Add(wallerHistory);
                    }
                    else
                    {
                        return Json(new
                        {
                            success = false
                        });
                    }
                }
                order.Status = OrderStatus.Cancel.ToString();
                db.SaveChanges();
            }
            return Json(new
            {
                success = true
            });
        }
        public JsonResult GetListComplaintSale()
        {
            if (!Request.IsAuthenticated)
            {
                return Json(new { success = false });
            }
            if (Session["UserType"] == null || (!Utils.CheckRole((string)Session["UserType"], (int)UserType.Sale) && !Utils.CheckRole((string)Session["UserType"], (int)UserType.Manager)))
            {
                return Json(new { success = false });
            }
            var getlistApproval = db.OrderInternals.Where(a => a.OrderType == OrderType.Complaint.ToString() && a.Status == OrderStatus.New.ToString()).ToList();
            var listOrderDisplay = new List<OrderInternal>();
            List<string> listUserManage =
                        db.SaleManageClients.Where(a => a.User_Sale == User.Identity.Name)
                            .Select(a => a.User_Client)
                            .ToList();
            listOrderDisplay.AddRange(
                        getlistApproval.Where(a => listUserManage.Contains(a.Phone) || string.IsNullOrEmpty(a.SaleManager))
                            .ToList());

            if (listOrderDisplay != null && listOrderDisplay.Count > 0)
            {
                string html = "<a class=\"dropdown-toggle face\" data-toggle=\"dropdown\" aria-expanded=\"true\">" +
                              "<span class=\"fa fa-check\"></span>" +
                            "<sup><b style =\"color:yellow\" >" + listOrderDisplay.Count + "</b></sup>" +
                        "</a>" +
                        "<ul class=\"dropdown-menu flat panel-notify\">";
                html = html + " <li>" +
                  " <a title =\"Đơn khiếu nại\" href =\"" + @Url.Action("ManagerOrderComplaint", "Account") + "\" >" +
                   "Hiện tại đang có  : " + listOrderDisplay.Count + " đơn hàng khiếu tại mới  </a>" + " </li>";
                html = html + " </ul>";

                return Json(new
                {
                    success = true,
                    data = html
                });
            }
            return Json(new
            {
                success = false
            });
        }
        public JsonResult GetListExcelError()
        {
            if (!Request.IsAuthenticated)
            {
                return Json(new { success = false });
            }
            if (Session["UserType"] == null || !Utils.CheckRole((string)Session["UserType"], (int)UserType.Sale, false))
            {
                return Json(new { success = false });
            }

            var getClientManage = db.SaleManageClients.Where(a => a.User_Sale == User.Identity.Name).Select(a => a.User_Client).Distinct().ToList();

            var getlistApproval = db.ExcelNotices.Where(a => ((a.IsHaveSaleManage && getClientManage.Contains(a.Phone)) || !a.IsHaveSaleManage) && a.Status == ExcelNoticeStatus.UnDone.ToString()).ToList();

            if (getlistApproval != null && getlistApproval.Count > 0)
            {

                string html = "<a class=\"dropdown-toggle face\" data-toggle=\"dropdown\" aria-expanded=\"true\">" +
                              "<span class=\"fa fa-file-excel-o\"></span>" +
                            "<sup><b style =\"color:red\" >" + getlistApproval.Count + "</b></sup>" +
                        "</a>" +
                        "<ul class=\"dropdown-menu flat panel-notify\">";

                for (int i = 0; i < getlistApproval.Count; i++)
                {
                    var approval = getlistApproval[i];

                    html = html + " <li>" +
                  " <a title =\"Đặt lệnh bằng excel lỗi : " + approval.Name + "\" href =\"" + Url.Action("ExcelErrorDetail", "Account", new { id = approval.ExcelNoticeID }) + "\" >" +
                   "Đặt lệnh bằng excel lỗi : " + approval.Name + " </a>" + " </li>";

                }

                html = html + " </ul>";

                return Json(new
                {
                    success = true,
                    data = html
                });
            }
            return Json(new
            {
                success = false
            });
        }
        public JsonResult GetListComplaintOrder()
        {
            if (!Request.IsAuthenticated)
            {
                return Json(new { success = false });
            }
            if (Session["UserType"] == null || (!Utils.CheckRole((string)Session["UserType"], (int)UserType.Orderer) && !Utils.CheckRole((string)Session["UserType"], (int)UserType.Manager)))
            {
                return Json(new { success = false });
            }
            var getlistApproval = db.OrderInternals.Where(a => a.OrderType == OrderType.Complaint.ToString() && a.Status == OrderStatus.Paid.ToString()).ToList();

            if (getlistApproval != null && getlistApproval.Count > 0)
            {
                string html = "<a class=\"dropdown-toggle face\" data-toggle=\"dropdown\" aria-expanded=\"true\">" +
                              "<span class=\"fa fa-check\"></span>" +
                            "<sup><b style =\"color:green\" >" + getlistApproval.Count + "</b></sup>" +
                        "</a>" +
                        "<ul class=\"dropdown-menu flat panel-notify\">";
                html = html + " <li>" +
                  " <a title =\"Đơn khiếu nại\" href =\"" + @Url.Action("ManagerOrderComplaint", "Account") + "\" >" +
                   "Hiện tại đang có  : " + getlistApproval.Count + " đơn hàng khiếu tại mới  </a>" + " </li>";
                html = html + " </ul>";

                return Json(new
                {
                    success = true,
                    data = html
                });
            }
            return Json(new
            {
                success = false
            });
        }

        #endregion
    }
}