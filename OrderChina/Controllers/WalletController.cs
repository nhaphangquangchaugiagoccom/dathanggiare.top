﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using OrderChina.Common;
using OrderChina.Models;
using PagedList;
using System.Reflection;
using System.ComponentModel.DataAnnotations;

namespace OrderChina.Controllers
{
    public class WalletController : Controller
    {
        DBContext db = new DBContext();
        //
        // GET: /Wallet/

        public ActionResult Index(string id, int? page)
        {
            var listUser = db.UserProfiles.ToList();
            var dicUserByPhone = new Dictionary<string, string>();
            foreach (var userProfile in listUser)
                dicUserByPhone[userProfile.Phone] = userProfile.Email;
            ViewBag.DicUser = dicUserByPhone;
            if (Session["UserType"] != null && (Utils.CheckRole((string)Session["UserType"], (int)UserType.Client, false) || Utils.CheckRole((string)Session["UserType"], (int)UserType.Orderer, false)))
            {
                const int pageSize = 50;
                int pageNumber = (page ?? 1);
                var model = new IndexWalletModel { Wallets = db.Wallets.Where(a => a.Client == User.Identity.Name) };

                var listId = model.Wallets.Select(a => a.Id).ToList();
                model.WalletHistorys = db.WalletHistorys.Where(a => listId.Contains(a.WalletId)).ToList()
                    .ToPagedList(pageNumber, pageSize);

                return View(model);
            }
            else if (Session["UserType"] != null && Utils.CheckRole((string)Session["UserType"]))
            {

                ViewBag.isadmin = true;
                ViewBag.phone = id;
                const int pageSize = 50;
                int pageNumber = (page ?? 1);
                var model = new IndexWalletModel { Wallets = db.Wallets.Where(a => a.Client == id) };

                var listId = model.Wallets.Select(a => a.Id).ToList();
                model.WalletHistorys = db.WalletHistorys.Where(a => listId.Contains(a.WalletId)).ToList()
                    .ToPagedList(pageNumber, pageSize);
                return View(model);
            }
            else if (Session["UserType"] != null && Utils.CheckRole((string)Session["UserType"], (int)UserType.Accounting, false))
            {

                ViewBag.isacc = true;
                ViewBag.phone = id;

                const int pageSize = 50;
                int pageNumber = (page ?? 1);
                var model = new IndexWalletModel { Wallets = db.Wallets.Where(a => a.Client == id) };

                var listId = model.Wallets.Select(a => a.Id).ToList();
                model.WalletHistorys = db.WalletHistorys.Where(a => listId.Contains(a.WalletId)).ToList()
                    .ToPagedList(pageNumber, pageSize);

                return View(model);
            }
            return RedirectToAction("Error", "Home");
        }

        public ActionResult CreateWallet(string id)
        {
            var user = db.UserProfiles.FirstOrDefault(a => a.Phone == id);
            var model = new Wallet
            {
                Client = id
            };

            ViewBag.UserType = user != null ? user.UserType : "system";
            ViewBag.ListBankName = GetListBankName(null, "VND");
            ViewBag.ListCurrency = GetListCurrency(id, user);
            ViewBag.ListWalletType = GetListWalletType();

            return PartialView("_CreateWallet", model);
        }

        private SelectList GetListCurrency(string id, UserProfile user)
        {
            List<Object> list = new List<Object>();
            if (user != null)
            {
                if (Utils.CheckRole(user.UserType, (int)UserType.Client, false))
                {
                    var listWallet = db.Wallets.Where(a => a.Client == id).Select(a => a.Currency).ToList();
                    var items =
                        db.Currencys.Where(a => !listWallet.Contains(a.Code) && a.Code == Utils.VND)
                        .Select(a => new { value = a.Code, display = a.Code + " - " + a.Description });
                    foreach (var item in items)
                    {
                        list.Add(item);
                    }

                }
                else if (Utils.CheckRole(user.UserType, (int)UserType.Orderer, false))
                {
                    var itemsOrder =
                        db.Currencys.Where(a => a.Code == Utils.CNY)
                        .Select(a => new { value = a.Code, display = a.Code + " - " + a.Description });
                    foreach (var item in itemsOrder)
                    {
                        list.Add(item);
                    }
                }

            }
            else
            {
                var itemsOrder =
                        db.Currencys.Distinct()
                        .Select(a => new { value = a.Code, display = a.Code + " - " + a.Description });
                foreach (var item in itemsOrder)
                {
                    list.Add(item);
                }
            }

            return new SelectList(list, "value", "display");
        }

        private SelectList GetListWalletType()
        {
            var list = new List<object>();
            foreach (FieldInfo fieldInfo in typeof(WalletType).GetFields())
            {
                if (fieldInfo.FieldType.Name != "WalletType")
                    continue;
                var attribute = Attribute.GetCustomAttribute(fieldInfo,
                    typeof(DisplayAttribute)) as DisplayAttribute;

                if (attribute != null)
                    list.Add(new { name = fieldInfo.Name, display = attribute.Name });
                else
                    list.Add(new { name = fieldInfo.Name, display = fieldInfo.Name });
            }

            return new SelectList(list, "name", "display", "Online");
        }

        private SelectList GetListBankName(string client, string currency)
        {
            var listWallet = string.IsNullOrWhiteSpace(currency) ?
                                                                      db.Wallets.Where(a => a.Client == client).ToList() :
                                                                                                                             db.Wallets.Where(a => a.Client == client && a.Currency == currency).ToList();

            var list = new List<Object>();

            var isEmptyCurrency = string.IsNullOrWhiteSpace(currency);

            foreach (var a in listWallet)
            {
                list.Add(isEmptyCurrency
                             ? new { value = a.Currency + "#" + a.Name + "#" + a.Id, display = a.Currency + " - " + a.Name }
                             : new { value = a.Name, display = a.Name });
            }

            return new SelectList(list, "value", "display");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult CreateWallet(Wallet model)
        {
            var userInfo = db.UserProfiles.FirstOrDefault(c => c.Phone == model.Client);
            if (userInfo != null)
            {
                // truong hop la tao vi khach hang cong tien ca tai khoan khach hang va tai khoan he thong
                if (userInfo.UserType == ((int)UserType.Client).ToString())
                {
                    if (string.IsNullOrWhiteSpace(model.Name))
                        return RedirectToAction("Index", new { id = model.Client });

                    var accountSystem = GetWallet(null, model.Name);
                    if (accountSystem == null)
                        return RedirectToAction("Index", new { id = model.Client });

                    accountSystem.Money += model.Money;

                    model.User_Update = User.Identity.Name;
                    model.LastUpdate = DateTime.Now;

                    db.Wallets.Add(model);
                    db.SaveChanges();

                    InsertWalletHistory(accountSystem, model.Money, string.Format("Tạo mới ví cho user: {0} số tiền trong ví: {1}", model.Client, model.Money.ToString("N0")));
                    InsertWalletHistory(model, model.Money, string.Format("Tạo mới ví, số tiền trong ví: {0}", model.Money.ToString("N0")));
                    model.Name = string.Empty;

                    db.SaveChanges();

                }
            }
            else
            {
                model.User_Update = User.Identity.Name;
                model.LastUpdate = DateTime.Now;

                db.Wallets.Add(model);
                db.SaveChanges();

                InsertWalletHistory(model, model.Money, string.Format("Tạo mới ví, số tiền trong ví: {0}", model.Money.ToString("N0")));
                db.SaveChanges();

            }



            return RedirectToAction("Index", new { id = model.Client });
        }
        private void InsertWalletHistory(Wallet walletClient, double money, string reason,string note="")
        {
            try
            {
                if (walletClient == null)
                    return;
                var wallerHistory = new WalletHistory
                {
                    WalletId = walletClient.Id,
                    Money = money,
                    UpdateType = money >= 0 ? WalletUpdateType.Addition.ToString() : WalletUpdateType.Subtract.ToString(),
                    Client = walletClient.Client,
                    Currency = walletClient.Currency,
                    User_Update = User.Identity.Name,
                    LastUpdate = DateTime.Now,
                    Reason = reason,
                    Note = note
                };
                db.WalletHistorys.Add(wallerHistory);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public ActionResult AdditionWallet(int id, string cmd)
        {
            ViewBag.isaddition = cmd == "add";
            var wallet = db.Wallets.FirstOrDefault(c => c.Id == id);
            ViewBag.UserType = "system";
            if (wallet != null)
            {
                var userInfo = db.UserProfiles.FirstOrDefault(c => c.Phone == wallet.Client);
                if (userInfo != null)
                {
                    ViewBag.UserType = userInfo.UserType;
                }

            }
            var model = new AdditionModel { walletid = id };
            ViewBag.ListBankName = GetListBankName(null, "VND");
            return PartialView("_AdditionWallet", model);
        }

        private Wallet GetWallet(string client, string bankName, string currency = Utils.VND)
        {
            if (string.IsNullOrWhiteSpace(bankName))
                return db.Wallets.FirstOrDefault(a => a.Client == client && a.Currency == currency);

            return db.Wallets.FirstOrDefault(a => a.Client == client && a.Currency == currency && a.Name == bankName);

        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult AdditionWallet(AdditionModel model, string cmd)
        {

            //truong hop cong them tien vao tai khoan
            if (cmd == "addition")
            {
                var wallet = db.Wallets.FirstOrDefault(a => a.Id == model.walletid);
                if (wallet != null)
                {
                    var client = db.UserProfiles.FirstOrDefault(a => a.Phone == wallet.Client);
                    //if (client == null)
                    //{
                    //    return RedirectToAction("Error", "Home");
                    //}
                    wallet.Money = wallet.Money + model.money;
                    wallet.User_Update = User.Identity.Name;
                    wallet.LastUpdate = DateTime.Now;
                    InsertWalletHistory(wallet, model.money, string.Format("Nạp thêm tiền cho tài khoản số tiền: {0}", model.money.ToString("N0")),model.reason);

                    if (client != null)
                    {
                        if (Utils.CheckRole(client.UserType, (int)UserType.Client, false))
                        {
                            //trong truong hop nap them tien cho khach hang thi cong vao tai khoan he thong
                            var systemWallet = GetWallet(null, model.BankName);
                            if (model.BankName == null || systemWallet == null)
                                return RedirectToAction("Index", new { id = client.Phone });
                            systemWallet.Money += model.money;
                            systemWallet.LastUpdate = DateTime.Now;
                            InsertWalletHistory(systemWallet, model.money, string.Format("Nạp thêm tiền cho tài khoản {1} số tiền: {0}", model.money.ToString("N0"), wallet.Client));
                        }



                        #region Thuc hiện trừ nợ
                        var listOrderHasOwe = db.OrderInternals.Where(c => c.Owe != 0 && c.Phone == wallet.Client).OrderBy(c => c.OrderInternalId).ToList();
                        foreach (var orderInternal in listOrderHasOwe)
                        {
                            
                            if (wallet.Money > orderInternal.Owe)
                            {
                                wallet.Money -= orderInternal.Owe;
                                InsertWalletHistory(wallet, -1 * orderInternal.Owe, string.Format("Trừ nợ khi nạp thêm tiền cho order:{0} số tiền: {1}", orderInternal.OrderInternalId, orderInternal.Owe.ToString("N0")));
                                orderInternal.Owe = 0;
                            }
                        } 
                        #endregion

                        #region Trừ tiền hàng đã về kho
                        var listOrderReceive = db.OrderInternals.Where(c => c.Status == OrderStatus.Receive.ToString() && c.Phone == wallet.Client).OrderBy(c => c.OrderInternalId).ToList();
                        foreach (var orderInternal in listOrderReceive)
                        {
                            var totalMoney = orderInternal.TotalPriceConvert +
                                        Math.Round((orderInternal.TotalPriceConvert * orderInternal.Fee) / 100, MidpointRounding.ToEven) +
                                        orderInternal.FeeShipChina + orderInternal.FeeShip;
                            var meneySub = totalMoney - orderInternal.DownPayment;
                            if (meneySub < wallet.Money)
                            {
                                wallet.Money -= meneySub;
                                InsertWalletHistory(wallet, -1 * meneySub, string.Format("Thanh toán số tiền tiền còn thiếu cho order:{0} số tiền: {1}", orderInternal.OrderInternalId, meneySub.ToString("N0")));
                            }
                        }
                        #endregion

                        //check order and run flow order
                        var listOrder =
                            db.OrderInternals.Where(
                                m => m.Status == OrderStatus.SaleConfirm.ToString() && m.Phone == wallet.Client)
                                .OrderByDescending(a => a.TotalPriceConvert)
                                .ToList();
                        if (listOrder.Any())
                        {
                            foreach (OrderInternal order in listOrder)
                            {
                                //Kiểm tra thông tin tiền trong ví và thực hiện thu tiền
                                //get wallet for client
                               
                                    var money = wallet.Money;
                                    //total price for order
                                    //(Model.TotalPriceConvert + Math.Round((Model.TotalPriceConvert * Model.Fee) / 100, MidpointRounding.ToEven) + Model.FeeShipChina + Model.FeeShip).ToString("##,###")
                                    var totalMoney = order.TotalPriceConvert +
                                                     Math.Round((order.TotalPriceConvert * order.Fee) / 100, MidpointRounding.ToEven) +
                                                     order.FeeShipChina + order.FeeShip;

                                    //tính toán số tiền đặt cọc
                                    //if client collect full money, set downpayment rate = 100
                                    var moneyHold = (totalMoney * client.DownPaymentRate) / 100;
                                    if (moneyHold <= money)
                                    {
                                        //collect money and switch status of Order
                                        wallet.Money = wallet.Money - moneyHold;

                                        var wallerHistory = new WalletHistory
                                        {
                                            WalletId = wallet.Id,
                                            Money = moneyHold,
                                            UpdateType = WalletUpdateType.Subtract.ToString(),
                                            Client = wallet.Client,
                                            Currency = wallet.Currency,
                                            User_Update = User.Identity.Name,
                                            LastUpdate = DateTime.Now,
                                            Reason =
                                                string.Format(
                                                    "Thu {5}% của tổng đơn hàng {6}. Chi tiết: /n Tiền hàng: {0}; Phí dịch vụ({1} %): {2}; Ship nội địa TQ : {3}; Cước vận chuyển: {4}",
                                                    order.TotalPriceConvert.ToString("##,###"), order.Fee,
                                                    Math.Round((order.TotalPriceConvert * order.Fee) / 100, MidpointRounding.ToEven)
                                                        .ToString("##,###"), order.FeeShipChina.ToString("##,###"),
                                                    order.FeeShip.ToString("##,###"), client.DownPaymentRate,
                                                    totalMoney.ToString("##,###"))
                                        };
                                        db.WalletHistorys.Add(wallerHistory);
                                        //switch order
                                        order.Status = OrderStatus.Paid.ToString();
                                    }
                                    else
                                    {
                                        //check money in rank
                                        moneyHold = ((totalMoney - 5 > 0 ? totalMoney - 5 : 0) * client.DownPaymentRate) / 100;

                                        if (moneyHold <= money)
                                        {
                                            //subtruct full money in wallet
                                            //collect money and switch status of Order
                                            wallet.Money = wallet.Money - wallet.Money;

                                            var wallerHistory = new WalletHistory
                                            {
                                                WalletId = wallet.Id,
                                                Money = wallet.Money,
                                                UpdateType = WalletUpdateType.Subtract.ToString(),
                                                Client = wallet.Client,
                                                Currency = wallet.Currency,
                                                User_Update = User.Identity.Name,
                                                LastUpdate = DateTime.Now,
                                                Reason =
                                                    string.Format(
                                                        "Thu {5}% của tổng đơn hàng {6}. Chi tiết: /n Tiền hàng: {0}; Phí dịch vụ({1} %): {2}; Ship nội địa TQ : {3}; Cước vận chuyển: {4}",
                                                        order.TotalPriceConvert.ToString("##,###"), order.Fee,
                                                        Math.Round((order.TotalPriceConvert * order.Fee) / 100, MidpointRounding.ToEven)
                                                            .ToString("##,###"), order.FeeShipChina.ToString("##,###"),
                                                        order.FeeShip.ToString("##,###"), client.DownPaymentRate,
                                                        totalMoney.ToString("##,###"))
                                            };
                                            db.WalletHistorys.Add(wallerHistory);

                                            order.Status = OrderStatus.Paid.ToString();

                                        }

                                }
                            }
                        }
                    }

                    db.SaveChanges();

                    return RedirectToAction("Index", new { id = wallet.Client });
                }
            }
            else
            {

                //truong hop tru tien
                var wallet = db.Wallets.FirstOrDefault(a => a.Id == model.walletid);
                if (wallet != null)
                {

                    var client = db.UserProfiles.FirstOrDefault(c => c.Phone == wallet.Client);
                    //if (client == null)
                    //    return RedirectToAction("Error", "Home");
                    if (client != null)
                    {
                        if (Utils.CheckRole(client.UserType, (int)UserType.Client, false))
                        {
                            //trong truong hop nap them tien cho khach hang thi cong vao tai khoan he thong
                            var systemWallet = GetWallet(null, model.BankName);
                            if (model.BankName == null || systemWallet == null)
                                return RedirectToAction("Index", new { id = client.Phone });
                            systemWallet.Money -= model.money;
                            systemWallet.LastUpdate = DateTime.Now;
                            InsertWalletHistory(systemWallet, -1 * model.money, string.Format("Rút bớt tiền tài khoản {1} số tiền: {0}", model.money.ToString("N0"), wallet.Client),model.reason);
                        }
                    }

                    wallet.Money = wallet.Money - model.money;
                    wallet.User_Update = User.Identity.Name;
                    wallet.LastUpdate = DateTime.Now;
                    InsertWalletHistory(wallet, -1 * model.money, string.Format("Thực hiện trừ bớt tiền trong tài khoản số tiền: {0}", model.money.ToString("N0")), model.reason);
                    db.SaveChanges();

                    return RedirectToAction("Index", new { id = wallet.Client });
                }
            }

            return RedirectToAction("Error", "Home");
        }

        [HttpGet]
        public ActionResult ListClient(string userName, string customerCode, int? page)
        {

            //listClient
            var list = new List<UserProfile>();
            //add node system
            list.Add(new UserProfile
            {
                Email = "Hệ thống",
                Name = "Hệ thống"
            });
            var userProfiles = db.UserProfiles.Where(a => a.UserType.Contains(((int)UserType.Client).ToString()) || a.UserType.Contains(((int)UserType.Orderer).ToString())).ToList();
            if (userProfiles.Count > 0)
            {
                list.AddRange(userProfiles);
            }
            if (!string.IsNullOrEmpty(userName))
            {
                list = list.FindAll(a => !String.Equals(a.Phone, User.Identity.Name, StringComparison.CurrentCultureIgnoreCase) && a.Phone == userName);
            }

            if (!string.IsNullOrEmpty(customerCode))
            {
                list = list.FindAll(a => a.customercode == customerCode);
            }

            ViewBag.CurrentUserName = userName;
            ViewBag.CurrentCustomercode = customerCode;

            const int pageSize = 50;
            int pageNumber = (page ?? 1);
            return View(list.ToPagedList(pageNumber, pageSize));
        }

    }
}
