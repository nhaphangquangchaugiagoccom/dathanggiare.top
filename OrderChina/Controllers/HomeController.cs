﻿using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using Newtonsoft.Json;
using OrderChina.Models;
using WebMatrix.WebData;
using System.Text.RegularExpressions;
using System;
using System.Collections.Generic;
using PagedList;
using System.Web;
using PagedList.Mvc;
using OrderChina.Filters;
using System.Net;
using System.Text;
using OrderChina.Common;

namespace OrderChina.Controllers
{
    public class HomeController : Controller
    {
        DBContext db = new DBContext();
        public ActionResult Index()
        {

            if (db.Rates.Any())
            {
                var rate = db.Rates.FirstOrDefault();
                if (rate != null)
                {
                    Session["Price"] = rate.Price.ToString("##,###");
                    Session["fee1"] = rate.FormatPrice(rate.fee1);
                    Session["fee2"] = rate.FormatPrice(rate.fee2);
                    Session["fee3"] = rate.FormatPrice(rate.fee3);
                }
            }
            if (Request.IsAuthenticated)
            {
                var userProfile = db.UserProfiles.FirstOrDefault(a => a.Phone == User.Identity.Name);
                var check = db.Wallets.Any(a => a.Client == User.Identity.Name);
                if (check)
                {
                    var vi = db.Wallets.FirstOrDefault(a => a.Client == User.Identity.Name);
                    if (userProfile != null)
                    {
                        Session["Name"] = userProfile.Name;
                        Session["ID"] = userProfile.UserId;
                        Session["Email"] = userProfile.Email;
                        Session["Phone"] = userProfile.Phone;
                        Session["UserType"] = userProfile.UserType;
                        if (userProfile.UserType == "5")
                        {

                            Session["currency_header"] = vi.Currency;
                            Session["wallet_header"] = vi.Money;

                        }
                    }
                }

                if (userProfile != null)
                {
                    Session["Name"] = userProfile.Name;
                    Session["ID"] = userProfile.UserId;
                    Session["Email"] = userProfile.Email;
                    Session["Phone"] = userProfile.Phone;
                    Session["UserType"] = userProfile.UserType;

                }
            }
            else
            {
                //tao cookie phuc vu chat
                var cookieChat = Request.Cookies["ChatUser"];
                if (cookieChat != null)
                {
                    Session["ChatId"] = cookieChat.Value;
                }
                else
                {
                    HttpCookie chatcookie = new HttpCookie("ChatUser", GetClientIpAddress(Request));
                    Response.Cookies.Add(chatcookie);
                }

                var cookie = Request.Cookies["ci_sessions_plugin"];
                if (cookie == null)
                {
                    var value = Guid.NewGuid().ToString().Replace("-", "");
                    HttpCookie chatcookie = new HttpCookie("ci_sessions_plugin", value);
                    Response.Cookies.Add(chatcookie);
                }
            }

            return View();
        }

        public ActionResult About()
        {

            return View();
        }

        public ActionResult Contact()
        {

            return View();
        }
        public ActionResult Introduce()
        {

            return View();
        }
        public ActionResult Banggia()
        {
            var model = new WatchListModel { ListFee = db.Fees.ToList(), ListClot = db.Clots.ToList(), ListFreight = db.Freights.ToList(), ListCNY = db.CNYs.ToList() };
            return View(model);
        }
        public ActionResult thanhtoan()
        {

            return View();
        }
        public ActionResult lienhe()
        {

            return View();
        }


        #region Mobile Function
        [HttpPost]
        [AllowAnonymous]
        public ActionResult LoginMobile()
        {
            var errorCode = "Exception";
            var token = string.Empty;
            UserProfile user = null;
            try
            {
                var json = GetStringRequest();
                var loginParam = JsonConvert.DeserializeObject<LoginParam>(json);
                if (loginParam != null)
                {
                    if (!string.IsNullOrWhiteSpace(loginParam.TokenId))
                        token = loginParam.TokenId.Replace(" ", "");

                    user = db.UserProfiles.FirstOrDefault(a => (a.Email == loginParam.UserName || a.Phone == loginParam.UserName) && a.Password == loginParam.PassWord);
                    if (user != null)
                    {
                        errorCode = user.UserType == "5" ? string.Empty : "Bạn không phải khách hàng";
                    }
                    else
                    {
                        errorCode = "Tên người dùng hoặc mật khẩu không đúng";
                    }
                }
                else
                {
                    errorCode = "Tên người dùng hoặc mật khẩu không đúng";
                }
            }
            catch (Exception)
            {
                errorCode = "Exception";
            }

            // truong hop thanh cong luu lai TokenId
            if (string.IsNullOrWhiteSpace(errorCode) && !string.IsNullOrWhiteSpace(token))
            {
                if (user != null)
                {
                    user.TokenId = token;
                    db.SaveChanges();
                }
            }
            return Json(new { ErrorCode = errorCode, UserProfile = user });
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult ViewOrderNoShopDetail()
        {
            var model = new ViewDetailOrderNonShopModel();
            try
            {
                var json = GetStringRequest();
                var loginParam = JsonConvert.DeserializeObject<OrderDetailParam>(json);
                if (loginParam != null)
                {
                    if (loginParam.IsOrder)
                    {
                        var order = db.Orders.FirstOrDefault(a => a.OrderId == loginParam.OrderId);
                        var orderDetail = db.OrderDetails.Where(a => a.OrderId == loginParam.OrderId).ToList();
                        model.OrderId = loginParam.OrderId;

                        if (order != null)
                        {
                            model.Phone = order.Phone;
                            model.Rate = order.Rate;
                            model.Status = order.Status;
                            model.StatusText = order.getStatusText();
                            model.TotalPriceConvert = order.TotalPriceConvert;
                            model.TotalPrice = order.TotalPrice;
                            model.Phone = order.Phone;
                            model.FeeShip = order.FeeShip;
                            model.FeeShipChina = order.FeeShipChina;
                            model.Fee = order.Fee;
                            model.DownPayment = order.DownPayment;
                            model.AccountingCollected = order.AccountingCollected;


                            if (!string.IsNullOrEmpty(order.SaleManager))
                            {
                                UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Phone == order.SaleManager);
                                if (user != null)
                                {
                                    model.SaleManageInfo = new SaleManageInfo
                                    {
                                        SaleName = user.Name,
                                        SalePhone = user.Phone,
                                        SalePhoneCompany = user.PhoneCompany
                                    };
                                }
                            }

                            if (!string.IsNullOrEmpty(order.Phone))
                            {
                                UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Phone == order.Phone);
                                if (user != null)
                                {
                                    model.Client = user;
                                }
                            }
                        }
                        model.ListOrderDetails = orderDetail;
                    }
                    else
                    {
                        var order = db.OrderInternals.FirstOrDefault(a => a.OrderId == loginParam.OrderId);
                        var orderDetail = db.OrderDetails.Where(a => a.OrderId == loginParam.OrderId).ToList();
                        model.OrderId = loginParam.OrderId;

                        if (order != null)
                        {
                            model.Phone = order.Phone;
                            model.Rate = order.Rate;
                            model.Status = order.Status;
                            model.StatusText = order.getStatusText();
                            model.TotalPriceConvert = order.TotalPriceConvert;
                            model.TotalPrice = order.TotalPrice;
                            model.Phone = order.Phone;
                            model.FeeShip = order.FeeShip;
                            model.FeeShipChina = order.FeeShipChina;
                            model.Fee = order.Fee;
                            model.DownPayment = order.DownPayment;
                            model.AccountingCollected = order.AccountingCollected;

                            if (!string.IsNullOrEmpty(order.SaleManager))
                            {
                                UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Phone == order.SaleManager);
                                if (user != null)
                                {
                                    model.SaleManageInfo = new SaleManageInfo
                                    {
                                        SaleName = user.Name,
                                        SalePhone = user.Phone,
                                        SalePhoneCompany = user.PhoneCompany
                                    };
                                }
                            }

                            if (!string.IsNullOrEmpty(order.Phone))
                            {
                                UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Phone == order.Phone);
                                if (user != null)
                                {
                                    model.Client = user;
                                }
                            }
                        }
                        model.ListOrderDetails = orderDetail;
                    }
                    
                }
            }
            catch (Exception exx)
            {
                Console.WriteLine(exx);
            }
            return Json(new { ObjectDetail = model }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [AllowAnonymous]
        public ActionResult ViewOrderTransDetail()
        {
            var model = new ViewDetailOrderModel();

            try
            {
                var json = GetStringRequest();
                var loginParam = JsonConvert.DeserializeObject<OrderDetailParam>(json);
                if (loginParam != null)
                {
                    var order = db.Orders.FirstOrDefault(a => a.OrderId == loginParam.OrderId);
                    if (order != null)
                    {
                        model.Content = order.Content;
                        model.OrderId = order.OrderId;
                        model.Phone = order.Phone;
                        model.Rate = order.Rate;
                        model.Status = order.Status;
                        model.StatusText = order.getStatusText();
                        model.Incurred = order.Incurred;
                        model.Inland = order.Inland;
                        model.Pulling = order.Pulling;
                        model.SaleManager = order.SaleManager;
                        model.UserName = order.UserName;
                        model.Phone = order.Phone;
                        model.FeeShip = order.FeeShip;
                        model.FeeShipChina = order.FeeShipChina;
                        model.CreateDate = order.CreateDate;
                        model.Fee = order.Fee;
                        model.Weight = order.Weight;
                        model.DownPayment = order.DownPayment;
                        model.AccountingCollected = order.AccountingCollected;
                        model.OrderType = order.OrderType;
                        model.TotalPriceConvert = model.Pulling + model.Incurred + model.Inland + model.FeeShipChina + model.FeeShip;

                        if (!string.IsNullOrEmpty(order.SaleManager))
                        {
                            UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Phone == order.SaleManager);
                            if (user != null)
                            {
                                model.SaleManageInfo = new SaleManageInfo
                                {
                                    SaleName = user.Name,
                                    SalePhone = user.Phone,
                                    SalePhoneCompany = user.PhoneCompany
                                };
                            }
                        }

                        if (!string.IsNullOrEmpty(order.Phone))
                        {
                            UserProfile user = db.UserProfiles.FirstOrDefault(a => a.Phone == order.Phone);
                            if (user != null)
                            {
                                model.Client = user;
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                Console.WriteLine("");
            }
            return Json(new { ObjectDetail = model });
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult UpdateLoginMobile()
        {
            var errorCode = "Exception";
            try
            {
                var json = GetStringRequest();
                var loginParam = JsonConvert.DeserializeObject<UserProfile>(json);
                if (loginParam != null)
                {

                    var user = db.UserProfiles.FirstOrDefault(a => a.Email == loginParam.Email && a.Phone == loginParam.Phone);
                    if (user != null)
                    {
                        user.Name = loginParam.Name;
                        //user.Phone = loginParam.Phone;
                        user.PhoneCompany = loginParam.PhoneCompany;
                        user.Birthday = loginParam.Birthday;
                        // user.Address = loginParam.Address;
                        user.Gender = loginParam.Gender;
                        db.SaveChanges();
                    }
                    else
                    {
                        errorCode = "Không tồn tại người dùng trong hệ thống";
                    }
                }
                else
                {
                    errorCode = "Không tồn tại người dùng trong hệ thống";
                }
            }
            catch (Exception)
            {
                errorCode = "Exception";
            }

            return Json(new { ErrorCode = errorCode }, JsonRequestBehavior.AllowGet);
        }


        private string GetStringRequest()
        {
            var req = Request.InputStream;
            var json = new StreamReader(req).ReadToEnd();
            return json;
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult ManageOrderList()
        {
            var listNoShop = new List<Order>();
            var listOrder = new List<OrderInternal>();
            var listTranf = new List<OrderInternal>();
            try
            {
                var json = GetStringRequest();
                var orderListParam = JsonConvert.DeserializeObject<OrderListParam>(json);
                if (orderListParam != null)
                {
                    var fromDate = orderListParam.FromDate.Date;
                    orderListParam.FromDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day);

                    var listTemp = db.OrderInternals.ToList();

                    var toDate = orderListParam.ToDate.AddDays(1);
                    toDate = new DateTime(toDate.Year, toDate.Month, toDate.Day);
                    orderListParam.ToDate = toDate.AddSeconds(-1);
                    listNoShop = (from order in db.Orders
                                  where orderListParam.FromDate <= order.CreateDate
                                  && order.CreateDate <= orderListParam.ToDate
                                  && order.OrderType != OrderType.Ship.ToString()
                                  && order.OrderType != OrderType.Complaint.ToString()
                                  && order.IsShop == false
                                  && order.Status != OrderStatus.Receive.ToString()
                                  && order.Status != OrderStatus.FullCollect.ToString()
                                  && order.Phone == orderListParam.UserName
                                  select order).ToList();

                    listOrder = (from order in db.OrderInternals
                                 where orderListParam.FromDate <= order.CreateDate
                                  && order.OrderType != OrderType.Ship.ToString()
                                  && order.OrderType != OrderType.Complaint.ToString()
                                  && order.IsShop 
                                 && order.CreateDate <= orderListParam.ToDate
                                  && order.Phone == orderListParam.UserName
                                 select order).ToList();

                    listTranf = (from order in db.OrderInternals
                                 where orderListParam.FromDate <= order.CreateDate
                                 && order.CreateDate <= orderListParam.ToDate
                                 && order.OrderType == OrderType.Complaint.ToString()
                                 && order.Phone == orderListParam.Email
                                 select order).ToList();
                }
            }
            catch (Exception)
            {

                Console.WriteLine("");
            }

            return Json(new
            {
                ListNoShop = listNoShop,
                ListOrder = listOrder,
                ListTranf = listTranf,
            });
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult OrderForex()
        {
            var listData = new List<Forex>();
            try
            {
                var json = GetStringRequest();
                var orderListParam = JsonConvert.DeserializeObject<OrderListParam>(json);
                if (orderListParam != null)
                {
                    var fromDate = orderListParam.FromDate.Date;
                    orderListParam.FromDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day);

                    var toDate = orderListParam.ToDate.AddDays(1);
                    toDate = new DateTime(toDate.Year, toDate.Month, toDate.Day);
                    orderListParam.ToDate = toDate.AddSeconds(-1);
                    listData = (from order in db.Forexs
                                where orderListParam.FromDate <= order.Date
                                && order.Date <= orderListParam.ToDate
                                && order.Forex_Phone == orderListParam.UserName
                                select order).ToList();
                }
            }
            catch (Exception)
            {
                Console.WriteLine("");
            }

            return Json(new
            {
                ListData = listData,
            });
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult OrderTransport()
        {
            var listData = new List<Order>();
            try
            {
                var json = GetStringRequest();
                var orderListParam = JsonConvert.DeserializeObject<OrderListParam>(json);
                if (orderListParam != null)
                {
                    var fromDate = orderListParam.FromDate.Date;
                    orderListParam.FromDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day);
                    var toDate = orderListParam.ToDate.AddDays(1);
                    toDate = new DateTime(toDate.Year, toDate.Month, toDate.Day);
                    orderListParam.ToDate = toDate.AddSeconds(-1);

                    listData = (from order in db.Orders
                                where orderListParam.FromDate <= order.CreateDate
                                && order.CreateDate <= orderListParam.ToDate
                                && order.OrderType == OrderType.Ship.ToString()
                                && order.Phone == orderListParam.UserName
                                select order).ToList();
                }
            }
            catch (Exception)
            {
                Console.WriteLine("");
            }

            return Json(new
            {
                ListData = listData,
            });
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult CancelOrderTranfomer()
        {
            var errorCode = "Exception";
            try
            {
                var json = GetStringRequest();
                var orderListParam = JsonConvert.DeserializeObject<OrderDetailParam>(json);
                if (orderListParam != null)
                {
                    var order = db.Orders.FirstOrDefault(m => m.OrderId == orderListParam.OrderId);
                    if (order != null)
                    {
                        order.Status = OrderStatus.Cancel.ToString();
                        db.SaveChanges();
                        errorCode = "Success";
                    }
                }
            }
            catch (Exception eee)
            {
                Console.WriteLine("");
            }
            return Json(new { ErrorCode = errorCode });
        }


        [HttpPost]
        [AllowAnonymous]
        public ActionResult CancelOrder()
        {
            var errorCode = "Exception";
            try
            {
                var json = GetStringRequest();
                var orderListParam = JsonConvert.DeserializeObject<OrderDetailParam>(json);
                if (orderListParam != null)
                {
                    var order = db.OrderInternals.FirstOrDefault(m => m.OrderInternalId == orderListParam.OrderId);
                    if (order != null)
                    {
                        order.Status = OrderStatus.Cancel.ToString();
                        db.SaveChanges();
                        errorCode = "Success";
                    }
                }
            }
            catch (Exception eee)
            {
                Console.WriteLine("");
            }
            return Json(new { ErrorCode = errorCode });
        }

        #endregion

        #region LoginCustomer
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (IsEmail(model.Email))
            {
                if (IsValid(model.Email, model.Password))
                {
                    var user = db.UserProfiles.FirstOrDefault(a => a.Email == model.Email);
                    var vi = db.Wallets.FirstOrDefault(a => a.Client == user.Phone);
                    if (user != null && user.UserType != "5")
                    {
                        ModelState.AddModelError("", "Bạn không phải khách hàng ");
                    }
                    else if (user != null)
                    {
                        FormsAuthentication.SetAuthCookie(user.Phone, model.RememberMe);
                        //var authTicket = new FormsAuthenticationTicket(model.Email, model.RememberMe, 1);
                        //var EncryptedTicket = FormsAuthentication.Encrypt(authTicket);
                        //var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, EncryptedTicket);

                        //Response.Cookies.Add(authCookie);
                        if (!string.IsNullOrEmpty(returnUrl))
                        {
                            if (db.Rates.Any())
                            {
                                Rate rate = db.Rates.FirstOrDefault();
                                if (rate != null)
                                {
                                    Session["Price"] = rate.Price.ToString("##,###");
                                    Session["fee1"] = rate.FormatPrice(rate.fee1);
                                    Session["fee2"] = rate.FormatPrice(rate.fee2);
                                    Session["fee3"] = rate.FormatPrice(rate.fee3);

                                }
                            }

                        }
                        if (user.UserType == "5")
                        {

                            Session["currency_header"] = vi.Currency;
                            Session["wallet_header"] = vi.Money;
                        }
                        Session["Name"] = user.Name;
                        Session["ID"] = user.UserId;
                        Session["UserType"] = user.UserType;
                        Session["Email"] = user.Email;
                        Session["Phone"] = user.Phone;
                        Session["Image"] = user.Image;
                        if (user.UserType == "5")
                        {
                            return RedirectToAction("BangChung_User", "Account");
                        }
                        return RedirectToAction("Index");

                    }

                }

            }
            else
            {
                //phone
                if (IsValidPhone(model.Email, model.Password))
                {

                    FormsAuthentication.SetAuthCookie(model.Email, model.RememberMe);
                    //var authTicket = new FormsAuthenticationTicket(model.Email, model.RememberMe, 1);
                    //var EncryptedTicket = FormsAuthentication.Encrypt(authTicket);
                    //var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, EncryptedTicket);

                    //Response.Cookies.Add(authCookie);
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        if (db.Rates.Any())
                        {
                            Rate rate = db.Rates.FirstOrDefault();
                            if (rate != null)
                            {
                                Session["Price"] = rate.Price.ToString("##,###");
                                Session["fee1"] = rate.FormatPrice(rate.fee1);
                                Session["fee2"] = rate.FormatPrice(rate.fee2);
                                Session["fee3"] = rate.FormatPrice(rate.fee3);
                            }
                        }
                        var userProfile = db.UserProfiles.FirstOrDefault(a => a.Email == model.Email);
                        var vi = db.Wallets.FirstOrDefault(a => a.Client == userProfile.Phone);
                        if (userProfile != null)
                        {
                            if (userProfile.UserType == "Client")
                            {

                                Session["currency_header"] = vi.Currency;
                                Session["wallet_header"] = vi.Money;
                            }
                            Session["Name"] = userProfile.Name;
                            Session["ID"] = userProfile.UserId;
                            Session["UserType"] = userProfile.UserType;
                            Session["Email"] = userProfile.Email;
                            Session["Phone"] = userProfile.Phone;
                            Session["Image"] = userProfile.Image;
                            if (userProfile.UserType == "5")
                            {
                                return RedirectToAction("BangChung_User", "Account");
                            }
                            return RedirectToAction("Index");
                        }
                    }

                }
            }


            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "Tên người dùng hoặc mật khẩu được cung cấp là không chính xác .");
            return View(model);
        }

        public ActionResult LogOff()
        {
            WebSecurity.Logout();

            Session.Remove("Name");
            Session.Remove("ID");
            Session.Remove("UserType");
            Session.Remove("wallet_header");
            Session.Remove("currency_header");
            Session.Remove("Image");

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult GetTextTranslate(string page_source, string input)
        {
            string url = string.Empty;
            string text = string.Empty;
            switch (page_source)
            {
                //< li class="" data-type="1">
                //           <a>Taobao.com</a>
                //       </li>
                //       <li data-type="2" class="t-active">
                //           <a>Alibaba.com</a>
                //       </li>
                //       <li data-type="3">
                //           <a>Tmall.com</a>
                //       </li>
                //       <li data-type="4">
                //           <a>1688.com</a>
                //       </li>
                case "1":
                    text = Translate(input, "zh_CN");
                    url = $"https://world.taobao.com/search/search.htm?sort=_deal&_ksTS=1479974326609_520&spm=a21bp.7806943.20151106.1&search_type=0&json=on&cna=PCq8EGomkmACAd78ESrhDfOA&module=sortList&_input_charset=utf-8&s=0&navigator=all&q={text}&callback=__jsonp_cb&abtest=_AB-LR517-LR854-LR895-PR517-PR854-PR895";
                    break;
                case "2":
                    text = Translate(input, "en");
                    url = $"http://www.alibaba.com/trade/search?fsb=y&IndexArea=product_en&CatId=&SearchText={text}";
                    break;
                case "3":
                    text = Translate(input, "zh_CN");
                
                    url = $"https://list.tmall.com/search_product.htm?spm=a220m.1000858.1000724.4.6K8dOu&q={text}&sort=d&style=g&from=.list.pc_1_searchbutton#J_Filter";
                    break;
                case "4":
                    text = Translate(input, "zh_CN");
                    //special
                    Encoding gbk = Encoding.GetEncoding("gbk");
                    text = HttpUtility.UrlEncode(text, gbk);
               
                    url = $" https://s.1688.com/selloffer/offer_search.htm?descendOrder=true&sortType=va_rmdarkgmv180&uniqfield=userid&keywords={text}&earseDirect=false&n=y#sm-filtbar";
                    break;

            }

            return Json(new { url = url });
        }

        private bool IsValidPhone(string phone, string password)
        {
            bool IsValid = false;

            var user = db.UserProfiles.FirstOrDefault(u => u.Phone == phone);
            if (user != null && user.UserType != "5")
            {
                ModelState.AddModelError("", "Bạn không phải khách hàng ");
            }
            else if (user != null)
            {
                if (user.Password == password)
                {
                    IsValid = true;
                }
            }

            return IsValid;
        }
        private bool IsValid(string email, string password)
        {
            bool IsValid = false;

            var user = db.UserProfiles.FirstOrDefault(u => u.Email == email);
            if (user != null)
            {
                if (user.Password == password)
                {
                    IsValid = true;
                }
            }

            return IsValid;
        }
        [AllowAnonymous]
        public ActionResult IsCheckExitsPhone(string Phone)
        {
            return Json(!db.UserProfiles.Any(lo => lo.Phone == Phone), JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult IsCheckEmail(string Email)
        {
            if (string.IsNullOrEmpty(Email))
                return Json(true, JsonRequestBehavior.AllowGet);

            var str = Email.ToLower();
            //kiem tra cac ki tu
            const String pattern =
                   @"^([0-9a-zA-Z]" + //Start with a digit or alphabetical
                   @"([\+\-_\.][0-9a-zA-Z]+)*" + // No continuous or ending +-_. chars in email
                   @")+" +
                   @"@(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9]{2,17})$";
            if (!Regex.IsMatch(str, pattern))
            {
                return Json("Email không đúng định dạng", JsonRequestBehavior.AllowGet);

            }
            //check trung
            if (db.UserProfiles.Any(lo => lo.Email.ToLower() == Email.ToLower()))
            {
                return Json("Email đã tồn tại trong hệ thống", JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        public bool IsEmail(string Email)
        {
            var str = Email.ToLower();
            //kiem tra cac ki tu
            const String pattern =
                   @"^([0-9a-zA-Z]" + //Start with a digit or alphabetical
                   @"([\+\-_\.][0-9a-zA-Z]+)*" + // No continuous or ending +-_. chars in email
                   @")+" +
                   @"@(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9]{2,17})$";

            return Regex.IsMatch(str, pattern);
        }

        #endregion

        public static string GetClientIpAddress(HttpRequestBase request)
        {
            try
            {
                var userHostAddress = request.UserHostAddress;

                // Attempt to parse.  If it fails, we catch below and return "0.0.0.0"
                // Could use TryParse instead, but I wanted to catch all exceptions
                IPAddress.Parse(userHostAddress);

                var xForwardedFor = request.ServerVariables["X_FORWARDED_FOR"];

                if (string.IsNullOrEmpty(xForwardedFor))
                    return userHostAddress;

                // Get a list of public ip addresses in the X_FORWARDED_FOR variable
                var publicForwardingIps = xForwardedFor.Split(',').Where(ip => !IsPrivateIpAddress(ip)).ToList();

                // If we found any, return the last one, otherwise return the user host address
                return publicForwardingIps.Any() ? publicForwardingIps.Last() : userHostAddress;
            }
            catch (Exception)
            {
                // Always return all zeroes for any failure (my calling code expects it)
                return "0.0.0.0";
            }
        }

        private static bool IsPrivateIpAddress(string ipAddress)
        {
            // http://en.wikipedia.org/wiki/Private_network
            // Private IP Addresses are: 
            //  24-bit block: 10.0.0.0 through 10.255.255.255
            //  20-bit block: 172.16.0.0 through 172.31.255.255
            //  16-bit block: 192.168.0.0 through 192.168.255.255
            //  Link-local addresses: 169.254.0.0 through 169.254.255.255 (http://en.wikipedia.org/wiki/Link-local_address)

            var ip = IPAddress.Parse(ipAddress);
            var octets = ip.GetAddressBytes();

            var is24BitBlock = octets[0] == 10;
            if (is24BitBlock) return true; // Return to prevent further processing

            var is20BitBlock = octets[0] == 172 && octets[1] >= 16 && octets[1] <= 31;
            if (is20BitBlock) return true; // Return to prevent further processing

            var is16BitBlock = octets[0] == 192 && octets[1] == 168;
            if (is16BitBlock) return true; // Return to prevent further processing

            var isLinkLocalAddress = octets[0] == 169 && octets[1] == 254;
            return isLinkLocalAddress;
        }

        private string Translate(string input, string lang)
        {
            string url = String.Format("http://www.google.com/translate_t?hl={1}&ie=UTF8&text={0}&langpair=vi", HttpUtility.UrlEncode(input), lang);
            //string url = String.Format("http://www.google.com/translate_t?hl=zh-CN&ie=UTF8&text={0}&langpair={1}", HttpUtility.UrlEncode(input), languagePair);

            WebClient webClient = new WebClient();
            webClient.Encoding = System.Text.Encoding.UTF8;
            webClient.Headers.Add(HttpRequestHeader.UserAgent, "Mozilla/5.0");
            webClient.Headers.Add(HttpRequestHeader.AcceptCharset, "UTF-8");

            string result = webClient.DownloadString(url);
            result = result.Substring(result.IndexOf("<span title=\"") + "<span title=\"".Length);
            result = result.Substring(result.IndexOf(">") + 1);
            result = result.Substring(0, result.IndexOf("</span>"));
            return result.Trim();
        }

        public ActionResult Error()
        {
            return View();
        }
    }
    public class LoginParam
    {
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public string TokenId { get; set; }
    }
    public class OrderDetailParam
    {
        public int OrderId { get; set; }
        public bool IsOrder { get; set; }
    }
    public class OrderListParam
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}
