﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.SignalR;
using OrderChina.Common;
using OrderChina.Models;
using System.Data.Entity.Core.Objects;

namespace OrderChina
{
    public class ChatHub : Hub
    {
        #region Data Members
        static List<UserDetail> ConnectedClientNonLogin = new List<UserDetail>();
        static List<UserDetail> ConnectedClientLogin = new List<UserDetail>();
        static List<UserDetail> ConnectedUser = new List<UserDetail>();

        private readonly DBContext db = new DBContext();
        #endregion

        #region Methods

        public void Connect(string isAuthen, string userId, string clientCookieId)
        {
            var id = Context.ConnectionId;
            if (isAuthen == "True")
            {

                var user = db.UserProfiles.FirstOrDefault(a => a.Phone == userId);
                if (user == null)
                    return;

                if (!Utils.CheckRole(user.UserType, (int)UserType.Client, false))
                {
                    if (ConnectedUser.Count(x => x.ConnectionId == id) == 0)
                    {
                        var userDetail = new UserDetail { ConnectionId = id, UserName = user.Name, Phone = user.Phone, UserTypeText = user.getUserType(), UserType = user.UserType, UserId = user.UserId.ToString(), isclient = false };

                        ConnectedUser.Add(userDetail);

                        if (Utils.CheckRole(user.UserType, (int)UserType.Sale, false))
                        {
                            //chat voi clientnonlogin and client sale manage
                            //get user sale manage
                            var clientUserNonLogin = ConnectedClientNonLogin.Where(a => (a.ischat && string.IsNullOrEmpty(a.UserSaleChatId)) || a.UserSaleChatId == userDetail.UserId).ToList();

                            var listClientManage =
                                db.SaleManageClients.Where(a => a.User_Sale == userId).Select(a => a.User_Client).ToList();
                            var listUserProfileClient = db.UserProfiles.Where(a => listClientManage.Contains(a.Phone));

                            var clientUserManage = new List<UserDetail>();

                            foreach (var item in listUserProfileClient)
                            {
                                var objectUser = new UserDetail
                                {
                                    UserId = item.UserId.ToString(),
                                    UserName = item.getUserType() + "-" + item.Name
                                };
                                if (ConnectedClientLogin.Exists(a => a.UserId == item.UserId.ToString()))
                                {
                                    objectUser.isonline = true;
                                    objectUser.ConnectionId = ConnectedClientLogin.FirstOrDefault(a => a.UserId == item.UserId.ToString()).ConnectionId;
                                }
                                else
                                {
                                    objectUser.isonline = false;
                                }

                                using (var dbException = new DBContext())
                                {
                                    var chatActivity = dbException.ChatActivitys.FirstOrDefault(a => a.ToUserId == userDetail.UserId && a.FromUserId == item.UserId.ToString());

                                    if (chatActivity != null)
                                    {
                                        if (chatActivity.IsChat)
                                        {
                                            objectUser.ischat = true;
                                        }
                                    }
                                }


                                clientUserManage.Add(objectUser);
                            }
                            //get list chat nhung chua co ai tra loi
                            var listConnectionNonManage = ConnectedClientLogin.Where(a => (string.IsNullOrEmpty(a.UserSaleChatId) || a.UserSaleChatId == user.UserId.ToString()));

                            foreach (var item in listConnectionNonManage)
                            {
                                if (!clientUserManage.Exists(a => a.UserId == item.UserId))
                                {
                                    item.isonline = true;
                                    clientUserManage.Add(item);
                                }
                            }

                            var listUserProfile = new List<UserProfile>();
                            var listUser = new List<UserDetail>();

                            foreach (var item in db.UserProfiles)
                            {
                                if (!Utils.CheckRole(item.UserType, (int)UserType.Client, false))
                                {
                                    if (userId == item.Phone)
                                        continue;

                                    listUserProfile.Add(item);
                                }
                            }
                            foreach (var item in listUserProfile)
                            {
                                var objectUser = new UserDetail
                                {
                                    UserId = item.UserId.ToString(),
                                    UserName = item.getUserType() + "-" + item.Name
                                };
                                if (ConnectedUser.Exists(a => a.UserId == item.UserId.ToString()))
                                {
                                    objectUser.isonline = true;
                                    objectUser.ConnectionId = ConnectedUser.FirstOrDefault(a => a.UserId == item.UserId.ToString()).ConnectionId;
                                }
                                else
                                {
                                    objectUser.isonline = false;
                                }
                                using (var dbException = new DBContext())
                                {
                                    var chatActivity = db.ChatActivitys.FirstOrDefault(a => a.ToUserId == userDetail.UserId && a.FromUserId == item.UserId.ToString());

                                    if (chatActivity != null)
                                    {
                                        if (chatActivity.IsChat)
                                        {
                                            objectUser.ischat = true;
                                        }
                                    }
                                }


                                listUser.Add(objectUser);
                            }

                            Clients.Caller.onConnected(id, userDetail, clientUserNonLogin, clientUserManage, listUser);
                        }
                        else
                        {

                            var listUserProfile = new List<UserProfile>();
                            var listUser = new List<UserDetail>();

                            foreach (var item in db.UserProfiles)
                            {
                                if (!Utils.CheckRole(item.UserType, (int)UserType.Client, false))
                                {
                                    if (userId == item.Phone)
                                        continue;

                                    listUserProfile.Add(item);
                                }
                            }
                            foreach (var item in listUserProfile)
                            {
                                var objectUser = new UserDetail
                                {
                                    UserId = item.UserId.ToString(),
                                    UserName = item.getUserType() + "-" + item.Name
                                };
                                if (ConnectedUser.Exists(a => a.UserId == item.UserId.ToString()))
                                {
                                    objectUser.isonline = true;
                                    objectUser.ConnectionId = ConnectedUser.FirstOrDefault(a => a.UserId == item.UserId.ToString()).ConnectionId;
                                }
                                else
                                {
                                    objectUser.isonline = false;
                                }
                                using (var dbException = new DBContext())
                                {
                                    var chatActivity = db.ChatActivitys.FirstOrDefault(a => a.ToUserId == userDetail.UserId && a.FromUserId == item.UserId.ToString());

                                    if (chatActivity != null)
                                    {
                                        if (chatActivity.IsChat)
                                        {
                                            objectUser.ischat = true;
                                        }
                                    }
                                }


                                listUser.Add(objectUser);
                            }

                            Clients.Caller.onConnected(id, userDetail, null, null, listUser);

                        }


                        //onNew to user other
                        var userOtherConnected = ConnectedUser.Where(a => !a.UserType.Contains(((int)UserType.Sale).ToString())).Select(a => a.ConnectionId).ToList();
                        if (userOtherConnected != null)
                        {
                            Clients.Clients(userOtherConnected).onNewUserConnected(id, userDetail);
                        }
                    }
                }
                else
                {
                    if (ConnectedClientLogin.Count(x => x.ConnectionId == id) == 0)
                    {
                        var userClient = db.UserProfiles.FirstOrDefault(a => a.Phone == userId);
                        var userDetail = new UserDetail { ConnectionId = id, UserName = user.Name, Phone = userClient.Phone, UserTypeText = userClient.getUserType(), UserType = userClient.UserType, UserId = userClient.UserId.ToString(), isclient = true, isNotLogin = false };
                        ConnectedClientLogin.Add(userDetail);

                        //var listUserLoadWindows = new List<string>();
                        //var listChatActivity = db.ChatActivitys.Where(a => a.ToUserId == userDetail.UserId && a.IsChat);

                        //if (listChatActivity != null && listChatActivity.Count() > 0)
                        //{
                        //    foreach (var item in listChatActivity)
                        //    {
                        //        listUserLoadWindows.Add(item.FromUserId);
                        //    }
                        //}

                        Clients.Caller.onConnected(id, userDetail);

                        var saleManage = db.SaleManageClients.FirstOrDefault(a => a.User_Client == userId);
                        if (saleManage != null)
                        {
                            userDetail.UserSaleChatId = userClient.UserId.ToString();

                            var saleConnected = ConnectedUser.FirstOrDefault(a => a.Phone == saleManage.User_Sale);
                            if (saleConnected != null)
                            {
                                Clients.Client(saleConnected.ConnectionId).onNewUserConnected(id, userDetail);
                            }
                        }
                        else
                        {
                            var listSale = ConnectedUser.Where(a => a.UserType.Contains(((int)UserType.Sale).ToString())).Select(a => a.ConnectionId).ToList();
                            //userOtherConnected.AddRange(saleConnected);
                            //}
                            if (listSale != null)
                            {
                                Clients.Clients(listSale).onNewUserConnected(id, userDetail);
                            }
                        }
                    }
                }

            }
            else
            {
                if (ConnectedClientNonLogin.Count(x => x.ConnectionId == id) == 0)
                {
                    var user = new UserDetail { ConnectionId = id, UserName = string.Format("Khách hàng có IP: {0}", clientCookieId), isNotLogin = true, UserId = clientCookieId, clientCookieId = clientCookieId, isclient = true };
                    ConnectedClientNonLogin.Add(user);

                    Clients.Caller.onConnected(id, user);

                    var listSaleConnected = ConnectedUser.Where(a => a.UserType.Contains(((int)UserType.Sale).ToString())).Select(a => a.ConnectionId).ToList();
                    //userOtherConnected.AddRange(saleConnected);
                    //}
                    if (listSaleConnected != null)
                    {
                        Clients.Clients(listSaleConnected).onUserReConnected(id, user);
                    }
                    //var userOtherConnected = ConnectedUser.Where(a => a.UserType.Contains(((int)UserType.Sale).ToString())).Select(a => a.ConnectionId).ToList();
                    //if (userOtherConnected != null)
                    //{
                    //    Clients.Clients(userOtherConnected).onNewUserConnected(id, user);
                    //}
                }
            }

        }

        public void SendMessageToAll(string userName, string message)
        {
            // store last 100 messages in cache
            //AddMessageinCache(userName, message);

            // Broad cast message
            Clients.All.messageReceived(userName, message);
        }

        public void createPrivateChatWindow(string connectId, string ctrId, string userId)
        {
            //get message defaul today
            var date = DateTime.Now.Date;
            var listMessage = new List<MessageDetail>();
            //user cua client
            if (ConnectedClientLogin.Exists(a => a.ConnectionId == Context.ConnectionId))
            {
                //kh co tai khoan. save mess

                var toUser = ConnectedClientLogin.FirstOrDefault(a => a.ConnectionId == Context.ConnectionId);

                if (string.IsNullOrEmpty(userId))
                {
                    listMessage = db.MessageDetails.Where(a => a.ClientId == toUser.UserId.ToString() && EntityFunctions.TruncateTime(a.DateTime) == date).ToList();

                    Clients.Caller.onCreatePrivateChatWindow(connectId, ctrId, null, listMessage);

                }
                else
                {

                    var fromUser = ConnectedUser.FirstOrDefault(a => a.UserId == userId);
                    if (fromUser != null)
                    {
                        listMessage = db.MessageDetails.Where(a => ((a.FromUserId == fromUser.UserId.ToString() && a.ToUserId == toUser.UserId.ToString()) || (a.ToUserId == fromUser.UserId.ToString() && a.FromUserId == toUser.UserId.ToString())) && EntityFunctions.TruncateTime(a.DateTime) == date).ToList();
                        Clients.Caller.onCreatePrivateChatWindow(fromUser.ConnectionId, ctrId, fromUser, listMessage);
                    }
                    else
                    {
                        var userDetail = new UserDetail();
                        var userProfile = db.UserProfiles.FirstOrDefault(a => a.UserId.ToString() == userId);
                        if (userProfile != null)
                        {
                            userDetail.UserId = userId;
                            userDetail.UserName = userProfile.Name;
                            userDetail.Phone = userProfile.Phone;
                            userDetail.UserType = userProfile.UserType;
                            userDetail.UserTypeText = userProfile.getUserType();
                            userDetail.isclient = Utils.CheckRole(userDetail.UserType, (int)UserType.Client, false);
                        }

                        listMessage = db.MessageDetails.Where(a => ((a.FromUserId == userId && a.ToUserId == toUser.UserId.ToString()) || (a.ToUserId == userId && a.FromUserId == toUser.UserId.ToString())) && EntityFunctions.TruncateTime(a.DateTime) == date).ToList();

                        Clients.Caller.onCreatePrivateChatWindow(connectId, ctrId, userDetail, listMessage);
                    }
                }
            }
            else if (ConnectedClientNonLogin.Exists(a => a.ConnectionId == Context.ConnectionId))
            {
                //kh co tai khoan. save mess

                var toUser = ConnectedClientNonLogin.FirstOrDefault(a => a.ConnectionId == Context.ConnectionId);
                listMessage = db.MessageDetails.Where(a => a.ClientId == toUser.UserId.ToString() && EntityFunctions.TruncateTime(a.DateTime) == date).ToList();

                if (string.IsNullOrEmpty(userId))
                {

                    Clients.Caller.onCreatePrivateChatWindow(connectId, ctrId, null, listMessage);

                }
                else
                {
                    var fromUser = ConnectedUser.FirstOrDefault(a => a.UserId == userId);
                    if (fromUser != null)
                    {
                        Clients.Caller.onCreatePrivateChatWindow(fromUser.ConnectionId, ctrId, fromUser, listMessage);
                    }
                }
            }
        }

        public void createChatWindow(string connectId, string ctrId, UserDetail userDetail)
        {
            //get message
            var listMessage = new List<MessageDetail>();
            //get message defaul today
            var date = DateTime.Now.Date;

            var fromUserDetail = ConnectedUser.FirstOrDefault(a => a.ConnectionId == Context.ConnectionId);
            if (fromUserDetail == null)
                return;

            if (string.IsNullOrEmpty(connectId))
            {
                if (userDetail != null && !string.IsNullOrEmpty(userDetail.UserId))
                {
                    listMessage = db.MessageDetails.Where(a => ((a.FromUserId == fromUserDetail.UserId.ToString() && a.ToUserId == userDetail.UserId.ToString()) || (a.ToUserId == fromUserDetail.UserId.ToString() && a.FromUserId == userDetail.UserId.ToString())) && EntityFunctions.TruncateTime(a.DateTime) == date).ToList();

                    var msgNotRead = db.MessageDetails.Where(a => a.ToUserId == fromUserDetail.UserId.ToString() && a.FromUserId == userDetail.UserId.ToString() && a.isRead == false).ToList();
                    if (msgNotRead != null && msgNotRead.Count > 0)
                    {
                        listMessage.AddRange(msgNotRead);

                        //update
                        foreach (var item in msgNotRead)
                        {
                            item.isRead = true;
                        }
                    }

                    listMessage = listMessage.Distinct().ToList();

                    //update user
                    if (userDetail.isupdate)
                    {
                        var userProfile = db.UserProfiles.FirstOrDefault(a => a.UserId.ToString() == userDetail.UserId);
                        if (userProfile != null)
                        {
                            userDetail.UserName = userProfile.Name;
                            userDetail.Phone = userProfile.Phone;
                            userDetail.UserType = userProfile.UserType;
                            userDetail.UserTypeText = userProfile.getUserType();
                            userDetail.isclient = Utils.CheckRole(userDetail.UserType, (int)UserType.Client, false);
                        }

                        var userConnected = ConnectedClientLogin.FirstOrDefault(a => a.UserId == userDetail.UserId);
                        if (userConnected != null)
                        {
                            userDetail.ConnectionId = userConnected.ConnectionId;
                        }
                    }
                    Clients.Caller.onCreateChatWindow(userDetail.ConnectionId, ctrId, userDetail, listMessage);

                    var chatActivity = db.ChatActivitys.FirstOrDefault(a => a.ToUserId == fromUserDetail.UserId.ToString() && a.FromUserId == userDetail.UserId.ToString());
                    if (chatActivity != null)
                    {
                        chatActivity.IsChat = false;
                    }

                    db.SaveChanges();

                }

            }
            else
            {
                var toUserDetail = ConnectedClientNonLogin.FirstOrDefault(a => a.UserId == userDetail.UserId);
                if (toUserDetail != null)
                {
                    //chat voi khach hang khong co tai khoan
                    toUserDetail.UserSaleChatId = fromUserDetail.UserId.ToString();
                    //thong bao remove
                    var listConnected = ConnectedUser.Where(a => a.ConnectionId != fromUserDetail.ConnectionId).Select(a => a.ConnectionId).Distinct().ToList();
                    Clients.Clients(listConnected).onRemoveClientNonLogin(connectId, fromUserDetail, toUserDetail);

                    listMessage = db.MessageDetails.Where(a => a.ClientId == toUserDetail.UserId && EntityFunctions.TruncateTime(a.DateTime) == date).ToList();

                    Clients.Caller.onCreateChatWindow(toUserDetail.ConnectionId, ctrId, toUserDetail, listMessage);

                }
                else if (ConnectedClientLogin.Exists(a => a.UserId == userDetail.UserId))
                {
                    //khach hang co tai khoan

                    var toUserDetailLogin = ConnectedClientLogin.FirstOrDefault(a => a.UserId == userDetail.UserId);
                    if (toUserDetailLogin != null)
                    {
                        toUserDetailLogin.UserSaleChatId = fromUserDetail.UserId.ToString();

                        var listConnected = ConnectedUser.Where(a => a.ConnectionId != fromUserDetail.ConnectionId).Select(a => a.ConnectionId).Distinct().ToList();
                        Clients.Clients(listConnected).onRemoveClientLogin(toUserDetailLogin.ConnectionId, fromUserDetail, toUserDetailLogin);

                        if (Utils.CheckRole(fromUserDetail.UserType, (int)UserType.Orderer, false))
                        {
                            listMessage = db.MessageDetails.Where(a => ((a.FromUserId == fromUserDetail.UserId.ToString() && a.ToUserId == toUserDetailLogin.UserId.ToString()) || (a.ToUserId == fromUserDetail.UserId.ToString() && a.FromUserId == toUserDetailLogin.UserId.ToString())) && EntityFunctions.TruncateTime(a.DateTime) == date).ToList();
                        }
                        else
                        {
                            listMessage = db.MessageDetails.Where(a => a.ClientId == toUserDetailLogin.UserId && EntityFunctions.TruncateTime(a.DateTime) == date).ToList();
                        }

                        var msgNotRead = db.MessageDetails.Where(a => a.ClientId == toUserDetailLogin.UserId && a.isRead == false).ToList();
                        if (msgNotRead != null && msgNotRead.Count > 0)
                        {
                            listMessage.AddRange(msgNotRead);

                            //update
                            foreach (var item in msgNotRead)
                            {
                                item.isRead = true;
                            }
                        }

                        listMessage = listMessage.Distinct().ToList();

                        var saleManage = db.SaleManageClients.FirstOrDefault(a => a.User_Client == toUserDetailLogin.Phone);
                        if (saleManage == null)
                        {
                            var saleManageNew = new SaleManageClient
                            {
                                LastUpdate = DateTime.Now,
                                User_Client = fromUserDetail.Phone,
                                User_Sale = toUserDetailLogin.Phone,
                                User_Update = ""
                            };
                            db.SaleManageClients.Add(saleManageNew);
                        }

                        Clients.Caller.onCreateChatWindow(toUserDetailLogin.ConnectionId, ctrId, toUserDetailLogin, listMessage);

                        var chatActivity = db.ChatActivitys.FirstOrDefault(a => a.ToUserId == fromUserDetail.UserId.ToString() && a.FromUserId == userDetail.UserId.ToString());
                        if (chatActivity != null)
                        {
                            chatActivity.IsChat = false;
                        }

                        db.SaveChanges();
                    }
                }
                else if (ConnectedUser.Exists(a => a.UserId == userDetail.UserId))
                {
                    //nguoi dung he thong
                    var toUser = ConnectedUser.FirstOrDefault(a => a.UserId == userDetail.UserId);
                    if (toUser != null)
                    {
                        listMessage = db.MessageDetails.Where(a => ((a.FromUserId == fromUserDetail.UserId.ToString() && a.ToUserId == toUser.UserId.ToString()) || (a.ToUserId == fromUserDetail.UserId.ToString() && a.FromUserId == toUser.UserId.ToString())) && EntityFunctions.TruncateTime(a.DateTime) == date).ToList();

                        var msgNotRead = db.MessageDetails.Where(a => a.ToUserId == fromUserDetail.UserId.ToString() && a.FromUserId == userDetail.UserId.ToString() && a.isRead == false).ToList();
                        if (msgNotRead != null && msgNotRead.Count > 0)
                        {
                            listMessage.AddRange(msgNotRead);

                            //update
                            foreach (var item in msgNotRead)
                            {
                                item.isRead = true;
                            }
                        }

                        listMessage = listMessage.Distinct().ToList();

                        Clients.Caller.onCreateChatWindow(toUser.ConnectionId, ctrId, toUser, listMessage);

                        var chatActivity = db.ChatActivitys.FirstOrDefault(a => a.ToUserId == fromUserDetail.UserId.ToString() && a.FromUserId == userDetail.UserId.ToString());
                        if (chatActivity != null)
                        {
                            chatActivity.IsChat = false;
                        }

                        db.SaveChanges();
                    }
                }
            }
        }

        public void getMessageAndSetPermission(string connectId, UserDetail userDetail)
        {
            //get message
            var listMessage = new List<MessageDetail>();
            //get message defaul today
            var date = DateTime.Now.Date;

            var fromUserDetail = ConnectedUser.FirstOrDefault(a => a.ConnectionId == Context.ConnectionId);
            if (fromUserDetail == null)
                return;


            var toUserDetail = ConnectedClientNonLogin.FirstOrDefault(a => a.UserId == userDetail.UserId);
            if (toUserDetail != null)
            {
                //chat voi khach hang khong co tai khoan
                toUserDetail.UserSaleChatId = fromUserDetail.UserId.ToString();
                //thong bao remove
                var listConnected = ConnectedUser.Where(a => a.ConnectionId != fromUserDetail.ConnectionId).Select(a => a.ConnectionId).Distinct().ToList();
                Clients.Clients(listConnected).onRemoveClientNonLogin(connectId, fromUserDetail, toUserDetail);

                listMessage = db.MessageDetails.Where(a => a.ClientId == toUserDetail.UserId && EntityFunctions.TruncateTime(a.DateTime) == date).ToList();
                Clients.Caller.onGetMessage(toUserDetail.ConnectionId, toUserDetail, listMessage);

            }
            else if (ConnectedClientLogin.Exists(a => a.UserId == userDetail.UserId))
            {
                //khach hang co tai khoan

                var toUserDetailLogin = ConnectedClientLogin.FirstOrDefault(a => a.UserId == userDetail.UserId);
                if (toUserDetailLogin != null)
                {
                    toUserDetailLogin.UserSaleChatId = fromUserDetail.UserId.ToString();

                    var listConnected = ConnectedUser.Where(a => a.ConnectionId != fromUserDetail.ConnectionId).Select(a => a.ConnectionId).Distinct().ToList();
                    Clients.Clients(listConnected).onRemoveClientLogin(toUserDetailLogin.ConnectionId, fromUserDetail, toUserDetailLogin);

                    listMessage = db.MessageDetails.Where(a => a.ClientId == toUserDetailLogin.UserId.ToString() && EntityFunctions.TruncateTime(a.DateTime) == date).ToList();
                    Clients.Caller.onGetMessage(toUserDetailLogin.ConnectionId, toUserDetailLogin, listMessage);
                }
            }
            else if (ConnectedUser.Exists(a => a.UserId == userDetail.UserId))
            {
                //nguoi dung he thong
                var toUser = ConnectedUser.FirstOrDefault(a => a.UserId == userDetail.UserId);
                if (toUser != null)
                {
                    listMessage = db.MessageDetails.Where(a => ((a.FromUserId == fromUserDetail.UserId.ToString() && a.ToUserId == toUser.UserId.ToString()) || (a.ToUserId == fromUserDetail.UserId.ToString() && a.FromUserId == toUser.UserId.ToString())) && EntityFunctions.TruncateTime(a.DateTime) == date).ToList();
                    Clients.Caller.onGetMessage(toUser.ConnectionId, toUser, listMessage);
                }
            }
        }

        public void loadChatWindow(string ctrId, string userId, bool isdisplay)
        {
            //get message
            var listMessage = new List<MessageDetail>();
            //get message defaul today
            var date = DateTime.Now.Date;

            var fromUserDetail = ConnectedUser.FirstOrDefault(a => a.ConnectionId == Context.ConnectionId);
            if (fromUserDetail == null)
                return;

            var toUserDetail = ConnectedClientNonLogin.FirstOrDefault(a => a.UserId == userId);
            if (toUserDetail != null)
            {
                //chat voi khach hang khong co tai khoan
                toUserDetail.UserSaleChatId = fromUserDetail.UserId.ToString();
                //thong bao remove
                var listConnected = ConnectedUser.Where(a => a.ConnectionId != fromUserDetail.ConnectionId).Select(a => a.ConnectionId).Distinct().ToList();
                Clients.Clients(listConnected).onRemoveClientNonLogin(toUserDetail.ConnectionId, fromUserDetail, toUserDetail);

                listMessage = db.MessageDetails.Where(a => a.ClientId == toUserDetail.UserId && EntityFunctions.TruncateTime(a.DateTime) == date).ToList();
                toUserDetail.isdisplay = isdisplay;
                Clients.Caller.onCreateChatWindow(toUserDetail.ConnectionId, ctrId, toUserDetail, listMessage);
                return;
            }
            else if (ConnectedClientLogin.Exists(a => a.UserId == userId))
            {
                //khach hang co tai khoan

                var toUserDetailLogin = ConnectedClientLogin.FirstOrDefault(a => a.UserId == userId);
                if (toUserDetailLogin != null)
                {
                    toUserDetailLogin.UserSaleChatId = fromUserDetail.UserId.ToString();

                    var listConnected = ConnectedUser.Where(a => a.ConnectionId != fromUserDetail.ConnectionId).Select(a => a.ConnectionId).Distinct().ToList();
                    Clients.Clients(listConnected).onRemoveClientLogin(toUserDetailLogin.ConnectionId, fromUserDetail, toUserDetailLogin);

                    listMessage = db.MessageDetails.Where(a => a.ClientId == toUserDetailLogin.UserId.ToString() && EntityFunctions.TruncateTime(a.DateTime) == date).ToList();
                    toUserDetailLogin.isdisplay = isdisplay;

                    Clients.Caller.onCreateChatWindow(toUserDetailLogin.ConnectionId, ctrId, toUserDetailLogin, listMessage);
                }
                return;
            }
            else if (ConnectedUser.Exists(a => a.UserId == userId))
            {
                //nguoi dung he thong
                var toUser = ConnectedUser.FirstOrDefault(a => a.UserId == userId);
                if (toUser != null)
                {
                    listMessage = db.MessageDetails.Where(a => ((a.FromUserId == fromUserDetail.UserId.ToString() && a.ToUserId == toUser.UserId.ToString()) || (a.ToUserId == fromUserDetail.UserId.ToString() && a.FromUserId == toUser.UserId.ToString())) && EntityFunctions.TruncateTime(a.DateTime) == date).ToList();
                    toUser.isdisplay = isdisplay;

                    Clients.Caller.onCreateChatWindow(toUser.ConnectionId, ctrId, toUser, listMessage);
                }
                return;
            }


            if (!string.IsNullOrEmpty(userId))
            {
                var user = db.UserProfiles.FirstOrDefault(a => a.UserId.ToString() == userId);
                if (user != null)
                {
                    //co tai khoan
                    if (Utils.CheckRole(user.UserType, (int)UserType.Client, false))
                    {
                        //client
                        var userDetail = new UserDetail { UserName = user.Name, Phone = user.Phone, UserTypeText = user.getUserType(), UserType = user.UserType, UserId = user.UserId.ToString(), isclient = true };

                        listMessage = db.MessageDetails.Where(a => a.ClientId == user.UserId.ToString() && EntityFunctions.TruncateTime(a.DateTime) == date).ToList();
                        userDetail.isdisplay = isdisplay;

                        Clients.Caller.onCreateChatWindow("", ctrId, userDetail, listMessage);
                    }
                    else
                    {
                        //client
                        var userDetail = new UserDetail { UserName = user.Name, Phone = user.Phone, UserTypeText = user.getUserType(), UserType = user.UserType, UserId = user.UserId.ToString(), isclient = true };

                        listMessage = db.MessageDetails.Where(a => ((a.FromUserId == fromUserDetail.UserId.ToString() && a.ToUserId == user.UserId.ToString()) || (a.ToUserId == fromUserDetail.UserId.ToString() && a.FromUserId == user.UserId.ToString())) && EntityFunctions.TruncateTime(a.DateTime) == date).ToList();
                        userDetail.isdisplay = isdisplay;

                        Clients.Caller.onCreateChatWindow("", ctrId, userDetail, listMessage);
                    }

                }
                else
                {
                    //client non login
                    var userDetail = new UserDetail { UserName = string.Format("Khách hàng có IP: {0}", userId), isNotLogin = true, UserId = userId, clientCookieId = userId, isclient = true };

                    listMessage = db.MessageDetails.Where(a => a.ClientId == userId && EntityFunctions.TruncateTime(a.DateTime) == date).ToList();
                    userDetail.isdisplay = isdisplay;

                    Clients.Caller.onCreateChatWindow("", ctrId, userDetail, listMessage);
                }

            }
        }

        public void SendPrivateMessage(string toConnectId, string message, UserDetail user)
        {
            //check user non login
            string fromConnectId = Context.ConnectionId;
            var fromUserDetail = ConnectedClientNonLogin.FirstOrDefault(x => x.ConnectionId == fromConnectId);
            //hoac da co nguoi chat
            if (fromUserDetail != null)
            {
                if (string.IsNullOrEmpty(toConnectId))
                {
                    fromUserDetail.ischat = true;
                    var mess = new MessageDetail
                    {
                        ClientId = fromUserDetail.UserId.ToString(),
                        FromUserId = fromUserDetail.UserId.ToString(),
                        //ToUserId = user.UserId.ToString(),
                        Message = message,
                        UserName = fromUserDetail.UserName,
                        DateTime = DateTime.Now
                    };

                    AddMessagetoDic(fromUserDetail, mess);

                    Clients.Caller.sendPrivateMessage("", mess);

                    var listSale = ConnectedUser.Where(a => a.UserType.Contains(((int)UserType.Sale).ToString())).Select(a => a.ConnectionId).Distinct().ToList();
                    Clients.Clients(listSale).onNewNonUserConnected(fromConnectId, fromUserDetail);
                }
                else
                {
                    var saleConnected = ConnectedUser.FirstOrDefault(a => a.UserId == user.UserId);
                    if (saleConnected == null)
                    {
                        //disconnected or switch page. continous chat
                        var mess = new MessageDetail
                        {
                            ClientId = fromUserDetail.UserId.ToString(),
                            FromUserId = fromUserDetail.UserId.ToString(),
                            ToUserId = user.UserId.ToString(),
                            Message = message,
                            UserName = fromUserDetail.UserName,
                            DateTime = DateTime.Now,
                            isRead = false
                        };

                        AddMessagetoDic(fromUserDetail, mess);

                        var chatActivity = new ChatActivity()
                        {
                            FromUserId = fromUserDetail.UserId.ToString(),
                            ToUserId = user.UserId.ToString(),
                            IsChat = true
                        };
                        var chatActivityUpdate = db.ChatActivitys.FirstOrDefault(a => a.FromUserId == chatActivity.FromUserId && a.ToUserId == chatActivity.ToUserId);
                        if (chatActivityUpdate != null)
                        {
                            chatActivityUpdate.IsChat = true;
                        }
                        else
                        {
                            db.ChatActivitys.Add(chatActivity);
                        }
                        db.SaveChanges();
                        // send to 
                        //Clients.Client(toConnectId).sendPrivateMessage(saleConnected.UserId.ToString(), mess);
                        // send to caller user
                        Clients.Caller.sendPrivateMessage(fromUserDetail.UserId.ToString(), mess);
                    }
                    else
                    {
                        var mess = new MessageDetail
                        {
                            ClientId = fromUserDetail.UserId.ToString(),
                            FromUserId = fromUserDetail.UserId.ToString(),
                            ToUserId = saleConnected.UserId.ToString(),
                            Message = message,
                            UserName = fromUserDetail.UserName,
                            DateTime = DateTime.Now
                        };

                        AddMessagetoDic(fromUserDetail, mess);

                        // send to 
                        Clients.Client(saleConnected.ConnectionId).sendPrivateMessage(fromUserDetail.UserId.ToString(), mess);
                        // send to caller user
                        Clients.Caller.sendPrivateMessage(saleConnected.UserId.ToString(), mess);
                    }

                }
            }
            else if (ConnectedClientLogin.Count(a => a.ConnectionId == fromConnectId) > 0)
            {
                //user had login
                fromUserDetail = ConnectedClientLogin.FirstOrDefault(a => a.ConnectionId == fromConnectId);

                var saleManage = db.SaleManageClients.FirstOrDefault(a => a.User_Client == fromUserDetail.Phone);
                if (saleManage != null)
                {
                    var saleConnected = ConnectedUser.FirstOrDefault(a => a.Phone == saleManage.User_Sale);
                    if (saleConnected != null)
                    {
                        //update 
                        var mess = new MessageDetail
                        {
                            ClientId = fromUserDetail.UserId.ToString(),
                            FromUserId = fromUserDetail.UserId.ToString(),
                            ToUserId = user != null ? user.UserId.ToString() : saleConnected.UserId,
                            Message = message,
                            UserName = fromUserDetail.UserName,
                            DateTime = DateTime.Now
                        };

                        AddMessagetoDic(fromUserDetail, mess);

                        // send to 
                        Clients.Client(saleConnected.ConnectionId).sendPrivateMessage(fromUserDetail.UserId, mess);
                        // send to caller user
                        Clients.Caller.sendPrivateMessage(saleConnected.UserId, mess);
                    }
                    else
                    {
                        var userProfile = db.UserProfiles.FirstOrDefault(a => a.Phone == saleManage.User_Sale);
                        var mess = new MessageDetail
                        {
                            ClientId = fromUserDetail.UserId.ToString(),
                            FromUserId = fromUserDetail.UserId.ToString(),
                            ToUserId = userProfile != null ? userProfile.UserId.ToString() : "",
                            Message = message,
                            UserName = fromUserDetail.UserName,
                            DateTime = DateTime.Now,
                            isRead = false
                        };

                        AddMessagetoDic(fromUserDetail, mess);

                        var chatActivity = new ChatActivity()
                        {
                            FromUserId = fromUserDetail.UserId.ToString(),
                            ToUserId = userProfile.UserId.ToString(),
                            IsChat = true
                        };
                        var chatActivityUpdate = db.ChatActivitys.FirstOrDefault(a => a.FromUserId == chatActivity.FromUserId && a.ToUserId == chatActivity.ToUserId);
                        if (chatActivityUpdate != null)
                        {
                            chatActivityUpdate.IsChat = true;
                        }
                        else
                        {
                            db.ChatActivitys.Add(chatActivity);
                        }
                        db.SaveChanges();

                        Clients.Caller.sendPrivateMessage(userProfile.UserId, mess);
                    }

                }
                else
                {
                    if (string.IsNullOrEmpty(toConnectId))
                    {
                        if (user != null && !string.IsNullOrEmpty(user.UserId))
                        {
                            var saleConnected = ConnectedUser.FirstOrDefault(a => a.UserId == user.UserId);
                            if (saleConnected == null)
                            {
                                //disconnected or switch page. continous chat
                                var mess = new MessageDetail
                                {
                                    ClientId = fromUserDetail.UserId.ToString(),
                                    FromUserId = fromUserDetail.UserId.ToString(),
                                    ToUserId = user.UserId.ToString(),
                                    Message = message,
                                    UserName = fromUserDetail.UserName,
                                    DateTime = DateTime.Now,
                                    isRead = false
                                };

                                AddMessagetoDic(fromUserDetail, mess);

                                var chatActivity = new ChatActivity()
                                {
                                    FromUserId = fromUserDetail.UserId.ToString(),
                                    ToUserId = user.UserId.ToString(),
                                    IsChat = true
                                };
                                var chatActivityUpdate = db.ChatActivitys.FirstOrDefault(a => a.FromUserId == chatActivity.FromUserId && a.ToUserId == chatActivity.ToUserId);
                                if (chatActivityUpdate != null)
                                {
                                    chatActivityUpdate.IsChat = true;
                                }
                                else
                                {
                                    db.ChatActivitys.Add(chatActivity);
                                }
                                db.SaveChanges();
                                // send to 
                                //Clients.Client(toConnectId).sendPrivateMessage(saleConnected.UserId.ToString(), mess);
                                // send to caller user
                                Clients.Caller.sendPrivateMessage(user.UserId.ToString(), mess);
                            }
                            else
                            {
                                var mess = new MessageDetail
                                {
                                    ClientId = fromUserDetail.UserId.ToString(),
                                    FromUserId = fromUserDetail.UserId.ToString(),
                                    ToUserId = saleConnected.UserId.ToString(),
                                    Message = message,
                                    UserName = fromUserDetail.UserName,
                                    DateTime = DateTime.Now
                                };

                                AddMessagetoDic(fromUserDetail, mess);

                                // send to 
                                Clients.Client(saleConnected.ConnectionId).sendPrivateMessage(fromUserDetail.UserId.ToString(), mess);
                                // send to caller user
                                Clients.Caller.sendPrivateMessage(saleConnected.UserId.ToString(), mess);
                            }

                        }
                        else
                        {
                            var mess = new MessageDetail
                            {
                                ClientId = fromUserDetail.UserId.ToString(),
                                FromUserId = fromUserDetail.UserId.ToString(),
                                //ToUserId = user.UserId.ToString(),
                                Message = message,
                                UserName = fromUserDetail.UserName,
                                DateTime = DateTime.Now
                            };

                            AddMessagetoDic(fromUserDetail, mess);

                            Clients.Caller.sendPrivateMessage("", mess);

                            var listSale = ConnectedUser.Where(a => a.UserType.Contains(((int)UserType.Sale).ToString())).Select(a => a.ConnectionId).Distinct().ToList();
                            Clients.Clients(listSale).sendPrivateMessage(fromUserDetail.UserId.ToString(), mess);
                        }
                    }
                    else
                    {
                        var saleConnected = ConnectedUser.FirstOrDefault(a => a.UserId == user.UserId);
                        if (saleConnected == null)
                        {
                            //disconnected or switch page. continous chat
                            var mess = new MessageDetail
                            {
                                ClientId = fromUserDetail.UserId.ToString(),
                                FromUserId = fromUserDetail.UserId.ToString(),
                                ToUserId = user.UserId.ToString(),
                                Message = message,
                                UserName = fromUserDetail.UserName,
                                DateTime = DateTime.Now,
                                isRead = false
                            };

                            AddMessagetoDic(fromUserDetail, mess);

                            var chatActivity = new ChatActivity()
                            {
                                FromUserId = fromUserDetail.UserId.ToString(),
                                ToUserId = user.UserId.ToString(),
                                IsChat = true
                            };
                            var chatActivityUpdate = db.ChatActivitys.FirstOrDefault(a => a.FromUserId == chatActivity.FromUserId && a.ToUserId == chatActivity.ToUserId);
                            if (chatActivityUpdate != null)
                            {
                                chatActivityUpdate.IsChat = true;
                            }
                            else
                            {
                                db.ChatActivitys.Add(chatActivity);
                            }
                            db.SaveChanges();
                            // send to 
                            //Clients.Client(toConnectId).sendPrivateMessage(saleConnected.UserId.ToString(), mess);
                            // send to caller user
                            Clients.Caller.sendPrivateMessage(user.UserId.ToString(), mess);
                        }
                        else
                        {
                            var mess = new MessageDetail
                            {
                                ClientId = fromUserDetail.UserId.ToString(),
                                FromUserId = fromUserDetail.UserId.ToString(),
                                ToUserId = saleConnected.UserId.ToString(),
                                Message = message,
                                UserName = fromUserDetail.UserName,
                                DateTime = DateTime.Now
                            };

                            AddMessagetoDic(fromUserDetail, mess);

                            // send to 
                            Clients.Client(saleConnected.ConnectionId).sendPrivateMessage(fromUserDetail.UserId.ToString(), mess);
                            // send to caller user
                            Clients.Caller.sendPrivateMessage(saleConnected.UserId.ToString(), mess);
                        }

                    }
                }
            }
            else if (ConnectedUser.Count(a => a.ConnectionId == fromConnectId) > 0)
            {
                fromUserDetail = ConnectedUser.FirstOrDefault(a => a.ConnectionId == fromConnectId);

                if (string.IsNullOrEmpty(toConnectId))
                {
                    if (user != null)
                    {
                        var toUser = ConnectedUser.FirstOrDefault(a => a.UserId == user.UserId);
                        if (toUser == null)
                        {
                            var mess = new MessageDetail
                            {
                                FromUserId = fromUserDetail.UserId.ToString(),
                                ToUserId = user.UserId.ToString(),
                                Message = message,
                                UserName = fromUserDetail.UserName,
                                DateTime = DateTime.Now,
                                isRead = false
                            };

                            AddMessagetoDic(user, mess);
                            var chatActivity = new ChatActivity()
                            {
                                FromUserId = fromUserDetail.UserId.ToString(),
                                ToUserId = user.UserId.ToString(),
                                IsChat = true
                            };
                            var chatActivityUpdate = db.ChatActivitys.FirstOrDefault(a => a.FromUserId == chatActivity.FromUserId && a.ToUserId == chatActivity.ToUserId);
                            if (chatActivityUpdate != null)
                            {
                                chatActivityUpdate.IsChat = true;
                            }
                            else
                            {
                                db.ChatActivitys.Add(chatActivity);
                            }
                            db.SaveChanges();
                            // send to caller user
                            Clients.Caller.sendPrivateMessage(user.UserId.ToString(), mess);
                        }
                        else
                        {
                            var mess = new MessageDetail
                            {
                                FromUserId = fromUserDetail.UserId.ToString(),
                                ToUserId = toUser.UserId.ToString(),
                                Message = message,
                                UserName = fromUserDetail.UserName,
                                DateTime = DateTime.Now
                            };

                            AddMessagetoDic(toUser, mess);

                            // send to 
                            Clients.Client(toUser.ConnectionId).sendPrivateMessage(fromUserDetail.UserId.ToString(), mess);
                            // send to caller user
                            Clients.Caller.sendPrivateMessage(toUser.UserId.ToString(), mess);
                        }
                    }

                }
                else
                {
                    if (!user.isclient)
                    {
                        var toUser = ConnectedUser.FirstOrDefault(a => a.UserId == user.UserId);
                        if (toUser == null)
                        {
                            //disconnected. switch offline mode
                            var mess = new MessageDetail
                            {
                                FromUserId = fromUserDetail.UserId.ToString(),
                                ToUserId = user.UserId.ToString(),
                                Message = message,
                                UserName = fromUserDetail.UserName,
                                DateTime = DateTime.Now,
                                isRead = false
                            };

                            AddMessagetoDic(toUser, mess);
                            var chatActivity = new ChatActivity()
                            {
                                FromUserId = fromUserDetail.UserId.ToString(),
                                ToUserId = user.UserId.ToString(),
                                IsChat = true
                            };
                            var chatActivityUpdate = db.ChatActivitys.FirstOrDefault(a => a.FromUserId == chatActivity.FromUserId && a.ToUserId == chatActivity.ToUserId);
                            if (chatActivityUpdate != null)
                            {
                                chatActivityUpdate.IsChat = true;
                            }
                            else
                            {
                                db.ChatActivitys.Add(chatActivity);
                            }
                            db.SaveChanges();
                            // send to 
                            //Clients.Client(toConnectId).sendPrivateMessage(fromUserDetail.UserId.ToString(), mess);
                            // send to caller user
                            Clients.Caller.sendPrivateMessage(user.UserId.ToString(), mess);
                        }
                        else
                        {
                            var mess = new MessageDetail
                            {
                                FromUserId = fromUserDetail.UserId.ToString(),
                                ToUserId = toUser.UserId.ToString(),
                                Message = message,
                                UserName = fromUserDetail.UserName,
                                DateTime = DateTime.Now
                            };

                            AddMessagetoDic(toUser, mess);

                            // send to 
                            Clients.Client(toUser.ConnectionId).sendPrivateMessage(fromUserDetail.UserId.ToString(), mess);
                            // send to caller user
                            Clients.Caller.sendPrivateMessage(toUser.UserId.ToString(), mess);
                        }

                    }
                    else
                    {
                        var toUser = ConnectedClientNonLogin.FirstOrDefault(a => a.UserId == user.UserId) ??
                             ConnectedClientLogin.FirstOrDefault(a => a.UserId == user.UserId);
                        if (toUser == null)
                        {
                            //client disconnected. switch offline mode
                            var mess = new MessageDetail
                            {
                                ClientId = user.UserId.ToString(),
                                FromUserId = fromUserDetail.UserId.ToString(),
                                ToUserId = user.UserId.ToString(),
                                Message = message,
                                UserName = fromUserDetail.UserName,
                                DateTime = DateTime.Now
                            };

                            AddMessagetoDic(toUser, mess);

                            var chatActivity = new ChatActivity()
                            {
                                FromUserId = fromUserDetail.UserId.ToString(),
                                ToUserId = user.UserId.ToString(),
                                IsChat = true
                            };
                            var chatActivityUpdate = db.ChatActivitys.FirstOrDefault(a => a.FromUserId == chatActivity.FromUserId && a.ToUserId == chatActivity.ToUserId);
                            if (chatActivityUpdate != null)
                            {
                                chatActivityUpdate.IsChat = true;
                            }
                            else
                            {
                                db.ChatActivitys.Add(chatActivity);
                            }
                            db.SaveChanges();

                            //// send to 
                            //Clients.Client(toConnectId).sendPrivateMessage(fromUserDetail.UserId.ToString(), mess);
                            // send to caller user
                            Clients.Caller.sendPrivateMessage(user.UserId.ToString(), mess);
                        }
                        else
                        {
                            var mess = new MessageDetail
                            {
                                ClientId = toUser.UserId.ToString(),
                                FromUserId = fromUserDetail.UserId.ToString(),
                                ToUserId = toUser.UserId.ToString(),
                                Message = message,
                                UserName = fromUserDetail.UserName,
                                DateTime = DateTime.Now
                            };

                            AddMessagetoDic(toUser, mess);

                            // send to 
                            Clients.Client(toUser.ConnectionId).sendPrivateMessage(fromUserDetail.UserId.ToString(), mess);
                            // send to caller user
                            Clients.Caller.sendPrivateMessage(user.UserId.ToString(), mess);
                        }
                    }
                }

            }

        }

        public override System.Threading.Tasks.Task OnDisconnected(bool stopCalled)
        {
            if (isSale(Context.ConnectionId))
            {
                var sale = ConnectedUser.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);
                if (sale != null)
                {
                    ConnectedUser.Remove(sale);

                    var id = Context.ConnectionId;

                    //kiem tra chiem quyen

                    Clients.All.onUserDisconnected(id, sale);
                }
            }
            else
            {
                var item = ConnectedClientNonLogin.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);
                if (item != null)
                {
                    ConnectedClientNonLogin.Remove(item);

                    var id = Context.ConnectionId;
                    Clients.All.onUserDisconnected(id, item);

                }
                else
                {
                    item = ConnectedClientLogin.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);
                    if (item != null)
                    {
                        ConnectedClientLogin.Remove(item);
                        var id = Context.ConnectionId;
                        Clients.All.onUserDisconnected(id, item);
                    }
                }

            }

            return base.OnDisconnected(stopCalled);
        }

        public bool isSale(string userid)
        {
            if (ConnectedUser.Count(a => a.ConnectionId == userid) > 0)
            {
                return true;
            }

            return false;
        }
        #endregion

        #region private Messages

        private void AddMessagetoDic(UserDetail user, MessageDetail message)
        {

            db.MessageDetails.Add(message);
            db.SaveChanges();
        }

        #endregion
    }

}