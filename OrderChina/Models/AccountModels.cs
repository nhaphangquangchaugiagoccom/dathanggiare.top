﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using OrderChina.Common;


namespace OrderChina.Models
{
    #region Database

    public class DBContext : DbContext
    {
        public DBContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }

        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<OrderInternalInDay> OrderInternalInDays { get; set; }
        public DbSet<OrderInternal> OrderInternals { get; set; }
        public DbSet<OrderInternalDetail> OrderInternalDetails { get; set; }

        public DbSet<Rate> Rates { get; set; }

        public DbSet<RateHistory> RateHistorys { get; set; }
        public DbSet<SaleManageClient> SaleManageClients { get; set; }
        public DbSet<New> News { get; set; }
        public DbSet<Orderpaintings> Orderpainting { get; set; }
        public DbSet<DepositOrder> DepositOrders { get; set; }
        public DbSet<Wallet> Wallets { get; set; }
        public DbSet<WalletHistory> WalletHistorys { get; set; }
        public DbSet<Currency> Currencys { get; set; }

        public DbSet<Fee> Fees { get; set; }
        public DbSet<Freight> Freights { get; set; }
        public DbSet<Feeshipment> Feeshipments { get; set; }
        public DbSet<Clot> Clots { get; set; }
        public DbSet<CNY> CNYs { get; set; }
        public DbSet<FeeClient> FeeClients { get; set; }
        public DbSet<FreightClient> FreightClients { get; set; }
        public DbSet<ClotClient> ClotClients { get; set; }
        public DbSet<Approval> Approvals { get; set; }

        public DbSet<OrderDetailExtensions> ListOrderDetailExtensions { get; set; }
        public DbSet<Historychangeuser> Historychangeusers { get; set; }
        public DbSet<Bank> Banks { get; set; }
        public DbSet<Forex> Forexs { get; set; }
        public DbSet<Shipment> Shipments { get; set; }
        public DbSet<PackageOrder> PackageOrders { get; set; }
        public DbSet<OrderAccount> OrderAccounts { get; set; }
        public DbSet<ExcelNotice> ExcelNotices { get; set; }
        public DbSet<MessageDetail> MessageDetails { get; set; }

        public DbSet<ChatActivity> ChatActivitys { get; set; }

    }
    [Table("New")]
    public class New
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int IDNews { get; set; }
        public string Title { get; set; }
        public string NewsContent { get; set; }
        public string Titlebig { get; set; }
        public string Img { get; set; }

    }

    [Table("Orderpainting")]
    public class Orderpaintings
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public string Titlele { get; set; }

        public string Content { get; set; }
        public DateTime Date { get; set; }
    }

    [Table("DepositOrders")]
    public class DepositOrder
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int UserId { get; set; }
        public string EmailUser { get; set; }
        public string IDDepOders { get; set; }
        [Display(Name = "Cân nặng")]
        public string Weight { get; set; }
        [Display(Name = "Loại hàng")]
        public string Catalogy { get; set; }

        [Display(Name = "Số lượng thùng")]
        public int Bucket { get; set; }
        [Display(Name = "Số lượng thùng đã về")]
        public int ReceiveBucket { get; set; }
        public string SizeOder { get; set; }
        [Display(Name = "Ghi chú")]
        public string Content { get; set; }
        [Display(Name = "Tổng tiền")]
        public double TotalPriceConvert { get; set; }
        [Display(Name = "Trạng thái")]
        public string Status { get; set; }
        [Display(Name = "Ngày tạo")]
        public DateTime CreateDate { get; set; }
        public string getStatusText()
        {
            foreach (FieldInfo fieldInfo in typeof(OrderStatus).GetFields())
            {
                if (fieldInfo.FieldType.Name != "OrderStatus")
                    continue;
                if (fieldInfo.Name.ToLower() == Status.ToLower())
                {
                    var attribute = Attribute.GetCustomAttribute(fieldInfo, typeof(DisplayAttribute)) as DisplayAttribute;

                    if (attribute != null)
                        return attribute.Name;
                }

            }

            return Status;
        }
    }

    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        [Required(ErrorMessage = "Sđt bắt buộc nhập.")]
        [Remote("IsCheckExitsPhone", "Account", ErrorMessage = "Sđt {0} đã tồn tại trong hệ thống.")]
        [Display(Name = "Số điện thoại *")]
        public string Phone { get; set; }

        public string PhoneCompany { get; set; }
        public string Image { get; set; }
        public string Name { get; set; }
        public string TokenId { get; set; }
        [Display(Name = "Mã Khách hàng *")]
        public string customercode { get; set; }
        [Required(ErrorMessage = "Sđt bắt buộc nhập.")]
        [Remote("IsCheckEmail", "Account", "")]
        [Display(Name = "Email *")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Ngày sinh bắt buộc nhập.")]
        public DateTime Birthday { get; set; }
        public string Address { get; set; }
        public string Account { get; set; }
        public string Gender { get; set; }
        public string Password { get; set; }

        [Display(Name = "Loại người dùng")]
        public string UserType { get; set; }

        [Display(Name = "Phần trăm đặt cọc")]
        public double DownPaymentRate { get; set; }

        [NotMapped]
        public string SaleManage { get; set; }

        private string getUserTypeText(string value)
        {
            foreach (FieldInfo fieldInfo in typeof(UserType).GetFields())
            {
                if (fieldInfo.FieldType.Name != "UserType")
                    continue;
                if (String.Equals(((int)fieldInfo.GetValue(fieldInfo)).ToString(), value, StringComparison.CurrentCultureIgnoreCase))
                {
                    var attribute = Attribute.GetCustomAttribute(fieldInfo, typeof(DisplayAttribute)) as DisplayAttribute;

                    if (attribute != null)
                        return attribute.Name;
                }

            }

            return value;
        }

        public string getUserType()
        {
            var listUserType = UserType.Split(',');
            var stringreturn = listUserType.Aggregate(string.Empty, (current, s) => current + "," + getUserTypeText(s));
            if (stringreturn.Length > 1)
            {
                return stringreturn.Substring(1, stringreturn.Length - 1);

            }
            return stringreturn;
        }
    }

    [Table("Order")]
    public class Order
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        [Display(Name = "Mã đơn hàng")]
        public int OrderId { get; set; }

        [Display(Name = "Mã giao dịch")]
        public string TransId { get; set; }

        public string UserName { get; set; }

        public string ClientName { get; set; } //chi phục vụ cho excel

        public string ClientAddress { get; set; } //chi phục vụ cho excel

        public string Phone { get; set; }

        [Display(Name = "Tỷ giá")]
        public double Rate { get; set; }

        [Display(Name = "Trạng thái")]
        public string Status { get; set; }

        [Display(Name = "Loại hàng")]
        public string Catalogy { get; set; }

        [Display(Name = "Ghi chú")]
        public string Content { get; set; }
        [Display(Name = "Mã đơn khiếu nại")]
        public string IDOrderCompain { get; set; }
        [Display(Name = "Phí dịch vụ phát sinh")]
        public double Incurred { get; set; }
        [Display(Name = "Phí vận chuyển nội địa")]
        public double Inland { get; set; }
        [Display(Name = "Tiền kéo")]
        public double Pulling { get; set; }
        public double TotalPrice { get; set; }

        [Display(Name = "Tổng tiền")]
        public double TotalPriceConvert { get; set; }

        [Display(Name = "Phí dịch vụ")]
        [Required(ErrorMessage = "Phí dịch vụ bắt buộc nhập")]
        public double Fee { get; set; }

        [Display(Name = "Phí ship nội địa TQ")]

        public double FeeShipChina { get; set; }

        [Display(Name = "Trọng lượng")]

        public int Weight { get; set; }

        [Display(Name = "Số lượng thùng")]
        public int Bucket { get; set; }
        [Display(Name = "Số lượng thùng đã về")]
        public int ReceiveBucket { get; set; }
        [Display(Name = "Cước vận chuyển")]
        public double FeeShip { get; set; }

        [Display(Name = "Ngày tạo")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "Ngày cập nhật")]
        public DateTime UpdateDate { get; set; }

        [Display(Name = "Tiền thu thêm")]
        public double AccountingCollected { get; set; }

        [Display(Name = "Tiền đặt cọc")]
        public double DownPayment { get; set; }

        public string SaleManager { get; set; }

        public string ShippingCode { get; set; }

        public string OrderType { get; set; }

        public bool IsShop { get; set; }

        public double DownPaymentRate { get; set; }

        public int ShipmentId { get; set; }

        public string CustomerCode { get; set; }

        public string getOrderTypeText()
        {
            if (string.IsNullOrEmpty(OrderType))
            {
                //mac dinh la don order
                OrderType = Models.OrderType.Order.ToString();
            }
            try
            {
                foreach (FieldInfo fieldInfo in typeof(OrderType).GetFields())
                {
                    if (fieldInfo.FieldType.Name != "OrderType")
                        continue;

                    if (fieldInfo.Name == OrderType)
                    {
                        var attribute = Attribute.GetCustomAttribute(fieldInfo, typeof(DisplayAttribute)) as DisplayAttribute;

                        if (attribute != null)
                            return attribute.Name;
                    }

                }

            }
            catch (Exception ex)
            {

                return ex.ToString();

            }
            return OrderType;
        }
        public string getStatusText()
        {
            foreach (FieldInfo fieldInfo in typeof(OrderStatus).GetFields())
            {
                if (fieldInfo.FieldType.Name != "OrderStatus")
                    continue;
                if (fieldInfo.Name.ToLower() == Status.ToLower())
                {
                    var attribute = Attribute.GetCustomAttribute(fieldInfo, typeof(DisplayAttribute)) as DisplayAttribute;

                    if (attribute != null)
                        return attribute.Name;
                }

            }

            return Status;
        }

        public OrderInternal CloneOrderInternal()
        {
            return new OrderInternal
            {
                AccountingCollected = AccountingCollected,
                CreateDate = CreateDate,
                DownPayment = DownPayment,
                DownPaymentRate = DownPaymentRate,
                Fee = Fee,
                FeeShip = FeeShip,
                FeeShipChina = FeeShipChina,
                OrderId = OrderId,
                OrderType = OrderType,
                Phone = Phone,
                Rate = Rate,
                SaleManager = SaleManager,
                ShippingCode = ShippingCode,
                Status = Status,
                TotalPrice = TotalPrice,
                TotalPriceConvert = TotalPriceConvert,
                UserName = UserName
            };
        }
    }

    [Table("OrderDetail")]
    public class OrderDetail
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int OrderDetailId { get; set; }

        public int OrderId { get; set; }

        public string Image { get; set; }

        [Required(ErrorMessage = "Link sản phẩm không được để trống")]
        [Display(Name = "Link sản phẩm")]
        public string Link { get; set; }

        public string Shop { get; set; }

        [Display(Name = "Mô tả")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Khối lượng không được để trống")]
        [Display(Name = "Số lượng")]
        public int? Quantity { get; set; }

        [Required(ErrorMessage = "Giá không được để trống")]
        [Display(Name = "Giá")]
        public double? Price { get; set; }

        [Display(Name = "Màu sắc")]
        public string Color { get; set; }

        public string Size { get; set; }

        [Required(ErrorMessage = "SĐT không được để trống")]
        public string Phone { get; set; }

        [Display(Name = "Đặt được")]
        public int? QuantitySellPlace { get; set; }

        [Display(Name = "Về kho")]
        public int? QuantityInWarehouse { get; set; }

        [Display(Name = "Tỷ giá đặt được")]
        public double? Rate_Real { get; set; }

        public string Note { get; set; }

        [Display(Name = "Ngày xuất kho")]
        public DateTime? DeliveryDate { get; set; }

        [Display(Name = "Trạng thái")]
        public string OrderDetailStatus { get; set; }
        [NotMapped]
        public string stt { get; set; }

        public string getStatusText()
        {
            foreach (FieldInfo fieldInfo in typeof(OrderDetailStatus).GetFields())
            {
                if (fieldInfo.FieldType.Name != "OrderDetailStatus")
                    continue;
                if (fieldInfo.Name.ToLower() == OrderDetailStatus.ToLower())
                {
                    var attribute = Attribute.GetCustomAttribute(fieldInfo, typeof(DisplayAttribute)) as DisplayAttribute;

                    if (attribute != null)
                        return attribute.Name;
                }

            }

            return OrderDetailStatus;
        }

        public OrderInternalDetail CloneInternal()
        {
            return new OrderInternalDetail
            {
                Color = Color,
                DeliveryDate = DeliveryDate,
                Description = Description,
                Link = Link,
                Note = Note,
                OrderDetailId = OrderDetailId,
                OrderDetailStatus = OrderDetailStatus,
                OrderId = OrderId,
                Phone = Phone,
                Price = Price,
                Quantity = Quantity,
                QuantityInWarehouse = QuantityInWarehouse,
                QuantitySellPlace = QuantitySellPlace,
                Rate_Real = Rate_Real,
                Shop = Shop,
                Size = Size,
                Image = Image
            };
        }
    }

    [Table("OrderAccount")]
    public class OrderAccount
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        [Display(Name = "Mã đơn hàng")]
        public int OrderAccountId { get; set; }

        [Display(Name = "Mã đơn hàng cha")]
        public int ParentOrderAccountId { get; set; }

        [Display(Name = "Trạng thái")]
        public string Status { get; set; }

        [Display(Name = "Mặt hàng")]
        public string SymbolName { get; set; }

        [Display(Name = "Giá")]
        public double Price { get; set; }

        [Display(Name = "Số lượng")]
        public double Quantity { get; set; }
        [Display(Name = "Số lượng còn lại")]
        public double LeaveQuantity { get; set; }

        [Display(Name = "Tổng tiền")]
        public double TotalValue { get; set; }

        [Display(Name = "Phí")]
        [Required(ErrorMessage = "Phí dịch vụ bắt buộc nhập")]
        public double Fee { get; set; }

        [Display(Name = "HT Thanh Toán")]
        public double ValueSystem { get; set; }
        [Display(Name = "HT Còn lại")]
        public double ValueRemain { get; set; }
        [Display(Name = "KH Thanh Toán")]
        public double ValueCustomer { get; set; }

        [Display(Name = "Ghi chú")]
        public string Note { get; set; }

        public string ExtandField { get; set; }

        [Display(Name = "Người tạo")]
        public string CreateBy { get; set; }

        [Display(Name = "Ngày tạo")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "Ngày cập nhật")]
        public DateTime UpdateDate { get; set; }
        [Display(Name = "Khách hàng")]
        public string Phone { get; set; }
    }

    //Tính số đơn tạo được trong ngày
    [Table("OrderInternalInDay")]
    public class OrderInternalInDay
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Display(Name = "Mã đơn hàng trong ngày")]
        public int OrderInternalInDayID { get; set; }

        [Display(Name = "Ngày tạo")]
        public DateTime CreateDate { get; set; }
    }


    [Table("OrderInternal")]
    public class OrderInternal
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        [Display(Name = "Mã đơn hàng")]
        public int OrderInternalId { get; set; }

        [Display(Name = "Mã đơn theo ngày")]
        public string OrderInternalInDayID { get; set; }
        
        public string NoteOrder { get; set; }

        [Display(Name = "Mã đơn hàng")]
        public int OrderId { get; set; }

        [Display(Name = "Mã giao dịch")]
        public string TransId { get; set; }

        public string UserName { get; set; }

        public string Phone { get; set; }

        [Display(Name = "Tỷ giá")]
        public double Rate { get; set; }
        [Display(Name = "Trạng thái")]
        public string Status { get; set; }
        public double TotalPrice { get; set; }
        [Display(Name = "Tổng tiền Khách trả")]
        public double TotalPriceClient { get; set; }
        [Display(Name = "Tổng tiền Order Mặc cả")]
        public double TotalPriceReality { get; set; }
        [Display(Name = "Nợ")]
        public double Owe { get; set; }

        [Display(Name = "Tổng tiền")]
        public double TotalPriceConvert { get; set; }

        [Display(Name = "Phí dịch vụ(%)")]
        [Required(ErrorMessage = "Phí dịch vụ bắt buộc nhập")]
        public double Fee { get; set; }
        [Display(Name = "Phí ship nội địa TQ")]
        public double FeeShipChina { get; set; }
        [Display(Name = "Ghi chú")]
        public string Content { get; set; }
        [Display(Name = "Mã đơn khiếu nại")]
        public string IDOrderCompain { get; set; }
        [Display(Name = "Đặt cọc(%)")]
        public double DownPaymentRate { get; set; }

        [Display(Name = "Lý do")]
        public string Reason { get; set; }

        [Display(Name = "Trong lượng")]

        public int Weight { get; set; }

        [Display(Name = "Cước vận chuyển")]
        public double FeeShip { get; set; }

        [Display(Name = "Ngày tạo")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "Ngày cập nhật")]
        public DateTime UpdateDate { get; set; }

        [Display(Name = "Tiền thu thêm")]
        public double AccountingCollected { get; set; }
        [Display(Name = "Tình trạng Oder")]
        public string StatusOder { get; set; }

        [Display(Name = "Tiền đặt cọc")]
        public double DownPayment { get; set; }

        public string SaleManager { get; set; }

        [Display(Name = "Chỉ định người đặt hàng")]
        public string OrdererManager { get; set; }

        [Display(Name = "Mã vận đơn")]
        public string ShippingCode { get; set; }

        //loại đơn hàng. Trong enum OrderType
        public string OrderType { get; set; }
        public bool CheckView { get; set; }
        public bool IsShop { get; set; }
        public string getOrderTypeText()
        {
            foreach (FieldInfo fieldInfo in typeof(OrderType).GetFields())
            {
                if (fieldInfo.FieldType.Name != "OrderType")
                    continue;
                if (fieldInfo.Name.ToLower() == OrderType.ToLower())
                {
                    var attribute = Attribute.GetCustomAttribute(fieldInfo, typeof(DisplayAttribute)) as DisplayAttribute;

                    if (attribute != null)
                        return attribute.Name;
                }
            }
            return OrderType;
        }
        public string getStatusText()
        {
            foreach (FieldInfo fieldInfo in typeof(OrderStatus).GetFields())
            {
                if (fieldInfo.FieldType.Name != "OrderStatus")
                    continue;
                if (fieldInfo.Name.ToLower() == Status.ToLower())
                {
                    var attribute = Attribute.GetCustomAttribute(fieldInfo, typeof(DisplayAttribute)) as DisplayAttribute;

                    if (attribute != null)
                        return attribute.Name;
                }
            }
            return Status;
        }
    }



    [Table("OrderInternalDetail")]
    public class OrderInternalDetail
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int OrderInternalDetailId { get; set; }
        public string NoteOrder { get; set; }
        public int OrderDetailId { get; set; }

        public int OrderId { get; set; }

        public int OrderInternalId { get; set; }
        public string NoteClient { get; set; }
        public string NoteSale { get; set; }
        public string Image { get; set; }

        [Required(ErrorMessage = "Link sản phẩm không được để trống")]
        [Display(Name = "Link sản phẩm")]
        public string Link { get; set; }
        [Display(Name = "Màu sắc")]
        public string Color { get; set; }
        [NotMapped]
        public string stt { get; set; }

        public string Size { get; set; }

        public string Shop { get; set; }

        [Display(Name = "Mô tả")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Khối lượng không được để trống")]
        [Display(Name = "Khối lượng")]
        public int? Quantity { get; set; }

        [Required(ErrorMessage = "Giá không được để trống")]
        [Display(Name = "Giá")]
        public double? Price { get; set; }
        [Display(Name = "Giá thực tế")]
        public double? PriceReality { get; set; }
        [Required(ErrorMessage = "SĐT không được để trống")]
        public string Phone { get; set; }

        [Display(Name = "Đặt được")]
        public int? QuantitySellPlace { get; set; }

        [Display(Name = "Về kho")]
        public int? QuantityInWarehouse { get; set; }

        [Display(Name = "Tỷ giá đặt được")]
        public double? Rate_Real { get; set; }

        public string Note { get; set; }

        [Display(Name = "Ngày xuất kho")]
        public DateTime? DeliveryDate { get; set; }

        [Display(Name = "Trạng thái")]
        public string OrderDetailStatus { get; set; }

        public string getStatusText()
        {
            foreach (FieldInfo fieldInfo in typeof(OrderDetailStatus).GetFields())
            {
                if (fieldInfo.FieldType.Name != "OrderDetailStatus")
                    continue;
                if (fieldInfo.Name.ToLower() == OrderDetailStatus.ToLower())
                {
                    var attribute = Attribute.GetCustomAttribute(fieldInfo, typeof(DisplayAttribute)) as DisplayAttribute;

                    if (attribute != null)
                        return attribute.Name;
                }

            }

            return OrderDetailStatus;
        }
    }

    [Table("Rate")]
    public class Rate
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int RateId { get; set; }

        public double Price { get; set; }

        public double fee1 { get; set; }
        public double fee2 { get; set; }
        public double fee3 { get; set; }
        public double rate_forex { get; set; }
        public string userUpdate { get; set; }
        public DateTime lastUpdated { get; set; }

        public RateHistory CloneHistory()
        {
            return new RateHistory
            {
                RateId = RateId,
                Price = Price,
                fee1 = fee1,
                fee2 = fee2,
                fee3 = fee3,
                rate_forex = rate_forex,
                UserUpdate = userUpdate,
                LastUpdate = lastUpdated
            };
        }

        public string FormatPrice(double index)
        {

            int sp = 1000;
            string hq = "k";
            if (1000 <= index && index < 1000000 || -1000 >= index && index > -1000000)
            {
                sp = 1000;
                hq = "k";
            }
            if (1000000 <= index && index < 1000000000 || -1000000 >= index && index > -1000000000)
            {
                sp = 1000000;
                hq = "m";
            }
            if (index >= 1000000000 || index <= -1000000000)
            {
                sp = 1000000000;
                hq = "b";
            }

            if (index >= 1000 || index <= -1000)
            {
                var pt = index;
                if (index < 0)
                {
                    pt = pt * (-1);
                }

                var p = Math.Round((float)pt / sp, 1);
                var txt = p.ToString(CultureInfo.InvariantCulture);

                if (p - (int)p == 0)
                {
                    if (index > 0)
                    {
                        return txt + hq;
                    }
                    return "-" + txt + hq;
                }
                else
                {
                    if (index > 0)
                    {
                        return txt.Replace(".", hq);
                    }
                    return "-" + txt.Replace(".", hq);
                }
            }

            return index.ToString(CultureInfo.InvariantCulture);
        }
    }

    [Table("RateHistory")]
    public class RateHistory
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int RateHistoryId { get; set; }
        public int RateId { get; set; }

        public double Price { get; set; }

        public double fee1 { get; set; }
        public double fee2 { get; set; }
        public double fee3 { get; set; }
        public double rate_forex { get; set; }
        public DateTime LastUpdate { get; set; }
        public string UserUpdate { get; set; }

    }

    [Table("SaleManageClient")]
    public class SaleManageClient
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Phải chọn sale")]
        [Display(Name = "Sale quản lý")]
        public string User_Sale { get; set; }

        public string User_Client { get; set; }
        public string User_Update { get; set; }
        public DateTime LastUpdate { get; set; }

    }

    [Table("Wallet")]
    public class Wallet
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Client { get; set; } //phone

        [Display(Name = "Loại tiền")]
        [Required(ErrorMessage = "Loại tiền không được để trống!")]
        public string Currency { get; set; }

        [Display(Name = "Loại ví")]
        public string WalletType { get; set; }

        [Display(Name = "Ngân hàng")]
        public string Name { get; set; }

        [Display(Name = "Số tài khoản")]
        [Remote("IsCheckExitsAccount", "Account", ErrorMessage = "Số tài khoản {0} đã tồn tại trong hệ thống.")]
        public string Account { get; set; }

        [Display(Name = "Tiền")]
        public double Money { get; set; }
        public double Owe { get; set; } // số nợ
        public string User_Update { get; set; }
        public DateTime LastUpdate { get; set; }

        public WalletHistory CloneHistory(string reason)
        {
            return new WalletHistory()
            {
                Currency = Currency,
                Account = Account,
                LastUpdate = DateTime.Now,
                User_Update = User_Update,
                Client = Client,
                Money = Money,
                Reason = reason,
                WalletId = Id
            };
        }
    }

    [Table("WalletHistory")]
    public class WalletHistory
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int WalletId { get; set; }
        public string Client { get; set; }

        public string Currency { get; set; }
        public string Account { get; set; }

        public double Money { get; set; }

        public string Reason { get; set; }
        public string Note { get; set; } // diễn giải

        public string UpdateType { get; set; }

        public string User_Update { get; set; }

        public DateTime LastUpdate { get; set; }

        public string getUpdateTypeText()
        {
            foreach (FieldInfo fieldInfo in typeof(WalletUpdateType).GetFields())
            {
                if (fieldInfo.FieldType.Name != "WalletUpdateType")
                    continue;
                if (fieldInfo.Name.ToLower() == UpdateType.ToLower())
                {
                    var attribute = Attribute.GetCustomAttribute(fieldInfo, typeof(DisplayAttribute)) as DisplayAttribute;

                    if (attribute != null)
                        return attribute.Name;
                }

            }

            return UpdateType;
        }
    }

    [Table("Currency")]
    public class Currency
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

    }

    [Table("Fee")]
    public class Fee
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public double From { get; set; } //default million ex: 1 <-> 1 trieu
        public double To { get; set; } //default million
        public double Value { get; set; }
        public DateTime LastUpdate { get; set; }
    }
    [Table("FeeClient")]
    public class FeeClient
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public double From { get; set; } //default million ex: 1 <-> 1 trieu
        public double To { get; set; } //default million
        public string ClientID { get; set; }
        public double Value { get; set; }
        public DateTime LastUpdate { get; set; }
    }
    [Table("Shipment")]
    public class Shipment
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ShipmentId { get; set; }

        public string Name { get; set; }
        public int ShipmentType { get; set; }//tiểu ngạch hoặc đi đồi
        public double Length { get; set; } //chieu dai
        public double Width { get; set; } //Chieu rong
        public double Height { get; set; } //Chieu cao
        public double Weight { get; set; } //Trong luong. Kg
        public double Volume { get; set; }//The tich m3
        public double PriceFee { get; set; }//Tiền ship
        public string Status { get; set; }
        public int PaymentStatus { get; set; }
        public DateTime LastUpdate { get; set; }

        public string getShipmentTypeText()
        {
            foreach (FieldInfo fieldInfo in typeof(ShipmentType).GetFields())
            {
                if (fieldInfo.FieldType.Name != "ShipmentType")
                    continue;
                if ((int)fieldInfo.GetValue(fieldInfo) == ShipmentType)
                {
                    var attribute = Attribute.GetCustomAttribute(fieldInfo, typeof(DisplayAttribute)) as DisplayAttribute;

                    if (attribute != null)
                        return attribute.Name;
                }

            }

            return ShipmentType.ToString();
        }

    }
    [Table("Feeshipment")]// theo Kg
    public class Feeshipment
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int FeeshipmentId { get; set; }
        public string Name { get; set; }
        public int ShipmentType { get; set; }
        public int UnitType { get; set; }
        public int From { get; set; }
        public int To { get; set; } //default million
        public double Value { get; set; }
        public DateTime LastUpdate { get; set; }
    }

    [Table("Freight")]// theo Kg
    public class Freight
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int FreightId { get; set; }
        public string Name { get; set; }
        public double From { get; set; }
        public double To { get; set; } //default million
        public double Value { get; set; }

        public DateTime LastUpdate { get; set; }
    }
    [Table("FreightClient")]// theo Kg
    public class FreightClient
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int FreightId { get; set; }
        public string Name { get; set; }
        public string IdClient { get; set; }
        public double From { get; set; }
        public double To { get; set; } //default million
        public double Value { get; set; }

        public DateTime LastUpdate { get; set; }
    }
    [Table("Clot")]//Theo m3
    public class Clot
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ClotId { get; set; }
        public string Name { get; set; }
        public double From { get; set; }
        public double To { get; set; }
        public double Value { get; set; }

        public DateTime LastUpdate { get; set; }
    }
    [Table("ClotClient")]//Theo m3
    public class ClotClient
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ClotId { get; set; }
        public string Name { get; set; }
        public string IdClient { get; set; }
        public double From { get; set; }
        public double To { get; set; }
        public double Value { get; set; }

        public DateTime LastUpdate { get; set; }
    }
    [Table("CNY")]//Bảng giá nạp tệ 
    public class CNY
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int CNYId { get; set; }
        public string NameType { get; set; }
        public double From { get; set; }
        public double To { get; set; }
        public double Value { get; set; }
        public DateTime LastUpdate { get; set; }
        public string getNameType()
        {
            foreach (FieldInfo fieldInfo in typeof(NameType).GetFields())
            {
                if (fieldInfo.FieldType.Name != "TypeChange")
                    continue;
                if (fieldInfo.Name.ToLower() == NameType.ToLower())
                {
                    var attribute = Attribute.GetCustomAttribute(fieldInfo, typeof(DisplayAttribute)) as DisplayAttribute;

                    if (attribute != null)
                        return attribute.Name;
                }
            }
            return NameType;
        }
    }
    [Table("OrderDetailExtensions")]//Plugin
    public class OrderDetailExtensions
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string cookie { get; set; }
        public string image { get; set; }
        public string item_src { get; set; }
        public string size { get; set; }
        public int? amount { get; set; }
        public double? price { get; set; }
        public string item_packed_box_price { get; set; }
        public string item_check_price { get; set; }
        public string item_express_price { get; set; }
        public string item_web { get; set; }
        public string item_shop { get; set; }
        public double exch_rate { get; set; }
    }

    [Table("Historychangeuser")]
    public class Historychangeuser
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string UserName { get; set; }
        public string Reason { get; set; }
        public string TypeChange { get; set; }
        public string SaleChange { get; set; }
        public DateTime Datetime { get; set; }
        public string getTypeText()
        {
            foreach (FieldInfo fieldInfo in typeof(TypeChange).GetFields())
            {
                if (fieldInfo.FieldType.Name != "TypeChange")
                    continue;
                if (fieldInfo.Name.ToLower() == TypeChange.ToLower())
                {
                    var attribute = Attribute.GetCustomAttribute(fieldInfo, typeof(DisplayAttribute)) as DisplayAttribute;

                    if (attribute != null)
                        return attribute.Name;
                }
            }
            return TypeChange;
        }

    }

    [Table("Approval")]
    public class Approval
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ApprovalId { get; set; }

        public string ApprovalType { get; set; }

        public string ApprovalValue { get; set; } //save json value need approval

        public string CreateBy { get; set; }

        public DateTime CreateDate { get; set; }

        public string ApprovalBy { get; set; }

        public DateTime? ApprovalDate { get; set; }

        public int OrderInternalId { get; internal set; }

        public string getApprovalType()
        {
            foreach (FieldInfo fieldInfo in typeof(ApprovalType).GetFields())
            {
                if (fieldInfo.FieldType.Name != "ApprovalType")
                    continue;
                if (fieldInfo.Name.ToLower() == ApprovalType.ToLower())
                {
                    var attribute = Attribute.GetCustomAttribute(fieldInfo, typeof(DisplayAttribute)) as DisplayAttribute;

                    if (attribute != null)
                        return attribute.Name;
                }

            }

            return ApprovalType;
        }
    }

    [Table("PhraseTranslate")]
    public class PhraseTranslate
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int phrase_Id { get; set; }

        public string phrase { get; set; }

        public string chinese { get; set; }

        public string vietnamese { get; set; }

    }

    [Table("Bank")]
    public class Bank
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Bank_Id { get; set; }

        public string Bank_Name { get; set; }

        public string Bank_ShortName { get; set; }

        public string Create_by { get; set; }

        public DateTime LastUpdated { get; set; }
    }

    [Table("PackageOrder")]
    public class PackageOrder
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int PackageOrderId { get; set; }
        public string ShippingCode { get; set; }
        public double Length { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public double Weight { get; set; }
        public double Volume { get; set; }//The tich 
        public int ShipmentId { get; set; }
        public string Status { get; set; }
    }
    [Table("Forex")]
    public class Forex
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ForexID { get; set; }
        public double Amount { get; set; }//Tiền nạp
        public string Forex_Orderer { get; set; }//Order xác nhận
        public string BankUserName { get; set; }//Tên tài khoản ngân hàng
        public string BankNumber { get; set; }//Số tài khoản
        public string BankName { get; set; }//Tên Ngân hàng
        public string Forex_Phone { get; set; }//Phone khách nạp
        public double Rate { get; set; }//Tỷ giá lúc nạp
        public double Service { get; set; }//phí lúc nạp
        public string Catalogy { get; set; }//Loại ngân hàng
        public string TypeForex { get; set; }//Trạng thái
        public DateTime Date { get; set; }//Ngày nạp
        public string getForexType()
        {
            foreach (FieldInfo fieldInfo in typeof(TypeForex).GetFields())
            {
                if (fieldInfo.FieldType.Name != "TypeForex")
                    continue;
                if (fieldInfo.Name.ToLower() == TypeForex.ToLower())
                {
                    var attribute = Attribute.GetCustomAttribute(fieldInfo, typeof(DisplayAttribute)) as DisplayAttribute;

                    if (attribute != null)
                        return attribute.Name;
                }

            }

            return TypeForex;
        }
    }

    [Table("ExcelNotice")]
    public class ExcelNotice
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ExcelNoticeID { get; set; }
        public string Phone { get; set; }//Client import error
        public string Location { get; set; }//Đường dẫn lưu file
        public string Name { get; set; }//Tên file
        public string Status { get; set; }//Trạng thái
        public string Note { get; set; }//Ghi chú
        public DateTime LastUpdate { get; set; }//Ngày nạp
        public bool IsHaveSaleManage { get; set; }
    }

    [Table("MessageDetail")]
    public class MessageDetail
    {
        public MessageDetail()
        {
            isRead = true;
        }
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string FromUserId { get; set; }

        public string ToUserId { get; set; }

        public string ClientId { get; set; }


        public string UserName { get; set; }

        public string Message { get; set; }

        public DateTime DateTime { get; set; }

        public bool isRead { get; set; }
    }

    [Table("ChatActivity")]
    public class ChatActivity
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string FromUserId { get; set; }

        public string ToUserId { get; set; }

        public bool IsChat { get; set; }
    }
    #endregion

    #region User
    public class LoginModel
    {
        [Required(ErrorMessage = "Email or Phone không được để trống")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Mật khẩu không được để trống")]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu")]
        public string Password { get; set; }

        [Display(Name = "Nhớ tài khoản?")]
        public bool RememberMe { get; set; }

    }

    public class RegisterModel
    {
        [Display(Name = "Mã khách hàng")]
        [Remote("IsCheckExitsCustomercode", "Account", ErrorMessage = "Mã khách hàng đã tồn tại")]
        public string customercode { get; set; }

        public string UserName { get; set; }

        [Required(ErrorMessage = "Email bắt buộc nhập.")]
        [Remote("IsCheckEmail", "Account", "")]
        [Display(Name = "Email *")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Mật khẩu bắt buộc nhập.")]
        [StringLength(100, ErrorMessage = "{0} phải lớn hơn {2} ký tự.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu *")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Nhập lại mật khẩu *")]
        [System.Web.Mvc.Compare("Password", ErrorMessage = "Mật khẩu không khớp.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Họ tên bắt buộc nhập.")]
        [Display(Name = "Họ tên *")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Sđt bắt buộc nhập.")]
        [Remote("IsCheckExitsPhone", "Account", ErrorMessage = "Sđt đã tồn tại trong hệ thống.")]
        [Display(Name = "Số điện thoại *")]
        public string Phone { get; set; }

        [Required]
        [Display(Name = "Giới tính *")]
        public string Gender { get; set; }

        [Required(ErrorMessage = "Ngày sinh bắt buộc nhập.")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Ngày sinh *")]
        public DateTime Birthday { get; set; }

        [Display(Name = "Địa chỉ")]
        public string Address { get; set; }

        [Display(Name = "TK ngân hàng")]
        public string Account { get; set; }

        [Display(Name = "Loại tài khoản")]
        public string[] UserType { get; set; }

        [Display(Name = "Phần trăm đặt cọc")]
        public double? DownPaymentRate { get; set; }

        public string orderid { get; set; }

    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }

    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [System.Web.Mvc.Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
    public class ChangeUserTypeModel
    {
        public string Phone { get; set; }

        [Display(Name = "Loại người sử dụng")]
        public string[] UserType { get; set; }
    }

    public class UpdateSetingClientUserModel
    {
        public string Phone { get; set; }

        [Display(Name = "Phần trăm đặt cọc")]
        public double? DownPaymentRate { get; set; }

        public SaleManageClient SaleManage { get; set; }
    }

    public class CreateOrderExtensionsNoUser
    {
        public string email { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
    }
    public class LoginExtensions
    {
        public string email { get; set; }
        public string password { get; set; }
        public string rememberme { get; set; }


    }
    #endregion

    #region Order
    public class CreateAccountDetailModel
    {
        public int OrderId { get; set; }
        public double LeaveQuantity { get; set; }
    }
    public class NewOrderModel
    {
        [Required(ErrorMessage = "Phải chọn đơn hàng")]
        [Display(Name = "Đơn hàng")]
        public string OrderType { get; set; }
        public IEnumerable<OrderDetail> ListOrderDetail { get; set; }

        public IEnumerable<OrderInternal> ListOrderDetails { get; set; }
        public IEnumerable<OrderInternalDetail> ListOrderInternalDetails { get; set; }

    }

    public class NewOrderWithPhoneModel
    {
        [Required(ErrorMessage = "Phải chọn khách hàng")]
        [Display(Name = "Khách hàng")]
        public string Phone { get; set; }
        [Required(ErrorMessage = "Phải chọn đơn hàng")]
        [Display(Name = "Đơn hàng")]
        public string OrderType { get; set; }
        [Display(Name = "Mã đơn hàng khiếu nại")]
        public string IDOrderCompain { get; set; }
        public IEnumerable<OrderDetail> ListOrderDetail { get; set; }
        public IEnumerable<OrderInternal> ListOrderDetails { get; set; }
        public IEnumerable<OrderInternalDetail> ListOrderInternalDetails { get; set; }

    }
    public class ComfirmOrderApply
    {
        public int IdW { get; set; }
        public int CIdOrder { get; set; }
        public string CAccount { get; set; }
        public string CIDShippingCode { get; set; }
        public string CIDTransId { get; set; }
        public double CIDFee { get; set; }
        public double CIDDownPaymentRate { get; set; }
        public double CIDFeeShipChina { get; set; }
        public string COrderComment { get; set; }
        public double CMoneyW { get; set; }
        public double CTotalMoney { get; set; }
    }
    public class UpdateOrderLink
    {

        public int UOrderInternalId { get; set; }
        public string ULink { get; set; }
        public string UColor { get; set; }
        public string USize { get; set; }
        public double UDonGia { get; set; }
        public double UTongGia { get; set; }
        public double UDonGiaTT { get; set; }
        public double UTongGiaTT { get; set; }
        public string UGhichuKhach { get; set; }
        public string UGhichuSale { get; set; }
        public int USLTT { get; set; }
        public int UorderId { get; set; }


    }
    public class ComfirmOrder
    {
        public int IdW { get; set; }
        public int CIdOrder { get; set; }
        public string CAccount { get; set; }
        public string CIDShippingCode { get; set; }
        public string CIDTransId { get; set; }
        public double CIDFee { get; set; }
        public double CIDDownPaymentRate { get; set; }
        public double CIDFeeShipChina { get; set; }
        public string COrderComment { get; set; }
        public double CMoneyW { get; set; }
        public double CTotalMoney { get; set; }
        public double CIDPriceRealityEdit { get; set; }
    }
    public class NewOrderExtensionModel
    {
        [Required(ErrorMessage = "Phải chọn khách hàng")]
        [Display(Name = "Khách hàng")]
        public string Phone { get; set; }

        public IEnumerable<OrderDetailExtensions> ListOrderDetail { get; set; }
    }

    public class ViewDetailOrderModel
    {
        public int OrderId { get; set; }

        public string UserName { get; set; }
        public string Phone { get; set; }
        [Display(Name = "Tỷ giá")]
        public double Rate { get; set; }
        [Display(Name = "Phí dịch vụ phát sinh")]
        public double Incurred { get; set; }
        [Display(Name = "Phí vận chuyển nội địa")]
        public double Inland { get; set; }
        [Display(Name = "Tiền kéo")]
        public double Pulling { get; set; }
        [Display(Name = "Trạng thái")]
        public string Status { get; set; }
        [Display(Name = "Ghi chú đơn hàng")]
        public string Content { get; set; }
        public string OrderType { get; set; }
        public double TotalPrice { get; set; }
        [Display(Name = "Tổng tiền")]
        public double TotalPriceConvert { get; set; }

        public double Fee { get; set; }

        public double FeeShipChina { get; set; }

        public int Weight { get; set; }

        [Display(Name = "Số lượng thùng")]
        public int Bucket { get; set; }
        [Display(Name = "Số lượng thùng đã về")]
        public int ReceiveBucket { get; set; }
        public double FeeShip { get; set; }

        public DateTime CreateDate { get; set; }

        [Display(Name = "Ngày xuất kho")]
        public DateTime DeliveryDate { get; set; }

        [Display(Name = "Tiền thu thêm")]
        public double AccountingCollected { get; set; }

        [Display(Name = "Tiền đặt cọc")]
        public double DownPayment { get; set; }
        public string SaleManager { get; set; }

        public SaleManageInfo SaleManageInfo { get; set; }

        public UserProfile Client { get; set; }

        public IEnumerable<OrderInternalDetail> ListOrderDetails { get; set; }
        public string StatusText { get; set; }
        public int OrderInternalId { get; set; }
    }

    public class ViewDetailOrderNonShopModel
    {
        public ViewDetailOrderNonShopModel()
        {
            ListShop = new List<ListShopModel>();
            ListOrderDetails = new List<OrderDetail>();
        }
        public int OrderId { get; set; }

        public List<ListShopModel> ListShop { get; set; }
        public UserProfile Client { get; set; }
        public List<OrderDetail> ListOrderDetails { get; internal set; }
        public double Rate { get; set; }
        public string Status { get; set; }
        public string StatusText { get; set; }
        public double Fee { get; set; }
        public double TotalPriceConvert { get; set; }
        public double TotalPrice { get; set; }
        public double FeeShipChina { get; set; }
        public double FeeShip { get; set; }
        public double DownPayment { get; set; }
        public double AccountingCollected { get; set; }
        public bool IsShop { get; set; }
        public SaleManageInfo SaleManageInfo { get; set; }
        public string Phone { get; set; }
        public double Weight { get; set; }
        public double DownPaymentRate { get; set; }
    }

    public class ListShopModel
    {
        public ListShopModel()
        {
            ListOrderDetails = new List<OrderDetail>();
        }
        public int OrderId { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }
        [Display(Name = "Tỷ giá")]
        public double Rate { get; set; }
        public double TotalPrice { get; set; }
        [Display(Name = "Tổng tiền")]
        public double TotalPriceConvert { get; set; }
        public double Fee { get; set; }

        public double FeeShipChina { get; set; }

        public int Weight { get; set; }

        public double FeeShip { get; set; }

        public IEnumerable<OrderDetail> ListOrderDetails { get; set; }
    }
    public class ViewDetailOrderModelNew
    {
        public int OrderId { get; set; }

        public string UserName { get; set; }
        public string Phone { get; set; }
        [Display(Name = "Tỷ giá")]
        public double Rate { get; set; }
        [Display(Name = "Trạng thái")]
        public string Status { get; set; }
        public double TotalPrice { get; set; }
        [Display(Name = "Tổng tiền")]
        public double TotalPriceConvert { get; set; }
        public double TotalPriceReality { get; set; }
        public double TotalPriceClient { get; set; }
        public double Money { get; set; }
        public string Currency { get; set; }
        public double Fee { get; set; }
        public double PriceReality { get; set; }

        public string TransId { get; set; }
        public double FeeShipChina { get; set; }

        public int Weight { get; set; }

        public double FeeShip { get; set; }

        public DateTime CreateDate { get; set; }

        [Display(Name = "Ngày xuất kho")]
        public DateTime DeliveryDate { get; set; }

        [Display(Name = "Tiền thu thêm")]
        public double AccountingCollected { get; set; }

        [Display(Name = "Tiền đặt cọc")]
        public double DownPayment { get; set; }
        public string SaleManager { get; set; }
        public SaleManageInfo SaleManageInfo { get; set; }
        public UserProfile Client { get; set; }
        public IEnumerable<OrderInternalDetail> ListOrderDetails { get; set; }
        public IEnumerable<Wallet> ListWallets { get; set; }
        public string StatusText { get; set; }
        public int OrderInternalId { get; set; }
        [Display(Name = "Số điện thoại *")]
        public string Name { get; set; }
        [Display(Name = "Email *")]
        public string Email { get; set; }
        [Display(Name = "Mã đơn hàng nhập *")]
        public string IDOrderCompain { get; set; }

        public string Address { get; set; }
        [Display(Name = "Chọn orderer")]
        public string OrdererManager { get; set; }
        public UserProfile Orderer { get; set; }

        public double DownPaymentRate { get; set; }
        public string OrderType { get; set; }
        public string Content { get; set; }

        public double Owe { get; set; }
    }
    public class ViewDetailOrderMoney
    {
        public double Money { get; set; }
    }
    public class SaleManageInfo
    {
        public int SaleUserId { get; set; }

        public string SaleName { get; set; }

        public string SalePhoneCompany { get; set; }
        public string SalePhone { get; set; }
    }

    public class ViewManager
    {
        public string UserName { get; set; }
        public string IdUser { get; set; }
        public OrderInternal OrderInternals { get; set; }
        public Order Orders { get; set; }
        public Forex Forex { get; set; }
    }
    #endregion

    #region Accounting

    public class RateModel
    {
        public Rate Rate { get; set; }

        public PagedList.IPagedList<RateHistory> ListRateHistory { get; set; }
    }
    #endregion

    #region Wallet

    public class AdditionModel
    {
        public int walletid { get; set; }

        public double money { get; set; }

        [Required(ErrorMessage = "Bắt buộc phải nhập lý do điều chỉnh!")]
        public string reason { get; set; }
        public string BankName { get; set; }

    }

    public class IndexWalletModel
    {
        public IEnumerable<Wallet> Wallets { get; set; }
        public PagedList.IPagedList<WalletHistory> WalletHistorys { get; set; }

    }
    #endregion

    #region DepositOrders
    public class NewDepositOrders
    {
        public IEnumerable<Order> ListDepositOrders { get; set; }
    }

    #endregion

    #region Fee

    public class WatchListModel
    {
        public List<Fee> ListFee { get; set; }
        public List<Freight> ListFreight { get; set; }
        public List<CNY> ListCNY { get; set; }
        public List<Feeshipment> FeeshipmentWeight { get; set; }
        public List<Feeshipment> FeeshipmentSize { get; set; }
        public List<Clot> ListClot { get; set; }
        public List<ClotClient> ListClotClient { get; set; }
        public List<FeeClient> ListFeeClient { get; set; }
        public List<FreightClient> ListFreightClient { get; set; }
    }
    #endregion

    #region Shipment

    public class ShipmentDetailModel
    {
        public ShipmentDetailModel()
        {
            ListPackageOrder = new List<PackageOrder>();
        }
        public int ShipmentId { get; set; }
        public string Name { get; set; }
        public double Length { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public double Weight { get; set; }
        public double Volume { get; set; }//Thể tích
        public double PriceFee { get; set; }//Tiền ship
        public DateTime LastUpdate { get; set; }
        public string Status { get; set; }
        public double sumWeight { get; set; }
        public double sumVolume { get; set; }
        public List<PackageOrder> ListPackageOrder { get; set; }

    }



    public class AccountShipmentModel
    {
        public int IdShipment { get; set; }
        public string ShipType { get; set; }
        public double Weight { get; set; }
        public double Height { get; set; }
        public double Width { get; set; }
        public double Length { get; set; }
        public string Name { get; set; }
        public string Amount { get; set; }
        public string AccountType { get; set; }
        public string Explain { get; set; }
        public string Note { get; set; }
    }
    #endregion

    #region Request Amount
    public class RequestAmount
    {
        public double Amount { get; set; }
        public string Reason { get; set; }
    }
    #endregion

    #region Approval
    public class ApprovalSaleChangeFee
    {
        public int OrderInternalId { get; set; }
        public double Fee_Old { get; set; }
        public double DownPaymentRate_Old { get; set; }
        public double Fee_New { get; set; }
        public double DownPaymentRate_New { get; set; }
        public string Reason { get; set; }
        public string OrdererManager { get; set; }
        public string OrdererManagerName { get; set; }
    }

    public class ApprovalDetailModel
    {
        public int ApprovalId { get; set; }
        public int OrderInternalId { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public ApprovalSaleChangeFee ApprovalSaleChangeFee { get; set; }
        public string ApprovalType { get; internal set; }
        public string ApprovalTypeText { get; internal set; }
        public string ApprovalBy { get; internal set; }
    }
    #endregion

    #region Reports

    public class ReportsModel
    {
        [Required(ErrorMessage = "Phải chọn báo cáo")]
        [Display(Name = "Báo cáo")]
        public string ReportType { get; set; }

        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime Date { get; set; }
        [Required(ErrorMessage = "Phải chọn tháng")]
        [Display(Name = "Tháng")]
        public int Month { get; set; }
    }

    #endregion

    #region Chat
    public class LisChat
    {
        public string FromName { get; set; }
        public string ToName { get; set; }
        public string FromId { get; set; }
        public string ToId { get; set; }
        public MessageDetail LisMessage { get; set; }
    }
    #endregion

    #region ExcelError
    public class ExcelErrorDetailModel
    {
        public string ClientName { get; set; }
        public int ExcelNoticeID { get; set; }
        public bool IsHaveSaleManage { get; set; }
        public string Location { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
        public string Phone { get; set; }
        public string Status { get; set; }
    }
    #endregion
    public class OweModel
    {
        public string Phone { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public double OweValue { get; set; }
    }
    #region Enum
    public enum ShipmentType
    {
        [Display(Name = "Tiểu ngạch")]
        Byroad = 1,
        [Display(Name = "Đi đồi")]
        Hill = 2,

    }
    public enum PaymentStatus
    {
        [Display(Name = "Chưa thánh toán")]
        None = 0,
        [Display(Name = "Thanh toán 1 phần")]
        Patirial = 2,
        [Display(Name = "Đã thanh toán")]
        Done = 2,

    }
    public enum UnitType
    {
        Weight = 1,
        Size = 2,
    }
    public enum FeeOrderChinaSystem
    {
        FeeOrderChinaVND,
        FeeOrderChinaCNY,
    }

    public enum ApprovalStatus
    {
        Request = 1,
        Approval = 2,
        Reject = 3,
        ApprovalPartial = 4,
    }

    public enum BankEnum
    {
        VCB,
        BIDV,
        TECHCOM,
        ACB,
    }

    public enum OrderStatus
    {
        [Display(Name = "Đơn mới")]
        New = 1,
        [Display(Name = "Đã thu tiền đặt cọc")]
        Paid = 2,
        [Display(Name = "Hàng chờ về")]
        Order = 3,
        [Display(Name = "Hoàn thành")]
        Finish = 4,
        [Display(Name = "Đơn hủy")]
        Cancel = 5,
        [Display(Name = "Khách hàng confirm")]
        ClientConfirm = 6,
        [Display(Name = "Sale chốt đơn hàng")]
        SaleConfirm = 7,
        [Display(Name = "Hàng về đã về kho")]
        Receive = 8,
        [Display(Name = "Đã thu đủ tiền")]
        FullCollect = 9,
        [Display(Name = "Chốt lại đơn hàng")]
        OrdererReject = 10,
        [Display(Name = "Chờ phê duyệt chốt đơn hàng")]
        WaitingApprovalSaleConfirm = 11,
        [Display(Name = "Kho hàng bán")]
        PendingExport = 13,
        [Display(Name = "Chờ kế toán chốt")]
        WaitOrder = 14,
        [Display(Name = "Hết hàng cần hoàn tiền khách")]
        MisOrder = 15,
        [Display(Name = "Đơn hàng còn thiếu số thùng")]
        MissingBucket = 16,
    }

    public enum OrderDetailStatus
    {
        [Display(Name = "Link hoạt động")]
        Active = 1,
        [Display(Name = "Link không hoạt động")]
        Inactive = 2
    }

    public enum UserType
    {
        [Display(Name = "Administrator")]
        Admin = 1,
        [Display(Name = "SupperUser")]
        SuperUser = 2,
        [Display(Name = "Nhân viên sale")]
        Sale = 3,
        [Display(Name = "Kế toán")]
        Accounting = 4,
        [Display(Name = "Khách hàng")]
        Client = 5,
        [Display(Name = "Nhân viên đặt hàng")]
        Orderer = 6,
        [Display(Name = "Nhân viên nhận hàng")]
        Recieve = 7,
        [Display(Name = "Nhân viên nhập mã vận đơn")]
        Staff = 8,
        [Display(Name = "Quản lý")]
        Manager = 9
    }

    public enum WalletUpdateType
    {
        [Display(Name = "Thêm tiền")]
        Addition = 1,
        [Display(Name = "Trừ tiền")]
        Subtract = 2
    }

    public enum WalletType
    {
        [Display(Name = "Online")]
        Online = 1,
        [Display(Name = "Ngân hàng")]
        Bank = 2
    }

    public enum OrderType
    {
        [Display(Name = "Đặt hàng")]
        Order = 1,
        [Display(Name = "Nạp Tệ")]
        Forex = 2,
        [Display(Name = "Vận chuyển")]
        Ship = 3,
        [Display(Name = "Hàng tự nhập")]
        BuyOrder = 4,
        [Display(Name = "Trọn gói")]
        AllInOne = 5,
        [Display(Name = "Khiếu nại")]
        Complaint = 6
    }

    public enum ApprovalType
    {
        [Display(Name = "Sale sửa phí dịch vụ")]
        SaleChangeFee = 1,
        [Display(Name = "Yêu cầu cấp tiền")]
        RequestAmount = 2
    }
    public enum TypeChange
    {
        [Display(Name = "Cập nhật mật khẩu")]
        ChangePass = 1,
        [Display(Name = "Sale Cập nhật thông tin User")]
        ChangeUser = 2
    }
    public enum NameType
    {
        [Display(Name = "Tài khoản  ALIPAY")]
        Alipay = 1,
        [Display(Name = "Tài khoản ngân hàng")]
        Bank = 2,
        [Display(Name = "Hàng thông thường")]
        Common = 3,
        [Display(Name = "Hàng theo lô")]
        Batch = 4,
        [Display(Name = "Hàng theo theo khối")]
        Weigh = 5,
    }
    public enum TypeForex
    {
        [Display(Name = "Đơn chưa được xử lý")]
        New = 1,
        [Display(Name = "Đơn đang xử lý nạp tệ")]
        Orderer = 2,
        [Display(Name = "Đơn đã xử lý")]
        Done = 3
    }
    public enum ExcelNoticeStatus
    {
        [Display(Name = "Đã xử lý")]
        Done = 1,
        [Display(Name = "Chưa xử lý")]
        UnDone = 2
    }
    public enum ReportType
    {
        [Display(Name = "Tồn kho đầu tháng")]
        SurviveEarlyMonth = 1
    }
    public enum ExportType
    {
        [Display(Name = "XLSX")]
        Xlsx = 1
    }
    #endregion

}
