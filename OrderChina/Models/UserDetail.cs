﻿namespace OrderChina.Models
{
    public class UserDetail
    {
        public UserDetail()
        {
            isNotLogin = false;
            isdisplay = true;
            ischat = false;
            isupdate = false;

        }

        public string ConnectionId { get; set; }
        public string UserName { get; set; }
        public string UserId { get; set; } //Phone
        public string UserSaleChatId { get; set; } //SaleManage
        public string UserType { get; set; }
        public string Phone { get; set; }
        public string UserTypeText { get; set; }
        public string clientCookieId { get; set; }
        public bool isonline { get; set; }
        public bool isclient { get; set; }
        public bool isNotLogin { get;  set; }
        public bool ischat { get; set; }
        public bool isdisplay { get; set; }
        public bool isupdate { get; set; }

    }
}