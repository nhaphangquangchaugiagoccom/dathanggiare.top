namespace OrderChina.Temp_Reports.SurviveEarlyMonth
{
    using Common;
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Telerik.Reporting;

    /// <summary>
    /// Summary description for SurviveEarlyMonth.
    /// </summary>
    public partial class SurviveEarlyMonth : Report
    {
        public SurviveEarlyMonth()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            
        }

        public SurviveEarlyMonth(int month)
            : this()
        {

            //get ngay dau thang
            var firstDateForMonth = new DateTime(DateTime.Now.Year, month, 1);
            using (var db = new DBContext())
            {
                var listOrderInternals = db.OrderInternals.Where(a => a.Status != OrderStatus.Cancel.ToString() && a.Status != OrderStatus.Finish.ToString() && a.Status != OrderStatus.MisOrder.ToString() && a.CreateDate >= firstDateForMonth);
                var listDataSource = new List<SurviveEarlyMonthValue>();
                var listChilds = new List<SurviveEarlyMonthValue>();
                foreach (var item in listOrderInternals)
                {
                    //item
                    using (var dbDetail = new DBContext())
                    {
                        var listDetail = dbDetail.OrderInternalDetails.Where(a => a.OrderInternalId == item.OrderInternalId).ToList();
                        if (listDetail != null && listDetail.Count() > 0)
                        {
                            foreach (var itemDetail in listDetail)
                            {
                                var value = new SurviveEarlyMonthValue();
                                value.orderid = itemDetail.OrderInternalId;
                                value.link = itemDetail.Link;
                                value.price = itemDetail.Price;
                                value.amount = itemDetail.Quantity;
                                //value.customercode = item.p;
                                value.feeshipchina = item.FeeShipChina;
                                //value.feecompany = itemDetail.OrderInternalId;
                                value.rate = item.Rate;
                                value.value = (itemDetail.Price ?? 0) * (itemDetail.Quantity ?? 0);
                                value.fee = (value.value * item.Fee) / 100;
                                value.date = item.CreateDate;
                                value.valueconvert = Utils.Round((value.value + value.feeshipchina) * value.rate, -3);
                                value.totalvalueconvert = Utils.Round(value.valueconvert + (value.fee * value.rate), -3);
                                listChilds.Add(value);
                            }
                        }

                        if (listChilds != null && listChilds.Count() > 0)
                        {
                            var first = listChilds.First();
                            first.totalprice = listChilds.Sum(a => a.totalvalueconvert);
                            first.downpayment = item.DownPayment;
                            first.accountcollect = first.totalprice - item.DownPayment;
                            listDataSource.AddRange(listChilds);
                        }
                    }
                   
                }

                table1.DataSource = listDataSource;
            }
        }
    }
}