﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrderChina.Temp_Reports.SurviveEarlyMonth
{
    public class SurviveEarlyMonthValue
    {
        //Ngày 
        public int orderid { get; set; }

        public DateTime date { get; set; }
        public DateTime createdate { get; set; }
        //Ngày tạo
        public DateTime update { get; set; }
        //Ngày cập nhật
        //STT 
        public int stt { get; set; }
        //Mã KH  
        public string customercode { get; set; }
        //Tên hàng hóa
        public string link { get; set; }
        //SL  
        public int? amount { get; set; }
        //Đơn giá 
        public double? price { get; set; }
        //Thành tiền  
        public double value { get; set; } //amount*price
        //Phí dịch vụ 
        public double fee { get; set; }
        //VC nội địa
        public double feeshipchina { get; set; }
        //Tỷ giá  
        public double rate { get; set; }
        //tiền oder
        public double valueconvert { get; set; }//(value + feeshipchina)*rate
        //Phí VC kenhvanchuyen  
        public double feecompany { get; set; } //cong ty anphat
        //Thành tiền order 
        public double totalvalueconvert { get; set; } //valueconvert + fee*rate

        //tổng tiền
        public double totalprice { get; set; }
        //Tiền ứng   
        public double downpayment { get; set; }
        //Phải thanh toán khi nhận hàng   
        public double accountcollect { get; set; }
        //CB Oder 
        //Thanh toán tiền tệ  
        //Trạng thái  
        //Ngày trả hàng 
        //Tình trạng thanh toán 
        //Số tiền nợ

    }
}