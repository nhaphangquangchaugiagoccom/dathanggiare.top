
$(function () {
    $(".nav-tabs").on("click", "a", function (e) {
        e.preventDefault();
        if (!$(this).hasClass('add-contact')) {
            $(this).tab('show');
        }
    })
      .on("click", "span", function () {
          var id = $(".nav-tabs").children().length - 1; //think about it ;)
          if (id >= 1) {
              var anchor = $(this).siblings('a');
              $(anchor.attr('href')).remove();
              $(this).parent().remove();
              var lengthA = $(".nav-tabs li").children('a').length;
              if (lengthA >= 2) {
                  $(".nav-tabs li").children('a').first().click();
              }

              var orderId = $(this).attr('id');
              var listTabsString = localStorage.getItem("listTabs");
              if (listTabsString) {
                  var listTabs = JSON.parse(listTabsString);
                  var orderInt = parseInt(orderId);
                  var isexits = $.inArray(orderInt, listTabs);
                  if (isexits >= 0) {
                      listTabs = $.grep(listTabs, function (value) {
                          return value != orderInt;
                      });
                      localStorage.setItem("listTabs", JSON.stringify(listTabs));
                  }
              }
          }
          else {
              $(".table").show();
              $(".tabrm").hide();
              //$(".tabrm").addClass("clearfix");

              //check = false;
          }

      });

    $('.add-contact').click(function (e) {

        var sessionValue = $("#gettype").val();;

        e.preventDefault();
        var id = $(".nav-tabs").children().length; //think about it ;)

        if (id == 6) {
            alert("Bạn không thể thực hiện quá 5 đơn cùng lúc!");
        }
        else {
            var tabId = 'contact_' + id;
            $(this).closest('li').before('<li><a href="#contact_' + id + '">Đơn hàng </a><span>x</span></li>');
            $('.tab-content').append('<div class="tab-pane" id="' + tabId + '"></div>');
            $('.nav-tabs li:nth-child(' + id + ') a').click();
        }

    });
    var check = false;
    $('#chien').click(function () { // Click event on the "Add Tab" button
        if (check == true) {
            $(".order-sidebar").show();
            $(".order-content").removeClass("col-md-12");
            $(".order-content").addClass("col-md-9");
            check = false;
        }
        else {
            $(".order-sidebar").hide();
            $(".order-content").removeClass("col-md-9");
            $(".order-content").addClass("col-md-12");
            check = true;
        }

    });
    $('#chien1').click(function () { // Click event on the "Add Tab" button
        if (check == true) {
            $(".table").show();

            check = false;
        }
        else {
            $(".table").hide();
            check = true;
        }

    });

});


function addtabdemo(orderInternalId, isReload, isActive) {
    check = true;

    var formData = {
        orderInternalId: orderInternalId

    };
    if (!isReload) {
        var listTabsString = localStorage.getItem("listTabs");
        if (listTabsString) {
            var listTabs = JSON.parse(listTabsString);
            var isexits = $.inArray(orderInternalId, listTabs);
            if (isexits == -1) {
                listTabs.push(orderInternalId);
                localStorage.setItem("listTabs", JSON.stringify(listTabs));
            }
        } else {
            var listTabs = new Array();
            listTabs.push(orderInternalId);
            localStorage.setItem("listTabs", JSON.stringify(listTabs));
        }
    }

    $(".order-sidebar").hide();
    //$(".table").hide();
    $(".order-content").removeClass("col-md-9");
    $(".order-content").addClass("col-md-12");
    $(".header-banner-hide").hide();
    $.ajax({
        type: 'POST',
        url: '/Account/GetModelOrderByIdNew',
        data: JSON.stringify(formData),
        cache: false,
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            console.log(data);
            var sessionValue = $("#gettype").val();

            if (data.success) {
                console.log(data);
                var divBody = '';
                var divOrderWallests = '';
                for (i = 0; i <= data.data.ListWallets.length - 1; i++) {

                    var divWallets = '<tr><td>' + data.data.ListWallets[i].Name + ' </td><td>' + data.data.ListWallets[i].Money + ' </td></tr>'

                    divOrderWallests = divOrderWallests + divWallets
                };
                for (i = 0; i <= data.data.ListOrderDetails.length - 1; i++) {
                    var divtr = '<tr>' +

                                        '<td class="text-center"> ' + data.data.ListOrderDetails[i].OrderInternalDetailId + '' +
                                        '</td>' +
                                        '<td class="text-center" width="120px">' +
                                         '<input type="text" class="form-control" id="UPLink'+ data.data.ListOrderDetails[i].OrderInternalDetailId + '" value=" ' + data.data.ListOrderDetails[i].Link + '">' +
                                          '<a href="' + data.data.ListOrderDetails[i].Link + '" , target="_blank" rel="tooltip" title="Cập nhật TT nhận hàng" class="btn btn-info btn-xs btn-edit-info"><i class="fa fa-link" aria-hidden="true"></i></a>' +
                                        '</td>' +
                                         '<td class="text-center"><a href="' + data.data.ListOrderDetails[i].Image + '" target="_blank"><img src="' + data.data.ListOrderDetails[i].Image + '" width="30" height="30"></a></td>' +

                                        '<td class="text-center"><input type="text" class="form-control" id="UpdateSize'+ data.data.ListOrderDetails[i].OrderInternalDetailId + '" value="  ' + data.data.ListOrderDetails[i].Size + '"></td>' +

                                        '<td class="text-center"><input type="text" class="form-control" id="UpdateColor'+ data.data.ListOrderDetails[i].OrderInternalDetailId + '" value="  ' + data.data.ListOrderDetails[i].Color + '"></td>' +
                                        '<td class="text-center">' +
                                       '<input type="text" class="form-control" readonly = true id="UpdateSL'+ data.data.ListOrderDetails[i].OrderInternalDetailId + '" value=" ' + data.data.ListOrderDetails[i].Quantity + '">' +
                                        '</td>' +

                                        '<td class="text-center">' +
                                         '<input type="text" class="form-control" id="UpdateSLTT'+ data.data.ListOrderDetails[i].OrderInternalDetailId + '" value="' + (data.data.ListOrderDetails[i].QuantitySellPlace != null ? data.data.ListOrderDetails[i].QuantitySellPlace : '') + ' ">' +
                                        '</td>' +
                                        '<td class="text-center"><input type="text" readonly = true  id="UpdateDongia" class="form-control" value="' + data.data.ListOrderDetails[i].Price + ' "></td>' +
                                          '<td class="text-center"><input type="text" id="UpdateDongiaTT'+ data.data.ListOrderDetails[i].OrderInternalDetailId + '" class="form-control" value="' + (data.data.ListOrderDetails[i].PriceReality != null ? data.data.ListOrderDetails[i].PriceReality : '') + ' "></td>' +
                                        '<td class="text-center"><input type="text" readonly = true class="form-control" id="UpdateTongGia" value="  ' + (data.data.ListOrderDetails[i].Quantity * data.data.ListOrderDetails[i].Price) + '"></td>' +
                                         '<td class="text-center"><input type="text" class="form-control" id="UpdateTongGiaTT" value="  ' + (data.data.ListOrderDetails[i].QuantitySellPlace * data.data.ListOrderDetails[i].PriceReality) + '"></td>' +
                                         '<td class="text-center">' +
                                        '<input type="text" class="form-control" id="UpdateGhichuKhach'+ data.data.ListOrderDetails[i].OrderInternalDetailId + '" value="' + (data.data.ListOrderDetails[i].NoteClient == null ? data.data.NoteClient : 'Không có') + '">' +
                                        '</td>' +
                                         '<td class="text-center">' +
                                            '<input type="text" class="form-control" id="UpdateGhichuSale" value=" ' + (data.data.ListOrderDetails[i].NoteSale == null ? data.data.NoteSale : 'Không có') + '">' +
                                        '</td>' +
                                          '<input type="hidden"  class="form-control" id="OrderIDO'+ data.data.ListOrderDetails[i].OrderInternalId +'" value=" ' + data.data.ListOrderDetails[i].OrderInternalId + '">' +
                                            '<input type="hidden"  class="form-control" id="OrderIDL" value=" ' + data.data.ListOrderDetails[i].OrderInternalDetailId + '">' +
                               
                                         '<td class="text-center">' +
                                                 '<a href="javascript:void(0)" onclick="UpdateLinkOder(' + data.data.ListOrderDetails[i].OrderInternalDetailId+","+ data.data.ListOrderDetails[i].OrderInternalId + ');" rel="tooltip" title="Sửa link" class="btn btn-info btn-xs btn-edit"><i class="fa fa-floppy-o"></i></a>' +
                                         '<a href="javascript:void(0)" onclick="DeleteOrderLink(' + data.data.ListOrderDetails[i].OrderInternalDetailId + ');" rel="tooltip" title="Xóa link" class="btn btn-danger btn-xs btn-deleted"><i class="fa fa-trash-o"></i></a>' +
                                        '</td>' +

                 '</tr>';

                    divBody += divtr;
                };

                var id = $(".nav-tabs").children().length; //think about it ;)
                if (id == 6) {
                    alert(1);
                }
                else {
                    var tabId = 'contact_' + id;
                    $('.nav-tabs li:last-child').before($('<li><a href="#contact_' + id + '">Đơn hàng   </a>' + data.data.OrderInternalId + '<span id="' + data.data.OrderInternalId + '">x</span></li>'));
                    $('.tab-content').append('<div class="tab-pane" id="' + tabId + '">' + '<div class="container tabrm">' +

    '<div class="container">' +
    '<div class="alert alert-default btn-flat row">' +
      '<div class="col-md-4"> ' +
       '</div>' +
     '<div class="col-md-6"> ' +
     '<p><h2>CHI TIẾT ĐƠN HÀNG  <span style="color:red">' + data.data.OrderInternalId + '</span></h2> </p>' +
      '<input type="hidden" class="form-control" id="IdOrder1' + data.data.OrderInternalId + '" readonly = true value="' + data.data.OrderInternalId + '">' +
       '<input type="hidden" class="form-control" id="IdOrder" readonly = true value="' + data.data.OrderInternalId + '">' +
       
     '<br>' +
     '</div>' +

        '<div class="col-md-4">' +
          '<h4><b>Thông tin Khách hàng:</b></h4>' +
            '<p>' +
                '<font>(-) Tên:</font>  <font style="float: right"> ' + (data.data.Name != null ? data.data.Name : 'Chưa có tài khoản') + ' </font>' +
            '</p>' +
            '<p>' +
                '<font>(-) Số điện thoại:  </font><font style="float: right"> ' + data.data.Phone + '</font>' +
            '</p>' +
            '<p>' +
                '<font>(-) Email:</font><font style="float: right"> ' + (data.data.Email != null ? data.data.Email : 'Chưa có Email') + '</font>' +
            '</p>' +
             '<p>' +
                '<font>(-) Địa chỉ:  </font><font style="float: right"> ' + (data.data.Address != null ? data.data.Address : 'Chưa có Địa chỉ') + '</font>' +
            '</p>' +

            '<p>' +
              '<font>(-) Username:</font><font style="float: right">  ' + data.data.UserName + '</font>' +

            '</p>' +
               '<p>' +
              '<font>(-) Tài khoản:</font><font style="float: right">  ' + data.data.UserName + '</font>' +

            '</p>' +
            '<hr>' +


        '</div>' +



        '<div class="col-md-4">' +
          '<h4><b>Thông tin Đơn hàng:</b></h4>' +
          '<p>' +
                '<font>(-) Tình trạng:</font> <font style=""> </font><font style=" color:red;float: right "> ' + data.data.StatusText + '</font>' +
            '</p>' +
            '<p>' +
                '<font>(-) Kho:</font><font style="float: right"></font>' +
            '</p>' +
            '<p>' +
                '<font>(-) Ngày:</font><font style="float: right"> ' + ((data.data.CreateDate) ? '' : DateTime.Now.ToString("dd/MM/yyyy")) + '</font>' +
            '</p>' +
            '<p>' +
                '<font>(-) UserSale:  </font><font style="float: right"> ' + (data.data.SaleManager != null ? data.data.SaleManager : 'Chưa có Sale') + '</font>' +
            '</p>' +
             '<p>' +
                '<font>(-) Mã Đơn Hàng:</> </font> <font style="float: right"> ' + data.data.OrderInternalId + ' </font>' +
            '</p>' +

             '<hr>' +
        '</div>' +

        '<div class="col-md-4">' +
        '<div class="row" data-spy="scroll">' +
             '<h4><b>Thông tin Tài khoản:</b></h4>' +
             '<p>' +
             '<div data-spy="scroll" style=" height: 80px !important;overflow-y:scroll;"><table><thead><tr><th>Tên Ngân hàng </th><th>Số tài khoản NH</th></tr></thead><tbody>' + divOrderWallests + '' + '</tbody></table>' +
              '</div>' +

            '</p>' +

            '</div>' +
             '<div class="row col-md-offset-4">' +


            '<button class="btn btn-primary" onclick="Cancel_Order(' + data.data.OrderInternalId + ');">Hủy đơn</button>' +
            ' <button class="btn btn-primary"  onclick="MisOrder_Order(' + data.data.OrderInternalId + ');">Hết Hàng</button>' +
            '<button class="btn btn-primary1 btn-sale-confirm btn-block" style="width:170px"title="Chốt đơn" onclick="CheckStatus(' + data.data.OrderInternalId + ');">Thanh toán đơn</button>' +


            '</div>' +



        '</div>' +

    '</div>' +

    '<div class="alert alert-default btn-flat row">' +

        '<div class="col-md-12">' +
        '<div class="form-group col-md-2">' +
                '<h4>Số dư Tiền mặc cả</h4>' +
                '<h3 style="color:red;">' + (data.data.TotalPriceClient - data.data.TotalPriceReality) + '</h3>' +
        '</div>' +
        '<div class="form-group col-md-10">' +
                '<label>Ghi chú</label>' +
                '<textarea class="form-control"  rows="3" id="OrderComment' + data.data.OrderInternalId + '"></textarea>' +
        '</div>' +



            '</div>' +

          '<div class="col-md-12">' +
        '<th class="table-responsive" style="width: 100%">' +
            '<table class="table table-bordered table-striped" style="font-size: 12px">' +
                '<thead>' +
                    '<tr>' +
                      '<th class="text-center">Mã vận đơn</th>' +
                      '<th class="text-center">Mã giao dịch</th> ' +
                      '<th class="text-center">% Dịch vụ</th> ' +
                      '<th class="text-center">% Đặt cọc</th>' +
                      '<th class="text-center">Phí Ship Nội địa TQ</th>' +
                      '<th class="text-center">Tổng tiền Ban đầu</th>' +
                      '<th class="text-center">Tổng tiền Khách trả</th>' +
                       '<th class="text-center">TT Thực tế</th>' +
                    '</tr>' +
                '</thead>' +
                '<tbody>' +
                    '<tr>' +
                        '<td><input type="text" class="form-control"  id="IDShippingCode' + data.data.OrderInternalId + '" value=""></td>' +
                        '<td><input type="text" class="form-control"   id="IDTransId' + data.data.OrderInternalId + '" value=""></td>' +
                        '<td><input type="text" class="form-control"  id="IDFee' + data.data.OrderInternalId + '" value="5"></td>' +
                        '<td><input type="text" class="form-control" type="number" readonly = true  id="IDDownPaymentRate' + data.data.OrderInternalId + '" value="' + data.data.DownPaymentRate + '"></td>' +
                        '<td><input type="text" class="form-control" type="number" id="IDFeeShipChina' + data.data.OrderInternalId + '"   value="' + data.data.FeeShipChina + '"></td>' +
                        '<td><input type="text" class="form-control" id="usr"  readonly = true  value=" ' + (data.data.TotalPrice) + '"></td>' +
                        '<td><input type="text" class="form-control"  readonly = true  type="number"  id="IDPriceReality' + data.data.OrderInternalId + '" value="' + data.data.TotalPriceClient + '" ></td>' +
                        '<td><input type="text" class="form-control" type="number"  id="IDPriceRealityEdit' + data.data.OrderInternalId + '" value="' + data.data.TotalPriceReality + '" ></td>' +
                        '<input type="hidden" class="form-control" id="IdOrder" value="' + data.data.OrderInternalId + '">' +
                        '<input type="hidden" class="form-control" id="TotalMoney" readonly = true value="' + data.data.TotalPriceClient + '">' +
                        
                    '</tr>' +
                '</tbody>' +
            '</table>' +
        '</div>' +
        '</div>' +
         '<div class="col-md-12">' +
        '<div class="table-responsive" style="width: 100%">' +
            '<table class="table table-bordered table-striped" style="font-size: 12px">' +
                '<thead>' +
                    '<tr>' +
                        '<th class="text-center">STT</th>' +
                        '<th class="text-center">Link</th>' +
                        '<th class="text-center">Link ảnh</th>' +
                        '<th class="text-center">Size</th>' +
                        '<th class="text-center">Màu</th>' +
                        '<th class="text-center">Số Lượng KH</th>' +
                        '<th class="text-center">Số Lượng TT</th>' +
                        '<th class="text-center">Đơn giá</th>' +
                        '<th class="text-center">Đơn giá TT</th>' +
                        '<th class="text-center">Tổng giá</th>' +
                         '<th class="text-center">Tổng giá TT</th>' +
                        '<th class="text-center">Ghi chú Khách</th>' +
                        '<th class="text-center">Ghi chú Sale</th>' +
                        '<th class="text-center">Thao tác</th>' +
                    '</tr>' +
                '</thead>' +
                '<tbody>' +
                                            divBody+

                    '</tbody>' +
                '</table>' +
            '</div>' +
     '</div>' +

        '</div>' +
    '</div>'
                        );
                    if (isActive) {
                        $('.nav-tabs li:nth-child(' + id + ') a').click();
                    }
                }
            }
        }
    })
};

function reloadTabs(orderId) {
    //remove all
    console.log("ok")
    $(".nav-tabs").children().remove();
    $(".tab-content").children().remove();
    var li = $("<li><a href=\"#\" class=\"add-contact\"></a></li>");
    $(".nav-tabs").prepend(li);

    var listTabsString = localStorage.getItem("listTabs");
    if (listTabsString) {
        var listTabs = JSON.parse(listTabsString);
        for (var i = 0; i < listTabs.length; i++) {
            if (listTabs[i] == orderId) {
                addtabdemo(listTabs[i], true, true);
            } else {
                addtabdemo(listTabs[i], true, false);
            }
        }
    }
}