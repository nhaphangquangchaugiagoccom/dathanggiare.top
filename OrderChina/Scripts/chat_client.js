﻿
$(function () {

    // Declare a proxy to reference the hub.
    var chatHub = $.connection.chatHub;

    registerClientMethods(chatHub);

    // Start Hub
    $.connection.hub.start().done(function () {

        registerEvents(chatHub);
        registerButtonChat(chatHub);
    });

});

function registerEvents(chatHub) {
    var isAuthen = $('#hdIsAuthen').val();
    var userId = $('#hdUserId').val();
    var clientCookieId = $('#hdClientCookieId').val();

    chatHub.server.connect(isAuthen, userId, clientCookieId);

}

function registerClientMethods(chatHub) {

    // Calls when user successfully logged in
    chatHub.client.onConnected = function (id, user) {

        $('#hdId').val(id);
        $('#hdchatUserId').val(user.UserId);
        OpenPrivateChatWindow(chatHub, '', '');

        //if (listLoadWindows && listLoadWindows.length > 0) {
        //    for (var i = 0; i < listLoadWindows.length; i++) {
        //        var ctrlId = "private_" + listLoadWindows[i];
        //        if ($(jq(ctrlId)).length === 0) {
        //            chatHub.server.createPrivateChatWindow('', ctrlId, listLoadWindows[i]);
        //        }

        //    }
        //};
    };

    chatHub.client.sendPrivateMessage = function (windowId, message) {

        var ctrId = 'private_' + windowId;

        if ($('#' + ctrId).length === 0) {

            OpenPrivateChatWindow(chatHub, '', windowId);

        } else {
            var userId = $('#hdchatUserId').val();
            $('#' + ctrId).find('#divMessage').append('<div class="message"><span class="userName">' + (userId === message.FromUserId ? "Bạn" : message.UserName) + '</span>: ' + message.Message + '</div>');
        }

        // set scrollbar
        if ($('#' + ctrId).length > 0) {
            var height = $('#' + ctrId).find('#divMessage')[0].scrollHeight;;
            $('#' + ctrId).find('#divMessage').animate({
                scrollTop: height
            });
        }

    };

    chatHub.client.onCreatePrivateChatWindow = function (id, ctrId, user, listMessage) {

        if (user && user.UserId !== "") {
            if ($('#' + 'private_').length > 0) {
                $('#' + 'private_').remove();

                var hdchatUserId = $('#hdchatUserId').val();

                for (var iii = 0; iii < popups.length; iii++) {
                    if ('private_' === popups[iii]) {
                        Array.remove(popups, iii);
                        removeRecordPopups(hdchatUserId);
                        calculate_popups();
                        break;
                    }
                }
            }
        }

        createPrivateChatWindow(chatHub, id, ctrId, user);

        var userId = $('#hdchatUserId').val();

        var isdisplay = getPopups(userId, user);
        if (isdisplay) {
            $(jq(ctrId)).find(jq('hien' + ctrId)).click();
        } else {
            $(jq(ctrId)).find(jq('an' + ctrId)).click();
        }

        var divMess = $('#' + ctrId).find('#divMessage');


        for (i = 0; i < listMessage.length; i++) {

            divMess.append('<div class="message"><span class="userName">' + (userId === listMessage[i].FromUserId ? "Bạn" : "Dịch vụ đặt hàng giá rẻ") + '</span>: ' + listMessage[i].Message + '</div>');

        }

        var height = $('#' + ctrId).find('#divMessage')[0].scrollHeight;;
        $('#' + ctrId).find('#divMessage').animate({
            scrollTop: height
        });
    };

}

function OpenPrivateChatWindow(chatHub, id, userId) {

    var ctrId = 'private_' + userId;

    if ($('#' + ctrId).length > 0) return;

    chatHub.server.createPrivateChatWindow(id, ctrId, userId);
}

function createPrivateChatWindow(chatHub, connectId, ctrId, user) {

    var hdchatUserId = $('#hdchatUserId').val();

    var div = '';
    if (user && user.UserId !== "") {
        div = '<div class="popup-box chat-popup" id="' + ctrId + '" conId="' + (connectId ? connectId : "") + '" style="display: block;height:350px">' +
        '<div class="popup-head" ><div class="popup-head-left">' + user.UserName +
       '</div><div class="popup-head-right"><a style="padding-right:5px; display:none" class="glyphicon glyphicon-comment" id="hien' + ctrId + '"></a><a style="padding-right:5px;display:block" class="glyphicon glyphicon-minus" id="an' + ctrId + '" ></a><a id="imgDelete">✕</a>' +
       '</div>' +
       '<div style="clear: both"></div>' +
       '</div>' +
       '<div class="popup-messages" id="ana' + ctrId + '" style="display:block">' +
       '<div id="divMessage" class="messageArea">' +
       '<div class="message"><div class="staff" style="float:left"><img src="/images/Telesales.jpg"></div><div class="message1"><span class="userName">Dịch vụ đặt hàng giá rẻ</span>: Chào Anh/Chị ạ !<br /> Em có thể giúp gì được cho anh/chị ?</div></div>' +
       '</div>' +
       '<div class="buttonBar">' +
       '<input id="txtPrivateMessage" class="msgText" type="text"   />' +
       '<input id="btnSendMessage" class="btn btn-primary submitButton button" type="button" value="Gửi"   />' +
       '</div>' +
       '</div>' +
       '</div>';
    } else {
        div = '<div class="popup-box chat-popup" id="' + ctrId + '" conId="' + (connectId ? connectId : "") + '" style="display: block;height:350px">' +
        '<div class="popup-head" ><div class="popup-head-left"> Chat với chúng tôi' +
       '</div><div class="popup-head-right"><a style="padding-right:5px; display:none" class="glyphicon glyphicon-comment" id="hien' + ctrId + '"></a><a style="padding-right:5px;display:block" class="glyphicon glyphicon-minus" id="an' + ctrId + '" ></a><a id="imgDelete">✕</a>' +
       '</div>' +
       '<div style="clear: both"></div>' +
       '</div>' +
       '<div class="popup-messages" id="ana' + ctrId + '" style="display:block">' +
       '<div id="divMessage" class="messageArea">' +
       '<div class="message"><div class="staff" style="float:left"><img src="/images/Telesales.jpg"></div><div class="message1"><span class="userName">Dịch vụ đặt hàng giá rẻ</span>: Chào Anh/Chị ạ !<br /> Em có thể giúp gì được cho anh/chị ?</div></div>' +
       '</div>' +
       '<div class="buttonBar">' +
       '<input id="txtPrivateMessage" class="msgText" type="text"   />' +
       '<input id="btnSendMessage" class="btn btn-primary submitButton button" type="button" value="Gửi"   />' +
       '</div>' +
       '</div>' +
       '</div>';
    }


    var $div = $(div);
    $div.find(".popup-head").click(function () {
        if (document.getElementById('ana' + ctrId).style.display === 'none') {
            document.getElementById('ana' + ctrId).style.display = 'block';
            document.getElementById('hien' + ctrId).style.display = 'none';
            document.getElementById('an' + ctrId).style.display = 'block';
            document.getElementById(ctrId).style.height = '350px';
            updatePopups(hdchatUserId, user, true);

        }
        else {
            document.getElementById('ana' + ctrId).style.display = 'none';
            document.getElementById('an' + ctrId).style.display = 'none';
            document.getElementById('hien' + ctrId).style.display = 'block';
            document.getElementById(ctrId).style.height = '30px';
            updatePopups(hdchatUserId, user, false);

        }
    });
    $div.find('#an' + ctrId).click(function (event) {
        event.stopPropagation();

        document.getElementById('ana' + ctrId).style.display = 'none';
        document.getElementById('an' + ctrId).style.display = 'none';
        document.getElementById('hien' + ctrId).style.display = 'block';
        document.getElementById(ctrId).style.height = '30px';
        updatePopups(hdchatUserId, user, false);

    });

    $div.find('#hien' + ctrId).click(function (event) {
        event.stopPropagation();

        document.getElementById('ana' + ctrId).style.display = 'block';
        document.getElementById('hien' + ctrId).style.display = 'none';
        document.getElementById('an' + ctrId).style.display = 'block';
        document.getElementById(ctrId).style.height = '350px';
        updatePopups(hdchatUserId, user, true);

    });

    // DELETE BUTTON IMAGE
    $div.find('#imgDelete').click(function () {
        $('#' + ctrId).remove();

        for (var iii = 0; iii < popups.length; iii++) {
            if (ctrId === popups[iii]) {
                Array.remove(popups, iii);
                removeRecordPopups(hdchatUserId, user);
                calculate_popups();
                return;
            }
        }
    });

    // Send Button event
    $div.find("#btnSendMessage").click(function () {

        $textBox = $div.find("#txtPrivateMessage");
        var msg = $textBox.val();
        if (msg.length > 0) {
            var conId = $div.attr('conId');
            chatHub.server.sendPrivateMessage(conId, msg, user);
            $textBox.val('');
        }
    });

    // Text Box event
    $div.find("#txtPrivateMessage").keypress(function (e) {
        if (e.which === 13) {
            $div.find("#btnSendMessage").click();
        }
    });

    AddDivToContainer($div);

    for (var iii = 0; iii < popups.length; iii++) {
        //already registered. Bring it to front.
        if (ctrId === popups[iii]) {
            Array.remove(popups, iii);
            popups.unshift(ctrId);
            calculate_popups();
            return;
        }
    }


    popups.unshift(ctrId);
    recordPopups(hdchatUserId, user, true)
    calculate_popups();

}

function jq(myid) {

    return "#" + myid.replace(/(:|\.|\[|\]|,|=)/g, "\\$1");

}
function AddDivToContainer($div) {
    $('#divContainer').prepend($div);
}

//this function can remove a array element.
Array.remove = function (array, from, to) {
    var rest = array.slice((to || from) + 1 || array.length);
    array.length = from < 0 ? array.length + from : from;
    return array.push.apply(array, rest);
};

//this variable represents the total number of popups can be displayed according to the viewport width
var total_popups = 0;

//arrays of popups ids
var popups = [];

//this is used to close a popup
function close_popup(id) {
    for (var iii = 0; iii < popups.length; iii++) {
        if (id === popups[iii]) {
            Array.remove(popups, iii);

            document.getElementById(id).style.display = "none";

            calculate_popups();

            return;
        }
    }
}

//displays the popups. Displays based on the maximum number of popups that can be displayed on the current viewport width
function display_popups() {
    var right = 0;

    var iii = 0;
    for (iii; iii < total_popups; iii++) {
        if (popups[iii] !== undefined) {
            var element = document.getElementById(popups[iii]);
            element.style.right = right + "px";
            right = right + 350;
            element.style.display = "block";
        }
    }

    for (var jjj = iii; jjj < popups.length; jjj++) {
        var element1 = document.getElementById(popups[jjj]);
        element1.style.display = "none";
    }
}

//calculate the total number of popups suitable and then populate the toatal_popups variable.
function calculate_popups() {
    var width = window.innerWidth;
    if (width < 540) {
        total_popups = 0;
    }
    else {
        width = width - 200;
        //350 is width of a single popup box
        total_popups = parseInt(width / 350);
    }

    display_popups();

}

function getPopups(userId, user) {
    var listWindows = localStorage.getItem("chat_windows_" + userId);
    if (listWindows) {
        var listTabs = JSON.parse(listWindows);
        var isExits = $.grep(listTabs, function (value) {
            return value.windowId === (user ? user.UserId : "");
        });
        if (isExits.length !== 0) {
            return isExits[0].isdisplay
        }
    }
    return true;
}

function updatePopups(userId, user, isdisplay) {
    var listWindows = localStorage.getItem("chat_windows_" + userId);
    if (listWindows) {
        var listTabs = JSON.parse(listWindows);
        var isExits = $.grep(listTabs, function (value) {
            return value.windowId === (user ? user.UserId : "");
        });
        if (isExits.length !== 0) {
            isExits[0].isdisplay = isdisplay;
            localStorage.setItem("chat_windows_" + userId, JSON.stringify(listTabs));
        }
    }
}

function recordPopups(userId, user, isdisplay) {
    var listWindows = localStorage.getItem("chat_windows_" + userId);
    if (listWindows) {
        var listTabs = JSON.parse(listWindows);
        var isExits = $.grep(listTabs, function (value) {
            return value.windowId === (user ? user.UserId : "");
        });
        if (isExits.length === 0) {
            var chatwindow = { windowId: (user ? user.UserId : ""), isdisplay: isdisplay };
            listTabs.push(chatwindow);
            localStorage.setItem("chat_windows_" + userId, JSON.stringify(listTabs));
        }
    } else {
        var listTabs = new Array();
        var chatwindow = { windowId: (user ? user.UserId : ""), isdisplay: isdisplay };
        listTabs.push(chatwindow);
        localStorage.setItem("chat_windows_" + userId, JSON.stringify(listTabs));
    }
}

function removeRecordPopups(userId, user) {
    var listChatWindows = localStorage.getItem("chat_windows_" + userId);
    if (listChatWindows) {
        var listWindows = JSON.parse(listChatWindows);
        var isExits = $.grep(listWindows, function (value) {
            return value.windowId === (user ? user.UserId : "");
        });
        if (isExits.length !== 0) {
            listWindows = $.grep(listWindows, function (value) {
                return value.windowId !== (user ? user.UserId : "");
            });
            localStorage.setItem("chat_windows_" + userId, JSON.stringify(listWindows));
        }
    }
}

function registerButtonChat(chatHub) {


    $("#view_order_new_chat_client").click(function () {

        var userId = $(this).attr('userid');

        if (userId && userId > 0) {
            var ctrlId = "private_" + userId;
            if ($(jq(ctrlId)).length === 0) {
                chatHub.server.createPrivateChatWindow('', ctrlId, userId);
            }

        }

    });
    $("#view_order_new_chat_order").click(function () {

        var userId = $(this).attr('userid');

        if (userId && userId > 0) {
            var ctrlId = "private_" + userId;
            if ($(jq(ctrlId)).length === 0) {
                chatHub.server.createPrivateChatWindow('', ctrlId, userId);
            }

        }

    });
    
}

//recalculate when window is loaded and also when window is resized.
window.addEventListener("resize", calculate_popups);
window.addEventListener("load", calculate_popups);