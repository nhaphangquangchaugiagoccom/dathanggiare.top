(function ($) {
	"use strict";
	
	$(window).on('load',function(){
		
		//Preloader
		$('.preloader').delay(500).fadeOut('slow');
        $('body').delay(500).css({'overflow':'visible'});
		
	
	}); 
}(jQuery));	