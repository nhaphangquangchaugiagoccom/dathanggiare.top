﻿

$(function () {


    $("<div class=\"floating-form\" id=\"contact_form\">" +
            "<div class=\"contact-opener\"><i id=\"btn_open_chat_mng\" isflash='false' class=\"fa fa-comments\" style=\"color:yellow\"}></i></div>" +
            "<div class=\"floating-form-heading\">Danh sách chat</div>" +
            "<div id=\"contact_body\" style=\"height:500px; overflow-x: scroll;overflow-y: scroll\">" +
                "<ul class=\"media-list\"><h4>Danh sách KH không có tài khoản</h4></ul>" +
                "<ul id=\"divclientnotlogin\" class=\"media-list\"></ul>" +
                "<hr />" +
                "<ul class=\"media-list\"><h4>Danh sách KH có tài khoản</h4></ul>" +
                "<ul id=\"divclientlogin\" class=\"media-list\"></ul>" +
                "<hr />" +
                "<ul class=\"media-list\"><h4>Danh sách người dùng</h4></ul>" +
                "<ul id=\"divusers\" class=\"media-list\"></ul>" +
            "</div>" +
        "</div>").appendTo(document.body);

    var _scroll = true, _timer = false, _floatbox = $("#contact_form"), _floatbox_opener = $(".contact-opener");
    _floatbox.css("right", "-300px"); //initial contact form position
    _floatbox.css("top", "10px"); //initial contact form position

    //Contact form Opener button
    _floatbox_opener.click(function () {
        if (_floatbox.hasClass('visiable')) {
            _floatbox.animate({ "right": "-300px" }, { duration: 300 }).removeClass('visiable');
        } else {
            _floatbox.animate({ "right": "0px" }, { duration: 300 }).addClass('visiable');
        }
    });

    //Effect on Scroll
    $(window).scroll(function () {
        if (_scroll) {
            _floatbox.animate({ "top": "30px" }, { duration: 300 });
            _scroll = false;
        }
        if (_timer !== false) { clearTimeout(_timer); }
        _timer = setTimeout(function () {
            _scroll = true;
            _floatbox.animate({ "top": "10px" }, { easing: "linear" }, { duration: 500 });
        }, 400);
    });

    // Declare a proxy to reference the hub.
    var chatHub = $.connection.chatHub;

    registerClientMethods(chatHub);

    // Start Hub
    $.connection.hub.start().done(function () {

        registerEvents(chatHub);
        registerButtonChat(chatHub);

    });

});

function registerEvents(chatHub) {
    var isAuthen = $('#hdIsAuthen').val();
    var userId = $('#hdUserId').val();

    chatHub.server.connect(isAuthen, userId, "");
}

function registerClientMethods(chatHub) {

    // Calls when user successfully logged in
    chatHub.client.onConnected = function (id, user, clientNonUser, clientUser, listUser) {

        $('#hdId').val(id);
        $('#hdchatUserId').val(user.UserId);

        // Add All Users
        if (clientNonUser) {
            for (i = 0; i < clientNonUser.length; i++) {

                AddNonUser(chatHub, clientNonUser[i].ConnectionId, clientNonUser[i]);
            }
        }
        if (clientUser) {
            for (i = 0; i < clientUser.length; i++) {

                AddDefaultUser(chatHub, clientUser[i], true);
                if (clientUser[i].ischat) {
                    addNewMsgAlert(clientUser[i].UserId);
                }
            }
        }
        if (listUser) {
            for (i = 0; i < listUser.length; i++) {

                AddDefaultUser(chatHub, listUser[i], false);

                if (listUser[i].ischat) {
                    addNewMsgAlert(listUser[i].UserId);
                }
            }
        }

        //load page
        loadChatWindows(chatHub, user.UserId);
    };

    // On New User Connected
    chatHub.client.onNewUserConnected = function (id, user) {
        AddUser(chatHub, id, user);

        if ($(jq('private_' + user.UserId)).length !== 0) {

            //get msg va chiem quyen
            chatHub.server.getMessageAndSetPermission(id, user);
        }
    };

    // On New User Connected
    chatHub.client.onNewNonUserConnected = function (id, user) {
        if ($(jq(user.UserId)).length === 0) {
            AddNonUser(chatHub, id, user);
            
            addNewMsgAlert(user.UserId);

        }
        if ($(jq('private_' + user.UserId)).length !== 0) {

            //get msg va chiem quyen
            $(jq('private_' + user.UserId)).attr('conId', id);
            chatHub.server.getMessageAndSetPermission(id, user);
        }
    };

    // On New User Connected
    chatHub.client.onGetMessage = function (id, user, listMessage) {
        $(jq(ctrId)).find('#divMessage').empty();
        var divMess = $(jq(ctrId)).find('#divMessage');

        divMess.append('<div class="message"><div class="staff" style="float:left"><img src="/images/Telesales.jpg"></div><div class="message1"><span class="userName">Dịch vụ vận tải</span>: Chào Anh/Chị ạ !<br /> Em có thể giúp gì được cho anh/chị ?</div></div>');

        for (i = 0; i < listMessage.length; i++) {

            var today = new Date(listMessage[i].DateTime);
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();
            var hour = today.getHours();
            var mi = today.getMinutes();
            var ss = today.getSeconds();

            if (dd < 10) {
                dd = '0' + dd
            }
            if (mm < 10) {
                mm = '0' + mm
            }
            if (mi < 10) {
                mi = '0' + mi
            }
            if (ss < 10) {
                ss = '0' + ss
            }
            var today = dd + '/' + mm + '/' + yyyy + ' ' + hour + ':' + mi + ':' + ss;


            divMess.append('<li class="media un_style">' +
            '<div class="media-body">' +
            '<div class="media">' +
            '<a class="pull-left" href="#">' +
            '<i class="fa fa-user"></i>' +
            '</a>' +
            '<div class="media-body">' +
            listMessage[i].Message +
            '<br>' +
            '<small class="text-muted">' + listMessage[i].UserName + ' | ' + today + '</small>' +
            '<hr>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</li>');
        }

        var height = divMess[0].scrollHeight;
        divMess.scrollTop(height);
    };

    // On User Disconnected
    chatHub.client.onUserDisconnected = function (id, user) {
        if ($(jq(user.UserId)).length !== 0) {
            $(jq(user.UserId)).attr('conId', "");
            $(jq(user.UserId)).find("i").css("color", "red");
        }
        var disc = $('<div class="disconnect">"' + user.UserName + '" logged off.</div>');

        $(disc).hide();
        $('#divusers').prepend(disc);
        $(disc).fadeIn(200).delay(2000).fadeOut(200);


    };

    // On User remove
    chatHub.client.onUserReConnected = function (id, user) {
        ClientReConnected(id, user);
    };

    chatHub.client.sendPrivateMessage = function (windowId, message) {

        var ctrId = 'private_' + windowId;

        if ($(jq(ctrId)).length === 0) {
            addNewMsgAlert(windowId);
        } else {
            var today = new Date(message.DateTime);
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();
            var hour = today.getHours();
            var mi = today.getMinutes();
            var ss = today.getSeconds();

            if (dd < 10) {
                dd = '0' + dd
            }
            if (mm < 10) {
                mm = '0' + mm
            }
            if (mi < 10) {
                mi = '0' + mi
            }
            if (ss < 10) {
                ss = '0' + ss
            }
            var today = dd + '/' + mm + '/' + yyyy + ' ' + hour + ':' + mi + ':' + ss;

            $(jq(ctrId)).find('#divMessage').append('<li class="media un_style">' +
                '<div class="media-body">' +
                '<div class="media">' +
                '<a class="pull-left" href="#">' +
                '<i class="fa fa-user"></i>' +
                '</a>' +
                '<div class="media-body">' +
                message.Message +
                '<br>' +
                '<small class="text-muted">' + message.UserName + ' | ' + today + '</small>' +
                '<hr>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</li>');
        }


        // set scrollbar
        if ($(jq(ctrId)).length !== 0) {

            var height = $(jq(ctrId)).find('#divMessage')[0].scrollHeight;;
            $(jq(ctrId)).find('#divMessage').animate({
                scrollTop: height
            });
        }
    };

    chatHub.client.onCreateChatWindow = function (id, ctrId, user, listMessage) {

        createPrivateChatWindow(chatHub, id, ctrId, user);
        if (user.isdisplay) {
            $(jq(ctrId)).find(jq('hien' + ctrId)).click();
        } else {
            $(jq(ctrId)).find(jq('an' + ctrId)).click();
        }

        removeNewMsgAlert(user.UserId);
        
        var divMess = $(jq(ctrId)).find('#divMessage');
        for (i = 0; i < listMessage.length; i++) {

            var today = new Date(listMessage[i].DateTime);
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();
            var hour = today.getHours();
            var mi = today.getMinutes();
            var ss = today.getSeconds();

            if (dd < 10) {
                dd = '0' + dd
            }
            if (mm < 10) {
                mm = '0' + mm
            }
            if (mi < 10) {
                mi = '0' + mi
            }
            if (ss < 10) {
                ss = '0' + ss
            }
            var today = dd + '/' + mm + '/' + yyyy + ' ' + hour + ':' + mi + ':' + ss;


            divMess.append('<li class="media un_style">' +
            '<div class="media-body">' +
            '<div class="media">' +
            '<a class="pull-left" href="#">' +
            '<i class="fa fa-user"></i>' +
            '</a>' +
            '<div class="media-body">' +
            listMessage[i].Message +
            '<br>' +
            '<small class="text-muted">' + listMessage[i].UserName + ' | ' + today + '</small>' +
            '<hr>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</li>');
        }

        var height = divMess[0].scrollHeight;
        divMess.scrollTop(height);
    };

    chatHub.client.onRemoveClientLogin = function (id, sale, user) {
        RemoveClientLogin();

        var disc = $('<div class="disconnect">Sale "' + sale.UserName + '" đã chat.</div>');

        $(disc).hide();
        $('#divclientlogin').prepend(disc);
        $(disc).fadeIn(200).delay(2000).fadeOut(200);
    };

    chatHub.client.onRemoveClientNonLogin = function (id, sale, user) {
        RemoveClientLogin();

        var disc = $('<div class="disconnect">Sale "' + sale.UserName + '" đã chat.</div>');

        $(disc).hide();
        $('#divclientnotlogin').prepend(disc);
        $(disc).fadeIn(200).delay(2000).fadeOut(200);
    };
}

function loadChatWindows(chatHub, hdchatUserId) {
    //get list windows

    var listChatWindows = localStorage.getItem('chat_windows_' + hdchatUserId);
    if (listChatWindows) {
        var listWindows = JSON.parse(listChatWindows);
        if (listWindows.length > 0) {
            for (var i = 0; i < listWindows.length; i++) {
                var ctrlId = 'private_' + listWindows[i].windowId;
                chatHub.server.loadChatWindow(ctrlId, listWindows[i].windowId, listWindows[i].isdisplay);
            }
        }
    }
};

function addNewMsgAlert(userId) {
    AddRingHaveChat(userId);
    flashon();
};

function removeNewMsgAlert(userId) {
    RemoveRingHaveChat(userId);
    flashoff();
};

var intervalId;
function flashon() {
    var isflash = $('#btn_open_chat_mng').attr('isflash');
    if (isflash == "false") {
        intervalId = setInterval(function () { blink() }, 1000);
        $('#btn_open_chat_mng').attr('isflash', true);
    }
}

function flashoff() {
    if ($(jq('contact_body')).find("sup").length === 0) {
        clearInterval(intervalId);
    }
}

function blink() {
    $("#btn_open_chat_mng").fadeTo(100, 0.1).fadeTo(200, 1.0);
}

function jq(myid) {

    return "#" + myid.replace(/(:|\.|\[|\]|,|=)/g, "\\$1");

}

function ClientReConnected(id, user) {
    if ($(jq(user.UserId)).length !== 0) {
        $(jq(user.UserId)).attr('conId', id);
        $(jq(user.UserId)).find("i").css('color', 'green');

    }
}

function RemoveClientLogin(id, user) {
    if ($(jq(user.UserId)).length !== 0) {
        $(jq(user.UserId)).remove();
    }
}

function AddUser(chatHub, id, user) {

    var connectId = $('#hdId').val();

    var code = "";
    if ($(jq(user.UserId)).length === 0) {
        if (connectId !== id) {
            code = $('<li id="' + user.UserId + '" conId="' + id + '" class="media">' +
                '<div class="media-body">' +
                '<h5><i class="fa fa-user" style="color:green"></i> ' + user.UserTypeText + "-" + user.UserName + '</h5>' +
                '</div>' +
                '</li>');

            $(code).click(function () {

                var id = $.trim($(this).attr('conId'));

                if (connectId !== id) {
                    var ctrlId = "private_" + user.UserId;
                    if ($(jq(ctrlId)).length === 0) {
                        chatHub.server.createChatWindow(id, ctrlId, user);
                    }
                }

            });

        }

        if (user.isclient) {
            $("#divclientlogin").append(code);

            AddRingHaveChat(user.UserId);
            flashon();
        } else {
            $("#divusers").append(code);
        }

    } else {
        $(jq(user.UserId)).attr('conId', id);
        $(jq(user.UserId)).find("i").css("color", "green");

        //update private chat window 
        $(jq('private_' + user.UserId)).attr('conId', id);
    }


}

function AddDefaultUser(chatHub, user, isclient) {

    var connectId = $('#hdId').val();

    var code = "";
    if ($(jq(user.UserId)).length === 0) {
        code = $('<li id="' + user.UserId + '" conId="' + (user.ConnectionId ? user.ConnectionId : "") + '" class="media">' +
            '<div class="media-body">' +
            '<h5><i class="fa fa-user" style="color:' + (user.isonline ? 'green' : 'red') + '"></i> ' + user.UserName + '</h5>' +
            '</div>' +
            '</li>');

        $(code).click(function () {

            var id = $.trim($(this).attr('conId'));

            if (connectId !== id) {
                var ctrlId = "private_" + user.UserId;
                if ($(jq(ctrlId)).length === 0) {
                    chatHub.server.createChatWindow(id, ctrlId, user);
                }
            }

        });

        if (isclient) {
            $("#divclientlogin").append(code);
        } else {
            $("#divusers").append(code);
        }
    }
}

function AddNonUser(chatHub, id, user) {

    var connectId = $('#hdId').val();

    var code = "";

    if (connectId !== id) {
        code = $('<li id="' + user.UserId + '"  conId="' + id + '" class="media">' +
                 '<div class="media-body">' +
                 '<h5><i class="fa fa-user" style="color:green"></i> ' + user.UserName + '</h5>' +
                 '</div>' +
                 '</li>');

        $(code).click(function () {

            var id = $(this).attr('conId');

            if (connectId !== id) {
                var ctrlId = "private_" + user.UserId;
                if ($(jq(ctrlId)).length === 0) {
                    chatHub.server.createChatWindow(id, ctrlId, user);
                }
            }

        });

    }

    $("#divclientnotlogin").append(code);

}

function AddRingHaveChat(id) {

    if ($(jq(id)).length > 0) {
        if ($(jq(id)).find("sup").length === 0) {
            $(jq(id)).append('<sup><b style="color: red;">New</b></sup>');
        }
    }

}

function RemoveRingHaveChat(id) {

    if ($(jq(id)).length > 0) {
        if ($(jq(id)).find("sup").length > 0)
            $(jq(id)).find("sup").remove();
    }

}

function createPrivateChatWindow(chatHub, connectId, ctrId, user) {

    var hdchatUserId = $('#hdchatUserId').val();

    var div = '<div class="popup-box chat-popup" id="' + ctrId + '" conId="' + (connectId ? connectId : "") + '" style="display: block;height:350px">' +
         '<div id="popup-head" class="popup-head" ><div class="popup-head-left">' + user.UserName +
        '</div><div class="popup-head-right"><a style="padding-right:5px;display:none" class="glyphicon glyphicon-comment" id="hien' + ctrId + '"></a><a style="padding-right:5px;display:block" class="glyphicon glyphicon-minus" id="an' + ctrId + '" ></a><a id="imgDelete">✕</a>' +
        '</div>' +
        '<div style="clear: both"></div>' +
        '</div>' +
        '<div class="popup-messages" id="ana' + ctrId + '" style="display:block">' +
        '<div id="divMessage" class="messageArea">' +
        '<div class="message"><div class="staff" style="float:left"><img src="/images/Telesales.jpg"></div><div class="message1"><span class="userName">Dịch vụ vận tải</span>: Chào Anh/Chị ạ !<br /> Em có thể giúp gì được cho anh/chị ?</div></div>' +
        '</div>' +
        '<div class="buttonBar">' +
        '<input id="txtPrivateMessage" class="msgText" type="text"   />' +
        '<input id="btnSendMessage" class="btn btn-primary submitButton button" type="button" value="Gửi"   />' +
        '</div>' +
        '</div>' +
        '</div>';

    var $div = $(div);
    $div.find("#popup-head").click(function () {
        if (document.getElementById('ana' + ctrId).style.display == 'none') {
            document.getElementById('ana' + ctrId).style.display = 'block';
            document.getElementById('hien' + ctrId).style.display = 'none';
            document.getElementById('an' + ctrId).style.display = 'block';
            document.getElementById(ctrId).style.height = '350px';
            updatePopups(hdchatUserId, user.UserId, true);

        }
        else {
            document.getElementById('ana' + ctrId).style.display = 'none';
            document.getElementById('an' + ctrId).style.display = 'none';
            document.getElementById('hien' + ctrId).style.display = 'block';
            document.getElementById(ctrId).style.height = '30px';
            updatePopups(hdchatUserId, user.UserId, false);

        }
    });
    $div.find(jq('an' + ctrId)).click(function (event) {
        event.stopPropagation();

        document.getElementById('ana' + ctrId).style.display = 'none';
        document.getElementById('an' + ctrId).style.display = 'none';
        document.getElementById('hien' + ctrId).style.display = 'block';
        document.getElementById(ctrId).style.height = '30px';
        updatePopups(hdchatUserId, user.UserId, false);

    });

    $div.find(jq('hien' + ctrId)).click(function (event) {
        event.stopPropagation();
        document.getElementById('ana' + ctrId).style.display = 'block';
        document.getElementById('hien' + ctrId).style.display = 'none';
        document.getElementById('an' + ctrId).style.display = 'block';
        document.getElementById(ctrId).style.height = '350px';

        updatePopups(hdchatUserId, user.UserId, true);
    });

    // DELETE BUTTON IMAGE
    $div.find('#imgDelete').click(function () {
        $(jq(ctrId)).remove();

        for (var iii = 0; iii < popups.length; iii++) {
            if (ctrId === popups[iii]) {
                Array.remove(popups, iii);

                removeRecordPopups(hdchatUserId, user.UserId);

                calculate_popups();

                return;
            }
        }
    });

    // Send Button event
    $div.find("#btnSendMessage").click(function () {

        $textBox = $div.find("#txtPrivateMessage");
        var msg = $textBox.val();
        if (msg.length > 0) {

            var conId = $div.attr('conId');

            chatHub.server.sendPrivateMessage(conId, msg, user);
            $textBox.val('');
        }
    });

    // Text Box event
    $div.find("#txtPrivateMessage").keypress(function (e) {
        if (e.which === 13) {
            $div.find("#btnSendMessage").click();
        }
    });

    AddDivToContainer($div);

    for (var iii = 0; iii < popups.length; iii++) {
        //already registered. Bring it to front.
        if (ctrId === popups[iii]) {
            Array.remove(popups, iii);
            popups.unshift(ctrId);
            calculate_popups();
            return;
        }
    }


    popups.unshift(ctrId);

    recordPopups(hdchatUserId, user.UserId, user.isdisplay);
    calculate_popups();

}

function AddDivToContainer($div) {
    $('#divContainer').append($div);
}


//this function can remove a array element.
Array.remove = function (array, from, to) {
    var rest = array.slice((to || from) + 1 || array.length);
    array.length = from < 0 ? array.length + from : from;
    return array.push.apply(array, rest);
};

//this variable represents the total number of popups can be displayed according to the viewport width
var total_popups = 0;

//arrays of popups ids
var popups = [];

//this is used to close a popup
function close_popup(id) {
    for (var iii = 0; iii < popups.length; iii++) {
        if (id === popups[iii]) {
            Array.remove(popups, iii);
            document.getElementById(id).style.display = "none";

            calculate_popups();

            return;
        }
    }
}

//displays the popups. Displays based on the maximum number of popups that can be displayed on the current viewport width
function display_popups() {
    var right = 0;

    var iii = 0;
    for (iii; iii < total_popups; iii++) {
        if (popups[iii] !== undefined) {
            var element = document.getElementById(popups[iii]);
            element.style.right = right + "px";
            right = right + 350;
            element.style.display = "block";
        }
    }

    for (var jjj = iii; jjj < popups.length; jjj++) {
        var element1 = document.getElementById(popups[jjj]);
        element1.style.display = "none";
    }
}


//calculate the total number of popups suitable and then populate the toatal_popups variable.
function calculate_popups() {
    var width = window.innerWidth;
    if (width < 540) {
        total_popups = 0;
    }
    else {
        width = width - 200;
        //350 is width of a single popup box
        total_popups = parseInt(width / 350);
    }

    display_popups();

}

function updatePopups(userId, windowId, isdisplay) {
    var listWindows = localStorage.getItem("chat_windows_" + userId);
    if (listWindows) {
        var listTabs = JSON.parse(listWindows);
        var isExits = $.grep(listTabs, function (value) {
            return value.windowId == windowId;
        });
        if (isExits.length !== 0) {
            isExits[0].isdisplay = isdisplay;
            localStorage.setItem("chat_windows_" + userId, JSON.stringify(listTabs));
        }
    }
}

function recordPopups(userId, windowId, isdisplay) {
    var listWindows = localStorage.getItem("chat_windows_" + userId);
    if (listWindows) {
        var listTabs = JSON.parse(listWindows);
        var isExits = $.grep(listTabs, function (value) {
            return value.windowId == windowId;
        });
        if (isExits.length === 0) {
            var chatwindow = { windowId: windowId, isdisplay: isdisplay };
            listTabs.push(chatwindow);
            localStorage.setItem("chat_windows_" + userId, JSON.stringify(listTabs));
        }
    } else {
        var listTabs = new Array();
        var chatwindow = { windowId: windowId, isdisplay: isdisplay };
        listTabs.push(chatwindow);
        localStorage.setItem("chat_windows_" + userId, JSON.stringify(listTabs));
    }
}

function removeRecordPopups(userId, windowId) {
    var listChatWindows = localStorage.getItem("chat_windows_" + userId);
    if (listChatWindows) {
        var listWindows = JSON.parse(listChatWindows);
        var isExits = $.grep(listWindows, function (value) {
            return value.windowId == windowId;
        });
        if (isExits.length !== 0) {
            listWindows = $.grep(listWindows, function (value) {
                return value.windowId != windowId;
            });
            localStorage.setItem("chat_windows_" + userId, JSON.stringify(listWindows));
        }
    }
}
function registerButtonChat(chatHub) {

    $("#view_order_chat_client").click(function () {

        var userId = $(this).attr('userid');

        if (userId && userId > 0) {
            var ctrlId = "private_" + userId;
            if ($(jq(ctrlId)).length === 0) {
                var user = { UserId: userId,isupdate : true };
                chatHub.server.createChatWindow('', ctrlId, user);
            }

        }

    });

    $("#view_order_new_chat_client").click(function () {

        var userId = $(this).attr('userid');

        if (userId && userId > 0) {
            var ctrlId = "private_" + userId;
            if ($(jq(ctrlId)).length === 0) {
                var user = { UserId: userId, isupdate: true };
                chatHub.server.createChatWindow('', ctrlId, user);
            }

        }

    });

    $('.tab-content').on('click','#view_order_orderer_new_chat_client', function () {

        var userId = $(this).attr('userid');

        if (userId && userId > 0) {
            var ctrlId = "private_" + userId;
            if ($(jq(ctrlId)).length === 0) {
                var user = { UserId: userId, isupdate: true };
                chatHub.server.createChatWindow('', ctrlId, user);
            }

        }

    });
}

//recalculate when window is loaded and also when window is resized.
window.addEventListener("resize", calculate_popups);
window.addEventListener("load", calculate_popups);
