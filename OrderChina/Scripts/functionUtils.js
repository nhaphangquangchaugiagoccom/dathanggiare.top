
function formatNumber(input, decimal) {
    try {
        if (input == null) return input;
        if (isNaN(input)) return input;
        if (typeof input === "string") input = convertFormatToNumber(input);
        if (decimal == null) decimal = 0;//TODO hash code fix decimal = 0
        if (typeof decimal !== "undefined") input = roundFloat(input, decimal);
        input = input.toString().split('.');
        input[0] = input[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
        return input.join('.');
    } catch (ex) {
        console.error(ex);
    }
    return input;
}
function roundFloat(numberFloat, lenght) {
    try {
        if (numberFloat == null || lenght == null)
            return numberFloat;
        var itenDivison = '1';
        for (var i = 0; i < lenght; i++) {
            itenDivison += '0';
        }
        var division = Number(itenDivison);
        return (Math.round(numberFloat * division) / division).toFixed(lenght);
    } catch (e) {
        console.error(e);
    }
    return 0;
}
function isNullOrEmpty(data) {

    if (data == null)
        return true;
    if (data == undefined) {
        return true;
    }
    var output = data;
    if (typeof output === 'string') {

    } else {
        output = output.toString();
    }
    output = output.trim();

    return output.length <= 0;
}
function convertFormatToNumber(stringNumberInput) {
    if (isNullOrEmpty(stringNumberInput)) return stringNumberInput;

    var stringNumber = stringNumberInput;

    if (typeof stringNumberInput !== "string")
        stringNumber = stringNumberInput.toString();

    stringNumber = stringNumber.replace('$', '');
    stringNumber = stringNumber.replace('%', '');
    try {
        if (isNullOrEmpty(stringNumber)) return 0;
        if (isNaN(stringNumber)) {
            if (typeof stringNumber != 'string') stringNumber = stringNumber.toString();
            var stringNumberTemp = stringNumber.replace(/,/gi, "");
            if (isNaN(stringNumberTemp)) {
                stringNumberTemp = stringNumberTemp.replace(/\(/gi, ""); //truong hop nay khi string duoc format thanh dang () se la so am nen se cong them dau -
                stringNumberTemp = stringNumberTemp.replace(/\)/gi, "");
                stringNumberTemp = "-" + stringNumberTemp;
                return parseFloat(stringNumberTemp);
            }
            return parseFloat(stringNumberTemp);
        }
        return parseFloat(stringNumber);
    } catch (e) {
        console.error(e);
    }
    return 0;
}