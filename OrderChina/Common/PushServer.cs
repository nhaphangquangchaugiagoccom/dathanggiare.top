﻿using System;
using System.Collections.Generic;
using System.Web.Configuration;
using Newtonsoft.Json.Linq;
using PushSharp.Apple;
using PushSharp.Core;
using PushSharp.Google;

namespace OrderChina.Common
{
    public class PushServer
    {
        private static NotificationConfig _config;
        public PushServer()
        {
            if (_config == null)
            {
                bool isProduct;
                bool.TryParse(WebConfigurationManager.AppSettings["IsProduct"], out isProduct);
                _config=new NotificationConfig
                {
                    AuthTokenId = WebConfigurationManager.AppSettings["AuthTokenId"],
                    GmcSenderId = WebConfigurationManager.AppSettings["GmcSenderId"],
                    CertPassword = WebConfigurationManager.AppSettings["CertPassword"],
                    IsProduct = isProduct,
                 };
            }
        }

        #region APNS PUSH
        public bool APNS_Push(NotificationObject notificationObject, List<string> listDeviceToken = null, bool isNotSendList = false)
        {
            if (_config == null)
            {
                Console.WriteLine("NO CONFIG NOTIFICATION FOUND...");
                return false;
            }
            var pathCert = AppDomain.CurrentDomain.BaseDirectory + @"App_Data\Cert\cert.p12";
            var certpwd = _config.CertPassword;
            // Configuration (NOTE: .pfx can also be used here)
            var config = new ApnsConfiguration(_config.IsProduct ? ApnsConfiguration.ApnsServerEnvironment.Production : ApnsConfiguration.ApnsServerEnvironment.Sandbox,
                pathCert, certpwd);


            // Create a new broker
            var apnsBroker = new ApnsServiceBroker(config);

            // Wire up events
            apnsBroker.OnNotificationFailed += (notification, aggregateEx) => aggregateEx.Handle(ex =>
                                                                                                     {

                                                                                                         // See what kind of exception it was to further diagnose
                                                                                                         if (ex is ApnsNotificationException)
                                                                                                         {
                                                                                                             var notificationException = (ApnsNotificationException)ex;

                                                                                                             // Deal with the failed notification
                                                                                                             var apnsNotification = notificationException.Notification;
                                                                                                             var statusCode = notificationException.ErrorStatusCode;

                                                                                                             Console.WriteLine("Apple Notification Failed: ID={0}, Code={1}", apnsNotification.Identifier, statusCode);

                                                                                                         }
                                                                                                         else
                                                                                                         {
                                                                                                             // Inner exception might hold more useful information like an ApnsConnectionException           
                                                                                                             Console.WriteLine("Apple Notification Failed for some unknown reason : {0}", ex.InnerException);
                                                                                                         }

                                                                                                         // Mark it as handled
                                                                                                         return true;
                                                                                                     });

            apnsBroker.OnNotificationSucceeded += (notification) =>
            {
                Console.WriteLine("Apple Notification Sent!");
            };

            // Start the broker
            apnsBroker.Start();
            if (listDeviceToken == null)
                listDeviceToken = new List<string>();

            foreach (var deviceToken in listDeviceToken)
            {
                // Queue a notification to send
                apnsBroker.QueueNotification(new ApnsNotification
                {
                    DeviceToken = deviceToken,
                    Payload = JObject.Parse("{\"aps\":{\"badge\":1,\"sound\":\"" + notificationObject.Sound + "\",\"alert\" : \"" + notificationObject.Message + "\"}}")
                    //                    Payload = JObject.Parse("{\"aps\":{\"badge\":1,\"sound\":\"default\",\"alert\" : \"" + notificationObject.Message + "\"}}")
                });
            }

            // Stop the broker, wait for it to finish   
            // This isn't done after every message, but after you're
            // done with the broker
            apnsBroker.Stop();
            return true;
        }

        #endregion

        #region GCM PUSH
        public bool GCM_Push(NotificationObject notificationObject, List<string> listDeviceToken = null, bool isNotSendList = false)
        {
            if (_config == null)
            {
                Console.WriteLine("NO CONFIG NOTIFICATION FOUND...");
                return false;
            }
            var gmcSenderId = _config.GmcSenderId;
            var authToken = _config.AuthTokenId;
            // Configuration
            var config = new GcmConfiguration(gmcSenderId, authToken, null);

            // Create a new broker
            var gcmBroker = new GcmServiceBroker(config);

            // Wire up events
            gcmBroker.OnNotificationFailed += (notification, aggregateEx) => aggregateEx.Handle(ex =>
                                                                                                    {

                                                                                                        // See what kind of exception it was to further diagnose
                                                                                                        if (ex is GcmNotificationException)
                                                                                                        {
                                                                                                            var notificationException = (GcmNotificationException)ex;

                                                                                                            // Deal with the failed notification
                                                                                                            var gcmNotification = notificationException.Notification;
                                                                                                            var description = notificationException.Description;

                                                                                                           Console.WriteLine("GCM Notification Failed: ID={0}, Desc={1}", gcmNotification.MessageId, description);
                                                                                                        }
                                                                                                        else if (ex is GcmMulticastResultException)
                                                                                                        {
                                                                                                            var multicastException = (GcmMulticastResultException)ex;

                                                                                                            foreach (var succeededNotification in multicastException.Succeeded)
                                                                                                            {
                                                                                                               Console.WriteLine("GCM Notification Failed: ID={0}", succeededNotification.MessageId);
                                                                                                            }

                                                                                                            foreach (var failedKvp in multicastException.Failed)
                                                                                                            {
                                                                                                                var n = failedKvp.Key;
                                                                                                                var e = failedKvp.Value;

                                                                                                               Console.WriteLine("GCM Notification Failed: ID={0}, Desc={1}", n.MessageId, e);
                                                                                                            }

                                                                                                        }
                                                                                                        else if (ex is DeviceSubscriptionExpiredException)
                                                                                                        {
                                                                                                            var expiredException = (DeviceSubscriptionExpiredException)ex;

                                                                                                            var oldId = expiredException.OldSubscriptionId;
                                                                                                            var newId = expiredException.NewSubscriptionId;

                                                                                                           Console.WriteLine(string.Format("Device RegistrationId Expired: {0}", oldId));

                                                                                                            if (!string.IsNullOrWhiteSpace(newId))
                                                                                                            {
                                                                                                                // If this value isn't null, our subscription changed and we should update our database
                                                                                                               Console.WriteLine("Device RegistrationId Changed To: {0}", newId);
                                                                                                            }
                                                                                                        }
                                                                                                        else if (ex is RetryAfterException)
                                                                                                        {
                                                                                                            var retryException = (RetryAfterException)ex;
                                                                                                            // If you get rate limited, you should stop sending messages until after the RetryAfterUtc date
                                                                                                           Console.WriteLine("GCM Rate Limited, don't send more until after {0}", retryException.RetryAfterUtc);
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                           Console.WriteLine("GCM Notification Failed for some unknown reason");
                                                                                                        }

                                                                                                        // Mark it as handled
                                                                                                        return true;
                                                                                                    });

            gcmBroker.OnNotificationSucceeded += (notification) =>
            {
               Console.WriteLine("GCM Notification Sent!");
            };

            // Start the broker
            gcmBroker.Start();
            if (listDeviceToken == null)
                listDeviceToken = new List<string>();
            if (isNotSendList)
            {
                foreach (var regId in listDeviceToken)
                {
                    // Queue a notification to send
                    gcmBroker.QueueNotification(new GcmNotification
                    {
                        RegistrationIds = new List<string> { 
                        regId
                    },
                        Data = JObject.Parse("{ \"message\" : \"" + notificationObject.Message + "\",\"title\" : \"" + notificationObject.Title + "\",\"sound\" : \"" + notificationObject.Sound + "\" }")
                    });
                }
            }
            else
            {
                // Queue a notification to send
                gcmBroker.QueueNotification(new GcmNotification
                {
                    RegistrationIds = listDeviceToken,
                    Data = JObject.Parse("{ \"message\" : \"" + notificationObject.Message + "\",\"title\" : \"" + notificationObject.Title + "\",\"sound\" : \"" + notificationObject.Sound + "\" }")
                });
            }


            // Stop the broker, wait for it to finish   
            // This isn't done after every message, but after you're
            // done with the broker
            gcmBroker.Stop();
            return true;
        }
        #endregion
    }
}
