﻿using OrderChina.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using Telerik.Reporting;
using Telerik.Reporting.Drawing;
using Telerik.Reporting.Processing;
using Report = Telerik.Reporting.Report;
using SubReport = Telerik.Reporting.SubReport;

namespace QuantEdge.Worker.ReportManager.Utils
{
    public class ExportFileUtils
    {
        //Export by RenderReport
        public static RenderingResult GetByteForReportOutputType_RenderReport(IReportDocument report, ExportType exportType)
        {
            try
            {
                //var reportProcessor = new ReportProcessor();

                ////set any deviceInfo settings if necessary
                ////var deviceInfo = new Hashtable();
                ////deviceInfo["FontEmbedding"] = "None";
                //var instanceReportSource = new InstanceReportSource { ReportDocument = report };

                //RenderingResult result =
                //    reportProcessor.RenderReport("XLSX", instanceReportSource, null);


                //if (result != null)
                //    return result.DocumentBytes;

                Telerik.Reporting.Processing.ReportProcessor reportProcessor =
    new Telerik.Reporting.Processing.ReportProcessor();

                // set any deviceInfo settings if necessary
                System.Collections.Hashtable deviceInfo =
                    new System.Collections.Hashtable();
                InstanceReportSource reportSource = new InstanceReportSource();
                reportSource.ReportDocument = report; 
                Telerik.Reporting.TypeReportSource typeReportSource =
                             new Telerik.Reporting.TypeReportSource();

                // reportName is the Assembly Qualified Name of the report
                typeReportSource.TypeName = ((Report)report).Name;

                Telerik.Reporting.Processing.RenderingResult result =
                    reportProcessor.RenderReport("XLSX", reportSource, deviceInfo);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public static string GetOutputType(ExportType exportType)
        {
            string outPut = String.Empty;
            switch (exportType)
            {
                case ExportType.Xlsx:
                    outPut = "XLSX";
                    break;
            }
            return outPut;
        }
    }
}