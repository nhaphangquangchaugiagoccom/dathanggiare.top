﻿namespace OrderChina.Common
{
    public class NotificationConfig 
    {
        public string CertPassword { get; set; }
        public bool IsProduct { get; set; }
        public string GmcSenderId { get; set; }
        public string AuthTokenId { get; set; }
       

        public void Dispose()
        {
            CertPassword = null;
            GmcSenderId = null;
            AuthTokenId = null;
        }

        public static string FileName()
        {
            return typeof(NotificationConfig).Name;
        }

        public string GetFileName()
        {
            return FileName();
        }
    }
}