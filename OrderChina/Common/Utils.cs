﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.UI.WebControls;
using OrderChina.Models;
using System.Web.Script.Serialization;

namespace OrderChina.Common
{
    public class Utils
    {
        public const string VND = "VND";
        public const string CNY = "CNY";

        public static bool CheckRole(string userType, int roleCheck = 0, bool isAdmin = true)
        {
            if (userType == null)
            {
                return false;
            }
            if (isAdmin)
            {
                if (userType.IndexOf(((int)UserType.Admin).ToString(CultureInfo.InvariantCulture), StringComparison.Ordinal) >= 0)
                {
                    return true;
                }
                if (userType.IndexOf(((int)UserType.SuperUser).ToString(CultureInfo.InvariantCulture), StringComparison.Ordinal) >= 0)
                {
                    return true;
                }
            }

            if (userType.IndexOf(roleCheck.ToString(CultureInfo.InvariantCulture), StringComparison.Ordinal) >= 0)
                return true;

            return false;
        }
        public static string GetOrderStatus(string state)
        {
            if (state == OrderStatus.New.ToString())
                return "Đơn mới";
            if (state == OrderStatus.Paid.ToString())
                return "Đã thu tiền đặt cọc";
            if (state == OrderStatus.Order.ToString())
                return "Hàng chờ về";
            if (state == OrderStatus.Finish.ToString())
                return "Hoàn thành";
            if (state == OrderStatus.Cancel.ToString())
                return "Đơn hủy";
            if (state == OrderStatus.ClientConfirm.ToString())
                return "Khách hàng confirm";
            if (state == OrderStatus.SaleConfirm.ToString())
                return "Sale chốt đơn hàng";
            if (state == OrderStatus.Receive.ToString())
                return "Hàng về đã về kho";
            if (state == OrderStatus.FullCollect.ToString())
                return "Đã thu đủ tiền";
            if (state == OrderStatus.OrdererReject.ToString())
                return "Chốt lại đơn hàng";
            if (state == OrderStatus.WaitingApprovalSaleConfirm.ToString())
                return "Chờ phê duyệt chốt đơn hàng";
            if (state == OrderStatus.PendingExport.ToString())
                return "Kho hàng bán";
            if (state == OrderStatus.WaitOrder.ToString())
                return "Chờ kế toán chốt";
            if (state == OrderStatus.MisOrder.ToString())
                return "Hết hàng cần hoàn tiền khách";
            return state;
        }

        public static string GetOrderAccountState(string state, bool isChild = false)
        {
            try
            {
                if (isChild)
                {
                    if (state == OrderStatus.New.ToString())
                    {
                        return "Đơn hàng xuất mới";
                    }
                    if (state == OrderStatus.Receive.ToString())
                    {
                        return "Đã thanh toán";
                    }
                    if (state == OrderStatus.Finish.ToString())
                    {
                        return "Đã xuất hàng";
                    }
                }
                else
                {
                    if (state == OrderStatus.Order.ToString())
                    {
                        return "Đơn hàng nhập mới";
                    }
                    if (state == OrderStatus.PendingExport.ToString())
                    {
                        return "Chờ xuất hàng";
                    }
                    if (state == OrderStatus.Finish.ToString())
                    {
                        return "Thành công";
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return state;
        }

        public static string GetApprovalStatus(int approvalStatus)
        {
            try
            {
                if (approvalStatus == (int)ApprovalStatus.Request)
                {
                    return "Chờ xử lý";
                }
                if (approvalStatus == (int)ApprovalStatus.Approval)
                {
                    return "Đã phê duyệt";
                }
                if (approvalStatus == (int)ApprovalStatus.ApprovalPartial)
                {
                    return "Đã phê duyệt một phần";
                }
                if (approvalStatus == (int)ApprovalStatus.ApprovalPartial)
                {
                    return "Từ chối";
                }
            }
            catch (Exception)
            {

            }
            return string.Empty;
        }

        public static string GenerateOrderId(int id, int saleId, int userid)
        {
            return string.Format("{0:ddMMyy}{1}{2}{3}", DateTime.Now, saleId, userid, id);
        }

        public static double Round(double value, int precision)
        {
            if (precision < -4 && precision > 15)
                throw new ArgumentOutOfRangeException("precision", "Must be and integer between -4 and 15");

            if (precision >= 0) return Math.Round(value, precision);
            else
            {
                precision = (int)Math.Pow(10, Math.Abs(precision));
                value = value + (5 * precision / 10);
                return Math.Round(value - (value % precision), 0);
            }
        }

    }
}