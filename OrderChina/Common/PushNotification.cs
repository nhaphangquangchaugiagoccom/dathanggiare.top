﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrderChina.Common
{
    public static class PushNotification
    {
        public static bool Push(List<string> listDevicesToken, NotificationObject notificationObject)
        {
            try
            {
                var listTokentAppleId = new List<string>();
                var listTokentGooleId = new List<string>();
                foreach (var deviceToken in listDevicesToken)
                {
                    // device token cua Apple co 64 ky tu
                    if (deviceToken.Length == 64)
                    {
                        listTokentAppleId.Add(deviceToken);
                    }
                    else
                    {
                        listTokentGooleId.Add(deviceToken);
                    }
                }
                var classPushServer = new PushServer();

                if (listTokentAppleId.Count > 0)
                {
                    classPushServer.APNS_Push(notificationObject, listTokentAppleId);
                }

                if (listTokentGooleId.Count > 0)
                {
                    classPushServer.GCM_Push(notificationObject, listTokentGooleId);
                }
                return true;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return false;

        }
    }
}