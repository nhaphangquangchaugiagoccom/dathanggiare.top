﻿namespace OrderChina.Common
{
    public class NotificationObject
    {
        public string Message { get; set; }
        public string Title { get; set; }
        public string Sound { get; set; }
        public string ContentType { get; set; }
    }
}
